//
//  playerSegmentation.cpp
//
//
//  Created by Henri Kuper on 23.06.17.
//
//

//#define GPU_ON

#include "playerSegmentation.hpp"

using namespace std;
using namespace cv;


//--------constructor/destructor-----------------
PlayerSegmentation::PlayerSegmentation(FieldDetection *field, int frameWidth, int frameHeight) : m_field(field), m_frameWidth(frameWidth), m_frameHeight(frameHeight){
#ifndef GPU_ON
	playerSegmentationInit();
#else
	playerSegmentationInitGPU();
#endif
}
//--------CPU - functions----------------
#ifndef GPU_ON
void PlayerSegmentation::playerSegmentationInit(){

	m_inputVideoResizingFactor = (float) m_frameWidth/1280;
	cout<<"PlayerSegmentation m_inputVideoResizingFactor: "<<m_inputVideoResizingFactor<<endl;

	m_foregroundMaskThresh = 12;//10
	m_humanMaskMinArea = 1500.0f * m_inputVideoResizingFactor * m_inputVideoResizingFactor;
	m_mog2UpdateRate = 0.02;         //0.025
	m_mog2UpdateTime = 30; //30

	m_frameCount = 0;
	m_color = Scalar(255);

	m_bgSubstractor = createBackgroundSubtractorMOG2(50, m_foregroundMaskThresh, false);         // 50//60
	m_isRightFieldSide = m_field->getIsRightFieldSide();

	m_resizing0 = 0.5f;//0.5
	m_resizing1 = 1.0f/(m_resizing0);
	m_dilateSizeClosing = 5;
	m_erodeSizeClosing = 3;
	m_dilateIterations = 3;
	m_erodeIterations = 2;

	m_humanMaskPositionOffset = (float)((m_dilateSizeClosing) * m_dilateIterations - (m_erodeSizeClosing + 1) * m_erodeIterations) / m_resizing0;
	cout<<"HumanMaskPositionOffset: "<<m_humanMaskPositionOffset<<endl;

	m_backgroundUpdateRate = 10;
	m_backgroundImgUpdateCounter = 0;



	// m_backgroundImg = m_field->getBackgroundImg();
	// m_frameWidth = m_backgroundImg.cols;
	// m_frameHeight = m_backgroundImg.rows;
	// cvtColor(m_backgroundImg, m_backgroundImg, COLOR_BGR2GRAY);
	// resize(m_backgroundImg,m_backgroundImg,Size(),m_resizing0,m_resizing0,INTER_NEAREST);
	// m_updateBackgroundImgMask = Mat(m_backgroundImg.size(), m_backgroundImg.type());
}

void PlayerSegmentation::initBackgroundModel(Mat& backgroundFrame){
	Mat resizedFrameGray, diffImage, diffImageThresholed;
	resize(backgroundFrame, resizedFrameGray, Size(), m_resizing0, m_resizing0, INTER_LINEAR);
	m_bgSubstractor->apply(resizedFrameGray, diffImageThresholed, m_mog2UpdateRate);
	m_bgSubstractor->getBackgroundImage(m_backgroundImg);
}

void PlayerSegmentation::createForegroundMaskWithMog2(Mat& originalFrameGray){
	Mat resizedFrameGray, diffImage, diffImageThresholed, outputErode;
	tm.reset(); tm.start();
	resize(originalFrameGray, resizedFrameGray, Size(), m_resizing0, m_resizing0, INTER_LINEAR);

	if(m_frameCount< 2 || m_frameCount%m_mog2UpdateTime == 0) {
		m_bgSubstractor->apply(resizedFrameGray, diffImageThresholed, m_mog2UpdateRate);
		m_bgSubstractor->getBackgroundImage(m_backgroundImg);
	}

	absdiff(m_backgroundImg, resizedFrameGray, diffImage);
	threshold(diffImage, diffImageThresholed, m_foregroundMaskThresh, 255.0f, THRESH_BINARY);
	tm.stop();
	// int erosion_size = 2;
	// erode(diffImageThresholed, m_foregroundMask, getStructuringElement( MORPH_ELLIPSE, Size( 2*erosion_size + 1, 2*erosion_size+1 ), Point( erosion_size, erosion_size ) ));
	// erosion_size = 3;
	// dilate(m_foregroundMask, m_foregroundMask, getStructuringElement( MORPH_ELLIPSE, Size( 2*erosion_size + 1, 2*erosion_size+1 ), Point( erosion_size, erosion_size ) ),Point(-1,-1), 2);
	m_erodeDilateSize = 3;//4
	//morphological opening (removes small objects from the foreground)
	erode(diffImageThresholed, m_foregroundMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );
	dilate( m_foregroundMask, m_foregroundMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );

	//morphological closing (removes small holes from the foreground)
	dilate( m_foregroundMask, m_foregroundMask, getStructuringElement(MORPH_ELLIPSE, Size(m_dilateSizeClosing, m_dilateSizeClosing)), Point(-1,-1), m_dilateIterations);
	//to get a brider humen mask
	erode(m_foregroundMask, m_foregroundMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeSizeClosing, m_erodeSizeClosing)), Point(-1,-1), m_erodeIterations);

	resize(m_foregroundMask, m_foregroundMask, Size(), m_resizing1, m_resizing1, INTER_NEAREST);

	// if(m_isRightFieldSide == true) {
	// 	imshow("foreground Right", m_foregroundMask);
	// }
	// else{
	// 	imshow("foreground Left", m_foregroundMask);
	// }
	// waitKey(30);

	createHumanMask(originalFrameGray);
	m_frameCount++;

	player_timesErode.push_back(tm.getTimeMilli());
	std::sort(player_timesErode.begin(), player_timesErode.end());
	double player_avg = std::accumulate(player_timesErode.begin(), player_timesErode.end(), 0.0) / player_timesErode.size();

	std::cout << "playerErode : Avg : " << player_avg << " ms FPS : " << 1000.0 / player_avg << std::endl;
}

void PlayerSegmentation::createForegroundMask(Mat& originalFrameGray){

	Mat calibratedDiffImage0, calibratedDiffImage1;
	m_foregroundMask = Mat(originalFrameGray.size(), originalFrameGray.type());

	//m_humanMask = Mat::zeros(originalFrameGray.rows, originalFrameGray.cols, CV_8UC1);

	absdiff(m_backgroundImg, originalFrameGray, calibratedDiffImage0);

	threshold(calibratedDiffImage0, m_foregroundMask, m_foregroundMaskThresh, 255.0f, THRESH_BINARY);

	tm.reset(); tm.start();
	m_erodeDilateSize = 3;//4
	//morphological opening (removes small objects from the foreground)
	erode(m_foregroundMask, m_foregroundMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );
	dilate( m_foregroundMask, m_foregroundMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );

	//morphological closing (removes small holes from the foreground)
	m_erodeDilateSize = 5;//20 /8
	dilate( m_foregroundMask, m_foregroundMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)), Point(-1,-1), 3);
	//to get a brider humen mask
	m_erodeDilateSize = 3;//12 /5
	erode(m_foregroundMask, m_foregroundMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)), Point(-1,-1), 2);
	resize(m_foregroundMask,m_foregroundMask,Size(),m_resizing1,m_resizing1,INTER_NEAREST);
	tm.stop();
	//clear border. Without some contours might not be recognized
	for(int32_t i = 0; i < m_foregroundMask.cols; i++) {
		m_foregroundMask.at<unsigned char>(0,i) = 0;
		m_foregroundMask.at<unsigned char>(m_foregroundMask.rows-1,i) = 0;
	}
	for(int32_t i = 0; i < m_foregroundMask.rows; i++) {
		m_foregroundMask.at<unsigned char>(i,0) = 0;
		m_foregroundMask.at<unsigned char>(i,m_foregroundMask.cols-1) = 0;
	}

	// if(m_isRightFieldSide == true){
	//      imshow("foreground Right", m_foregroundMask);
	// }
	// else{
	//      imshow("foreground Left", m_foregroundMask);
	// }
	// waitKey(30);
	createHumanMask(originalFrameGray);
	updateBackgroundImg(originalFrameGray);
}

void PlayerSegmentation::createHumanMask(Mat& originalFrameGray){

	m_humanMaskROI = Mat::zeros(originalFrameGray.rows, originalFrameGray.cols, CV_8UC1);
	double area=0;
	vector<vector<Point> > contours;
	vector<vector<Point> > contours0;
	vector<Vec4i> hierarchy0;

	vector<Rect> humanBoundRects;
	m_playerBoundRectsOfFrames.push_back(humanBoundRects);

	findContours( m_foregroundMask, contours0, hierarchy0, RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE );

	//läuft noch nicht gut genug um es einzusetzen. Erst andere mergefunktion entwickel und parameter dynamisch anpassen.
	//mergeNearContoursOfPlayer(contours, contours0);
	m_humanMaskMoments.clear();
	m_mainHumanMaskContours.clear();
	m_mainPlayerPositionInModel.clear();
	m_humanMaskContours.clear();
	m_playerPositionInModel.clear();
	m_foregroundMaskBoundRect.clear();

	for( uint32_t i = 0; i < contours0.size(); i++) {
		area = contourArea(contours0[i], false);
		vector<Point> contoursPoly;
		approxPolyDP( Mat(contours0[i]), contoursPoly, 3, true );

		Rect boundRect = boundingRect( Mat(contoursPoly) );
		float heightDivWidthRect = (float)boundRect.height / (float)boundRect.width;
		m_foregroundMaskBoundRect.push_back(boundRect);
		Point2f boundRectCenter(boundRect.x + boundRect.width/2, boundRect.y + boundRect.height/2);

		if(area > m_humanMaskMinArea && boundRectCenter.y < m_frameHeight / 7 * 5 && boundRectCenter.y > m_frameHeight / 7 * 2) {
			//drawing bounding rectangle

			Rect boundSquareRect = rectToBoundSquare(boundRect);
			rectangle( m_humanMaskROI, boundSquareRect.tl(), boundSquareRect.br(), m_color, -1, 8, 0 );
			Moments playerMoment = moments( contours0[i], false );
			m_humanMaskMoments.push_back(playerMoment);

			m_playerBoundRectsOfFrames.back().push_back(boundRect);

			//Minimizing the shadow before calculating the humanStandingPosition
			vector<Point> dstContour;
			imgProc::imageObjects::reshapeContourWithMassCenter(contoursPoly, dstContour, 2);
			Rect boundRectForHumanPosition = boundingRect( Mat(dstContour) );
			Point2f humanStandingPosition = Point2f(boundRectForHumanPosition.x + boundRectForHumanPosition.width / 2, boundRectForHumanPosition.y + boundRectForHumanPosition.height - m_humanMaskPositionOffset);
			Point2f transformedHumanStanding = m_field->transformPointToModel(humanStandingPosition);
			m_mainPlayerPositionInModel.push_back(transformedHumanStanding);
			m_mainHumanMaskContours.push_back(contours0[i]);

			m_playerPositionInModel.push_back(transformedHumanStanding);
			m_humanMaskContours.push_back(contours0[i]);
		}
		else if(area > m_humanMaskMinArea && (boundRectCenter.y > m_frameHeight / 7 * 2 || boundRectCenter.y < m_frameHeight / 7 * 5)) {
			boundRect = rectToBoundSquare(boundRect);
			rectangle( m_humanMaskROI, boundRect.tl(), boundRect.br(), m_color, -1, 8, 0 );
			addHumanMaskContourAndPosition(contoursPoly);
		}
		else if(area > m_humanMaskMinArea) {
			boundRect = rectToBoundSquare(boundRect);
			if(m_field->getIsRightFieldSide() == true && boundRectCenter.x > m_frameWidth / 2) {
				rectangle( m_humanMaskROI, boundRect.tl(), boundRect.br(), m_color, -1, 8, 0 );
				addHumanMaskContourAndPosition(contoursPoly);
			}
			else if(m_field->getIsRightFieldSide() == false && boundRectCenter.x < m_frameWidth / 2) {
				rectangle( m_humanMaskROI, boundRect.tl(), boundRect.br(), m_color, -1, 8, 0 );
				addHumanMaskContourAndPosition(contoursPoly);
			}
		}
		else if(area > m_humanMaskMinArea/2 && heightDivWidthRect > 1.2f && boundRectCenter.y < m_frameHeight / 7 * 5 && boundRectCenter.y > m_frameHeight / 7 * 2) {
			boundRect = rectToBoundSquare(boundRect);
			rectangle( m_humanMaskROI, boundRect.tl(), boundRect.br(), m_color, -1, 8, 0 );
			addHumanMaskContourAndPosition(contoursPoly);
		}
	}
	//resize(m_humanMaskROI, m_humanMaskROI, Size(), m_resizing0,m_resizing0);
	m_humanMaskMomentsOfFrames.push_back(m_humanMaskMoments);
	// if(m_isRightFieldSide == true && m_frameCount > 10400){
	//   imshow("m_humanMaskROI Right",m_humanMaskROI);
	//   //imshow("m_foregroundMask Right",m_foregroundMask);
	// 	//waitKey(0);
	// }
	// else if(m_isRightFieldSide == false && m_frameCount > 10400){
	// 	imshow("m_humanMaskROI Left",m_humanMaskROI);
	// 	//waitKey(0);
	// }
	// if(m_frameCount > 10400){
	// 	waitKey(0);
	// }
	// waitKey(30);
	// player_times.push_back(tm.getTimeMilli());
	// std::sort(player_times.begin(), player_times.end());
	// double player_avg = std::accumulate(player_times.begin(), player_times.end(), 0.0) / player_times.size();
	//
	// std::cout << "playerErode : Avg : " << player_avg << " ms FPS : " << 1000.0 / player_avg << std::endl;
}

void PlayerSegmentation::addHumanMaskContourAndPosition(std::vector<cv::Point>& contour){
	vector<Point> dstContour;
	imgProc::imageObjects::reshapeContourWithMassCenter(contour, dstContour, 2);
	Rect boundRectForHumanPosition = boundingRect( Mat(dstContour) );
	Point2f humanStandingPosition = Point2f(boundRectForHumanPosition.x + boundRectForHumanPosition.width / 2, boundRectForHumanPosition.y + boundRectForHumanPosition.height - m_humanMaskPositionOffset);
	Point2f transformedHumanStanding = m_field->transformPointToModel(humanStandingPosition);
	m_playerPositionInModel.push_back(transformedHumanStanding);
	m_humanMaskContours.push_back(contour);
}

void PlayerSegmentation::mergeNearContoursOfPlayer(vector<vector<Point> > contours, vector<vector<Point> > &dstContours){
	//TODO anpassen der y und x differenzen anhand der höhe und breite der contour.
	//für jede contour den höchsten/tiefsten punkt berechnen. dann den abstand vergleichen von x und y
	//und ggfs mit neuer mergeContourfunktion mergen.


	dstContours.clear();
	vector<Point2f> momentsMassCenter;
	for(int i = 0; i < contours.size(); i++) {
		momentsMassCenter.push_back(Point2f());
		imgProc::imageMath::calculateMomentMassCenter(moments(contours[i], false), momentsMassCenter.back());
	}
	for(int i = 0; i < momentsMassCenter.size(); i++) {
		dstContours.push_back(contours[i]);

		if(contourArea(contours[i]) > m_humanMaskMinArea && momentsMassCenter[i].y < m_frameHeight / 7 * 5 && momentsMassCenter[i].y > m_frameHeight / 7 * 2) {
			for(int j = 0; j < momentsMassCenter.size(); j++) {
				if( i!=j && abs(momentsMassCenter[i].x - momentsMassCenter[j].x) < 50 * m_inputVideoResizingFactor && momentsMassCenter[j].y - momentsMassCenter[i].y < 150 * m_inputVideoResizingFactor && (momentsMassCenter[j].y - momentsMassCenter[i].y) > 0 ) {
					//cout<<"momentsMassCenter[j].y - momentsMassCenter[i].y  "<<momentsMassCenter[j].y - momentsMassCenter[i].y <<endl;
					imgProc::imageObjects::mergeContoursWithConvexHull(dstContours.back(), contours[j], dstContours.back());
					contours.erase(contours.begin() + j);
					momentsMassCenter.erase(momentsMassCenter.begin() + j);
					j--;
					i--;
				}
			}
		}
	}

	Mat playerMaskNew = Mat::zeros(Size(m_frameWidth,m_frameHeight), CV_8UC1);
	// cout<<"newContours.size(): "<<dstContours.size()<<endl;
	for(int i = 0; i < dstContours.size(); i++) {
		drawContours( playerMaskNew, dstContours, i, Scalar(255), CV_FILLED, 8, noArray() );
	}
	for(int i = 0; i < contours.size(); i++) {
		drawContours( playerMaskNew, contours, i, Scalar(150), CV_FILLED, 8, noArray() );
	}

	imshow("playerMaskNew", playerMaskNew);
	waitKey(30);
}

cv::Rect PlayerSegmentation::rectToBoundSquare(cv::Rect boundRect){
	if(boundRect.height > boundRect.width) {
		float width = boundRect.height*1.1;
		float x = boundRect.x + boundRect.width/2 - width/2;
		if(x < 0) {
			x = 0;
		}
		if(x + width > m_frameWidth) {
			width = m_frameWidth - x-1;
		}
		boundRect.x = x;
		boundRect.width = width;

	}
	return boundRect;
}

void PlayerSegmentation::updateBackgroundImg(Mat& originalFrameGray){
	if(m_backgroundImgUpdateCounter%m_backgroundUpdateRate == 0) {
		m_updateBackgroundImgMask = Mat::zeros(originalFrameGray.size(), originalFrameGray.type());
		for(int i = 0; i < m_foregroundMaskBoundRect.size(); i++) {
			int rectCenterY = m_foregroundMaskBoundRect[i].y+ m_foregroundMaskBoundRect[i].height/2;
			float heightWithRatio = (float)m_foregroundMaskBoundRect[i].width / m_foregroundMaskBoundRect[i].height;
			if( rectCenterY < m_frameHeight / 7 * 2 || rectCenterY > m_frameHeight / 7 * 5 || heightWithRatio > 2.0f) {
				rectangle( m_updateBackgroundImgMask, m_foregroundMaskBoundRect[i].tl(), m_foregroundMaskBoundRect[i].br(), m_color, -1, 8, 0 );
			}
		}
		//imshow("m_updateBackgroundImgMask",m_updateBackgroundImgMask);
	}
	else if(m_backgroundImgUpdateCounter%m_backgroundUpdateRate == m_backgroundUpdateRate/2) {
		Mat actualBackgroundMask = Mat::zeros(originalFrameGray.size(), originalFrameGray.type());
		for(int i = 0; i < m_foregroundMaskBoundRect.size(); i++) {
			int rectCenterY = m_foregroundMaskBoundRect[i].y+ m_foregroundMaskBoundRect[i].height/2;
			float heightWithRatio = (float)m_foregroundMaskBoundRect[i].width / m_foregroundMaskBoundRect[i].height;
			if( rectCenterY < m_frameHeight / 7 * 2 || rectCenterY > m_frameHeight / 7 * 5 || heightWithRatio > 2.0f) {
				rectangle( actualBackgroundMask, m_foregroundMaskBoundRect[i].tl(), m_foregroundMaskBoundRect[i].br(), m_color, -1, 8, 0 );
			}
		}
		bitwise_and(m_updateBackgroundImgMask,actualBackgroundMask,m_updateBackgroundImgMask);
		originalFrameGray.copyTo(m_backgroundImg, m_updateBackgroundImgMask);
		//imshow("m_backgroundImg", m_backgroundImg);
		//imshow("m_updateBackgroundImgMaskAnd",m_updateBackgroundImgMask);
		//imshow("actualBackgroundMask",actualBackgroundMask);
	}

	m_backgroundImgUpdateCounter++;
}

#else
//--------GPU - functions----------------
void PlayerSegmentation::playerSegmentationInitGPU(){
	m_foregroundMaskThresh = 15.0;
	m_humanMaskMinArea = 4000.0f;
	m_color = Scalar(255);

	Mat calibrationImg = m_field->getBackgroundImg();
	gpu::GpuMat calibrationImgGPU;
	calibrationImgGPU.upload(calibrationImg);
	gpu::cvtColor(calibrationImgGPU, m_backgroundImgGPU, COLOR_BGR2GRAY);
}

void PlayerSegmentation::createHumanMaskGPU(gpu::GpuMat originalFrameGrayGPU){

	m_erodeDilateSize = 5;
	double area=0;

	gpu::GpuMat calibratedDiffImage0GPU, calibratedDiffImage1, foregroundMaskGPUtmp;
	gpu::GpuMat foregroundMaskGPU(originalFrameGrayGPU.size(), originalFrameGrayGPU.type());

	//m_humanMaskGPU = Mat::zeros(originalFrameGrayGPU.rows, originalFrameGrayGPU.cols, CV_8UC1);
	Mat humanMaskROI = Mat::zeros(originalFrameGrayGPU.rows, originalFrameGrayGPU.cols, CV_8UC1);

	gpu::absdiff(m_backgroundImgGPU, originalFrameGrayGPU, calibratedDiffImage0GPU);


	gpu::threshold(calibratedDiffImage0GPU, foregroundMaskGPU, m_foregroundMaskThresh, 255.0f, THRESH_BINARY);

	Mat foregroundMask;
	foregroundMaskGPU.download(foregroundMask);

	m_erodeDilateSize = 4;//6
	tm.reset(); tm.start();
	//morphological opening (removes small objects from the foreground)
	//gpu::erode(foregroundMaskGPU, foregroundMaskGPUtmp, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );
	//gpu::dilate(foregroundMaskGPUtmp, foregroundMaskGPU, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );

	erode(foregroundMask, foregroundMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );
	dilate(foregroundMask, foregroundMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );

	//morphological closing (removes small holes from the foreground)
	m_erodeDilateSize = 20;
	//gpu::dilate( foregroundMaskGPU, foregroundMaskGPUtmp, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );
	dilate(foregroundMask, foregroundMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );

	//to get a brider humen mask
	m_erodeDilateSize = 12;
	//gpu::erode(foregroundMaskGPUtmp, foregroundMaskGPU, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );
	erode(foregroundMask, foregroundMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );

	tm.stop();
	//clear border
	//Mat foregroundMask;
	//foregroundMaskGPU.download(foregroundMask);
	for(int32_t i = 0; i < foregroundMask.cols; i++) {
		foregroundMask.at<unsigned char>(0,i) = 0;
		foregroundMask.at<unsigned char>(foregroundMask.rows-1,i) = 0;
	}
	for(int32_t i = 0; i < foregroundMask.rows; i++) {
		foregroundMask.at<unsigned char>(i,0) = 0;
		foregroundMask.at<unsigned char>(i,foregroundMask.cols-1) = 0;
	}


	vector<vector<Point> > contours0;
	vector<Vec4i> hierarchy0;

	vector<Rect> humanBoundRects;
	m_playerBoundRectsOfFrames.push_back(humanBoundRects);

	findContours( foregroundMask, contours0, hierarchy0,
	              RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE );

	m_humanMaskMoments.clear();
	m_mainHumanMaskContours.clear();
	m_playerPositionInModel.clear();

	for( uint32_t i = 0; i < contours0.size(); i++) {
		area = contourArea(contours0[i], false);
		//cout<<"Area: "<<area<<endl;
		if(area > m_humanMaskMinArea) {
			//drawContours( m_humanMask0, contours0, i, m_color, CV_FILLED, 8, hierarchy0 );

			//drawing bounding rectangle
			vector<Point> contoursPoly;
			approxPolyDP( Mat(contours0[i]), contoursPoly, 3, true );

			Rect boundRect = boundingRect( Mat(contoursPoly) );
			rectangle( humanMaskROI, boundRect.tl(), boundRect.br(), m_color, -1, 8, 0 );

			m_humanMaskMoments.push_back(moments( contours0[i], false ));
			m_playerBoundRectsOfFrames.back().push_back(boundRect);
			Point2f humanStandingPosition = Point2f(boundRect.x + boundRect.width / 2, boundRect.y + boundRect.height);
			Point2f transformedHumanStanding = m_field->transformPointToModel(humanStandingPosition);

			m_playerPositionInModel.push_back(transformedHumanStanding);
			//TODO: checken ob nicht einfach jede Contour gepushed werden soll.
			m_mainHumanMaskContours.push_back(contours0[i]);
		}
	}
	m_humanMaskMomentsOfFrames.push_back(m_humanMaskMoments);

	m_humanMaskROIGPU.upload(humanMaskROI);

	//imshow("HumanMask", m_humanMask0);
	//waitKey(30);
	// player_times.push_back(tm.getTimeMilli());
	// std::sort(player_times.begin(), player_times.end());
	// double player_avg = std::accumulate(player_times.begin(), player_times.end(), 0.0) / player_times.size();
	// std::cout << "playerErode : Avg : " << player_avg << " ms FPS : " << 1000.0 / player_avg << std::endl;
}
#endif
