//
//  videoCreator.hpp
//
//
//  Created by Henri Kuper on 13.07.17.
//
//

#ifndef videoCreator_hpp
#define videoCreator_hpp

#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include "ballDetection.hpp"
#include "curveDetection.hpp"
#include "statistics.hpp"
#include "../statisticsLib/match.hpp"

class VideoCreator {

private:
std::string m_safePathOutputVideo;

float m_resizingFactor;

int m_frameWidth;
int m_frameHeight;
int m_videoImgOffset;
int m_framesPrediction;

CurveDetection *m_curveDetectionRight;
CurveDetection *m_curveDetectionLeft;
Statistics* m_statistics;
Match* m_match;

cv::VideoWriter m_videoOutputWriter;

int m_frameCount;
int m_actualCurveImgCount;
int m_actualStrokeNameImgCount;
int m_actualStrokeCurveNumber;

bool m_actualStrokeIsRight;

std::string m_actualCurveName;
std::string m_actualStrokeName;
cv::Mat m_rightImg;
cv::Mat m_leftImg;

void videoCreatorInit();

cv::Mat createActualCurveNameDisplay();
cv::Mat createActualeStrokeSpeedDisplay();
cv::Mat createActualScoreDisplay();
cv::Mat createActualStrokeNameDisplay();
cv::Mat createActualStatisticsDisplay();
float getActualStrokeSpeed();
std::string getActualScore(int playerNumber);

cv::TickMeter tm;
std::vector<double> video_times;

public:
VideoCreator(std::string, int, int, int, int, CurveDetection*, CurveDetection*, Statistics*, Match*);
~VideoCreator();

void stopVideoCreation();

void writeVideoOutput(cv::Mat& rightImg, cv::Mat& leftImg);

//set functions
void setActualCurveName(std::string actualCurveName){
	m_actualCurveName = actualCurveName;
}
void setActualStrokeName(std::string actualStrokeName){
	m_actualStrokeName = actualStrokeName;
}
void setActualCurveImgCount(int actualCurveImgCount){
	m_actualCurveImgCount = actualCurveImgCount;
}
void setActualStrokeNameImgCount(int actualStrokeNameImgCount){
	m_actualStrokeNameImgCount = actualStrokeNameImgCount;
}
void setActualStrokeCurveNumber(int actualStrokeCurveNumber){
	m_actualStrokeCurveNumber = actualStrokeCurveNumber;
}
void setActualStrokeIsRight(bool isRight){
	m_actualStrokeIsRight = isRight;
}
void setFrameCount(int frameCount){
	m_frameCount = frameCount;
}
};

#endif /* videoCreator_hpp */
