//
//  fieldDetection.cpp
//
//
//  Created by Henri Kuper on 30.03.17.
//
//

#include "fieldDetection.hpp"

using namespace cv;
using namespace std;

/* TODO

   - einfügen 4 return feldern.
   - einfügen der 4 Serve felder.
   - config datei übergeben und calibration streams in der klasse aufrufen

 */

FieldDetection::FieldDetection() : m_frameHeight(1080),m_frameWidth(1920), m_isRightFieldSide(false){
	fieldDetectionInit();
}

FieldDetection::FieldDetection(int height, int width, Mat cameraMatrix, Mat distortionCoefficients, bool isRightFieldSide, struct ConfigData &config) : m_config(config), m_frameHeight(height), m_frameWidth(width), m_isRightFieldSide(isRightFieldSide){
	m_cameraMatrix = cameraMatrix.clone();
	m_distortionCoefficients = distortionCoefficients.clone();
	fieldDetectionInit();
}

FieldDetection::~FieldDetection(){
}

void FieldDetection::fieldDetectionInit(){
	m_inputVideoResizingFactor = (float)m_frameWidth/1280.0f;
	cout<<"FieldDetection m_inputVideoResizingFactor: "<<m_inputVideoResizingFactor<<endl;

	//TODO: Vorab provisorisch frameWidth und frameHeight wieder auf den Default Wert gesetzt
	m_frameWidth = 1280;
	m_frameHeight = 720;

	m_lowBinaryThreshold = 200;
	m_highBinaryThreshold = 255;

	m_minCannyThresh = 100;
	m_maxCannyThresh = 255;

	m_calibratedImgLines = Mat::zeros(m_frameHeight, m_frameWidth, CV_8UC1); //mat to

	m_modelHeight = 1497;
	m_modelWidth = 3177;

	m_modelOffset = 200;

	m_calibrationCount = 30;

	m_lineWidth = 4.0;

	m_useCalibrationType = m_config.m_useCalibrationType;

	initFieldModel();
}

void FieldDetection::initFieldModel(){

	//m_fieldModel = Mat(m_modelHeight, m_modelWidth, CV_8UC3, Scalar(120,89,17));
	m_fieldModel = Mat(m_modelHeight, m_modelWidth, CV_8UC3, Scalar(68,41,0));
	//singleFieldPoints
	m_singleFieldPoints.push_back(Point2f(400,337));
	m_singleFieldPoints.push_back(Point2f(2775,337));
	m_singleFieldPoints.push_back(Point2f(2775,1160));
	m_singleFieldPoints.push_back(Point2f(400,1160));

	//leftCenterFieldPoints
	/*m_leftCenterFieldPoints.push_back(Point2f(200,1160)); //down-left(west)
	   m_leftCenterFieldPoints.push_back(Point2f(200,337)); //up-left(North)
	   m_leftCenterFieldPoints.push_back(Point2f(748,337)); //up-right(east)
	   m_leftCenterFieldPoints.push_back(Point2f(748,1160)); //down-right(south)*/

	//leftCenterFieldPoints angepasst auf die innen ecken, die erkannt werden.
	m_leftCenterFieldPoints.push_back(Point2f(403,1157)); //down-left(west)
	m_leftCenterFieldPoints.push_back(Point2f(403,340)); //up-left(North)
	m_leftCenterFieldPoints.push_back(Point2f(945,340)); //up-right(east)
	m_leftCenterFieldPoints.push_back(Point2f(945,1157)); //down-right(south)
	//rightCenterFieldPoints
	/*m_rightCenterFieldPoints.push_back(Point2f(2028,337)); //up-left(west)
	   m_rightCenterFieldPoints.push_back(Point2f(2575,337)); //up-right(north)
	   m_rightCenterFieldPoints.push_back(Point2f(2575,1160)); //down-right(east)
	   m_rightCenterFieldPoints.push_back(Point2f(2028,1160)); //down-left(south)
	 */
	//rightCenterFieldPoints angepasst auf die innen ecken, die erkannt werden.
	m_rightCenterFieldPoints.push_back(Point2f(2231,340)); //up-left(west)
	m_rightCenterFieldPoints.push_back(Point2f(2772,340)); //up-right(north)
	m_rightCenterFieldPoints.push_back(Point2f(2772,1157)); //down-right(east)
	m_rightCenterFieldPoints.push_back(Point2f(2231,1157)); //down-left(south)


	m_CalibrationFieldPoints_Left.push_back(Point2f(400,1160));
	m_CalibrationFieldPoints_Left.push_back(Point2f(948,1160));
	m_CalibrationFieldPoints_Left.push_back(Point2f(948,1297));
	m_CalibrationFieldPoints_Left.push_back(Point2f(400,1297));

	//TODO: CenterFieldPoints in Calibrierung einfügen
	m_CalibrationCenterFieldPoints_Left.push_back(Point2f(400,748));
	m_CalibrationCenterFieldPoints_Left.push_back(Point2f(948,748));
	m_CalibrationCenterFieldPoints_Left.push_back(Point2f(948,1160));
	m_CalibrationCenterFieldPoints_Left.push_back(Point2f(400,1160));

	m_CalibrationFieldPoints_Right.push_back(Point2f(2777,1160));	//Achtung: Ecken in anderer Reihenfolge gepusht, damit sie sowohl auf linken, als auch auf dem rechten
	m_CalibrationFieldPoints_Right.push_back(Point2f(2777,1297));	//Bild in der Reihenfolge North East South West auftreten.
	m_CalibrationFieldPoints_Right.push_back(Point2f(2228,1297));
	m_CalibrationFieldPoints_Right.push_back(Point2f(2228,1160));

	//TODO: CenterFieldPoints in Calibrierung einfügen
	m_CalibrationCenterFieldPoints_Right.push_back(Point2f(2777,748));
	m_CalibrationCenterFieldPoints_Right.push_back(Point2f(2777,1160));
	m_CalibrationCenterFieldPoints_Right.push_back(Point2f(2228,1160));
	m_CalibrationCenterFieldPoints_Right.push_back(Point2f(2228,748));

	m_leftSingelFieldPoints.push_back(Point2f(400,337));
	m_leftSingelFieldPoints.push_back(Point2f(1587,337));
	m_leftSingelFieldPoints.push_back(Point2f(1587,1160));
	m_leftSingelFieldPoints.push_back(Point2f(400,1160));

	m_rightSingelFieldPoints.push_back(Point2f(1587,337));
	m_rightSingelFieldPoints.push_back(Point2f(2775,337));
	m_rightSingelFieldPoints.push_back(Point2f(2775,1160));
	m_rightSingelFieldPoints.push_back(Point2f(1587,1160));

	m_courtBoundary.push_back(Point2f(0 - m_modelOffset,0 - m_modelOffset));
	m_courtBoundary.push_back(Point2f(m_modelWidth + m_modelOffset, 0 - m_modelOffset));
	m_courtBoundary.push_back(Point2f(m_modelWidth + m_modelOffset, m_modelHeight + m_modelOffset));
	m_courtBoundary.push_back(Point2f(0 - m_modelOffset, m_modelHeight + m_modelOffset));

	m_fieldPoints.push_back(Point2f(400,200)); //0
	m_fieldPoints.push_back(Point2f(1587,200));//1
	m_fieldPoints.push_back(Point2f(2775,200));//2

	m_fieldPoints.push_back(Point2f(400,337));//3
	m_fieldPoints.push_back(Point2f(948,337));//4
	m_fieldPoints.push_back(Point2f(1587,337));//5
	m_fieldPoints.push_back(Point2f(2228,337));//6
	m_fieldPoints.push_back(Point2f(2775,337));//7

	m_fieldPoints.push_back(Point2f(948,748));//8
	m_fieldPoints.push_back(Point2f(1587,748));//9
	m_fieldPoints.push_back(Point2f(2228,748));//10

	m_fieldPoints.push_back(Point2f(400,1160));//11
	m_fieldPoints.push_back(Point2f(948,1160));//12
	m_fieldPoints.push_back(Point2f(1587,1160));//13
	m_fieldPoints.push_back(Point2f(2228,1160));//14
	m_fieldPoints.push_back(Point2f(2775,1160));//15

	m_fieldPoints.push_back(Point2f(400,1297));//16
	m_fieldPoints.push_back(Point2f(1587,1297));//17
	m_fieldPoints.push_back(Point2f(2775,1297));//18


	m_serveZoneLeft1.push_back(Point2f(50,400)); // m_serveZoneLeft1.push_back(Point2f(200,507));
	m_serveZoneLeft1.push_back(Point2f(600,400)); // m_serveZoneLeft1.push_back(Point2f(500,507));
	m_serveZoneLeft1.push_back(Point2f(600,748)); // m_serveZoneLeft1.push_back(Point2f(500,748));
	m_serveZoneLeft1.push_back(Point2f(50,748)); // m_serveZoneLeft1.push_back(Point2f(200,748));

	m_serveZoneLeft2.push_back(Point2f(50,748)); // m_serveZoneLeft2.push_back(Point2f(200,748));
	m_serveZoneLeft2.push_back(Point2f(600,748)); // m_serveZoneLeft2.push_back(Point2f(500,748));
	m_serveZoneLeft2.push_back(Point2f(600,1100)); // m_serveZoneLeft2.push_back(Point2f(500,1000));
	m_serveZoneLeft2.push_back(Point2f(50,1100)); // m_serveZoneLeft2.push_back(Point2f(200,1000));

	m_serveZoneRight3.push_back(Point2f(2575,400)); // m_serveZoneRight3.push_back(Point2f(2675,507));
	m_serveZoneRight3.push_back(Point2f(m_modelWidth - 50, 400)); // m_serveZoneRight3.push_back(Point2f(2975,507));
	m_serveZoneRight3.push_back(Point2f(m_modelWidth - 50,748)); // m_serveZoneRight3.push_back(Point2f(2975,748));
	m_serveZoneRight3.push_back(Point2f(2575,748)); // m_serveZoneRight3.push_back(Point2f(2675,748));

	m_serveZoneRight4.push_back(Point2f(2575,748)); // m_serveZoneRight4.push_back(Point2f(2675,748));
	m_serveZoneRight4.push_back(Point2f(m_modelWidth - 50,748)); // m_serveZoneRight4.push_back(Point2f(2975,748));
	m_serveZoneRight4.push_back(Point2f(m_modelWidth - 50,1100)); // m_serveZoneRight4.push_back(Point2f(2975,1000));
	m_serveZoneRight4.push_back(Point2f(2575,1100)); // m_serveZoneRight4.push_back(Point2f(2675,1000));

	m_returnZoneLeft1.push_back(Point2f(0,0));
	m_returnZoneLeft1.push_back(Point2f(1000,0));
	m_returnZoneLeft1.push_back(Point2f(1000,748));
	m_returnZoneLeft1.push_back(Point2f(0,748));

	m_returnZoneLeft2.push_back(Point2f(0,748));
	m_returnZoneLeft2.push_back(Point2f(1000,748));
	m_returnZoneLeft2.push_back(Point2f(1000,m_modelHeight));
	m_returnZoneLeft2.push_back(Point2f(0,m_modelHeight));

	m_returnZoneRight3.push_back(Point2f(2075,0));
	m_returnZoneRight3.push_back(Point2f(m_modelWidth,0));
	m_returnZoneRight3.push_back(Point2f(m_modelWidth,748));
	m_returnZoneRight3.push_back(Point2f(2075,748));

	m_returnZoneRight4.push_back(Point2f(2075,748));
	m_returnZoneRight4.push_back(Point2f(m_modelWidth,748));
	m_returnZoneRight4.push_back(Point2f(m_modelWidth,m_modelHeight));
	m_returnZoneRight4.push_back(Point2f(2075,m_modelHeight));

	m_serveImpactZoneLeft1.push_back(Point2f(948,337));
	m_serveImpactZoneLeft1.push_back(Point2f(1587,337));
	m_serveImpactZoneLeft1.push_back(Point2f(1587,748));
	m_serveImpactZoneLeft1.push_back(Point2f(948,748));

	m_serveImpactZoneLeft2.push_back(Point2f(948,748));
	m_serveImpactZoneLeft2.push_back(Point2f(1587,748));
	m_serveImpactZoneLeft2.push_back(Point2f(1587,1160));
	m_serveImpactZoneLeft2.push_back(Point2f(948,1160));

	m_serveImpactZoneRight3.push_back(Point2f(1587,337));
	m_serveImpactZoneRight3.push_back(Point2f(2228,337));
	m_serveImpactZoneRight3.push_back(Point2f(2228,748));
	m_serveImpactZoneRight3.push_back(Point2f(1587,748));

	m_serveImpactZoneRight4.push_back(Point2f(1587,748));
	m_serveImpactZoneRight4.push_back(Point2f(2228,748));
	m_serveImpactZoneRight4.push_back(Point2f(2228,1160));
	m_serveImpactZoneRight4.push_back(Point2f(1587,1160));

	//cout<<"LastPointOfZone4: "<<m_serveImpactZoneRight4.back()<<endl;

	Scalar color = Scalar(255,255,255);

	/*for(int i = 0; i < m_fieldPoints.size(); i++){
	    circle( m_fieldModel, m_fieldPoints[i], 4 , color, -1, 8, 0 );
	   }*/
	int lineThick = 6;
	//leftField
	rectangle(m_fieldModel, m_fieldPoints[3], m_fieldPoints[12], color, lineThick, 8, 0);
	rectangle(m_fieldModel, m_fieldPoints[4], m_fieldPoints[9], color, lineThick, 8, 0);
	rectangle(m_fieldModel, m_fieldPoints[8], m_fieldPoints[13], color, lineThick, 8, 0);
	rectangle(m_fieldModel, m_fieldPoints[0], m_fieldPoints[17], color, lineThick, 8, 0);

	//rightField
	rectangle(m_fieldModel, m_fieldPoints[5], m_fieldPoints[10], color, lineThick, 8, 0);
	rectangle(m_fieldModel, m_fieldPoints[9], m_fieldPoints[14], color, lineThick, 8, 0);
	rectangle(m_fieldModel, m_fieldPoints[6], m_fieldPoints[15], color, lineThick, 8, 0);
	rectangle(m_fieldModel, m_fieldPoints[1], m_fieldPoints[18], color, lineThick, 8, 0);

	/*rectangle(m_fieldModel, m_serveZoneLeft1[0], m_serveZoneLeft1[2], color, 3, 8, 0);
	   rectangle(m_fieldModel, m_serveZoneLeft2[0], m_serveZoneLeft2[2], color, 3, 8, 0);
	   rectangle(m_fieldModel, m_serveZoneRight3[0], m_serveZoneRight3[2], color, 3, 8, 0);
	   rectangle(m_fieldModel, m_serveZoneRight4[0], m_serveZoneRight4[2], color, 3, 8, 0);
	 */
	/*color = Scalar(0,255,0);

	   rectangle(m_fieldModel, m_serveImpactZoneLeft1[0], m_serveImpactZoneLeft1[2], color, 3, 8, 0);
	   rectangle(m_fieldModel, m_serveImpactZoneLeft2[0], m_serveImpactZoneLeft2[2], color, 3, 8, 0);
	   rectangle(m_fieldModel, m_serveImpactZoneRight3[0], m_serveImpactZoneRight3[2], color, 3, 8, 0);
	   rectangle(m_fieldModel, m_serveImpactZoneRight4[0], m_serveImpactZoneRight4[2], color, 3, 8, 0);
	 */
	//imshow("fieldModel", m_fieldModel);
	//waitKey(30);

	resize(m_fieldModel, m_fieldModelSmall, Size(),0.2,0.2);
}

void FieldDetection::readVectorOfPointsFromFileNode(cv::FileNode& node, std::vector<cv::Point2f>& vec){

	for (FileNodeIterator itPts = node.begin(); itPts != node.end(); ++itPts) {
		// Read each point
		FileNode pt = *itPts;

		Point2f point;
		FileNodeIterator itPt = pt.begin();
		point.x = *itPt; ++itPt;
		point.y = *itPt;

		vec.push_back(point);
	}
}

void FieldDetection::calibrateFieldLinesWithPresetPoints(){
	cout<<"calibrateFieldLinesWithPresetPoints"<<endl;
	//Read Data from XML File
	FileStorage fileReader(m_config.getFilePathToFieldCalibrationPoints(), FileStorage::READ);
	fileReader.open(m_config.getFilePathToFieldCalibrationPoints(), FileStorage::READ);

	vector<Point2f> filepoints_preset_right;
	vector<Point2f> filepoints_preset_left;
	vector<Point2f> filepoints_new_right;
	vector<Point2f> filepoints_new_left;
	vector<Point2f> filepoints_calibration_right;
	vector<Point2f> filepoints_calibration_left;

	int image_width = (int) fileReader["image_width"];
	int image_height = (int) fileReader["image_height"];

	FileNode fourCornerVecNode = fileReader["right_side_points_preset"];
	readVectorOfPointsFromFileNode(fourCornerVecNode, filepoints_preset_right);
	fourCornerVecNode = fileReader["left_side_points_preset"];
	readVectorOfPointsFromFileNode(fourCornerVecNode, filepoints_preset_left);

	fourCornerVecNode = fileReader["right_side_points_new"];
	if(!fourCornerVecNode.empty()){readVectorOfPointsFromFileNode(fourCornerVecNode, filepoints_new_right);}
	fourCornerVecNode = fileReader["left_side_points_new"];
	if(!fourCornerVecNode.empty()){readVectorOfPointsFromFileNode(fourCornerVecNode, filepoints_new_left);}

	fourCornerVecNode = fileReader["right_side_points_calibration"];
	if(!fourCornerVecNode.empty()){readVectorOfPointsFromFileNode(fourCornerVecNode, filepoints_calibration_right);}
	fourCornerVecNode = fileReader["left_side_points_calibration"];
	if(!fourCornerVecNode.empty()){readVectorOfPointsFromFileNode(fourCornerVecNode, filepoints_calibration_left);}

	fileReader.release();

	//calibration type 0 reads preset fourCornersOfCenterField from an xml data
	//and uses these points to create a calibration matrix
	if(m_useCalibrationType == 0) {
		if(m_isRightFieldSide == true) {
			cout<<"Reading FileStorageRight preset: "<<endl;
			m_fourCornersOfCenterField = filepoints_preset_right;
			cout<<"CenterFieldRight: "<< m_fourCornersOfCenterField[2]<<" "<<m_fourCornersOfCenterField[3]<<endl;
		}
		else{
			cout<<"Reading FileStorageLeft preset: "<<endl;
			m_fourCornersOfCenterField = filepoints_preset_left;
			cout<<"CenterFieldLeft: "<< m_fourCornersOfCenterField[2]<<" "<<m_fourCornersOfCenterField[3]<<endl;
		}
	}
	//calibration type 1 reads new fourCornersOfCenterField from an xml data
	//and uses these points to create a calibration matrix
	else if(m_useCalibrationType == 1) {
		if(m_isRightFieldSide == true) {
			cout<<"Reading FileStorageRight new: "<<endl;
			if(!filepoints_new_right.empty()) {
				m_fourCornersOfCenterField = filepoints_new_right;
			}
			else{
				m_fourCornersOfCenterField = filepoints_preset_right;
			}
		}
		else{
			cout<<"Reading FileStorageLeft new: "<<endl;
			if(!filepoints_new_left.empty()) {
				m_fourCornersOfCenterField = filepoints_new_left;
			}
			else{
				m_fourCornersOfCenterField = filepoints_preset_left;
			}
		}
	}
	//Calibration type 2 reads current by calibration type 3 detected cornerpoints from an xml data
	//these points were detected during last calibration and uses these points to create a calibration matrix
	else if(m_useCalibrationType == 2){
		if(m_isRightFieldSide == true){
			cout<<"Reading FileStorageRight from last Calibration: "<<endl;
			if(!filepoints_calibration_right.empty()) {
				m_fourCornersOfCenterField = filepoints_calibration_right;
			}
			else{
				m_fourCornersOfCenterField = filepoints_preset_right;
			}
		}
		else{
			cout<<"Reading FileStorageLeft from last Calibration: "<<endl;
			if(!filepoints_calibration_left.empty()) {
				m_fourCornersOfCenterField = filepoints_calibration_left;
			}
			else{
				m_fourCornersOfCenterField = filepoints_preset_left;
			}
		}
	}
	else{
		cout<<"FieldLines something went wrong. Not a correct Calibration type for this function"<<endl;
	}

	calculateHomomorphyMatrix();
}

void FieldDetection::calibrateFieldLinesWithCalibrationVideo(VideoCapture calibrationCapture){

	vector<Mat> imgBuffer;
	for(int i = 0; i < m_calibrationCount; i++) {
		Mat imgOriginal;

		bool bSuccess = calibrationCapture.read(imgOriginal); // read a new frame from video

		if (!bSuccess) {
			cout << "FieldDetection: Cannot read a frame from video stream" << endl;
			break;
		}
		imgBuffer.push_back(Mat());
		undistort(imgOriginal, imgBuffer.back(), m_cameraMatrix, m_distortionCoefficients);
	}

	//Read Data from XML File
	FileStorage fileReader(m_config.getFilePathToFieldCalibrationPoints(), FileStorage::READ);
	fileReader.open(m_config.getFilePathToFieldCalibrationPoints(), FileStorage::READ);

	vector<Point2f> filepoints_preset_right;
	vector<Point2f> filepoints_preset_left;
	vector<Point2f> filepoints_new_right;
	vector<Point2f> filepoints_new_left;
	vector<Point2f> filepoints_calibration_right;
	vector<Point2f> filepoints_calibration_left;

	int image_width = (int) fileReader["image_width"];
	int image_height = (int) fileReader["image_height"];

	FileNode fourCornerVecNode = fileReader["right_side_points_preset"];
	readVectorOfPointsFromFileNode(fourCornerVecNode, filepoints_preset_right);
	fourCornerVecNode = fileReader["left_side_points_preset"];
	readVectorOfPointsFromFileNode(fourCornerVecNode, filepoints_preset_left);

	fourCornerVecNode = fileReader["right_side_points_new"];
	if(!fourCornerVecNode.empty()){readVectorOfPointsFromFileNode(fourCornerVecNode, filepoints_new_right);}
	fourCornerVecNode = fileReader["left_side_points_new"];
	if(!fourCornerVecNode.empty()){readVectorOfPointsFromFileNode(fourCornerVecNode, filepoints_new_left);}

	fourCornerVecNode = fileReader["right_side_points_calibration"];
	if(!fourCornerVecNode.empty()){readVectorOfPointsFromFileNode(fourCornerVecNode, filepoints_calibration_right);}
	fourCornerVecNode = fileReader["left_side_points_calibration"];
	if(!fourCornerVecNode.empty()){readVectorOfPointsFromFileNode(fourCornerVecNode, filepoints_calibration_left);}

	fileReader.release();

	//calibration type 0 reads preset fourCornersOfCenterField from an xml data
	//and uses these points to create a calibration matrix
	if(m_useCalibrationType == 0) {
		if(m_isRightFieldSide == true) {
			cout<<"Reading FileStorageRight preset: "<<endl;
			m_fourCornersOfCenterField = filepoints_preset_right;
			cout<<"CenterFieldRight: "<< m_fourCornersOfCenterField[2]<<" "<<m_fourCornersOfCenterField[3]<<endl;
		}
		else{
			cout<<"Reading FileStorageLeft preset: "<<endl;
			m_fourCornersOfCenterField = filepoints_preset_left;
			cout<<"CenterFieldLeft: "<< m_fourCornersOfCenterField[2]<<" "<<m_fourCornersOfCenterField[3]<<endl;
		}
	}
	//calibration type 1 reads new fourCornersOfCenterField from an xml data
	//and uses these points to create a calibration matrix
	else if(m_useCalibrationType == 1) {
		if(m_isRightFieldSide == true) {
			cout<<"Reading FileStorageRight new: "<<endl;
			if(!filepoints_new_right.empty()) {
				m_fourCornersOfCenterField = filepoints_new_right;
			}
			else{
				m_fourCornersOfCenterField = filepoints_preset_right;
			}
		}
		else{
			cout<<"Reading FileStorageLeft new: "<<endl;
			if(!filepoints_new_left.empty()) {
				m_fourCornersOfCenterField = filepoints_new_left;
			}
			else{
				m_fourCornersOfCenterField = filepoints_preset_left;
			}
		}
	}
	//Calibration type 2 reads current by calibration type 3 detected cornerpoints from an xml data
	//these points were detected during last calibration and uses these points to create a calibration matrix
	else if(m_useCalibrationType == 2){
		if(m_isRightFieldSide == true){
			cout<<"Reading FileStorageRight from last Calibration: "<<endl;
			if(!filepoints_calibration_right.empty()) {
				m_fourCornersOfCenterField = filepoints_calibration_right;
			}
			else{
				m_fourCornersOfCenterField = filepoints_preset_right;
			}
		}
		else{
			cout<<"Reading FileStorageLeft from last Calibration: "<<endl;
			if(!filepoints_calibration_left.empty()) {
				m_fourCornersOfCenterField = filepoints_calibration_left;
			}
			else{
				m_fourCornersOfCenterField = filepoints_preset_left;
			}
		}
	}
	//calibration type 3 trys to find the fourCornersOfCenterField in the video data
	//and uses these points to create a calibration matrix
	//1. The frames are converted to grayscale.
	//2. The grayscale images are thresholded to finde the bright white lines.
	//3. The canny edge detection finds edges in the thresholded images
	//4. The edges are found with houghLines and summed up over 30 images.
	//5. calculateCorrespondingCornerPoints() is called.
	//6. this function inverts the summed up houghLines image to get the fieldparts.
	//7. At least the field parts are scanned to find the centerField. And then the fourCornersOfCenterField are detected.
	else{
		//m_calibrationIsRunning = true;
		//vector<Mat> imgBuffer;
		cout<<"imgBuffer.size(): "<<imgBuffer.size()<<endl;
		if(imgBuffer.size() == 0) {
			cout<<"imgBuffer contains no calibration images"<<endl;
		}
		/*
		for(int i = 0; i < imgBuffer.size(); i++)
		{
			detecFieldLines(imgBuffer[i]);
			//detecFieldLinesSobel(imgBuffer[i]);
			//imgOriginal = imgOriginal + m_calibratedImgLines;
			//imshow("Original", imgOriginal); //show the original image

			//waitKey(30);
			//if (waitKey(30) == 27) //wait for 'esc' key press for 30ms. If 'esc' key is pressed, break loop
			//   {
			//   cout << "esc key is pressed by user" << endl;
			//   break;
			//   }
			cout<<i<<endl;
		}
		*/

		detecFieldLinesSobel(imgBuffer[20]);
		cout<<"Successfully detected Fieldlines"<<endl;

		calculateCorrespondingCornerPoints();
	}
	calculateHomomorphyMatrix();
	calculateBlendedBackgroundImg(imgBuffer);
	m_calibrationIsRunning = false;

	//Calculate the Centerfieldpoints with homography Mat
	if(m_useCalibrationType == 3){
		Mat invertHomographyMat;
		vector<Point2f> transformedCenterFieldPoints;
		//invertAffineTransform(m_homographyMat, invertHomographyMat);
		Mat drawing = m_originalImg.clone();

		if(m_isRightFieldSide){
			invertHomographyMat = findHomography(m_CalibrationFieldPoints_Right, m_fourCornersOfCenterField, 0);
			//perspectiveTransform(m_rightCenterFieldPoints, transformedCenterFieldPoints, invertHomographyMat);
			perspectiveTransform(m_CalibrationCenterFieldPoints_Right, transformedCenterFieldPoints, invertHomographyMat);

			for(int i=0; i< m_fourCornersOfCenterField.size(); i++){
				cout<<transformedCenterFieldPoints[i].x<<" | "<<transformedCenterFieldPoints[i].y<<endl;
				circle(drawing, transformedCenterFieldPoints[i], 3, Scalar(0, 0, 255), -1,8,0);
			}
			filepoints_calibration_right = transformedCenterFieldPoints;
		}
		else{
			invertHomographyMat = findHomography(m_CalibrationFieldPoints_Left, m_fourCornersOfCenterField, 0);
			//perspectiveTransform(m_leftCenterFieldPoints, transformedCenterFieldPoints, invertHomographyMat);
			perspectiveTransform(m_CalibrationCenterFieldPoints_Left, transformedCenterFieldPoints, invertHomographyMat);

			for(int i=0; i< m_fourCornersOfCenterField.size(); i++){
				cout<<transformedCenterFieldPoints[i].x<<" | "<<transformedCenterFieldPoints[i].y<<endl;
				circle(drawing, transformedCenterFieldPoints[i], 3, Scalar(0, 0, 255), -1,8,0);
			}
			filepoints_calibration_left = transformedCenterFieldPoints;
		}
		//imshow("Window", drawing);
		//waitKey(0);
	}

	//Overwrite current XML file:
	//Write back ALL Pointvectors, that are not empty to XML file
	FileStorage fileStorage(m_config.getFilePathToFieldCalibrationPoints(), FileStorage::WRITE);
	fileStorage.open(m_config.getFilePathToFieldCalibrationPoints(), FileStorage::WRITE);

	cout<<"Writing Calibration Points to FileStorageRight: "<<endl;
  time_t rawtime; time(&rawtime);

  fileStorage << "calibrationDate" << asctime(localtime(&rawtime));
	fileStorage << "image_Width" << m_originalImg.cols;
	fileStorage << "image_Height" << m_originalImg.rows;

	//Preset Points
	fileStorage << "right_side_points_preset";
	fileStorage << "[:";
	for (int i = 0; i < filepoints_preset_right.size(); ++i){ fileStorage << "[:" << filepoints_preset_right[i].x << filepoints_preset_right[i].y << "]";}
	fileStorage << "]";

	fileStorage << "left_side_points_preset";
	fileStorage << "[:";
	for (int i = 0; i < filepoints_preset_left.size(); ++i){ fileStorage << "[:" << filepoints_preset_left[i].x << filepoints_preset_left[i].y << "]";}
	fileStorage << "]";

	//Clicked Points
	if(!filepoints_new_right.empty()){
		fileStorage << "right_side_points_new";
		fileStorage << "[:";
		for (int i = 0; i < filepoints_new_right.size(); ++i){ fileStorage << "[:" << filepoints_new_right[i].x << filepoints_new_right[i].y << "]";}
		fileStorage << "]";
	}

	if(!filepoints_new_left.empty()){
		fileStorage << "left_side_points_new";
		fileStorage << "[:";
		for (int i = 0; i < filepoints_new_left.size(); ++i){ fileStorage << "[:" << filepoints_new_left[i].x << filepoints_new_left[i].y << "]";}
		fileStorage << "]";
	}

	//Calibration Points
	if(!filepoints_calibration_right.empty()){
		fileStorage << "right_side_points_calibration";
		fileStorage << "[:";
		for (int i = 0; i < filepoints_calibration_right.size(); ++i){ fileStorage << "[:" << filepoints_calibration_right[i].x << filepoints_calibration_right[i].y << "]";}
		fileStorage << "]";
	}

	if(!filepoints_calibration_left.empty()){
		fileStorage << "left_side_points_calibration";
		fileStorage << "[:";
		for (int i = 0; i < filepoints_calibration_left.size(); ++i){ fileStorage << "[:" << filepoints_calibration_left[i].x << filepoints_calibration_left[i].y << "]";}
		fileStorage << "]";
	}
	fileStorage.release();
}

void FieldDetection::detecFieldLines(Mat& originalImg){
    Mat cannyOutput;
    //just for faster testing
    m_lowBinaryThreshold = 130;
    m_highBinaryThreshold = 255;

    m_originalImg = originalImg.clone();
    Mat grayScaleImg(originalImg.size(), CV_8U);
    m_imgLines = Mat::zeros( originalImg.size(), CV_8UC3 );
    vector<Vec4i> lines;
    //imshow("OriginalImage", originalImg);
    //waitKey(30);
    //Mat imadjust = imageAdjustment(originalImg, 30);
    //imshow("ImageAdjustment", imadjust);
    //Mat histogramE = histogramEqualisation(originalImg);
    //imshow("HistogrammEqualisation", histogramE);
    //Mat aheq4 = adaptiveHistogramEqualisation(originalImg, 2);
    //imshow("adaptiveHistogramEqualisation4", aheq4);
    //Mat aheq8 = adaptiveHistogramEqualisation(originalImg, 50);
    //imshow("adaptiveHistogramEqualisation8", aheq8);
    //Mat contrast = contrastNormalisation(aheq4);
    //imshow("contrast", contrast);
    //waitKey(0);
    // select a region of interest
    Mat originalImgRoi = originalImg.clone();
    //Mat originalImgRoi = aheq4.clone();
    Mat roi = originalImgRoi(cv::Rect(0, 0, grayScaleImg.cols, 250));
    roi.setTo(cv::Scalar(0,0,0));

    //Grayscale matrix

    //Convert BGR to Gray
    cv::cvtColor( originalImgRoi, grayScaleImg, CV_BGR2GRAY );
    //imshow("GrayScale", grayScaleImg);
    cv::Mat binaryMat(grayScaleImg.size(), grayScaleImg.type());

    cv::threshold(grayScaleImg, binaryMat, m_lowBinaryThreshold, m_highBinaryThreshold, cv::THRESH_BINARY);
    imshow("binaryMat", binaryMat);

    Canny( binaryMat, cannyOutput, m_minCannyThresh, m_maxCannyThresh*2, 3 );

    HoughLinesP( cannyOutput, lines, 1, CV_PI/180, (int)80*m_inputVideoResizingFactor, 30*m_inputVideoResizingFactor, 30*m_inputVideoResizingFactor );
    for( uint32_t i = 0; i < lines.size(); i++ )
    {
        line( m_imgLines, Point(lines[i][0], lines[i][1]),
             Point(lines[i][2], lines[i][3]), Scalar(0,0,255), 2, 8 ); //eig thickness = 2
    }
    if(m_calibrationIsRunning == true){
        m_calibratedImgLines += m_imgLines;
        //imshow("CalibrationImg",m_calibratedImgLines);
    }
   }

void FieldDetection::detecFieldLinesSobel(Mat& originalImg){
	Mat Img_gray, Img_sobel, Img_sobel_cut, Img_sobel_trashed;
  vector<Vec4i> lines;
  Point point1, point2, borderpoint1, borderpoint2;
  //int tresh_hough, length_hough, gap_hough;

	//Mat drawing = originalImg.clone();
	m_originalImg = originalImg.clone();
	imshow("Window", originalImg);
	waitKey(1);

	m_imgLines = Mat::zeros( originalImg.size(), CV_8UC3 );
	//m_imgLines = originalImg.clone();

	cvtColor(originalImg, Img_gray, CV_BGR2GRAY);

	//Sobel( Img_gray, Img_sobelx, -1, 1, 0, 1, 1, 0, BORDER_DEFAULT );
	Sobel( Img_gray, Img_sobel, -1, 0, 1, 1, 1, 0, BORDER_DEFAULT );
	//addWeighted(Img_sobelx, 1, Img_sobely, 1, 0, Img_sobel);

	cut_Image(Img_sobel, Img_sobel_cut);
	imshow("Window", Img_sobel_cut);
	waitKey(1);

	threshold(Img_sobel_cut, Img_sobel_trashed, 4, 255, THRESH_BINARY); //Threshold the image
	HoughLinesP( Img_sobel_trashed, lines, 1, CV_PI/180, (int)130*m_inputVideoResizingFactor, 100*m_inputVideoResizingFactor, 5*m_inputVideoResizingFactor );

	//cvtColor(Img_sobelTrashed, Img_sobelTrashed, CV_GRAY2BGR);

	//Draw lines up to border
	for( uint32_t i = 0; i < lines.size(); i++ ){
		//line( m_imgLines, Point(lines[i][0], lines[i][1]), Point(lines[i][2], lines[i][3]), Scalar(0,0,255), 2, 8 ); //eig thickness = 2
		point1 = Point(lines[i][0], lines[i][1]);
		point2 = Point(lines[i][2], lines[i][3]);

		calculateLinePointsBorder(point1, point2, borderpoint1, borderpoint2, originalImg.cols, originalImg.rows);
		line( m_imgLines, borderpoint1, borderpoint2, Scalar(0,255,0), 2, 8 ); //eig thickness = 2
		//line( Img_sobelTrashed, borderpoint1, borderpoint2, Scalar(0,255,0), 2, 8 ); //eig thickness = 2
	}
	imshow("Window", m_imgLines);
	waitKey(1);

	cvtColor(m_imgLines, m_imgLines, CV_BGR2GRAY);
	threshold(m_imgLines, m_imgLines, 70, 255, THRESH_BINARY_INV);
	imshow("Window", m_imgLines);
	waitKey(1);
}

//Calculates the CornerPoints of searched Contour
void FieldDetection::calculateCorrespondingCornerPoints(){
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	vector<Point2f> cornerFieldPoints;
	//RNG rng(12345);

	findContours( m_imgLines, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
	Mat drawing = m_originalImg.clone();
	cout<<"Numbers of Contours: "<<contours.size()<<endl;

	Point2f midpoint;
  if(m_isRightFieldSide){
		midpoint = Point(0.836*m_frameWidth, 0.68*m_frameHeight); //Point for right Image
	}
  else{
		midpoint = Point(0.164*m_frameWidth, 0.68*m_frameHeight); //Point for left Image
  }
  int index = 0;

  imgProc::imageObjects::findContourIndexBelongingToPoint(contours, midpoint, index);
  double area = contourArea(contours[index], false);
  cout<<"Size of contour: "<<area<<endl;

  if(area > 8000*m_inputVideoResizingFactor*m_inputVideoResizingFactor && area < 12000*m_inputVideoResizingFactor*m_inputVideoResizingFactor){
    drawContours( drawing, contours, index, Scalar(0, 255, 0), -1, 8, hierarchy, 0, Point() );
    imgProc::imageObjects::findContourCornerPoints(contours[index], cornerFieldPoints, m_isRightFieldSide);

    for(int i = 0; i < cornerFieldPoints.size(); i++){
      cout<<cornerFieldPoints[i].x<<" | "<<cornerFieldPoints[i].y;
			cout<<" Point added"<<endl;
     	circle(drawing, cornerFieldPoints[i], 3, Scalar(0, 0, 255), -1,8,0);
    }
  }
	imshow( "Detected Contour", drawing );
  waitKey(0);
	destroyWindow("Detected Contour");

	m_fourCornersOfCenterField.push_back(cornerFieldPoints[0]);
	m_fourCornersOfCenterField.push_back(cornerFieldPoints[1]);
	m_fourCornersOfCenterField.push_back(cornerFieldPoints[2]);
	m_fourCornersOfCenterField.push_back(cornerFieldPoints[3]);
}

//Cuts a Frame along the main field
void FieldDetection::cut_Image(Mat& originalImg, Mat& dstImg){
	dstImg = originalImg.clone();
	Point points[2][5];

	//Points Left Image
	points[0][0] = Point(0, 0);
	points[0][1] = Point(0, originalImg.rows*0.6);
	points[0][2] = Point(originalImg.cols*0.55, originalImg.rows*0.545);
	points[0][3] = Point(originalImg.cols, originalImg.rows*0.58);
	points[0][4] = Point(originalImg.cols, 0);
	//Points Right Image
	points[1][0] = Point(0, 0);
	points[1][1] = Point(0, originalImg.rows*0.58);
	points[1][2] = Point(originalImg.cols*0.45, originalImg.rows*0.545);
	points[1][3] = Point(originalImg.cols, originalImg.rows*0.6);
	points[1][4] = Point(originalImg.cols, 0);

  if(m_isRightFieldSide){
		const Point* ppt[1] = {points[1]};
    int npt[] = {5};
    //line(cut_Img, point1_R, point2_R, Scalar(0, 0, 255), 2, 8, 0);
    //line(cut_Img, point2_R, point3_R, Scalar(0, 255, 0), 2, 8, 0);
    fillPoly( dstImg, ppt, npt, 1, Scalar( 0, 0, 0 ), 8 );
	}
  else{
		const Point* ppt[1] = {points[0]};
		int npt[] = {5};
		//line(cut_Img, point1_L, point2_L, Scalar(0, 0, 255), 2, 8, 0);
		//line(cut_Img, point2_L, point3_L, Scalar(0, 255, 0), 2, 8, 0);
		fillPoly( dstImg, ppt, npt, 1, Scalar( 0, 0, 0 ), 8 );
 	}
}

//TODO: Outsourcen into imageMath
//Calculates the two Points in that a line would contact a border if the line would be continued up to the image border
void FieldDetection::calculateLinePointsBorder(Point& point1, Point& point2, Point& borderpoint1, Point& borderpoint2, int imageWidth, int imageHeight){
   double m;
   if(point1.x > point2.x){
      Point switchpoint = point2;
      point2 = point1;
      point1 = switchpoint;
   }
   cout<<"Point 1: "<<point1.x<<" "<<point1.y<<endl;
   cout<<"Point 2: "<<point2.x<<" "<<point2.y<<endl;

   m = (double(point2.y - point1.y)) / double((point2.x - point1.x));

   cout<<"Steigung: "<<m<<endl;
   //3 Cases for each point
   //Left Point contacts upper border
   if((point1.y -m * point1.x) < 0){
      borderpoint1.x = point1.x - abs(1/m)* point1.y;
      borderpoint1.y = 0;
   }
   //Left point contacts lower border
   else if((point1.y -m * point1.x) > imageHeight){
      borderpoint1.x = point1.x - abs(1/m)* (imageHeight - point1.y);
      borderpoint1.y = imageHeight;
   }
   //Left point contacts left border
   else{
      borderpoint1.x = 0;
      borderpoint1.y = point1.y -m * point1.x;
   }
   //Right point contacts upper border
   if((point2.y +m * (imageWidth - point2.x)) < 0){
      borderpoint2.x = point2.x + abs(1/m)* point2.y;
      borderpoint2.y = 0;
   }
   //Right point contacts lower border
   else if((point2.y +m * (imageWidth - point2.x)) > imageHeight){
      borderpoint2.x = point2.x + abs(1/m)* (imageHeight - point2.y);
      borderpoint2.y = imageHeight;
   }
   //Right point contacts right border
   else{
      borderpoint2.x = imageWidth;
      borderpoint2.y = point2.y + m* (imageWidth-point2.x);
   }

   cout<<"BorderPoint 1: "<<borderpoint1.x<<" | "<<borderpoint1.y<<endl;
   cout<<"BorderPoint 2: "<<borderpoint2.x<<" | "<<borderpoint2.y<<endl;
   cout<<endl;
}

/*
void FieldDetection::calculateCorrespondingCornerPoints(){

	Mat detectedLinesGray;
	cvtColor(m_calibratedImgLines,detectedLinesGray, CV_BGR2GRAY);

	threshold (detectedLinesGray, detectedLinesGray, 70, 255, CV_THRESH_BINARY_INV);
	imshow("detectedLinesGray",detectedLinesGray);
	waitKey(30);
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	findContours(detectedLinesGray, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	Mat Contours = Mat::zeros(m_frameHeight, m_frameWidth, CV_8UC1);

	vector<Point> centerFieldContour;
	int mainFieldnumber = 0;


	Point2f north;
	Point2f east;
	Point2f south;
	Point2f west;
	//vector<Point2f> fourCornersOfCenterField;

	//float distanceMultAreaMax = 0.0;
	float ratioMax = 0.0;


	//int conCount = 0;
	for( int32_t idx = 0; idx >= 0; idx = hierarchy[idx][0] )
	{
		if(contours[idx].size() > 3) {

			west = Point2f(m_frameWidth,m_frameHeight);
			east = Point2f(0,0);
			north = Point2f(m_frameWidth,m_frameHeight);
			south = Point2f(0,0);

			double area = contourArea(contours[idx], false);
			std::vector<cv::Point> approx;
			// Approximate contour with accuracy proportional
			// to the contour perimeter
			cv::approxPolyDP(cv::Mat(contours[idx]), approx, cv::arcLength(cv::Mat(contours[idx]), true)*0.02, true);
			//skip small or convex Contours
			if(area > 10000 && isContourConvex(approx)) {

				Rect boundRect = boundingRect( Mat(contours[idx]) );

				//calculate width/height //evt kombinieren mit abstand zum mittelpunkt des bildes.
				float ratio = boundRect.width/boundRect.height;

				if(centerFieldContour.size() == 0) {
					centerFieldContour = contours[idx];
					mainFieldnumber = idx;
					ratioMax = ratio;
				}

				else if(ratioMax < ratio) {
					centerFieldContour = contours[idx];
					mainFieldnumber = idx;
					ratioMax = ratio;
				}
			}
		}
	}

	drawContours( Contours, contours, mainFieldnumber, 255, CV_FILLED, 8, hierarchy );

//    Rectangular box: MORPH_RECT
//    Cross: MORPH_CROSS
//    Ellipse: MORPH_ELLIPSE
//
	int delateSizex = 13;
	int delateSizey = 3;
	dilate( Contours, Contours, getStructuringElement(MORPH_CROSS, Size(delateSizex, delateSizey)), Point(-1,-1), 2);
	dilate( Contours, Contours, getStructuringElement(MORPH_RECT, Size(delateSizex, delateSizey)), Point(-1,-1), 1);
	drawContours( Contours, contours, mainFieldnumber, 150, CV_FILLED, 8, hierarchy );
//    imshow("Contour",Contours);
//    waitKey(30);

	vector<vector<Point> > contours1;
	vector<Vec4i> hierarchy1;
	findContours(Contours, contours1, hierarchy1, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	centerFieldContour = contours1[0];

	west = Point2f(m_frameWidth,m_frameHeight);
	east = Point2f(0,0);
	north = Point2f(m_frameWidth,m_frameHeight);
	south = Point2f(0,0);

	if(m_isRightFieldSide == true) {
		for(uint32_t i = 0; i < centerFieldContour.size(); i++) {
			if(centerFieldContour[i].x < west.x || (centerFieldContour[i].x == west.x && centerFieldContour[i].y < west.y)) {west = centerFieldContour[i]; }
			if(centerFieldContour[i].x > east.x || (centerFieldContour[i].x == east.x && centerFieldContour[i].y > east.y)) {east = centerFieldContour[i]; }
			if(centerFieldContour[i].y < north.y || (centerFieldContour[i].y == north.y && centerFieldContour[i].x < north.x)) {north = centerFieldContour[i]; }
			if(centerFieldContour[i].y > south.y || (centerFieldContour[i].y == south.y && centerFieldContour[i].x > south.x)) {south = centerFieldContour[i]; }
		}
		north.x -= 9.0;//eig 5
		north.y += 0.0;//eig 5
		west.x -= 14.0;//eig 5
		west.y += 0.0;

	}
	if(m_isRightFieldSide == false) {
		for(uint32_t i = 0; i < centerFieldContour.size(); i++) {
			if(centerFieldContour[i].x < west.x || (centerFieldContour[i].x == west.x && centerFieldContour[i].y > west.y)) {west = centerFieldContour[i]; }
			if(centerFieldContour[i].x > east.x || (centerFieldContour[i].x == east.x && centerFieldContour[i].y < east.y)) {east = centerFieldContour[i]; }
			if(centerFieldContour[i].y < north.y || (centerFieldContour[i].y == north.y && centerFieldContour[i].x > north.x)) {north = centerFieldContour[i]; }
			if(centerFieldContour[i].y > south.y || (centerFieldContour[i].y == south.y && centerFieldContour[i].x < south.x)) {south = centerFieldContour[i]; }
		}
		north.x += 5.0;
		east.x += 11.0;
		east.y += 3.0;
	}

	Scalar color = Scalar(150);
	circle( detectedLinesGray, north, 2, color, -1, 8, 0 );
	circle( detectedLinesGray, south, 2, color, -1, 8, 0 );
	circle( detectedLinesGray, west, 2, color, -1, 8, 0 );
	circle( detectedLinesGray, east, 2, color, -1, 8, 0 );

	//imshow("BinaryInverted", detectedLinesGray);
	//waitKey(5000);

	m_fourCornersOfCenterField.push_back(west);
	m_fourCornersOfCenterField.push_back(north);
	m_fourCornersOfCenterField.push_back(east);
	m_fourCornersOfCenterField.push_back(south);

	cout<<"north: "<<north<<endl;
	cout<<"south: "<<south<<endl;
	cout<<"west: "<<west<<endl;
	cout<<"east: "<<east<<endl;


	color = Scalar( rand()&255, rand()&255, rand()&255 );
	drawContours( Contours, contours, mainFieldnumber, color, CV_FILLED, 8, hierarchy );
	for(int i = 0; i < m_fourCornersOfCenterField.size(); i++) {
		circle(m_originalImg, m_fourCornersOfCenterField[i], 3, color, -1,8,0);
	}

	if(m_isRightFieldSide == false) {
		cout<<"Left Field Size"<<endl;
		m_homographyMat = findHomography(m_fourCornersOfCenterField, m_leftCenterFieldPoints, 0);
	}
	else if(m_isRightFieldSide == true) {
		cout<<"Right Field Size"<<endl;
		m_homographyMat = findHomography(m_fourCornersOfCenterField, m_rightCenterFieldPoints, 0);
	}

	cout<<m_homographyMat<<endl;

		//std::vector<Point2f> obj_corners(2);
	   //obj_corners[0] = Point2f(1392.67, 349.987);
	   //obj_corners[1] = Point2f(759.229, 523.867);
	   //obj_corners[1] = Point2f(759.229, 523.867);
	   //std::vector<Point2f> scene_corners(2);

	   //perspectiveTransform( obj_corners, scene_corners, m_homographyMat);

	//color = Scalar(0,255,0);
	//circle( m_fieldModel, scene_corners[0], 7, color, -1, 8, 0 );
	//circle( m_fieldModel, scene_corners[1], 7, color, -1, 8, 0 );

	//cout<<scene_corners[0]<<endl;
	//Mat resizedFieldModel;
	//resize(m_fieldModel, resizedFieldModel, Size(), 0.5, 0.5, INTER_LINEAR);
	//imshow("resizedFieldModel", resizedFieldModel);
	//imshow("Contours", Contours);
	imshow("Original", m_originalImg);
	waitKey(30);
	if(m_isRightFieldSide == false) {
		waitKey(0);
	}
}
*/

void FieldDetection::calculateHomomorphyMatrix(){
	vector<Point2f> resizedFourCornersOfCenterField;
	resizedFourCornersOfCenterField.clear();
	for(int i=0; i<m_fourCornersOfCenterField.size(); i++){
		resizedFourCornersOfCenterField.push_back(m_fourCornersOfCenterField[i] * m_inputVideoResizingFactor);
	}

	if(m_useCalibrationType == 0 || m_useCalibrationType == 1 || m_useCalibrationType == 2){
		if(m_isRightFieldSide == false) {
			cout<<"Left Field Size"<<endl;
			m_homographyMat = findHomography(resizedFourCornersOfCenterField, m_leftCenterFieldPoints, 0);
		}
		else if(m_isRightFieldSide == true) {
			cout<<"Right Field Size"<<endl;
			m_homographyMat = findHomography(resizedFourCornersOfCenterField, m_rightCenterFieldPoints, 0);
		}
	}

	else if(m_useCalibrationType == 3){
		if(m_isRightFieldSide == false) {
			cout<<"Left Field Size"<<endl;
			m_homographyMat = findHomography(resizedFourCornersOfCenterField, m_CalibrationFieldPoints_Right, 0);
		}
		else if(m_isRightFieldSide == true) {
			cout<<"Right Field Size"<<endl;
			m_homographyMat = findHomography(resizedFourCornersOfCenterField, m_CalibrationFieldPoints_Left, 0);
		}
	}
	cout<<m_homographyMat<<endl;
	// cout<<"homography Testing"<<endl;
	// cout<<"Point2f(547, 394): "<<transformPointToModel(Point2f(547, 394))<<endl;
	// cout<<"Point2f(547, 380): "<<transformPointToModel(Point2f(547, 380))<<endl;
	// cout<<"Point2f(547, 370): "<<transformPointToModel(Point2f(547, 370))<<endl;
	// cout<<"Point2f(547, 360): "<<transformPointToModel(Point2f(547, 360))<<endl;
	// cout<<"Point2f(547, 350): "<<transformPointToModel(Point2f(547, 350))<<endl;
	// cout<<"Point2f(547, 340): "<<transformPointToModel(Point2f(547, 340))<<endl;
	// cout<<"Point2f(547, 337): "<<transformPointToModel(Point2f(547, 339.7))<<endl;
	// cout<<"Point2f(547, 337): "<<transformPointToModel(Point2f(547, 339.6))<<endl;
	// cout<<"Point2f(547, 335): "<<transformPointToModel(Point2f(547, 335))<<endl;
	// cout<<"Point2f(547, 333): "<<transformPointToModel(Point2f(547, 333))<<endl;
	// cout<<"Point2f(547, 330): "<<transformPointToModel(Point2f(547, 330))<<endl;
	// cout<<"Point2f(547, 100): "<<transformPointToModel(Point2f(547, 100))<<endl;
	// cout<<"Point2f(547, 0): "<<transformPointToModel(Point2f(547, 0))<<endl;

}

void FieldDetection::calculateBlendedBackgroundImg(vector<Mat>& imgBuffer){

	double factor = 1.0/double(5.0);
	Mat dst = imgBuffer[5]*factor;

	for(int i = 6; i < 10; i++) {
		dst += imgBuffer[i]*factor;
	}

	m_backgroundImg = dst.clone();

	//imshow("calibrationImg1", m_backgroundImg);
	//waitKey(30);
}

Point2f FieldDetection::transformPointToModel(Point2f pointToTransform){

	std::vector<Point2f> pointsToTransform(1);
	std::vector<Point2f> transformedPoints(1);

	pointsToTransform[0] = pointToTransform;

	perspectiveTransform( pointsToTransform, transformedPoints, m_homographyMat);

	return transformedPoints[0];
}

bool FieldDetection::isInsideServeZone(int serveZoneNumber, Point2f positionAtModel){
	bool isInsideZone = false;
	vector<Point2f> serveZone;
	switch(serveZoneNumber) {
	case 1: serveZone = m_serveZoneLeft1; break;
	case 2: serveZone = m_serveZoneLeft2; break;
	case 3: serveZone = m_serveZoneRight3; break;
	case 4: serveZone = m_serveZoneRight4; break;
	default: serveZone = m_serveZoneLeft1; cout<<"ERROR: ServeZone not available"<<endl; break;
	}

	if(serveZone[0].x - m_lineWidth < positionAtModel.x && serveZone[2].x + m_lineWidth > positionAtModel.x && serveZone[0].y - m_lineWidth < positionAtModel.y && serveZone[2].y + m_lineWidth > positionAtModel.y) {
		isInsideZone = true;
	}
	return isInsideZone;
}
