//
//  eventDetection.hpp
//
//
//  Created by Henri Kuper on 17.05.17.
//
//

#ifndef eventDetection_hpp
#define eventDetection_hpp

//#define GPU_ON

#include <iostream>
#include <vector>
#include <algorithm>
#include <stdio.h>
#include <mutex>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "fieldDetection.hpp"
#include "configData.hpp"
#include "curveDetection.hpp"
#include "playerDetection.hpp"
#include "playerSegmentation.hpp"
#include "event.hpp"
#include "statistics.hpp"
#include "../walterLib/network.hpp"

class EventDetection {
private:
//--------CPU-GPU shared Variables--------
//structs
struct ConfigData m_config;
//Objects
FieldDetection *m_fieldDetectionRight;
FieldDetection *m_fieldDetectionLeft;

CurveDetection *m_curveDetectionRight;
CurveDetection *m_curveDetectionLeft;

PlayerDetection *m_playerDetection;

PlayerSegmentation *m_playerSegmentationRight;
PlayerSegmentation *m_playerSegmentationLeft;

//float m_inputVideoResizingFactor;

int m_frameCount;
int m_frameWidth;
int m_frameHeight;
int m_videoFramesOffset;
int m_framesPrediction;
float m_lineWidth;

int m_alreadyEventCheckedCurveNumberRight;
int m_alreadyEventCheckedCurveNumberLeft;

int m_frameCountCurveOverNet;
int m_minServeFrameCountDiff;

int m_minimumServesForSafeChangeOfServes;
uint m_actualNumberOfServiceSides;
int m_frameCountChangeOfServe;
int m_numberOfServesFromSameEnd;
bool m_serveIsRight;
float m_minPercentageInsideServeZones;

float m_impactProbabilityModulation;
float m_impactAfterServeProbabilityModulation;

int m_actualCurveNumber;
int m_actualCurveFrameCount;
bool m_actualCurveIsRight;
int m_actualStrokeType;


bool m_serveDetected;
bool m_actualServePlayerRight;
bool m_rallyIsRunning;
bool m_firstServeImpactWasOutside;
int m_firstServeImpactFrameCount;
int m_insertPositionContinuationOfRally;
int m_actualServePos;
int m_actualServeTry;

enum m_eventTypes {STROKE, SERVE, IMPACT_AFTER_SERVE, OVER_NET, CRITICAL_IMPACT, END_OF_RALLY, END_OF_GAME, END_OF_SET, END_OF_MATCH, CONTINUATION_OF_RALLY};

enum m_curveCharacters {DEFAULT, IMPACT, STROKE_LEFT_PLAYER, STROKE_RIGHT_PLAYER, FROM_RIGHT_OVER_NET, FROM_LEFT_OVER_NET, RALLY_ENDING_RIGHT, RALLY_ENDING_LEFT, SERVE_LEFT_PLAYER, SERVE_RIGHT_PLAYER};
int m_actualCurveCharacter;
int m_actualStrokeNameFrameCount;
enum m_strokeTypes {DEFAULT_STROKE, FOREHAND_SPIN, FOREHAND_SLICE, BACKHAND_SPIN, BACKHAND_SLICE, SERVE_STROKE};

std::vector<int> m_servesPerSide;
std::vector<int> m_frameCountsChangeOfServes;
std::vector<int> m_insertPositionChangeOfServesEvent;

std::vector<int> m_frameCountsChangeOfSides;
std::mutex m_mutexChangeOfSides;

std::vector<StrokeEvent> m_strokeEvents;
std::vector<Event> m_events;

Statistics *m_statistics;
network *m_walter;

cv::TickMeter tm;
cv::TickMeter tm2;
std::vector<double> event_times;
std::vector<double> frame_times;

void eventDetectionInit();

void updateActualCurve();
void updateServeCounter(int servePos);


void appendImpactAfterServeEvent(bool isInsideServeImpactZone, int serveImpactZone, float probabilityEventHappend);
void appendStrokeEvent(float speed);
void appendOverNetEvent();
void appendImpactEvent(bool isInsideSingleField, float probabilityEventHappend);
void appendRallyEndingEvent();
void appendContinuationOfRallyEvent();


bool checkPlayerIsInsideCourt(bool curveIsRight, int numberOfCurve, bool checkLastPoints);

float calculateProbabilityOfImpact(const std::vector<cv::Point2f> &impactField, cv::Point2f impactPoint, float modulation);

public:
EventDetection();
EventDetection(struct ConfigData &config, FieldDetection *rightField, FieldDetection *leftField, CurveDetection *rightCurveDetection, CurveDetection *leftCurveDetection, PlayerDetection *playerDetection, int frameWidth, int frameHeight, int videoFramesOffset, int framesPrediction);
~EventDetection();

void detecEvents(const std::vector<cv::Mat> &bufferRight, const std::vector<cv::Mat> &bufferLeft);
void detecServeEvents();
void detecImpactAfterServeEvents();
void detecImpactEvents();
void detecRallyEvents();
void detecStrokeEvent();
void detecOverNetEvent();
void detecInNetEvent();
void detecChangeOfServiceEvents();
void detecChangeOfSideEvent();

void appendServeEvent(int frameCount, int servePos, float speed);
void appendEndOfGameEvent(int frameCount, int positionInEventVec, bool isChangeOfServe);
void appendEndOfMatchEvent(int frameCount);
void addChangeOfSideEvent(int frameCount);

void categoriseStroke(const std::vector<cv::Mat> &bufferRight, const std::vector<cv::Mat> &bufferLeft);

void printEvents();

int getCurveCharacter(int curveCharacterisationNumber);
std::string getEventName(int eventNumber);

//--get/set Methods
uint getEventsSize(){return m_events.size();}
int getEventTypeOfEvent(int number){return m_events[number].getEventType();}
const Event &getEvent(int number){return m_events[number];}


};

#endif /* eventDetection_hpp */
