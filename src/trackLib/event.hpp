//
// event.hpp
//
//
// Created by Henri Kuper on 03.10.2017
//

#ifndef event_hpp
#define event_hpp

#include <iostream>
#include <vector>
#include <algorithm>

class StrokeEvent {
private:
enum m_strokeTypes {DEFAULT_STROKE, FOREHAND_SPIN, FOREHAND_SLICE, BACKHAND_SPIN, BACKHAND_SLICE, SERVE_STROKE};
int m_strokeType;
int m_numberOfFrame;
bool m_isRight;

void strokeEventInit();
public:
StrokeEvent();
StrokeEvent(int strokeType, int frameCount, bool isRight);
~StrokeEvent();

int getStrokeType(){
	return m_strokeType;
}
int getNumberOfFrame(){
	return m_numberOfFrame;
}
bool getIsRight(){
	return m_isRight;
}
};

// class ServeEvent{
// private:
//      void serveEventInit();
// public:
//      ServeEvent();
//      ~ServeEvent();
// };

class Event {
private:
enum m_eventTypes {STROKE, SERVE, IMPACT_AFTER_SERVE, CRITICAL_IMPACT, END_OF_RALLY, END_OF_GAME};
enum m_strokeTypes {DEFAULT_STROKE, FOREHAND_SPIN, FOREHAND_SLICE, BACKHAND_SPIN, BACKHAND_SLICE, SERVE_STROKE};
int m_eventType;
int m_numberOfFrame;

float m_probabilityEventHappend;
bool m_isRight;

//Stroke
int m_strokeType;
float m_strokeSpeed;
//Serve
int m_serveZoneNumber;
//Impact after serve
int m_serveImpactZoneNumber;
bool m_isInsideServeImpactZone;
//Impact
bool m_isInsideSingleField;
//Change of service
bool m_isChangeOfServe;

void eventInit();
public:
Event();
Event(int eventType, int frameCount, bool isRight);
~Event();

int getEventType() const{
	return m_eventType;
}
int getNumberOfFrame() const {
	return m_numberOfFrame;
}
bool getIsRight() const {
	return m_isRight;
}

int getStrokeType() const {
	return m_strokeType;
}
float getStrokeSpeed() const {
	return m_strokeSpeed;
}

int getServeZoneNumber() const {
	return m_serveZoneNumber;
}
float getProbabilityEventHappend() const {
	return m_probabilityEventHappend;
}
bool getIsInsideServeImpactZone() const {
	return m_isInsideServeImpactZone;
}
bool getIsInsideSingleFieldZone() const {
	return m_isInsideSingleField;
}
bool getIsChangeOfServe() const {
	return m_isChangeOfServe;
}

void setStrokeType(int strokeType){
	m_strokeType = strokeType;
}
void setStrokeSpeed(float strokeSpeed){
	m_strokeSpeed = strokeSpeed;
}
void setServeZoneNumber(int serveZoneNumber){
	m_serveZoneNumber = serveZoneNumber;
}
void setIsInsideServeImpactZone(bool isInsideServeImpactZone){
	m_isInsideServeImpactZone = isInsideServeImpactZone;
}
void setServeImpactZone(int serveImpactZoneNumber){
	m_serveImpactZoneNumber = serveImpactZoneNumber;
}
void setIsInsideSingleField(bool isInsideSingleField){
	m_isInsideSingleField = isInsideSingleField;
}
void setIsChangeOfServe(bool isChangeOfServe){
	m_isChangeOfServe = isChangeOfServe;
}

void setProbabilityEventHappend(float probabilityEventHappend){
	m_probabilityEventHappend = probabilityEventHappend;
}
void setProbabilityEventHappendToSafe(){
	m_probabilityEventHappend = 0.99f;
}

};

#endif /* event_hpp */
