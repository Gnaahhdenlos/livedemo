//
//  curveDetection.hpp
//
//
//  Created by Henri Kuper on 23.06.17.
//
//

#ifndef curveDetection_hpp
#define curveDetection_hpp

#include <iostream>
#include <vector>
#include <numeric>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/opencv.hpp"

#include <stdio.h>

#include "fieldDetection.hpp"
#include "ballDetection.hpp"
#include "playerSegmentation.hpp"
#include "curve.hpp"

#include "../imgProcLib/imageProcessing.hpp"
#include "../imgProcLib/imageMath.hpp"

class CurveDetection {
private:

float m_inputVideoResizingFactor;

int m_frameHeight;                              //height of the original image
int m_frameWidth;                               //width of the original image

int m_framesPredictionCount;                  //number of frames to wait between processing and show the results
int m_frameCount;                             //number of actual processed frame

float m_resizingFactor;                       //factor of scaling down the input image

int m_maxFrameCountDiffForNewPoint;             //the maximal number of frames betweeen a new point and the last added point of theCurve
int m_minimumCurvePoints;                     //minimum count of points a flight Curve needs to be a Curve
int m_frameCountForErronousCurveDelete;         //if the Curve has less than minimumCurvePoints after this count, the Curve will be deleted

float m_minHeightOfServe;                     //minimum height, a serve Curve needs to flight
float m_yMaxWhichLeadsToImpact;               //maximum y value that an impact can have
float m_yDiffWhichLeadsToImpact;              //y difference value between the last point of Curve0 and the first point of following Curve, so that Curve0 leads to an impact

float m_ballRadiusFactor;                     //factor of the ballBoundRect.height, that is added to the impact point, to get the real impact height

float m_predictedBallCenterRadius;            //the radius in which a ball can be placed around the ballcenter prediction
float m_avarageDistMultMaxDistToPlayer;       //maximal distance to player multiplied by the avarage distance to player of the Curve
float m_avarageDistMaxToPlayerContour;        //maximal avarage distance to the player contour
int m_frameCountWhichLeadsToFieldChanging;      //count of frames between two following Curves that leads to a fieldchange of the first Curve
int m_frameCountWhichLeadsToEndCurve;           //count of frame between two following Curves that leads to an end of rally
float m_maxCurveDeleteIndex;                  //if a Curves index is greater than this variable id will not be deleted by the deleteLessFittingFlightCurve function

bool m_isRightFieldSide;                      //information if the CurveDetection object detects Curves of the right or the left field side

int m_alreadyCharacterisedCurve;							//Number of first curve that is not characterised all prev curves are already characterised.
int m_alreadyFinishedCurve;										//Number of first curve that is not finished yet. All prev curves are already finished.

cv::Mat m_fieldModel;                         //contains the field model with drawn lines
cv::Mat m_drawnCurves;                          //contains the last Curves drwan with different colors
std::vector<std::vector<cv::Point2f>> m_drawnBallCenters;							//contains all ballCenterPredictions of each Frame for drawing in output video
std::vector<std::vector<float>> m_drawnBallCentersRadius;							//contains all ballCenter radius of each Frame for drawing in output video

//std::vector<float> m_actualPointsDistToHumanMask;         //contains the distance of each point to the nearest Human mask of the actual image
//std::vector<cv::Point2f> m_actualPointsPlayerPosition;    //contains the position of theplayer which is near the ball of the actual image
//std::vector<cv::Rect> m_actualBallBoundRects;             //contains the bounding rectangles of each possible ball of the actual image

//std::vector<std::vector<cv::Moments> > m_humanMaskMomentsOfFrames;        //contains all humanMask moments listed in a vector per image

std::vector<cv::Point2f> m_transformedPointsOfImpact;     //contains all transformed points of impact

FieldDetection* m_fieldDetection;                                      //pointer to the corresponding FieldDetection object
PlayerSegmentation* m_playerSegmentation;                       //pointer to the corresponding PlayerSegmentation object
BallDetection* m_ballDetection;                           //pointer to the coresponding BallDetection object

//Tickmeter to calculate time needed by a function
cv::TickMeter tm;
cv::TickMeter m_tmUpdateBallData;
cv::TickMeter m_tmCalculatingFlightPath;
cv::TickMeter m_tmDeleteErrorCurves;
cv::TickMeter m_tmDeleteDoublicatedCurves;
cv::TickMeter m_tmCharacteriseCurves;
cv::TickMeter m_tmUpdateCurves;
cv::TickMeter m_tmDrawCurves;
//double to store the time of different functions
double m_calculatingFlightPathTime;
double m_deleteErrorCurvesTime;
double m_deleteDoublicatedCurvesTime;
double m_characterisCurvesTime;
double m_updateCurvesTime;
double m_drawCurvesTime;

/*
   Initialises the CurveDetection object. And sets all parameters and private variables.
 */
void curveDetectionInit();
/*
   Updates  vectors m_possibleBallCenterOfFrames, m_actualPointsDistToHumanMask,
   m_actualPointsPlayerPosition, m_actualBallBoundRects and m_humanMaskMomentsOfFrames
   from m_ballDetection and m_playerSegmentation objects.
 */
void updateBallDetectionData();
/*
	Checks all not finished curves if they are finished now and updates their status if they are.
	Afterwards it checks if new curves are finished an updates the m_alreadyFinishedCurve variable.
*/
void updateFinishedCurves();
/*
	Checks if a new curves are characterised and updates the m_alreadyCharacterisedCurve variable.
*/
void updateCharacterisedCurves();
/*
   Calculates the Derivative with repect to the image count
 */
float calculateDerivativeWithRespectToImageCount(float x0, float x1, int imgCount0, int imgCount1);

public:

std::vector<Curve> m_curves;        //contains a list of all Curve objects which are detected

CurveDetection();
CurveDetection(int, int, FieldDetection *, PlayerSegmentation *, BallDetection *);
~CurveDetection();

/*
   detecCurves is the main function of this class. It calls the detecBallWithDiff function
   and every other function to calculate the Curves and characterise them.
 */
void detecCurves();
/*
   Iterates through all detected balls and try to match them with existing Curves.
   It also creates a new Curve object for every detected ball.
 */
void calculatingFlightPath();
/*
   Proof if a ball fits to a prediction of the Curve and append the Points if it fits.
 */
// bool proofBallDetection(cv::Point2f, int, int);
bool proofBallDetection(cv::Point2f, int, int, cv::Point2f, bool);
/*
   Proof if a ball in human ROI fits to a prediction of the Curve and append the Points if it fits.
 */
//void proofBallDetectionROI(cv::Point2f, int, int);
/*
   Returns a prediction of the next Curve point.
 */
cv::Point2f makeBallCenterPrediction(int);
/*
   Calculates if a ball center is in a rectangle around the ball center prediction.
 */
bool detectedBallIsInPredictedBallCenterRadius(cv::Point2f, cv::Point2f, bool isInHumanMaskROI);
/*
	Calculates if a ball center is in a radius around the ball center prediction.
*/
bool detectedBallIsInPredictedBallCenterRadiusCircle(cv::Point2f, cv::Point2f, bool isInHumanMaskROI);
/*
   Characterises every detected Curve. Into 22 differen classes:
   -1:default
   0: false detected Curve
   1: Curve leads to another Curve with the same flight direction: rigth up and left down
   2: Curve leads to another Curve with the same flight direction: left up and right down
   3: Curve leads to another Curve with the same flight direction: right to left up
   4: Curve leads to another Curve with the same flight direction: left to right up
   5: Curve leads to another Curve with the same flight direction: right to left down
   6: Curve leads to another Curve with the same flight direction: left to right down

   7: Curve leads to impact: from right to left
   8: Curve leads to impact: from left to right

   9: Curve leads to stroke: from right up to right up
   10: Curve leads to stroke: from left up to left up
   11: Curve leads to stroke: from right down to right down
   12: Curve leads to stroke: from left down to left down

   13: Curve leads to stroke: from right down to right up
   14: Curve leads to stroke: from left down to left up
   15: Curve leads to stroke: from right up to right down
   16: Curve leads to stroke: from left up to left down

   17: Curve leads to way out of the right Image over the net
   18: Curve leads to way out of the left Image over the net

   19: Curve leads to right player and is stopped there or right out of image right, rally ending right
   20: Curve leads to left player and is stopped there or left out of image left, rally ending left

   21: Curve leads to serve from left to right
   22: Curve leads to serve from right to left
 */
void characteriseCurve();
/*
	Checks all curves, that start max 30 frames after the last frame of the curve.
	It stores the curveNumber of the best fittin Curve in numberOfBestFittingCurve.
*/
void detecBestFittingFollowingCurve(int curveNumber);
/*
	Checks all curves, that end max 30 frames after the first frame of the curve.
	It stores the curveNumber of the best fittin Curve in curve -> m_followingCurveIndex.
*/
void detecBestFittingPreviousCurve(int curveNumber);
/*
	Characterises a Single curve that does not have a following curve that fits the characteristics.
	It stores the curveNumber of the best fittin Curve in curve -> m_PreviousCurveIndex.
*/
void characteriseSingleCurve(int curveNumber);
/*
	Characterises a curve that does have a following curve that fits the characteristics.
*/
void characteriseCurveWithBestFittingCurve(int curveNumber, int curverNumerBestFittingCurve);
/*
   Search for dublicated flight Curves and deletes them.
 */
void deleteDublicatedFlightCurves();
/*
   If two Curves have overlapping frame counts, this function trys to merge them, if they have the
   same flight direction and related heights. If they are not mergeable the function returns false.
 */
bool mergeSimultanCurves(int, int);
/*
   Returns true if Curves have overlapping image counts.
 */
bool checkSimultaneity(int, int);
/*
   Deletes the less fitting of two Curves.
 */
void deleteLessFittingFlightCurve(int curveNumber0, int curveNumber1);
/*
   Deletes Curves that have less than m_minimumCurvePoints and no point is
   added for more than m_frameCountForErronousCurveDelete.
 */
void deleteErronousFlightCurves();
/*
   Deletes the Curve curveNumber and erase it from m_curves.
 */
void deleteFalseDetectedCurve(int curveNumber);
/*
   Returns the average distance of a curve to a player contour.
 */
float calcAvarageDistanceToHumanContour(int curveNumber);
/*
   Calculates the speed of a curve and stores it in the curve object.
 */
void calculateCurveSpeed(int curveNumber, int curveNumberBestFittingCurve);
/*
   Calculates the point of impact between the curves curveNumber0 and curveNumber1.
   The point of intersection is calculated with the their slopeIntercept values.
 */
void calculatingPointOfImpact(int curveNumber0, int curveNumber1);
/*
	Calculates the point where the stroke is hitten between curveNumber0 and curveNumber1.
*/
void calculatePointOfStroke(int curveNumber0, int curveNumber1, cv::Point2f &pointOfStroke, float &frameCountOfStroke);
/*
   Draws the last few curves to the m_drawnCurves Mat.
 */
void drawDetectedCurves();
/*
   Drwas the all curves which leads to impacts. Just for debugging.
 */
void drawImpactCurves();
/*
   Transforms the impact points and draws them to the m_drawnCurves Mat.
 */
void transformAndDrawImpactPoints(cv::Point2f);
/*
   Prints the detected curves with their mein data to the terminal. Just for debugging.
 */
void printCurves();
/*
	Calculates and Prints the avarage time and FPS of different functions.
*/
void printFPS();

//get functions
cv::Mat getDrawnCurves(){
	return m_drawnCurves;
}
cv::Mat getFieldModel(){
	return m_fieldModel;
}

const std::vector<Curve> &getCurves(){
	return m_curves;
}
const Curve &getCurve(int curveNumber){
	return m_curves[curveNumber];
}

BallDetection* getBallDetection(){
	return m_ballDetection;
}

};

#endif /* curveDetection_hpp */
