


#include "scoringNode.hpp"

using namespace std;

ScoringNode::ScoringNode(){
	scoringNodeInit();
}
ScoringNode::ScoringNode(const ScoringNode& other) :
	m_probabilityScoreIsCorrect(other.m_probabilityScoreIsCorrect), m_setsWonByPlayer1(other.m_setsWonByPlayer1), m_setsWonByPlayer2(other.m_setsWonByPlayer2), m_gamesWonLeft(other.m_gamesWonLeft), m_gamesWonRight(other.m_gamesWonRight), m_gamesWonByPlayer1(other.m_gamesWonByPlayer1), m_gamesWonByPlayer2(other.m_gamesWonByPlayer2), m_pointsWonLeft(other.m_pointsWonLeft), m_pointsWonRight(other.m_pointsWonRight), m_actualServePos(other.m_actualServePos), m_actualServeTry(other.m_actualServeTry), m_playerNumberRight(other.m_playerNumberRight), m_playerNumberWhoServes(other.m_playerNumberWhoServes), m_serveIsRight(other.m_serveIsRight){
	m_hasFollowingNodes = false;
	m_probabilityActualPointIsCorrect = 1.0f;
	m_isMarkedAsPossibleScoringPathNode = false;
	m_impactInServeImpactZone = false;
	m_actualServeTry = 1;
	m_hasDetectedDoubleFault = false;
	m_serveIsAce = false;
	m_servePosGameEnded = 0;

	m_strokeSpeed.push_back(vector<float>());
	m_strokeSpeed.push_back(vector<float>());

	for(int i = 0; i < 2; i++){
		m_forehandSpin.push_back(0);
		m_forehandSlice.push_back(0);
		m_backhandSpin.push_back(0);
		m_backhandSlice.push_back(0);
	}

	m_scoringNodeLeft = NULL;
	m_scoringNodeRight = NULL;
}

ScoringNode::ScoringNode(const ScoringNode& other, bool deepCopy) :
	m_probabilityScoreIsCorrect(other.m_probabilityScoreIsCorrect), m_probabilityActualPointIsCorrect(other.m_probabilityActualPointIsCorrect),
	m_gamesWonLeft(other.m_gamesWonLeft), m_gamesWonRight(other.m_gamesWonRight),
	m_pointsWonLeft(other.m_pointsWonLeft), m_pointsWonRight(other.m_pointsWonRight), m_actualServePos(other.m_actualServePos),
	m_actualServeTry(other.m_actualServeTry), m_hasFollowingNodes(other.m_hasFollowingNodes), m_pointWinForSide(other.m_pointWinForSide),
	m_impactInServeImpactZone(other.m_impactInServeImpactZone), m_isMarkedAsPossibleScoringPathNode(other.m_isMarkedAsPossibleScoringPathNode){
	if(deepCopy == true){
		if(other.getScoringNodeParent() == NULL){
			m_scoringNodeParent = NULL;
		}
		if(other.getScoringNodeLeft() != NULL){
			m_scoringNodeLeft = new ScoringNode(*other.getScoringNodeLeft(), true);
			m_scoringNodeLeft->setScoringNodeParent(*this);
		}
		if(other.getScoringNodeRight() != NULL){
			m_scoringNodeLeft = new ScoringNode(*other.getScoringNodeRight(), true);
			m_scoringNodeRight->setScoringNodeParent(*this);
		}
	}
	else{

		m_strokeSpeed.push_back(vector<float>());
		m_strokeSpeed.push_back(vector<float>());
		for(int i = 0; i < 2; i++){
			m_forehandSpin.push_back(0);
			m_forehandSlice.push_back(0);
			m_backhandSpin.push_back(0);
			m_backhandSlice.push_back(0);
		}
		m_scoringNodeLeft = NULL;
		m_scoringNodeRight = NULL;
		m_scoringNodeParent = NULL;
	}
}

ScoringNode::~ScoringNode(){
	if(m_scoringNodeParent != NULL){
		if(m_pointWinForSide == 1){
			m_scoringNodeParent->resetScoringNodeLeft();
		}
		else if(m_pointWinForSide == 2){
			m_scoringNodeParent->resetScoringNodeRight();
		}
	}
	delete m_scoringNodeLeft;
	delete m_scoringNodeRight;
	m_scoringNodeParent = NULL;
	m_scoringNodeLeft = NULL;
	m_scoringNodeRight = NULL;
}

void ScoringNode::mergeWith(const ScoringNode& other){

	m_probabilityScoreIsCorrect += other.getProbabilityScoreIsCorrect();
	if(other.getScoringNodeParent() != NULL){
		if(other.getPointWinForSide() == 1){
			other.getScoringNodeParent()->resetScoringNodeLeft();
		}
		else if(other.getPointWinForSide() == 2){
			other.getScoringNodeParent()->resetScoringNodeRight();
		}
	}
}

void ScoringNode::scoringNodeInit(){

	m_probabilityScoreIsCorrect = 1.0f;
	m_probabilityActualPointIsCorrect = 1.0f;

	m_gamesWonLeft = 0;
	m_gamesWonRight = 0;

	m_gamesWonByPlayer1 = 0;
	m_gamesWonByPlayer2 = 0;

	m_pointsWonLeft = 0;
	m_pointsWonRight = 0;

	m_setsWonByPlayer1 = 0;
	m_setsWonByPlayer2 = 0;

	m_actualServeTry = 1;
	m_pointWinForSide = 0;

	m_playerNumberRight = 2;
	m_serveIsRight = false;
	m_hasDetectedDoubleFault = false;
	m_serveIsAce = false;

	m_servePosGameEnded = 0;

	m_strokeSpeed.push_back(vector<float>());
	m_strokeSpeed.push_back(vector<float>());

	for(int i = 0; i < 2; i++){
		m_forehandSpin.push_back(0);
		m_forehandSlice.push_back(0);
		m_backhandSpin.push_back(0);
		m_backhandSlice.push_back(0);
	}

	m_hasFollowingNodes = false;
	m_impactInServeImpactZone = false;
	m_isMarkedAsPossibleScoringPathNode = false;
	m_scoringNodeParent = NULL;
	m_scoringNodeLeft = NULL;
	m_scoringNodeRight = NULL;
}

void ScoringNode::increaseServeTry(){
	m_actualServeTry++;
}

void ScoringNode::addScoringNodes(int newServePos, int startFrameCount, bool serveSideIsRight){
	m_scoringNodeLeft = new ScoringNode(*this);
	m_scoringNodeRight = new ScoringNode(*this);

	m_scoringNodeLeft->setScoringNodeParent(*this);
	m_scoringNodeRight->setScoringNodeParent(*this);

	m_scoringNodeLeft->setActualServePos(newServePos);
	m_scoringNodeRight->setActualServePos(newServePos);

	updateNewScores();

	m_scoringNodeLeft->setPointWinForSide(1);
	m_scoringNodeRight->setPointWinForSide(2);

	m_scoringNodeLeft->addRallyStartFrameCount(startFrameCount);
	m_scoringNodeRight->addRallyStartFrameCount(startFrameCount);

	if(m_pointsWonRight == 0 && m_pointsWonLeft == 0){
		updatePlayerSide(*m_scoringNodeLeft);
		updatePlayerSide(*m_scoringNodeRight);
	}
	if((m_scoringNodeLeft->getPlayerNumberRight() == 1 && serveSideIsRight == true) || (m_scoringNodeLeft->getPlayerNumberRight() == 2 && serveSideIsRight == false)){
		m_scoringNodeLeft->setPlayerNumberWhoServes(1);
		m_scoringNodeRight->setPlayerNumberWhoServes(1);
	}
	else if((m_scoringNodeLeft->getPlayerNumberRight() == 1 && serveSideIsRight == false) || (m_scoringNodeLeft->getPlayerNumberRight() == 2 && serveSideIsRight == true)){
		m_scoringNodeLeft->setPlayerNumberWhoServes(2);
		m_scoringNodeRight->setPlayerNumberWhoServes(2);
	}
	m_scoringNodeLeft->setServeIsRight(serveSideIsRight);
	m_scoringNodeRight->setServeIsRight(serveSideIsRight);
}

void ScoringNode::deleteChildNodes(){
	delete m_scoringNodeLeft;
	delete m_scoringNodeRight;
	m_scoringNodeLeft = NULL;
	m_scoringNodeRight = NULL;
}

void ScoringNode::addRallyStartFrameCount(int frameCount){
	if(m_rallyStopFrameCounts.size() < m_rallyStartFrameCounts.size()){
		m_rallyStopFrameCounts.push_back(frameCount-1);
	}
	m_rallyStartFrameCounts.push_back(frameCount);
}

void ScoringNode::addRallyStopFrameCount(int frameCount){
	m_rallyStopFrameCounts.push_back(frameCount);
}

void ScoringNode::resetLastRallyStopFrameCount(){
	if(m_rallyStopFrameCounts.empty() == false){
		m_rallyStopFrameCounts.pop_back();
	}
}

void ScoringNode::addServeSpeed(float speed){
	m_serveSpeed.push_back(speed);
}

void ScoringNode::addStrokeSpeed(int playerNumber, float speed){
	if(playerNumber >= 0 && playerNumber < 2){
		m_strokeSpeed[playerNumber].push_back(speed);
	}
	else{
		cout<<"Added Speed to non existing player: "<<playerNumber<<endl;
	}
}
void ScoringNode::addStroke(int playerNumber, int strokeType){
	if(playerNumber == 0){
		switch(strokeType){
			case FOREHAND_SPIN: m_forehandSpin[0]++;break;
			case FOREHAND_SLICE: m_forehandSlice[0]++;break;
			case BACKHAND_SPIN: m_backhandSpin[0]++;break;
			case BACKHAND_SLICE: m_backhandSlice[0]++;break;
		}
	}
	else if(playerNumber == 1){
		switch(strokeType){
			case FOREHAND_SPIN: m_forehandSpin[1]++;break;
			case FOREHAND_SLICE: m_forehandSlice[1]++;break;
			case BACKHAND_SPIN: m_backhandSpin[1]++;break;
			case BACKHAND_SLICE: m_backhandSlice[1]++;break;
		}
	}
	else{
		cout<<"Added Stroke to non existing player: "<<playerNumber<<endl;
	}
}

void ScoringNode::updateNewScores(){
	if(m_pointsWonLeft < 3 && m_pointsWonRight < 3){
		m_scoringNodeLeft->setPointsWonLeft(m_pointsWonLeft+1);
		m_scoringNodeRight->setPointsWonRight(m_pointsWonRight+1);
	}
	else if(m_pointsWonLeft < 3 && m_pointsWonRight == 3){
		m_scoringNodeLeft->setPointsWonLeft(m_pointsWonLeft+1);

		m_scoringNodeRight->setPointsWonRight(0);
		m_scoringNodeRight->setPointsWonLeft(0);
		m_scoringNodeRight->increaseGamesWonRight();
		updateServePosGameEnded(*m_scoringNodeRight);
		updateSets(*m_scoringNodeRight);
		m_scoringNodeRight->setActualServePos(2);
		m_scoringNodeRight->resetActualServeTry();
	}
	else if(m_pointsWonLeft == 3 && m_pointsWonRight < 3){
		m_scoringNodeLeft->setPointsWonLeft(0);
		m_scoringNodeLeft->setPointsWonRight(0);
		m_scoringNodeLeft->increaseGamesWonLeft();
		updateServePosGameEnded(*m_scoringNodeLeft);
		updateSets(*m_scoringNodeLeft);
		m_scoringNodeLeft->setActualServePos(2);
		m_scoringNodeLeft->resetActualServeTry();

		m_scoringNodeRight->setPointsWonRight(m_pointsWonRight+1);
	}
	else if(m_pointsWonLeft == 3 && m_pointsWonRight == 3){
		m_scoringNodeLeft->setPointsWonLeft(m_pointsWonLeft+1);
		m_scoringNodeRight->setPointsWonRight(m_pointsWonRight+1);
	}
	else if(m_pointsWonLeft == 4 && m_pointsWonRight == 3){
		m_scoringNodeLeft->setPointsWonLeft(0);
		m_scoringNodeLeft->setPointsWonRight(0);
		m_scoringNodeLeft->increaseGamesWonLeft();
		updateServePosGameEnded(*m_scoringNodeLeft);
		updateSets(*m_scoringNodeLeft);
		m_scoringNodeLeft->setActualServePos(2);
		m_scoringNodeLeft->resetActualServeTry();

		m_scoringNodeRight->setPointsWonLeft(m_pointsWonLeft - 1);
	}
	else if(m_pointsWonLeft == 3 && m_pointsWonRight == 4){
		m_scoringNodeLeft->setPointsWonRight(m_pointsWonRight - 1);

		m_scoringNodeRight->setPointsWonRight(0);
		m_scoringNodeRight->setPointsWonLeft(0);
		m_scoringNodeRight->increaseGamesWonRight();
		updateServePosGameEnded(*m_scoringNodeRight);
		updateSets(*m_scoringNodeRight);
		m_scoringNodeRight->setActualServePos(2);
		m_scoringNodeRight->resetActualServeTry();
	}
}

void ScoringNode::updateSets(ScoringNode &scoringNode){
	if(scoringNode.getGamesWonByPlayer1() == 6){
		resetGames(scoringNode);
		scoringNode.increaseSetsWonByPlayer(1);
	}
	else if(scoringNode.getGamesWonByPlayer2() == 6){
		resetGames(scoringNode);
		scoringNode.increaseSetsWonByPlayer(2);
	}
}

void ScoringNode::updateServePosGameEnded(ScoringNode& scoringNode){
	if(scoringNode.getActualServePos() == 1){
		scoringNode.setServePosGameEnded(2);
	}
	else{
		scoringNode.setServePosGameEnded(1);
	}
}

void ScoringNode::updatePlayerSide(ScoringNode& scoringNode){
	int gameCount = 0;
	if((scoringNode.getSetsWonByPlayer1() > 0 || scoringNode.getSetsWonByPlayer2() > 0) && scoringNode.getGamesWonByPlayer1() == 0 && scoringNode.getGamesWonByPlayer2() == 0){
		gameCount = scoringNode.getScoringNodeParent()->getGamesWonByPlayer1() + scoringNode.getScoringNodeParent()->getGamesWonByPlayer2() + 1;
	}
	else{
		gameCount = scoringNode.getGamesWonByPlayer1() + scoringNode.getGamesWonByPlayer2();
	}
	if(gameCount% 2 == 1){
		scoringNode.setPlayerNumberRight(3 - scoringNode.getPlayerNumberRight()); //player1 -> player2 && player2->player1
	}
}

void ScoringNode::updateProbabilityScoreIsCorrect(float newProbability){
	m_probabilityScoreIsCorrect = m_probabilityScoreIsCorrect / m_probabilityActualPointIsCorrect* newProbability;
}

void ScoringNode::increaseGamesWonLeft(){
	m_gamesWonLeft++;
	if(m_playerNumberRight == 1){
		m_gamesWonByPlayer2++;
	}
	else if(m_playerNumberRight == 2){
		m_gamesWonByPlayer1++;
	}
}

void ScoringNode::increaseGamesWonRight(){
	m_gamesWonRight++;
	if(m_playerNumberRight == 1){
		m_gamesWonByPlayer1++;
	}
	else if(m_playerNumberRight == 2){
		m_gamesWonByPlayer2++;
	}
}

void ScoringNode::increaseSetsWonByPlayer(int playerNumber){
	if(playerNumber == 1){
		m_setsWonByPlayer1++;
	}
	else if(playerNumber == 2){
		m_setsWonByPlayer2++;
	}
}

void ScoringNode::resetGames(ScoringNode &scoringNode){
	scoringNode.setGamesWonByPlayer1(0);
	scoringNode.setGamesWonByPlayer2(0);
	scoringNode.setPointsWonRight(0);
	scoringNode.setPointsWonLeft(0);
}
