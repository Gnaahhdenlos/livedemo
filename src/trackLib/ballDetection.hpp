//
//  ballDetection.hpp
//
//
//  Created by Henri Kuper on 30.03.17.
//
//

#ifndef ballDetection_hpp
#define ballDetection_hpp

//#define GPU_ON

#include <stdio.h>

#include <iostream>
#include <vector>
#include <numeric>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/opencv.hpp"

#ifdef GPU_ON

#include <opencv2/gpu/gpu.hpp>

#endif

#include "fieldDetection.hpp"
#include "playerSegmentation.hpp"

class BallDetection {

private:
//--------CPU-GPU shared Variables--------
float m_inputVideoResizingFactor;

int m_lowH;                             //lower bound of Hue value for colorthresholding
int m_highH;                            //upper bound of Hue value for colorthresholding

int m_lowS;                             //lower bound of Saturation value for colorthresholding
int m_highS;                            //upper bound of Saturation value for colorthresholding

int m_lowV;                             //lower bound of Value value for colorthresholding
int m_highV;                            //upper bound of Value value for colorthresholding

int m_erodeDilateSize;                  //size of the erode and dilate filter

int m_frameHeight;                      //height of the original input frames
int m_frameWidth;                       //width of the original input frames

int m_frameCount;                       //number of actual processed frame
int m_framesPredictionCount;            //number of frames to wait between processing and show the results

float m_differenceImgThreshold;         //value for thresholding the difference frames

float m_maxHeightOfContour;             //maximal height a recognized ball contour is allowed to have
float m_maxBallArea;                    //maximal area a recognized ball is allowed to have

float m_avarageDistMaxToContour;        //maximal avarage distance a curve is allowed to have to the human contour

bool m_isRightFieldSide;                //information if the BallDetection object detects balls of the right or the left field side

float m_resizing0;                      //factor of scaling down the input frame
float m_resizing1;                      //factor of scaling up the scaled down input frame

std::vector<std::vector<cv::Point> > m_actualPossibleBallContours;				//contains all detected ballContours listed in a vector.
std::vector<std::vector<cv::Point> > m_actualPossibleBallContoursROI;			//contains all detected ballContours inside human ROI listed in a vector.

std::vector<std::vector<cv::Point2f> > m_possibleBallCenterOfFrames;      //contains all possible detected ballcenters listed in a vector per frame
std::vector<cv::Point2f> m_actualBallCenters;														//contains all possible detected ballcenters listed in a vector
std::vector<float> m_actualPointsDistToHumanMask;                         //contains the distance of each point to the nearest Human mask of the actual frame
std::vector<cv::Point2f> m_actualPointsPlayerPosition;                    //contains the position of the player which is near the ball of the actual frame
std::vector<cv::Rect> m_actualBallBoundRects;                             //contains the bounding rectangles of each possible ball of the actual frame

std::vector<std::vector<cv::Point2f> > m_possibleBallCenterOfFramesROI;		//contains all possible detected ballcenters in human ROI listed in a vector per frame
std::vector<cv::Point2f> m_actualBallCentersROI;													//contains all possible detected ballcenters in human ROI listed in a vector
std::vector<float> m_actualPointsDistToHumanMaskROI;											//contains the distance of each point in human ROI to the nearest Human mask of the actual frame
std::vector<cv::Point2f> m_actualPointsPlayerPositionROI;									//contains the position of the player which is near the ball in human ROI of the actual frame
std::vector<cv::Rect> m_actualBallBoundRectsROI;													//contains the bounding rectangles of each possible ball in human ROI of the actual frame


std::vector<cv::Point2f> m_humanMaskPositionInModel;                      //contains the position of every human mask of the actual frame
std::vector<std::vector<cv::Point> > m_humanMaskContours;                 //contains every human Mask contour of the actual frame

FieldDetection *m_field;                                                      //pointer to the corresponding FieldDetection object
PlayerSegmentation * m_playerSegmentation;                                      //pointer to the corresponding PlayerSegmentation object

cv::TickMeter tm;                                                         //Tickmeter to calculate time needed by a function
std::vector<double> erode_times0;                                         //vector to store the time of different functions
std::vector<double> player_times;                                         //vector to store the time of different functions

//--------CPU-GPU shared private functions--------

void ballDetectionInit();

#ifndef GPU_ON
//--------CPU - Variables--------
cv::Mat m_HSVImg;                           //stores the BGR to HSV color converted mat of the actual frame

cv::Mat m_movementMask;                     //stores the movementMask of the actual frame

cv::Mat m_humanMaskROI;                     //stores all possible human mask rois to filter the movement of the player
cv::Mat m_humanMask;                        //stores all possible human mask contours
cv::Mat m_movementMaskHumanROI;							//stores all movements that appears under the humanMask ROI.

cv::Mat m_diffImage0;                       //stores the difference frame between frame 0 and frame 1
cv::Mat m_diffImage1;                       //stores the difference frame between frame 1 and frame 2


//std::vector<cv::Mat> grayFrameBuffer;   //stores the gray converted frames until they are processed
/*
   updates the vectors/Mats m_humanMaskROI, m_humanMaskPositionInModel, m_humanMask
   and m_humanMaskContours from the PlayerSegmentation object
 */
void updateHumanMask();

#else
//--------GPU - Variables--------
cv::Mat m_HSVImg;                           //stores the BGR to HSV color converted mat of the actual image

cv::Mat m_movementMask;                     //stores the movementMask of the actual image

cv::gpu::GpuMat m_humanMaskROIGPU;          //store all possible human mask rois to filter the movement of the player GPU

cv::gpu::GpuMat m_diffImage0GPU;            //stores the difference image between image 0 and image 1 GPU
cv::gpu::GpuMat m_diffImage1GPU;            //stores the difference image between image 1 and image 2 GPU
cv::gpu::GpuMat m_diffImage2GPU;            //stores the difference image between image 0 and image 2 GPU

std::vector<cv::gpu::GpuMat> m_grayFrameBufferGPU;    //stores the gray converted frames until they are processed
/*
   updates the vectors/GPUMats m_humanMaskROIGPU, m_humanMaskPositionInModel, m_humanMaskGPU
   and m_humanMaskContours from the PlayerSegmentation object
 */
void updateHumanMaskGPU();

#endif

public:
//--------CPU-GPU shared functions--------
/*
   Check if all detected ball positions are correct and allowed positions and stores them in m_possibleBallCenterOfFrames.
   Also calculates the position of each ball position to the nearest player, stores the bounding rect of the balls
   and the playerPostion of the nearest player to the ball.
 */
void checkPossibleBallPositions();

/*
   Check if all in HumanMask ROI detected ball positions are correct and stores them in m_possibleBallCenterOfFramesROI.
   Also calculates the position of each ball position to the nearest player, stores the bounding rect of the balls
   and the playerPostion of the nearest player to the ball.
 */
void checkPossibleBallPositionsROI();

void drawPossibleBallContours();
//--------constructor/destructor----------
BallDetection();
BallDetection(int, int, FieldDetection *, PlayerSegmentation *);
~BallDetection();

//--------get/set Methods-----------------

//m_actualBallCenters
const std::vector<cv::Point2f>& getActualBallCenters(){
	return m_actualBallCenters;
}
cv::Point2f getActualBallCenter(int index){
	if(index >= m_actualBallCenters.size()){ std::cout <<"getActualBallCenter "<< index <<" m_actualBallCenters.size() " << m_actualBallCenters.size() << std::endl;}
	return m_actualBallCenters[index];
}
uint getActualBallCentersSize(){
	return m_actualBallCenters.size();
}

//m_actualPointsDistToHumanMask
const std::vector<float>& getActualPointsDistToHumanMask(){
	return m_actualPointsDistToHumanMask;
}
float getActualPointDistToHumanMask(int index){
	if(index >= m_actualPointsDistToHumanMask.size()){ std::cout <<"getActualPointDistToHumanMask "<< index <<" m_actualPointsDistToHumanMask.size() " << m_actualPointsDistToHumanMask.size() << std::endl;}
	return m_actualPointsDistToHumanMask[index];
}
uint getActualPointsDistToHumanMaskSize(){
	return m_actualPointsDistToHumanMask.size();
}

//m_actualPointsPlayerPosition
const std::vector<cv::Point2f>& getActualPointsPlayerPosition(){
	return m_actualPointsPlayerPosition;
}
cv::Point2f getActualPointPlayerPosition(int index){
	if(index >= m_actualPointsPlayerPosition.size()){ std::cout <<"getActualPointPlayerPosition "<< index <<" m_actualPointsPlayerPosition.size() " << m_actualPointsPlayerPosition.size() << std::endl;}
	return m_actualPointsPlayerPosition[index];
}
uint getActualPointsPlayerPositionSize(){
	return m_actualPointsPlayerPosition.size();
}

//m_actualBallBoundRects
const std::vector<cv::Rect>& getActualBallBoundRects(){
	return m_actualBallBoundRects;
}
cv::Rect getActualBallBoundRect(int index){
	if(index >= m_actualBallBoundRects.size()){ std::cout <<"getActualBallBoundRect "<< index <<" m_actualBallBoundRects.size() " << m_actualBallBoundRects.size() << std::endl;}
	return m_actualBallBoundRects[index];
}
uint getActualBallBoundRectsSize(){
	return m_actualBallBoundRects.size();
}


//--------get/set Methods for ROI-----------------
const std::vector<std::vector<cv::Point2f> >& getPossibleBallCenterOfFramesROI(){
	return m_possibleBallCenterOfFramesROI;
}
const std::vector<cv::Point2f>& getActualBallCentersROI(){
	return m_actualBallCentersROI;
}
const cv::Point2f& getActualBallCenterROI(int index){
	if(index >= m_actualBallCentersROI.size()){ std::cout <<"getActualBallCenterROI "<< index <<" m_actualBallCentersROI.size() " << m_actualBallCentersROI.size() << std::endl;}
	return m_actualBallCentersROI[index];
}
uint getActualBallCentersROISize(){
	return m_actualBallCentersROI.size();
}

const std::vector<cv::Point2f>& getPossibleBallCenterOfFramesROI(int position){
	if(position >= m_possibleBallCenterOfFramesROI.size()){ std::cout <<"getPossibleBallCenterOfFramesROI "<< position <<" m_possibleBallCenterOfFramesROI.size() " << m_possibleBallCenterOfFramesROI.size() << std::endl;}
	return m_possibleBallCenterOfFramesROI[position];
}
const std::vector<cv::Point2f>& getActualPossibleBallCenterROI(){
	return m_possibleBallCenterOfFramesROI.back();
}
const std::vector<float>& getActualPointsDistToHumanMaskROI(){
	return m_actualPointsDistToHumanMaskROI;
}
float getActualPointsDistToHumanMaskROI(int position){
	if(position >= m_actualPointsDistToHumanMaskROI.size()){ std::cout <<"getActualPointsDistToHumanMaskROI "<< position <<" m_actualPointsDistToHumanMaskROI.size() " << m_actualPointsDistToHumanMaskROI.size() << std::endl;}
	return m_actualPointsDistToHumanMaskROI[position];
}

const std::vector<cv::Point2f>& getActualPointsPlayerPositionROI(){return m_actualPointsPlayerPositionROI;}
const cv::Point2f& getActualPointsPlayerPositionROI(int position){
	if(position >= m_actualPointsDistToHumanMaskROI.size()){ std::cout <<"getActualPointsPlayerPositionROI "<< position <<" m_actualPointsPlayerPositionROI.size() " << m_actualPointsPlayerPositionROI.size() << std::endl;}
	return m_actualPointsPlayerPositionROI[position];
}

const std::vector<cv::Rect>& getActualBallBoundRectsROI(){return m_actualBallBoundRectsROI; }
const cv::Rect& getActualBallBoundRectsROI(int position){
	if(position >= m_actualBallBoundRectsROI.size()){ std::cout <<"getActualBallBoundRectsROI "<< position <<" m_actualBallBoundRectsROI.size() " << m_actualBallBoundRectsROI.size() << std::endl;}
	return m_actualBallBoundRectsROI[position];
}

cv::Mat getMovementMask(){
	return m_movementMask;
}

void printPossibleBallCenters();

#ifndef GPU_ON
//--------CPU - functions----------------
/*
   Calculating image differences and calls the createMovementMask function.
   Detects the ball by finding contours of the movementMask.
 */
void detecBallWithDiff(const std::vector<cv::Mat> &originalFrameBuffer, const std::vector<cv::Mat> &grayFrameBuffer);
/*
   Creates a movementMask by thresholding the image different Mats, substraction of the player movement
   and noise reduction with morphological operations
 */
void createMovementMask();

#else
//--------GPU - functions----------------
/*
   Calculating image differences and calls the createMovementMaskGPU function.
   Detects the ball by finding contours of the movementMaskGPU.
 */
void detecBallWithDiffGPU(const std::vector<cv::Mat> &originalFrameBuffer);
/*
   Creates a movementMaskGPU by thresholding the image different MatsGPU, substraction of the player movement
   and noise reduction with morphological operations
 */
void createMovementMaskGPU();

#endif
};


#endif /* ballDetection_hpp */
