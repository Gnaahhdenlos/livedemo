//
//  ScoringNode.hpp
//
//
//  Created by Henri Kuper on 16.10.17.
//
//

#ifndef scoringNode_hpp
#define scoringNode_hpp

#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>

class ScoringNode {
private:

float m_probabilityScoreIsCorrect;
float m_probabilityActualPointIsCorrect;

int m_setsWonByPlayer1;
int m_setsWonByPlayer2;

int m_gamesWonLeft;
int m_gamesWonRight;

int m_gamesWonByPlayer1;
int m_gamesWonByPlayer2;

int m_pointsWonLeft;
int m_pointsWonRight;

int m_actualServePos;
int m_actualServeTry;
bool m_impactInServeImpactZone;
int m_servePosGameEnded;

std::vector<int> m_rallyStartFrameCounts;
std::vector<int> m_rallyStopFrameCounts;

std::vector<float> m_serveSpeed;
std::vector<std::vector<float> >m_strokeSpeed;

enum m_strokeTypes {DEFAULT_STROKE, FOREHAND_SPIN, FOREHAND_SLICE, BACKHAND_SPIN, BACKHAND_SLICE, SERVE_STROKE};

std::vector<int> m_forehandSpin;
std::vector<int> m_forehandSlice;
std::vector<int> m_backhandSpin;
std::vector<int> m_backhandSlice;


bool m_hasFollowingNodes;
int m_pointWinForSide;										//Shows which side the point of this scoring node made. 1 is the player who is actual on the left side. 2 is the player who is actual on the right side.
//Statistics
int m_playerNumberRight;
int m_playerNumberWhoServes;
bool m_serveIsRight;

bool m_hasDetectedDoubleFault;
bool m_serveIsAce;
int m_playerNumberCausedAnError;
int m_playerNumberCausedAWinner;

//Node information
bool m_isMarkedAsPossibleScoringPathNode;

ScoringNode *m_scoringNodeParent;
ScoringNode *m_scoringNodeLeft;
ScoringNode *m_scoringNodeRight;

void scoringNodeInit();

public:
ScoringNode();
ScoringNode(const ScoringNode& other);
ScoringNode(const ScoringNode& other, bool deepCopy);
~ScoringNode();

bool operator() (ScoringNode *node0, ScoringNode *node1) {
	return (node0->getProbabilityScoreIsCorrect() > node1->getProbabilityScoreIsCorrect());
}
bool operator<(const ScoringNode &other) const {
		return m_probabilityScoreIsCorrect < other.getProbabilityScoreIsCorrect();
}
bool operator>(const ScoringNode &other) const {
		return m_probabilityScoreIsCorrect > other.getProbabilityScoreIsCorrect();
}
friend std::ostream& operator<<(std::ostream& out, const ScoringNode& scoringNode){
	return out <<"GamesSide: "<<scoringNode.getGamesWonLeft()<<" : "<<scoringNode.getGamesWonRight()
	<<" SetsPerPlayer: "<<scoringNode.getSetsWonByPlayer1()<<" : "<<scoringNode.getSetsWonByPlayer2()
	<<" GamesPerPlayer: "<<scoringNode.getGamesWonByPlayer1()<<" : "<<scoringNode.getGamesWonByPlayer2()
	<<" Rallys: "<<scoringNode.getPointsWonLeft()<<" : "<<scoringNode.getPointsWonRight()
	<<" Prob: "<<scoringNode.getProbabilityScoreIsCorrect()
	<<" ServePos: "<<scoringNode.getActualServePos()
	<<" ServePosGameEnded: "<<scoringNode.getServePosGameEnded()
	<<" ServeIsRight: "<<scoringNode.getServeIsRight()
	<<" Marked: "<<scoringNode.getIsMarkedAsPossibleScorePathNode()
	<<" player right: "<<scoringNode.getPlayerNumberRight()
	<<" StartFrame: "<<scoringNode.getStartFrameCount()
	<<" StopFrame: "<<scoringNode.getStopFrameCount()
	<<" ServeTry: "<<scoringNode.getActualServeTry();
}


void addScoringNodes(int newServePos, int startFrameCount, bool serveSideIsRight);
void updateNewScores();
void updateSets(ScoringNode &scoringNode);
void updateServePosGameEnded(ScoringNode &scoringNode);
void updatePlayerSide(ScoringNode &scoringNode);
void updateProbabilityScoreIsCorrect(float newProbability);
void mergeWith(const ScoringNode& other);
void deleteChildNodes();

void increaseServeTry();
void addRallyStartFrameCount(int frameCount);
void addRallyStopFrameCount(int frameCount);
void resetLastRallyStopFrameCount();
void addServeSpeed(float speed);
void addStrokeSpeed(int playerNumber, float speed);
void addStroke(int playerNumber, int strokeType);

ScoringNode *getScoringNodeLeft() const {return m_scoringNodeLeft;}
ScoringNode *getScoringNodeRight() const {return m_scoringNodeRight;}
ScoringNode *getScoringNodeParent() const {return m_scoringNodeParent;}

int getPointsWonLeft() const {return m_pointsWonLeft;}
int getPointsWonRight() const {return m_pointsWonRight;}
int getGamesWonLeft() const {return m_gamesWonLeft;}
int getGamesWonRight() const {return m_gamesWonRight;}
int getGamesWonByPlayer1() const {return m_gamesWonByPlayer1;}
int getGamesWonByPlayer2() const {return m_gamesWonByPlayer2;}
int getSetsWonByPlayer1() const {return m_setsWonByPlayer1;}
int getSetsWonByPlayer2() const {return m_setsWonByPlayer2;}

int getActualServePos() const {return m_actualServePos;}
int getServePosGameEnded() const {return m_servePosGameEnded;}
int getPointWinForSide() const {return m_pointWinForSide;}
int getPlayerNumberRight() const {return m_playerNumberRight;}
int getPlayerNumberWhoServes() const {return m_playerNumberWhoServes;}
bool getServeIsRight() const {return m_serveIsRight;}
int getActualServeTry() const {return m_actualServeTry;}
bool getHasDetectedDoubleFault(){return m_hasDetectedDoubleFault;}
bool getServeIsAce(){return m_serveIsAce;}
int getPlayerNumberCausedAnError(){return m_playerNumberCausedAnError;}
int getPlayerNumberCausedAWinner(){return m_playerNumberCausedAWinner;}

const std::vector<float> &getServeSpeedVec(){return m_serveSpeed;}
float getServeSpeed(int index){return m_serveSpeed[index];}
const std::vector<float> &getStrokeSpeedVec(int playerNumber){if(playerNumber == 0){return m_strokeSpeed[playerNumber];}else if(playerNumber == 1){return m_strokeSpeed[playerNumber];}else{return m_strokeSpeed[1];}}
float getStrokeSpeed(int playerNumber, int index){return m_strokeSpeed[playerNumber][index];}

int getNumberOfForehandSpin(int playerNumber){return m_forehandSpin[playerNumber];}
int getNumberOfForehandSlice(int playerNumber){return m_forehandSlice[playerNumber];}
int getNumberOfBackhandSpin(int playerNumber){return m_backhandSpin[playerNumber];}
int getNumberOfBackhandSlice(int playerNumber){return m_backhandSlice[playerNumber];}

float getProbabilityScoreIsCorrect() const {return m_probabilityScoreIsCorrect;}
bool getIsMarkedAsPossibleScorePathNode() const {return m_isMarkedAsPossibleScoringPathNode;}
bool getImpactInServeImpactZone(){return m_impactInServeImpactZone;}

int getStartFrameCount() const {if(m_rallyStartFrameCounts.empty()){return -1;} else{return m_rallyStartFrameCounts.front();}}
int getStopFrameCount() const {if(m_rallyStopFrameCounts.empty()){return -1;}else{return m_rallyStopFrameCounts.back();}}

void setPointsWonLeft(int points){m_pointsWonLeft = points;}
void setPointsWonRight(int points){m_pointsWonRight = points;}
void setGamesWonLeft(int games){m_gamesWonLeft = games;}
void setGamesWonRight(int games){m_gamesWonRight = games;}
void setGamesWonByPlayer1(int games){m_gamesWonByPlayer1 = games;}
void setGamesWonByPlayer2(int games){m_gamesWonByPlayer2 = games;}

void increaseGamesWonLeft();
void increaseGamesWonRight();
void increaseSetsWonByPlayer(int playerNumber);

void setActualServePos(int servePos){m_actualServePos = servePos;}
void setServePosGameEnded(int servePos){m_servePosGameEnded = servePos;}
void setPointWinForSide(int side){m_pointWinForSide = side;}
void setPlayerNumberRight(int playerNumber){m_playerNumberRight = playerNumber;}
void setPlayerNumberWhoServes(int playerNumber){m_playerNumberWhoServes = playerNumber;}
void setServeIsRight(bool serveIsRight){m_serveIsRight = serveIsRight;}

void setHasDetectedDoubleFault(bool hasDetectedDoubleFault){m_hasDetectedDoubleFault = hasDetectedDoubleFault;}
void setServeIsAce(bool isAce){m_serveIsAce = isAce;}
void setPlayerNumberCausedAnError(int playerNumber){m_playerNumberCausedAnError = playerNumber;}
void setPlayerNumberCausedAWinner(int playerNumber){m_playerNumberCausedAWinner = playerNumber;}

void setScoringNodeParent(ScoringNode& other){m_scoringNodeParent = &other;}
void setScoringNodeLeft(ScoringNode& other){m_scoringNodeLeft = &other;}
void setScoringNodeRight(ScoringNode& other){m_scoringNodeRight = &other;}

void setIsMarkedAsPossibleScorePathNode(bool isMarked){m_isMarkedAsPossibleScoringPathNode = isMarked;}
void setImpactInServeImpactZone(bool inServeImpactZone){m_impactInServeImpactZone = inServeImpactZone;}

void resetGames(ScoringNode &scoringNode);
void resetActualServeTry(){m_actualServeTry = 1;}
void resetScoringNodeLeft(){m_scoringNodeLeft = NULL;}
void resetScoringNodeRight(){m_scoringNodeRight = NULL;}
void setProbabilityScoreIsCorrect(float probality){m_probabilityScoreIsCorrect = probality;}
void setProbabilityActualPointIsCorrect(float probality){
	updateProbabilityScoreIsCorrect(probality);
	m_probabilityActualPointIsCorrect = probality;
}
void resetProbabilityActualPointIsCorrect(float probality){
	m_probabilityScoreIsCorrect *= probality;
	m_probabilityActualPointIsCorrect = probality;
}

};

#endif
