//
//  configData.hpp
//
//
//  Created by Henri Kuper on 22.05.17.
//
//

#ifndef configData_hpp
#define configData_hpp

#include <stdio.h>
#include <string>

struct ConfigData {

	std::string m_filePathToRightVideo;
	std::string m_filePathToRightVideoCalibration;

	std::string m_filePathToLeftVideo;
	std::string m_filePathToLeftVideoCalibration;

	std::string m_safePathOutputVideo;
	std::string m_filePathToWeights;
	std::string m_filePathToCameraCalibrationMatrix;
	std::string m_filePathToFieldCalibrationPoints;
	//0 use preset calibration, 1 use own previous calibration, 2 try to recalibrate
	int m_useCalibrationType;
	bool m_withCamera;

	ConfigData(){
		m_useCalibrationType = 2;
		m_withCamera = false;
	};
	ConfigData(std::string filePathToRightVideo, std::string filePathToRightVideoCalibration, std::string filePathToLeftVideo, std::string filePathToLeftVideoCalibration, std::string filePathOutputVideo, std::string filePathToWeights, std::string filePathToCameraCalibrationMatrix, std::string filePathToFieldCalibrationPoints, int useCalibrationType) : m_filePathToRightVideo(filePathToRightVideo), m_filePathToRightVideoCalibration(filePathToRightVideoCalibration), m_filePathToLeftVideo(filePathToLeftVideo), m_filePathToLeftVideoCalibration(filePathToLeftVideoCalibration), m_safePathOutputVideo(filePathOutputVideo), m_filePathToWeights(filePathToWeights), m_filePathToCameraCalibrationMatrix(filePathToCameraCalibrationMatrix), m_filePathToFieldCalibrationPoints(filePathToFieldCalibrationPoints), m_useCalibrationType(useCalibrationType){
	};

	std::string getFilePathToVideoRight(){
		return m_filePathToRightVideo;
	}
	std::string getFilePathToCalibrationVideoRight(){
		return m_filePathToRightVideoCalibration;
	}
	std::string getFilePathToVideoLeft(){
		return m_filePathToLeftVideo;
	}
	std::string getFilePathToCalibrationVideoLeft(){
		return m_filePathToLeftVideoCalibration;
	}

	std::string getSafePathOutputVideo(){
		return m_safePathOutputVideo;
	}
	std::string getFilePathToWeights(){
		return m_filePathToWeights;
	}
	std::string getFilePathToCameraCalibrationMatrix(){
		return m_filePathToCameraCalibrationMatrix;
	}
	std::string getFilePathToFieldCalibrationPoints(){
		return m_filePathToFieldCalibrationPoints;
	}
	int getCalibrationType(){
		return m_useCalibrationType;
	}
	bool getWithCamera(){
		return m_withCamera;
	}

	void setFilePathToVideoRight(std::string filePathToVideoRight){m_filePathToRightVideo = filePathToVideoRight;}
	void setFilePathToVideoLeft(std::string filePathToVideoLeft){m_filePathToLeftVideo = filePathToVideoLeft;}
	void setFilePathToCalibrationVideoRight(std::string filePathToCalibrationVideoRight){ m_filePathToRightVideoCalibration = filePathToCalibrationVideoRight;}
	void setFilePathToCalibrationVideoLeft(std::string filePathToCalibrationVideoLeft){ m_filePathToLeftVideoCalibration = filePathToCalibrationVideoLeft;}
	void setSafePathOutputVideo(std::string safePathOutputVideo){m_safePathOutputVideo = safePathOutputVideo;}
	void setFilePathToWeights(std::string filePathToWeights){m_filePathToWeights = filePathToWeights;}
	void setFilePathToCameraCalibrationMatrix(std::string filePathToCameraCalibration){m_filePathToCameraCalibrationMatrix = filePathToCameraCalibration;}
	void setFilePathToFieldCalibrationPoints(std::string filePathToFieldCalibrationPoints){m_filePathToFieldCalibrationPoints = filePathToFieldCalibrationPoints;}
	void setCalibrationType(int calibrationType){m_useCalibrationType = calibrationType;}
	void setWithCamera(bool withCamera){m_withCamera = withCamera;}

};

#endif /* configData_hpp */
