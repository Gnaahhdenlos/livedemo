//
//  frameGrabber.hpp
//
//
//  Created by Henri Kuper on 5.09.17.
//
//

#ifndef frameGrabber_hpp
#define frameGrabber_hpp

#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <thread>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "../imgProcLib/imageProcessing.hpp"


class FrameGrabber {

private:
cv::VideoCapture m_rightVideoStream;
cv::VideoCapture m_leftVideoStream;

std::vector<cv::Mat> m_frameBufferRight;
std::vector<cv::Mat> m_frameBufferLeft;

std::vector<cv::Mat> m_frameBufferGrayRight;
std::vector<cv::Mat> m_frameBufferGrayLeft;

cv::Mat m_undistortionRemap1;
cv::Mat m_undistortionRemap2;

cv::Mat m_frameRight, m_frameLeft;
cv::Mat m_frameUndistortedRight, m_frameUndistortedLeft;
cv::Mat m_frameGrayRight, m_franeGrayLeft;

//float m_inputVideoResizingFactor;

int m_framesPrediction;

int m_frameBufferSizeRight;
int m_frameBufferSizeLeft;
int m_frameBufferGraySizeRight;
int m_frameBufferGraySizeLeft;

int32_t m_videoStreamStatus;

cv::TickMeter tm;
std::vector<double> frameTimes;

void frameGrabberInit();
void updateBufferSizes();
void grabFramesFromVideoStreamRight();
void grabFramesFromVideoStreamLeft();

public:
FrameGrabber(cv::VideoCapture m_rightVideoStream, cv::VideoCapture m_leftVideoStream, cv::Mat undistortionRemap1, cv::Mat undistortionRemap2, int framesPrediction);
//FrameGrabber(cv::VideoCapture rightVideoStream, cv::VideoCapture leftVideoStream, std::vector<cv::Mat>& frameBufferRight, std::vector<cv::Mat>& frameBufferLeft, std::vector<cv::Mat>& frameBufferGrayRight, std::vector<cv::Mat>& frameBufferGrayLeft, cv::Mat undistortionRemap1, cv::Mat undistortionRemap2, float scaleFactor);
~FrameGrabber();
/*
        Grabs new frame from video stream and pushs it to the frameBuffers.
 */
//void grabFramesFromVideoStream(std::vector<cv::Mat>& frameBufferRight, std::vector<cv::Mat>& frameBufferLeft, std::vector<cv::Mat>& frameBufferGrayRight, std::vector<cv::Mat>& frameBufferGrayLeft);
void grabFramesFromVideoStream();
void deleteFramesFromBuffer();

int32_t getVideoStreamStatus(){
	return m_videoStreamStatus;
}

std::vector<cv::Mat>& getBufferRight(){return m_frameBufferRight;}
std::vector<cv::Mat>& getBufferLeft(){return m_frameBufferLeft;}
std::vector<cv::Mat>& getBufferGrayRight(){return m_frameBufferGrayRight;}
std::vector<cv::Mat>& getBufferGrayLeft(){return m_frameBufferGrayLeft;}

int getFrameBufferSizeRight(){return m_frameBufferSizeRight;}
int getFrameBufferSizeLeft(){return m_frameBufferSizeLeft;}
int getFrameBufferGraySizeRights(){return m_frameBufferGraySizeRight;}
int getFrameBufferGraySizeLeft(){return m_frameBufferGraySizeLeft;}

cv::Mat& getFirstFrameBufferRight(){return m_frameBufferRight.front();}
cv::Mat& getFirstFrameBufferLeft(){return m_frameBufferLeft.front();}
cv::Mat& getFirstFrameBufferGrayRight(){return m_frameBufferGrayRight.front();}
cv::Mat& getFirstFrameBufferGrayLeft(){return m_frameBufferGrayLeft.front();}

};

#endif /* frameGrabber_hpp */
