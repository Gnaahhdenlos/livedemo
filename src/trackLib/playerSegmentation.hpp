//
//  playerSegmentation.hpp
//
//
//  Created by Henri Kuper on 06.09.17.
//
//

#ifndef playerSegmentation_hpp
#define playerSegmentation_hpp

//#define GPU_ON

#include <iostream>
#include <vector>
#include <numeric>

#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/opencv.hpp"
#include "opencv2/video/tracking.hpp"

#include "fieldDetection.hpp"
#include "player.hpp"

#ifdef GPU_ON
#include <opencv2/gpu/gpu.hpp>
#endif

class PlayerSegmentation{

private:

float m_inputVideoResizingFactor;

int m_frameWidth;                             //width of the original input frames
int m_frameHeight;                            //height of the original input frames

int m_erodeDilateSize;                        //size of the erode and dilate filter
int m_dilateSizeClosing;											//size of the dilate closing filter
int m_erodeSizeClosing;												//size of the erode closing filter
int m_dilateIterations;												//iterations of the dilate closing operation
int m_erodeIterations;												//iterations of the erode closing operation

float m_humanMaskPositionOffset;							//Offset of the humanMaskPosition which

float m_foregroundMaskThresh;                 //value for thresholding the difference between background image and actual frame
float m_humanMaskMinArea;                     //minimum area of a contour that is drwan to the m_humanMaskROI

float m_resizing0;                            //factor of scaling down the input image
float m_resizing1;                            //factor of scaling up the scaled down input image

float m_mog2UpdateRate;												//Factor how much the new image is updated
int m_mog2UpdateTime;													//Number of frames to wait for updating the background image

int m_backgroundUpdateRate;                   //Number of frames to wait for updating the background image
int m_backgroundImgUpdateCounter;             //Counter that stores the frame count

int m_frameCount;															//Counter that stores the frame count

cv::Scalar m_color;                           //Scalar that stores the color that is used for drawing the masks (255)

std::vector<std::vector<cv::Rect> > m_playerBoundRectsOfFrames;    //contains every player bound rect of every frame
std::vector<cv::Rect>  m_foregroundMaskBoundRect;                  //contains every foreground mask boundrect of the actual frame
std::vector<cv::Point2f> m_mainPlayerPositionInModel;                  //contains main playerposition in the model of the actual frame
std::vector<cv::Point2f> m_playerPositionInModel;                  //contains every playerposition in the model of the actual frame
std::vector<cv::Moments> m_humanMaskMoments;                       //contains every humanMask moment of the actual frame
std::vector<std::vector<cv::Point> > m_mainHumanMaskContours;      //contains main humanMask contours of the actual frame
std::vector<std::vector<cv::Point> > m_humanMaskContours;         //contains every humanMask contour of the actual frame
std::vector<std::vector<cv::Moments> > m_humanMaskMomentsOfFrames; //contains every humanMask moment of every frame

bool m_isRightFieldSide;

cv::Ptr<cv::BackgroundSubtractor> m_bgSubstractor;

FieldDetection* m_field;                      //pointer to the corresponding FieldDetection object

cv::TickMeter tm;                             //Tickmeter to calculate time needed by a function
cv::TickMeter tmErode;                        //Tickmeter to calculate time needed by a function
std::vector<double> player_times;             //vector to store the time of different functions
std::vector<double> player_timesErode;        //vector to store the time of different functions
#ifndef GPU_ON
  //--------CPU - Variables--------
  cv::Mat m_backgroundImg;                      //empty background image which is updated with m_backgroundUpdateRate
  cv::Mat m_foregroundMask;                     //mask that contains the information if a pixel is background or foreground
  cv::Mat m_humanMaskROI;                       //mask that contains the ROIs of player or big foreground objects
  cv::Mat m_updateBackgroundImgMask;            //mask of background image pixels that has to be updated

  /*
    Initialises the playerSegmentation object. It sets the variables and
    the background image.
  */
  void playerSegmentationInit();

  void addHumanMaskContourAndPosition(std::vector<cv::Point>& contour);
#else
  //--------GPU - Variables--------
  cv::gpu::GpuMat m_backgroundImgGPU;
  cv::gpu::GpuMat m_humanMaskGPU;
  cv::gpu::GpuMat m_humanMaskROIGPU;

  void playerSegmentationInitGPU();
#endif

public:
//--------CPU-GPU shared functions--------
//--------constructor/destructor----------
PlayerSegmentation(FieldDetection*, int frameWidth, int frameHeight);
~PlayerSegmentation();
/*
  calculates the bounding Square of a bounding Rect, but just if the rects width is
  smaller than the hight.
*/
cv::Rect rectToBoundSquare(cv::Rect);

//--------get/set Methods-----------------
const std::vector<std::vector<cv::Point> > &getMainHumanMaskContours(){return m_mainHumanMaskContours;}
const std::vector<std::vector<cv::Point> > &getHumanMaskContours(){return m_humanMaskContours;}
const std::vector<cv::Point2f> &getMainPlayerPositionInModel(){return m_mainPlayerPositionInModel;}
const std::vector<cv::Point2f> &getPlayerPositionInModel(){return m_playerPositionInModel;}

const std::vector<std::vector<cv::Moments> > &getHumanMaskMomentsOfFrames(){return m_humanMaskMomentsOfFrames;}
const std::vector<cv::Moments> &getHumanMaskMoments(int frameCount){return m_humanMaskMomentsOfFrames[frameCount];}
const std::vector<cv::Moments> &getLastHumanMaskMoments(){return m_humanMaskMomentsOfFrames.back();}

uint getHumanMaskMomentsOfFramesSize(){
  return m_humanMaskMomentsOfFrames.size();
}
uint getHumanMaskMomentsSize(int frameCount){
  return m_humanMaskMomentsOfFrames[frameCount].size();
}
cv::Moments getHumanMaskMoment(int frameCount, int index){
  return m_humanMaskMomentsOfFrames[frameCount][index];
}

const std::vector<std::vector<cv::Rect> > &getHumanMaskBoundRectsOfFrames(){return m_playerBoundRectsOfFrames;}
const std::vector<cv::Rect> &getHumanMaskBoundRects(){return m_playerBoundRectsOfFrames.back();}

bool getIsRightFieldSide(){return m_isRightFieldSide;}

#ifndef GPU_ON
  //--------CPU - functions----------------
  void initBackgroundModel(cv::Mat& backgroundFrame);
	/*
		Creates the foreground mask by thresholding the difference between actual frame
		and background image. The result is stored in m_foregroundMask. The bgImage is updated by the MOG2 bgSubtractor
	*/
	void createForegroundMaskWithMog2(cv::Mat& originalFrameGray);
	/*
    Creates the foreground mask by thresholding the difference between actual frame
    and background image.The result is stored in m_foregroundMask.
  */
  void createForegroundMask(cv::Mat& originalFrameGray);
  /*
    Decides which contours of the foreground mask are player and stores the information
    in diferent vectors. Also creates the m_humanMaskROI with all player ROIs and
    ROIs of huge contours.
  */
  void createHumanMask(cv::Mat& );
  /*

  */
  void mergeNearContoursOfPlayer(std::vector<std::vector<cv::Point> > contours, std::vector<std::vector<cv::Point> > &dstContours);
  /*
    Updates the backround image. If a foreground contour is static over
    m_backgroundUpdateRate frame and is not a player it will be background.
  */
  void updateBackgroundImg(cv::Mat& originalFrameGray);
  //--------get/set Methods-----------------
  cv::Mat getHumanMaskRoi(){return m_humanMaskROI;}
  cv::Mat getForegroundMask(){return m_foregroundMask;}

#else
  //--------GPU - functions----------------
  void createHumanMaskGPU(cv::gpu::GpuMat);
  //--------get/set Methods-----------------
  cv::gpu::GpuMat getHumanMaskRoiGPU(){return m_humanMaskROIGPU;}
  cv::gpu::GpuMat getHumanMaskGPU(){return m_humanMaskGPU;}

#endif
};

#endif /* playerSegmentation_hpp */
