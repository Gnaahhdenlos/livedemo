//
//  player.cpp
//
//
//  Created by Henri Kuper on 22.08.17.
//
//
#include "player.hpp"

using namespace std;
using namespace cv;


void Player::playerInit(){

	m_histogramSize = 12;
	m_detectionCounter = 1;
	m_detectionCounterRight = 0;
	m_detectionCounterLeft = 0;
	m_avarageVariationOfHistogramsRight = 0;
	m_avarageVariationOfHistogramsLeft = 0;

	m_isOutOfScope = false;
	m_isComparedToOtherHistograms = false;
	m_hasRightSideData = false;
	m_hasLeftSideData = false;

	m_xMin = 10000.0f;
	m_xMax = 0.0f;
	m_yMin = 10000.0f;
	m_yMax = 0.0f;
}

Player::Player(bool isRight) : m_isRight(isRight){
	playerInit();
}
Player::Player(bool isRight, int frameCount, cv::Point2f centerOfPlayer, cv::Point2f positionOfPlayer, cv::Mat &histogram) : m_isRight(isRight){
	playerInit();
	if(isRight==true) {
		m_histogramRight = histogram.clone();
		m_frameCountOfAppearingRight.push_back(frameCount);
		m_frameCountOfDisappearingRight.push_back(frameCount);
		m_detectionCounterRight++;
		m_hasRightSideData = true;
	}
	else{
		m_histogramLeft = histogram.clone();
		m_frameCountOfAppearingLeft.push_back(frameCount);
		m_frameCountOfDisappearingLeft.push_back(frameCount);
		m_detectionCounterLeft++;
		m_hasLeftSideData = true;
	}
	m_frameCounts.push_back(frameCount);
	m_centersOfPlayer.push_back(centerOfPlayer);
	m_positionsOfPlayerInModel.push_back(positionOfPlayer);
}

Player::~Player(){
}

void Player::updateHistogramm(Mat &histogram){
	float updateFactor = 0.995f;
	if(m_isRight == true) {
		cout<<"Rate: "<<((float)(m_detectionCounterRight-1) / m_detectionCounterRight)<<endl;
		if(((float)(m_detectionCounterRight-1) / m_detectionCounterRight) < 0.995) {
			m_histogramRight = m_histogramRight * ((float)(m_detectionCounterRight-1) / m_detectionCounterRight) + histogram * (1.0f / m_detectionCounterRight);
		}
		else{
			m_histogramRight = m_histogramRight * updateFactor + histogram *(1.0-updateFactor);
		}
		//normalize(m_histogramRight, m_histogramRight, 1, 0, NORM_L1, -1, Mat() );
	}
	else{
		if(((float)(m_detectionCounterLeft-1) / m_detectionCounterLeft) < 0.995) {
			m_histogramLeft = m_histogramLeft * ((float)(m_detectionCounterLeft-1) / m_detectionCounterLeft) + histogram * (1.0f / m_detectionCounterLeft);
		}
		else{
			m_histogramLeft = m_histogramLeft * updateFactor + histogram *(1.0-updateFactor);
		}
		//normalize(m_histogramLeft, m_histogramLeft, 1, 0, NORM_L1, -1, Mat() );
	}
}

void Player::updateAvarageVariatonOfHistograms(cv::Mat &histogram){
	if(m_isRight == true) {
		//float compareDist = compareHist(m_histogramRight, histogram, CV_COMP_BHATTACHARYYA);
		float compareDist;
		if(m_histogramRight.rows == 0) {
			cout<<"m_histogram is Empty, something went wrong"<<endl;
		}

		imgProc::imageMath::compareHistogram(m_histogramRight, histogram, compareDist);
		cout<<"compareDistRight: "<<compareDist<<endl;
		if(m_detectionCounterRight <= 2) {
			m_avarageVariationOfHistogramsRight = compareDist;
		}
		else{
			m_avarageVariationOfHistogramsRight = m_avarageVariationOfHistogramsRight * ((float)(m_detectionCounterRight-2) / (m_detectionCounterRight-1)) + compareDist * (1.0f / (m_detectionCounterRight-1));
		}
	}
	else{
		//float compareDist = compareHist(m_histogramLeft, histogram, CV_COMP_BHATTACHARYYA);
		float compareDist;
		if(m_histogramLeft.rows == 0) {
			cout<<"m_histogram is Empty, something went wrong"<<endl;
		}
		imgProc::imageMath::compareHistogram(m_histogramLeft, histogram, compareDist);
		cout<<"compareDistLeft: "<<compareDist<<endl;
		if(m_detectionCounterLeft <= 2) {
			m_avarageVariationOfHistogramsLeft = compareDist;
		}
		else{
			m_avarageVariationOfHistogramsLeft = m_avarageVariationOfHistogramsLeft * ((float)(m_detectionCounterLeft-2) / (m_detectionCounterLeft-1)) + compareDist * (1.0f / (m_detectionCounterLeft-1));
		}
	}
	if(histogram.rows == 0) {
		cout<<"update AvarageVariation histogram is Empty, something went wrong"<<endl;
	}
}

void Player::appendPlayerData(int frameCount, cv::Point2f centerOfPlayer, cv::Point2f positionOfPlayerInModel, cv::Mat &histogram){
	m_frameCounts.push_back(frameCount);
	m_centersOfPlayer.push_back(centerOfPlayer);
	m_positionsOfPlayerInModel.push_back(positionOfPlayerInModel);
	m_detectionCounter++;
	updateHistogramm(histogram);
	updateAvarageVariatonOfHistograms(histogram);

	if(centerOfPlayer.x < m_xMin) {
		m_xMin = centerOfPlayer.x;
	}
	if(centerOfPlayer.x > m_xMax) {
		m_xMax = centerOfPlayer.x;
	}
	if(centerOfPlayer.y < m_yMin) {
		m_yMin = centerOfPlayer.y;
	}
	if(centerOfPlayer.y > m_yMax) {
		m_yMax = centerOfPlayer.y;
	}

	if(m_isRight == true) {
		m_frameCountOfDisappearingRight.back() = frameCount;
		m_detectionCounterRight++;
	}
	else{
		m_frameCountOfDisappearingLeft.back() = frameCount;
		m_detectionCounterLeft++;
	}
}

void Player::mergeWithOtherPlayer(Player &otherPlayer){

	//TODO mergen des histograms integrieren

	int detectionCounterSumRight = m_detectionCounterRight + otherPlayer.getDetectionCounterRight();
	float detectionCounterRatioOldRight = (float)m_detectionCounterRight / detectionCounterSumRight;
	float detectionCounterRatioNewRight = (float)otherPlayer.getDetectionCounterRight() / detectionCounterSumRight;

	int detectionCounterSumLeft = m_detectionCounterLeft + otherPlayer.getDetectionCounterLeft();
	float detectionCounterRatioOldLeft = (float)m_detectionCounter / detectionCounterSumLeft;
	float detectionCounterRatioNewLeft = (float)otherPlayer.getDetectionCounterLeft() / detectionCounterSumLeft;

	if(m_hasRightSideData == true && otherPlayer.getHasRightSideData() == true) {
		m_avarageVariationOfHistogramsRight = detectionCounterRatioOldRight * m_avarageVariationOfHistogramsRight + detectionCounterRatioNewRight * otherPlayer.getAvarageVariationOfHistogramsRight();
		m_histogramRight = m_histogramRight * detectionCounterRatioOldRight + otherPlayer.getHistogramRight() * detectionCounterRatioNewRight;
	}
	else if(otherPlayer.getHasRightSideData() == true) {
		m_avarageVariationOfHistogramsRight = otherPlayer.getAvarageVariationOfHistogramsRight();
		m_histogramRight = otherPlayer.getHistogramRight().clone();
		m_hasRightSideData = true;
		cout<<"Merged Player added Right Histogram"<<endl;
		cout<<"HistogramRight Size: "<<m_histogramRight.rows<<endl;
	}
	if(m_hasLeftSideData == true && otherPlayer.getHasLeftSideData() == true) {
		m_avarageVariationOfHistogramsLeft = detectionCounterRatioOldLeft * m_avarageVariationOfHistogramsLeft + detectionCounterRatioNewLeft * otherPlayer.getAvarageVariationOfHistogramsLeft();
		m_histogramLeft = m_histogramLeft * detectionCounterRatioOldLeft + otherPlayer.getHistogramLeft() * detectionCounterRatioNewLeft;
	}
	else if(otherPlayer.getHasLeftSideData() == true) {
		m_avarageVariationOfHistogramsLeft = otherPlayer.getAvarageVariationOfHistogramsLeft();
		m_histogramLeft = otherPlayer.getHistogramLeft().clone();
		m_hasLeftSideData = true;
		cout<<"Merged Player added Left Histogram"<<endl;
		cout<<"HistogramLeft Size: "<<m_histogramLeft.rows<<endl;
	}

	// m_xMin = m_xMin > otherPlayer.getXMin() ? otherPlayer.getXMin() : m_xMin;
	// m_xMax = m_xMax < otherPlayer.getXMax() ? otherPlayer.getXMax() : m_xMax;
	// m_yMin = m_yMin > otherPlayer.getYMin() ? otherPlayer.getYMin() : m_yMin;
	// m_yMax = m_yMax < otherPlayer.getYMax() ? otherPlayer.getYMax() : m_yMax;

	m_xMin = otherPlayer.getXMin();
	m_xMax = otherPlayer.getXMax();
	m_yMin = otherPlayer.getYMin();
	m_yMax = otherPlayer.getYMax();

	m_frameCounts.insert(m_frameCounts.end(), otherPlayer.getFrameCounts().begin(), otherPlayer.getFrameCounts().end());
	m_centersOfPlayer.insert(std::end(m_centersOfPlayer), std::begin(otherPlayer.getCentersOfPlayer()), std::end(otherPlayer.getCentersOfPlayer()));
	m_positionsOfPlayerInModel.insert(std::end(m_positionsOfPlayerInModel), std::begin(otherPlayer.getPositionsOfPlayerInModel()), std::end(otherPlayer.getPositionsOfPlayerInModel()));

	m_frameCountOfDisappearingRight.insert(m_frameCountOfDisappearingRight.end(), otherPlayer.getFrameCountOfDisappearingRight().begin(), otherPlayer.getFrameCountOfDisappearingRight().end());
	m_frameCountOfAppearingRight.insert(m_frameCountOfAppearingRight.end(), otherPlayer.getFrameCountOfAppearingRight().begin(), otherPlayer.getFrameCountOfAppearingRight().end());

	m_frameCountOfDisappearingLeft.insert(m_frameCountOfDisappearingLeft.end(), otherPlayer.getFrameCountOfDisappearingLeft().begin(), otherPlayer.getFrameCountOfDisappearingLeft().end());
	m_frameCountOfAppearingLeft.insert(m_frameCountOfAppearingLeft.end(), otherPlayer.getFrameCountOfAppearingLeft().begin(), otherPlayer.getFrameCountOfAppearingLeft().end());

	m_isRight = otherPlayer.getIsRight();
	if(m_hasRightSideData == true || otherPlayer.getHasRightSideData() == true) {
		m_hasRightSideData = true;
	}
	if(m_hasLeftSideData == true || otherPlayer.getHasLeftSideData() == true) {
		m_hasLeftSideData = true;
	}

	m_detectionCounter += otherPlayer.getDetectionCounter();
}
