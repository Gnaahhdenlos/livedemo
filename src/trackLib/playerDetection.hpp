//
//  playerDetection.hpp
//
//
//  Created by Henri Kuper on 06.09.17.
//
//

#ifndef playerDetection_hpp
#define playerDetection_hpp

//#define GPU_ON

#include <iostream>
#include <vector>
#include <numeric>
#include <thread>

#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/opencv.hpp"

#include "fieldDetection.hpp"
#include "playerSegmentation.hpp"
#include "player.hpp"

#include "../imgProcLib/imageMath.hpp"

class PlayerDetection {

private:

PlayerSegmentation* m_playerSegmentationRight;
PlayerSegmentation* m_playerSegmentationLeft;

FieldDetection* m_fieldDetectionRight;
FieldDetection* m_fieldDetectionLeft;

std::vector<Player> m_players;

float m_inputVideoResizingFactor;

int m_frameWidth;
int m_frameHeight;

int m_numberOfLeftPlayer;
int m_numberOfRightPlayer;

int m_frameCount;
int m_histSize;
int m_frameCountDiff;
int m_numberOfDataNeeded;
int m_numberOfDataNeededForHistComparison;

float m_minVerticalMovement;
float m_minHorizontalMovement;
float m_maxDistanceToPreviusDetection;
float m_maxXDistanceToPreviusDetection;
float m_maxYDistanceToPreviusDetection;

bool m_isInitialMergeDone;

//Tickmeter to calculate time needed by a function
cv::TickMeter m_tmCreateForegroundMask;
cv::TickMeter m_tmTrackPlayer;
cv::TickMeter m_tmDetecFalseDetectedPlayer;
cv::TickMeter m_tmMergeCorrespondingPlayer;
//vector to store the time of different functions
std::vector<double> m_foreGroundMaskTimes;
std::vector<double> m_trackPlayerTimes;
std::vector<double> m_deleteFalseDetectedPlayerTimes;
std::vector<double> m_mergeCorrespondingPlayer;

//--------CPU - Variables--------

/*
   Initialises the playerDetection object. It sets the variables and
   the background image.
 */
void playerDetectionInit();

public:
//--------CPU-GPU shared functions--------
//--------constructor/destructor----------
PlayerDetection(FieldDetection*, FieldDetection*, int frameWidth, int frameHeight);
~PlayerDetection();

void initBackgroundModel(cv::Mat& frameRight, cv::Mat& frameLeft);
void detecPlayer(std::vector<cv::Mat>& frameBufferGrayRight, std::vector<cv::Mat>& frameBufferGrayLeft);
void trackPlayer(PlayerSegmentation *playerSeg, cv::Mat &frameGray);
void searchForPlayerPositionBevoreFrameCount(int frameCount, bool isRight, std::vector<cv::Point2f> &dstPosition);
void createHistogramOfContour(cv::Mat &grayFrame, const std::vector<std::vector<cv::Point> > &playerContours, int contourNumber, cv::Mat &histogram, const std::vector<cv::Rect> &playerRects, const std::vector<cv::Moments> &playerMoments);
void deleteFalseDetectedPlayer();
void mergeCorrespondingPlayer();
void mergePlayer(int playerNumber0, int playerNumber1);
void mergeDifferentSidePlayer();
void deletePlayer(int position);

void showHistogram(int playerNumber);
void showTracking(cv::Mat &frameGray);

void printPlayer();
void printFPS();

PlayerSegmentation* getPlayerSegmentationRight(){
	return m_playerSegmentationRight;
}
PlayerSegmentation* getPlayerSegmentationLeft(){
	return m_playerSegmentationLeft;
}

Player getPlayer(int playerNumber){return m_players[playerNumber];}


};

#endif /* playerDetection_hpp */
