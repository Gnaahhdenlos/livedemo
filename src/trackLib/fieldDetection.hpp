//
//  fieldDetection.hpp
//
//
//  Created by Henri Kuper on 30.03.17.
//
//

#ifndef fieldDetection_hpp
#define fieldDetection_hpp

#include <iostream>
#include <string>
#include <vector>

#include "configData.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "../imgProcLib/imageProcessing.hpp"


#include <stdio.h>

//using namespace std;

class FieldDetection {

private:

struct ConfigData m_config;

float m_inputVideoResizingFactor;

int m_lowBinaryThreshold;             //lower threshold to calculate calculate the binary image
int m_highBinaryThreshold;            //upper threshold to calculate calculate the binary image

int m_minCannyThresh;                 //lower threshold for canny edge detection
int m_maxCannyThresh;                 //upper threshold for canny edge detection

int m_frameHeight;                    //height of the original input frames
int m_frameWidth;                     //width of the original input frames

int m_modelHeight;                    //height of the field model
int m_modelWidth;                     //width of the field model
int m_modelOffset;										//offset in cm aroung the field/court.

int m_calibrationCount;               //counter to track the number of frames used for the calibration

bool m_isRightFieldSide;              //information if the curveDetection object detects curves of the right or the left field side
int m_useCalibrationType;             //information which type of calibration is used, standard stored calibration, new stored calibration or new calibration

float m_lineWidth;                    // Witdh of the field lines.

std::vector<cv::Point2f> m_fieldPoints;               //contains every corner point of the tennis field model.

std::vector<cv::Point2f> m_singleFieldPoints;         //contains the 4 main corners which border the singlefield in the model.

std::vector<cv::Point2f> m_leftSingelFieldPoints;     //contains the 4 main corners which border the left singlefield in the model.
std::vector<cv::Point2f> m_rightSingelFieldPoints;    //contains the 4 main corners which border the right singlefield in the model.

std::vector<cv::Point2f> m_leftCenterFieldPoints;     //contains the 4 corners which border the left Center Field in the model.
std::vector<cv::Point2f> m_rightCenterFieldPoints;    //contains the 4 corners which border the right Center Field in the model.

std::vector<cv::Point2f> m_courtBoundary;							//contains 4 points that limit the court. So every action taking place behind these limits are illegal.

std::vector<cv::Point2f> m_fourCornersOfCenterField;  //contains the detected corners of the center field.

std::vector<cv::Point2f> m_serveZoneLeft1;            //contains 4 corners which border a left service box advantage.
std::vector<cv::Point2f> m_serveZoneLeft2;            //contains 4 corners which border a right service box advantage.
std::vector<cv::Point2f> m_serveZoneRight3;           //contains 4 corners which border a left service box deuce.
std::vector<cv::Point2f> m_serveZoneRight4;           //contains 4 corners which border a right service box deuce.

std::vector<cv::Point2f> m_returnZoneLeft1;           //contains the 4 corners which boder a left serve zone, where the returning player stands.
std::vector<cv::Point2f> m_returnZoneLeft2;           //contains the 4 corners which boder a left serve zone, where the returning player stands.
std::vector<cv::Point2f> m_returnZoneRight3;          //contains the 4 corners which boder a right serve zone, where the returning player stands.
std::vector<cv::Point2f> m_returnZoneRight4;          //contains the 4 corners which boder a right serve zone, where the returning player stands.

std::vector<cv::Point2f> m_serveImpactZoneLeft1;      //contains the 4 corners which border a left serve impact zone.
std::vector<cv::Point2f> m_serveImpactZoneLeft2;      //contains the 4 corners which border a left serve impact zone.
std::vector<cv::Point2f> m_serveImpactZoneRight3;     //contains the 4 corners which border a right serve impact zone.
std::vector<cv::Point2f> m_serveImpactZoneRight4;     //contains the 4 corners which border a right serve impact zone.

std::vector<cv::Point2f> m_CalibrationFieldPoints_Left;    	//contains the 4 corners which border the left Calibration Field.
std::vector<cv::Point2f> m_CalibrationFieldPoints_Right;    //contains the 4 corners which border the right Calibration Field.

std::vector<cv::Point2f> m_CalibrationCenterFieldPoints_Left;    	//contains the 4 corners which border the left Calibration Field.
std::vector<cv::Point2f> m_CalibrationCenterFieldPoints_Right;    //contains the 4 corners which border the right Calibration Field.


bool m_calibrationIsRunning;                          //is true if calibration is running

cv::Mat m_imgLines;                                   //contains the field lines of the actual frame.
cv::Mat m_calibratedImgLines;                         //contains the field lines of all frames added together.
cv::Mat m_fieldModel;                                 //contains the fieldmodel with all the lines.
cv::Mat m_fieldModelSmall;                            //contains the fieldmodel with all the lines rescaled with fator 0.2.
cv::Mat m_backgroundImg;                             //contains the background image.
cv::Mat m_originalImg;                                //coontains the actual original image.

cv::Mat m_cameraMatrix;                               //contains the camera matrix parameters.
cv::Mat m_distortionCoefficients;                     //contains the coefficient to undistort the camera frames.
cv::Mat m_homographyMat;                              //contains the calculated homography matrix to transform points from a frame to the model.

/*
   Initialises the field lines object. Sets variabes like thresholds.
 */
void fieldDetectionInit();
/*
   Sets all vectors that contain field points and the field model mat.
 */
void initFieldModel();
/*
   Reads calibration field points that are stored in a filenode. These can be used to calculate the homography matrix
 */
void readVectorOfPointsFromFileNode(cv::FileNode& node, std::vector<cv::Point2f>& vec);

public:

FieldDetection(int height, int width, cv::Mat cameraMatrix, cv::Mat distortionCoefficients, bool isRightFieldSide, struct ConfigData &config);
FieldDetection();
~FieldDetection();
/*
	Calibrates the fieldDetection and the camera with the field model.
	It uses the calibration points of the data file.
*/
void calibrateFieldLinesWithPresetPoints();
/*
   Calibrates the fieldDetection and the camera with the field model.
   It can use the calibration points of the data file or recalibrate with the detecfieldDetection function.
	 Writes the calibration fieldPoints back to XML File
 */
void calibrateFieldLinesWithCalibrationVideo(cv::VideoCapture);
/*
   Thresholding the original image to filter the brighter field lines.
   Finds fieldlines by using edgedetection and HoughLinesP.
   Detected field lines are stored in m_calibratedImgLines.
 */
void detecFieldLines(cv::Mat&);

/*
	Detects Field Lines by using the Sobel, Treshold and HoughLinesP operator.
	Detected field lines are stored in m_calibratedImgLines
*/
void detecFieldLinesSobel(cv::Mat&);

/*
   Uses m_calibratedImgLines to find corresponding points in the model.
   It searches for rectangle which is the centerfield. If it is found, it detects its four corners.
   Calls calculateHomomorphyMatrix() to calc the homography matrix to transform the detected points to
   the corresponding points in the field model.
 */
void calculateCorrespondingCornerPoints();

//void detecFieldLinesSobel(std::vector<cv::Mat>&);

/*
	Cuts the imput Image along the court with a small tolerance.
	Causes that desruptive factors for wrong edge detection are minimalized.
*/
void cut_Image(cv::Mat&, cv::Mat&);

/*
	Calculates the points of a line that touch the border of the Frame.
*/
void calculateLinePointsBorder(cv::Point&, cv::Point&, cv::Point&, cv::Point&, int, int);

/*
   Calculates the homography mat with corresponding points from the field model and the calibration frames.
 */
void calculateHomomorphyMatrix();
/*
   Calculates a backgroubd image by avaraging 5 frames that only contain background images.
 */
void calculateBlendedBackgroundImg(std::vector<cv::Mat>&);

/*
   Transforms a point from the image plane to the field model.
 */
cv::Point2f transformPointToModel(cv::Point2f);
bool isInsideServeZone(int serveZoneNumber, cv::Point2f positionAtModel);

cv::Mat getImgLines(){
	return m_imgLines;
}
cv::Mat getCalibratedImgLines(){
	return m_calibratedImgLines;
}
cv::Mat getFieldModel(){
	cv::Mat _fieldModel = m_fieldModel.clone(); return _fieldModel;
}
cv::Mat getFieldModelSmall(){
	cv::Mat _fieldModelSmall = m_fieldModelSmall.clone(); return _fieldModelSmall;
}
cv::Mat getHomographyMat(){
	cv::Mat _homographyMat = m_homographyMat.clone(); return _homographyMat;
}
cv::Mat getBackgroundImg(){
	cv::Mat _backgroundImg = m_backgroundImg.clone(); return _backgroundImg;
}
std::vector<cv::Point2f> getSingleFieldPoints(){
	return m_singleFieldPoints;
}
std::vector<cv::Point2f> getFourCornersOfCenterField(){
	return m_fourCornersOfCenterField;
}
int getCalibrationCount(){
	return m_calibrationCount;
}
bool getIsRightFieldSide(){
	return m_isRightFieldSide;
}

int getFrameWidth(){return m_frameWidth;}
int getFrameHeight(){return m_frameHeight;}

const std::vector<cv::Point2f> &getLeftSingelFieldPoints() const {
	return m_leftSingelFieldPoints;
}
const std::vector<cv::Point2f> &getRightSingelFieldPoints() const {
	return m_rightSingelFieldPoints;
}

const std::vector<cv::Point2f> &getCourtBoundry() const {
	return m_courtBoundary;
}

const std::vector<cv::Point2f> &getServeZoneLeft1() const {
	return m_serveZoneLeft1;
}
const std::vector<cv::Point2f> &getServeZoneLeft2() const {
	return m_serveZoneLeft2;
}
const std::vector<cv::Point2f> &getServeZoneRight3() const {
	return m_serveZoneRight3;
}
const std::vector<cv::Point2f> &getServeZoneRight4() const {
	return m_serveZoneRight4;
}

const std::vector<cv::Point2f> &getReturnZoneLeft1() const {
	return m_returnZoneLeft1;
}
const std::vector<cv::Point2f> &getReturnZoneLeft2() const {
	return m_returnZoneLeft2;
}
const std::vector<cv::Point2f> &getReturnZoneRight3() const {
	return m_returnZoneRight3;
}
const std::vector<cv::Point2f> &getReturnZoneRight4() const {
	return m_returnZoneRight4;
}

const std::vector<cv::Point2f> &getServeImpactZoneLeft1() const {
	return m_serveImpactZoneLeft1;
}
const std::vector<cv::Point2f> &getServeImpactZoneLeft2() const {
	return m_serveImpactZoneLeft2;
}
const std::vector<cv::Point2f> &getServeImpactZoneRight3() const {
	return m_serveImpactZoneRight3;
}
const std::vector<cv::Point2f> &getServeImpactZoneRight4() const {
	return m_serveImpactZoneRight4;
}

};

#endif /* fieldDetection_hpp */
