//
//  player.hpp
//
//
//  Created by Henri Kuper on 22.08.17.
//
//

#ifndef player_hpp
#define player_hpp

#include <stdio.h>
#include <iostream>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "../imgProcLib/imageMath.hpp"

class Player {
private:
int m_histogramSize;
int m_detectionCounter;
int m_detectionCounterRight;
int m_detectionCounterLeft;
float m_avarageVariationOfHistogramsRight;
float m_avarageVariationOfHistogramsLeft;

bool m_isOutOfScope;
bool m_isComparedToOtherHistograms;

float m_xMin;
float m_xMax;
float m_yMin;
float m_yMax;

std::vector<int> m_frameCounts;
std::vector<cv::Point2f> m_centersOfPlayer;
std::vector<cv::Point2f> m_positionsOfPlayerInModel;

std::vector<int> m_frameCountOfDisappearingRight;
std::vector<int> m_frameCountOfAppearingRight;
std::vector<int> m_frameCountOfDisappearingLeft;
std::vector<int> m_frameCountOfAppearingLeft;

cv::Mat m_histogramRight;
cv::Mat m_histogramLeft;

bool m_isRight;
bool m_hasRightSideData;
bool m_hasLeftSideData;

void playerInit();
public:
Player(bool isRight);
Player(bool isRight, int frameCount, cv::Point2f centerOfPlayer, cv::Point2f positionOfPlayer, cv::Mat &histogram );
~Player();

void updateHistogramm(cv::Mat &histogram);
void updateAvarageVariatonOfHistograms(cv::Mat &histogram);
void appendPlayerData(int frameCount, cv::Point2f centerOfPlayer, cv::Point2f positionOfPlayerInModel, cv::Mat &histogram);
void mergeWithOtherPlayer(Player &otherPlayer);
//get/set-Methods
int getHistogramSize(){
	return m_histogramSize;
}
int getDetectionCounter(){
	return m_detectionCounter;
}
int getDetectionCounterRight(){
	return m_detectionCounterRight;
}
int getDetectionCounterLeft(){
	return m_detectionCounterLeft;
}

float getAvarageVariationOfHistogramsRight(){
	return m_avarageVariationOfHistogramsRight;
}
float getAvarageVariationOfHistogramsLeft(){
	return m_avarageVariationOfHistogramsLeft;
}

float getHorizontalMovement(){
	return (m_xMax - m_xMin);
}
float getVerticalMovement(){
	return (m_yMax - m_yMin);
}

const std::vector<int> &getFrameCounts() const {
	return m_frameCounts;
}
const std::vector<cv::Point2f> &getCentersOfPlayer() const{
	return m_centersOfPlayer;
}
const std::vector<cv::Point2f> &getPositionsOfPlayerInModel() const{
	return m_positionsOfPlayerInModel;
}
const std::vector<int> &getFrameCountOfDisappearingRight() const{
	return m_frameCountOfDisappearingRight;
}
const std::vector<int> &getFrameCountOfAppearingRight() const{
	return m_frameCountOfAppearingRight;
}
const std::vector<int> &getFrameCountOfDisappearingLeft() const{
	return m_frameCountOfDisappearingLeft;
}
const std::vector<int> &getFrameCountOfAppearingLeft() const{
	return m_frameCountOfAppearingLeft;
}

cv::Point2f getPositionOfPlayerInModel(int position){return m_positionsOfPlayerInModel[position];}

bool getFrameCountOfDisappearingRightIsEmpty(){return m_frameCountOfDisappearingRight.empty();}
bool getFrameCountOfAppearingRightIsEmpty(){return m_frameCountOfAppearingRight.empty();}
bool getFrameCountOfDisappearingLeftIsEmpty(){return m_frameCountOfDisappearingLeft.empty();}
bool getFrameCountOfAppearingLeftIsEmpty(){return m_frameCountOfAppearingLeft.empty();}

int getLastFrameCountOfDisappearingRight(){return m_frameCountOfDisappearingRight.back();}
int getLastFrameCountOfAppearingRight(){return m_frameCountOfAppearingRight.back();}
int getLastFrameCountOfDisappearingLeft(){return m_frameCountOfDisappearingLeft.back();}
int getLastFrameCountOfAppearingLeft(){return m_frameCountOfAppearingLeft.back();}

cv::Mat getHistogramRight(){
	return m_histogramRight;
}
cv::Mat getHistogramLeft(){
	return m_histogramLeft;
}

int getLastFrameCount(){
	return m_frameCounts.back();
}
cv::Point2f getLastCenterOfPlayer(){
	return m_centersOfPlayer.back();
}
cv::Point2f getLastPositionOfPlayerInModel(){
	return m_positionsOfPlayerInModel.back();
}

bool getIsOutOfScope(){
	return m_isOutOfScope;
}
bool getIsComparedToOtherHistograms(){
	return m_isComparedToOtherHistograms;
}
bool getIsRight(){
	return m_isRight;
}
bool getHasRightSideData(){
	return m_hasRightSideData;
}
bool getHasLeftSideData(){
	return m_hasLeftSideData;
}

float getXMin(){
	return m_xMin;
}
float getXMax(){
	return m_xMax;
}
float getYMin(){
	return m_yMin;
}
float getYMax(){
	return m_yMax;
}

void setHistogramSize(int size){
	m_histogramSize = size;
}
void setIsOutOfScope(bool isOutOfScope){
	m_isOutOfScope = isOutOfScope;
}
void setIsComparedToOtherHistograms(bool isComparedToOtherHistograms){
	m_isComparedToOtherHistograms = isComparedToOtherHistograms;
}

};


#endif /* player_hpp */
