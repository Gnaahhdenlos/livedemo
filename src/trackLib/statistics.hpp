//
//  playerStatistics.hpp
//
//
//  Created by Henri Kuper on 03.07.17.
//
//

#ifndef playerStatistics_hpp
#define playerStatistics_hpp

#include <stdio.h>

/*
   TODO:
   - Statistics an imgCount koppeln. Struct erstellen, welches int imgCount + entsprechende Statistic enhält.
   - das Führen dieser imgCount erfassten Statistic in die haupt classen (eventdetection) einführen.
   -Number of aces Funktion implementieren

 */

struct PlayerStatistics {

	int m_numberOfAces;
	int m_numberOfDoubleFaults;
	int m_numberOfServes;
	int m_numberOfFirstServes;
	int m_numberOfSecondServes;

	int m_numberOfForehandSlice;
	int m_numberOfBackhandSlice;
	int m_numberOfForehandSpin;
	int m_numberOfBackhandSpin;

	float m_firstServePercentage;

	PlayerStatistics(){
		m_numberOfAces = 0;
		m_numberOfDoubleFaults = 0;
		m_numberOfServes = 0;
		m_numberOfSecondServes = 0;
		m_firstServePercentage = 100.0f;
		m_numberOfForehandSlice = 0;
		m_numberOfBackhandSlice = 0;
		m_numberOfForehandSpin = 0;
		m_numberOfBackhandSpin = 0;
	};

	void updateStatistics(){
		std::cout<<"m_numberOfFirstServes: "<<m_numberOfFirstServes<<" m_numberOfSecondServes: "<<m_numberOfSecondServes<<std::endl;
		if(m_numberOfFirstServes > 0) {
			m_firstServePercentage = (float)(m_numberOfFirstServes - m_numberOfSecondServes)/m_numberOfFirstServes*100.0;
		}
		else{
			m_firstServePercentage = 100.0f;
		}
	};
};

struct Statistics {

	PlayerStatistics *player1;
	PlayerStatistics *player2;

	Statistics(){
		player1 = new PlayerStatistics();
		player2 = new PlayerStatistics();
	};

	void updateStatistics(){
		player1->updateStatistics();
		player2->updateStatistics();
	}

	std::string getActualStatistics(int statisticsNumber){
		std::string statistics = "";
		switch(statisticsNumber) {
		case 0: statistics = "Burlage               Brunken"; break;
		case 1: statistics = std::to_string(player1->m_numberOfAces) + "        Aces       " + std::to_string(player2->m_numberOfAces); break;
		case 2: statistics = std::to_string(player1->m_numberOfDoubleFaults) + "   Double Faults   "+ std::to_string(player2->m_numberOfDoubleFaults); break;
		case 3: statistics = std::to_string((int)player1->m_firstServePercentage) + "     1st Serve %     "+ std::to_string((int)player2->m_firstServePercentage); break;
		case 4: statistics = std::to_string(player1->m_numberOfForehandSpin) + "   Forehand Spin   " + std::to_string(player2->m_numberOfForehandSpin); break;
		case 5: statistics = std::to_string(player1->m_numberOfForehandSlice) + "   Forehand Slice   " + std::to_string(player2->m_numberOfForehandSlice); break;
		case 6: statistics = std::to_string(player1->m_numberOfBackhandSpin) + "   Backhand Spin   " + std::to_string(player2->m_numberOfBackhandSpin); break;
		case 7: statistics = std::to_string(player1->m_numberOfBackhandSlice) + "   Backhand Slice   " + std::to_string(player2->m_numberOfBackhandSlice); break;
		default: break;
		}
		return statistics;
	}

};



#endif /* playerStatistics_hpp */
