//
//  frameGrabber.cpp
//
//
//  Created by Henri Kuper on 05.09.17.
//
//

#include "frameGrabber.hpp"

using namespace cv;
using namespace std;

// FrameGrabber::FrameGrabber(cv::VideoCapture rightVideoStream, cv::VideoCapture leftVideoStream, vector<Mat>& frameBufferRight, vector<Mat>& frameBufferLeft, vector<Mat>& frameBufferGrayRight, vector<Mat>& frameBufferGrayLeft, cv::Mat undistortionRemap1, cv::Mat undistortionRemap2, float scaleFactor)
// 	: m_rightVideoStream(rightVideoStream), m_leftVideoStream(leftVideoStream),m_frameBufferRight(frameBufferRight), m_frameBufferLeft(frameBufferLeft), m_frameBufferGrayRight(frameBufferGrayRight), m_frameBufferGrayLeft(frameBufferGrayLeft), m_undistortionRemap1(undistortionRemap1), m_undistortionRemap2(undistortionRemap2), m_inputVideoResizingFactor(scaleFactor){
// 	frameGrabberInit();
// }

FrameGrabber::FrameGrabber(cv::VideoCapture rightVideoStream, cv::VideoCapture leftVideoStream, cv::Mat undistortionRemap1, cv::Mat undistortionRemap2, int framesPrediction)
	: m_rightVideoStream(rightVideoStream), m_leftVideoStream(leftVideoStream), m_undistortionRemap1(undistortionRemap1), m_undistortionRemap2(undistortionRemap2), m_framesPrediction(framesPrediction){
	frameGrabberInit();
}

FrameGrabber::~FrameGrabber(){

}

void FrameGrabber::frameGrabberInit(){
	m_videoStreamStatus = EXIT_SUCCESS;

	m_frameBufferSizeRight = 0;
	m_frameBufferSizeLeft = 0;
	m_frameBufferGraySizeRight = 0;
	m_frameBufferGraySizeLeft = 0;
}

void FrameGrabber::grabFramesFromVideoStream(){

	tm.reset(); tm.start();
	grabFramesFromVideoStreamRight();
	grabFramesFromVideoStreamLeft();

	//Threading
	//thread grabFramesRight(&FrameGrabber::grabFramesFromVideoStreamRight, this);
	//thread grabFramesLeft(&FrameGrabber::grabFramesFromVideoStreamLeft, this);

	//grabFramesRight.join();
	//grabFramesLeft.join();

	updateBufferSizes();
	tm.stop();
	frameTimes.push_back(tm.getTimeMilli());
	std::sort(frameTimes.begin(), frameTimes.end());
	double frame_avg = std::accumulate(frameTimes.begin(), frameTimes.end(), 0.0) / frameTimes.size();
	std::cout << "Grap : Avg : " << frame_avg << " ms FPS : " << 1000.0 / frame_avg << std::endl;
}

void FrameGrabber::grabFramesFromVideoStreamRight(){
	m_frameBufferRight.push_back(Mat());
	m_frameBufferGrayRight.push_back(Mat());

	Mat frameRight;
	bool canReadRight = m_rightVideoStream.read(frameRight); // read a new frame from video
	if (!canReadRight) {
		cout << "FrameGrabber: Cannot read a frame from video stream" << endl;
		m_videoStreamStatus = EXIT_FAILURE;
	}
	else{
		m_frameBufferRight.back() = frameRight;
		//remap(frameRight, m_frameBufferRight.back(), m_undistortionRemap1, m_undistortionRemap2, INTER_LINEAR);
		//m_frameUndistortedRight = frameRight;
		//resize(m_frameUndistortedRight, m_frameBufferRight.back(), Size(), m_inputVideoResizingFactor, m_inputVideoResizingFactor, INTER_LINEAR);        //INTER_NEAREST
		cvtColor(frameRight, m_frameBufferGrayRight.back(), COLOR_BGR2GRAY);
	}
}
void FrameGrabber::grabFramesFromVideoStreamLeft(){
	m_frameBufferLeft.push_back(Mat());
	m_frameBufferGrayLeft.push_back(Mat());

	Mat frameLeft;
	bool canReadLeft = m_leftVideoStream.read(frameLeft); // read a new frame from video
	if (!canReadLeft) {
		cout << "FrameGrabber: Cannot read a frame from video stream" << endl;
		m_videoStreamStatus = EXIT_FAILURE;
	}
	else{
		m_frameBufferLeft.back() = frameLeft;
		//remap(frameLeft, m_frameBufferLeft.back(), m_undistortionRemap1, m_undistortionRemap2, INTER_LINEAR);
		//m_frameUndistortedLeft = frameLeft;
		//resize(m_frameUndistortedLeft, m_frameBufferLeft.back(), Size(), m_inputVideoResizingFactor, m_inputVideoResizingFactor, INTER_LINEAR);        //INTER_NEAREST
		cvtColor(frameLeft, m_frameBufferGrayLeft.back(), COLOR_BGR2GRAY);
	}
}


void FrameGrabber::deleteFramesFromBuffer(){
	int frameBufferSize = (int32_t)m_frameBufferRight.size();

	if(frameBufferSize > m_framesPrediction+1) {
		m_frameBufferRight.erase(m_frameBufferRight.begin());
		m_frameBufferLeft.erase(m_frameBufferLeft.begin());
	}
	if((int32_t)m_frameBufferGrayRight.size() > 3) {
		m_frameBufferGrayRight.erase(m_frameBufferGrayRight.begin());
		m_frameBufferGrayLeft.erase(m_frameBufferGrayLeft.begin());
	}
	updateBufferSizes();
}

void FrameGrabber::updateBufferSizes(){
	m_frameBufferSizeRight = (int)m_frameBufferRight.size();
	m_frameBufferSizeLeft = (int)m_frameBufferLeft.size();
	m_frameBufferGraySizeRight = (int)m_frameBufferGrayRight.size();
	m_frameBufferGraySizeLeft = (int)m_frameBufferGrayLeft.size();
}
// void FrameGrabber::grabFramesFromVideoStream(vector<Mat>& frameBufferRight, vector<Mat>& frameBufferLeft, vector<Mat>& frameBufferGrayRight, vector<Mat>& frameBufferGrayLeft){
//
// 	tm.reset(); tm.start();
// 	frameBufferRight.push_back(Mat());
// 	frameBufferLeft.push_back(Mat());
// 	frameBufferGrayRight.push_back(Mat());
// 	frameBufferGrayLeft.push_back(Mat());
//
// 	Mat frameRight, frameLeft;
// 	bool canReadRight = m_rightVideoStream.read(frameRight); // read a new frame from video
// 	bool canReadLeft = m_leftVideoStream.read(frameLeft); // read a new frame from video
// 	if (!canReadRight || !canReadLeft) {
// 		cout << "FrameGrabber: Cannot read a frame from video stream" << endl;
// 		m_videoStreamStatus = EXIT_FAILURE;
// 	}
// 	else{
// 		remap(frameRight, m_frameUndistortedRight, m_undistortionRemap1, m_undistortionRemap2, INTER_LINEAR);
// 		//m_frameUndistortedRight = frameRight;
// 		resize(m_frameUndistortedRight, frameBufferRight.back(), Size(), m_inputVideoResizingFactor, m_inputVideoResizingFactor, INTER_LINEAR);        //INTER_NEAREST
// 		cvtColor(frameBufferRight.back(), frameBufferGrayRight.back(), COLOR_BGR2GRAY);
//
// 		remap(frameLeft, m_frameUndistortedLeft, m_undistortionRemap1, m_undistortionRemap2, INTER_LINEAR);
// 		//m_frameUndistortedLeft = frameLeft;
// 		resize(m_frameUndistortedLeft, frameBufferLeft.back(), Size(), m_inputVideoResizingFactor, m_inputVideoResizingFactor, INTER_LINEAR);        //INTER_NEAREST
// 		cvtColor(frameBufferLeft.back(), frameBufferGrayLeft.back(), COLOR_BGR2GRAY);
//
// 	}
// 	tm.stop();
// 	frameTimes.push_back(tm.getTimeMilli());
// 	std::sort(frameTimes.begin(), frameTimes.end());
// 	double frame_avg = std::accumulate(frameTimes.begin(), frameTimes.end(), 0.0) / frameTimes.size();
// 	std::cout << "Grap : Avg : " << frame_avg << " ms FPS : " << 1000.0 / frame_avg << std::endl;
// }
