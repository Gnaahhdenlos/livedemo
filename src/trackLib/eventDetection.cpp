//
//  eventDetectionOld.cpp
//
//
//  Created by Henri Kuper on 17.05.17.
//
//
//#define GPU_ON

#include "eventDetection.hpp"


using namespace std;
using namespace cv;
using namespace imgProc;
/* TODO:

    - fehlerdetektion, wenn der ball ins netz geht verbessern
    - zwei direkt aufeinanderfolgende serves abfangen (als beispiel, wenn sie innerhalb von 50 bildern detectiert werden.)(wurde zum Teil gelöst du durch verhinderung des Auftretens in der Klasse curveDetection)

    - wahrscheinlichkeit für die korrekte serveerkennung integrieren. Auch als nicht korrekte servers erkannte schläge einfügen(nur mit schlechter ws)
    - end of game and end of rally vor dem serve koppeln?

                Events:
                1. Strokes (Categories)
                2. Serves
                3. Impact after serve
                4. Impact (in, out, critical)
                5. End of Rally (Next serve, Point loss)
                6. Game
                7. Change of Ends
                8. Set
                9. Match
 */

EventDetection::EventDetection(){
	eventDetectionInit();
}
EventDetection::EventDetection(struct ConfigData &config, FieldDetection *rightFieldDetection, FieldDetection *leftFieldDetection, CurveDetection *rightCurveDetection, CurveDetection *leftCurveDetection, PlayerDetection *playerDetection,int frameWidth, int frameHeight, int videoFramesOffset, int framesPrediction)
	: m_config(config), m_fieldDetectionRight(rightFieldDetection), m_fieldDetectionLeft(leftFieldDetection), m_curveDetectionRight(rightCurveDetection), m_curveDetectionLeft(leftCurveDetection), m_playerDetection(playerDetection),m_frameWidth(frameWidth), m_frameHeight(frameHeight), m_videoFramesOffset(videoFramesOffset), m_framesPrediction(framesPrediction){
	eventDetectionInit();
}
EventDetection::~EventDetection(){

}

void EventDetection::eventDetectionInit(){
	//TODO
	//m_inputVideoResizingFactor = (float)m_frameWidth / 1280.0f;

	m_playerSegmentationRight = m_playerDetection->getPlayerSegmentationRight();
	m_playerSegmentationLeft = m_playerDetection->getPlayerSegmentationLeft();

	m_lineWidth = 6.0;

	m_statistics = new Statistics();

	m_walter = new network();

	m_walter->initWeights(m_config.getFilePathToWeights());

	m_frameCount = 0;

	m_alreadyEventCheckedCurveNumberRight = -1;
	m_alreadyEventCheckedCurveNumberLeft = -1;

	m_frameCountCurveOverNet = -1;

	m_minimumServesForSafeChangeOfServes = 2;
	m_minServeFrameCountDiff = 40;
	m_actualNumberOfServiceSides = 1;
	m_frameCountChangeOfServe = -1;
	m_numberOfServesFromSameEnd = 0;
	m_serveIsRight = false;
	m_minPercentageInsideServeZones = 0.5f;

	m_impactProbabilityModulation = 0.004f;//0.006432f
	m_impactAfterServeProbabilityModulation = 0.007f;//0.012863f

	m_actualCurveCharacter = DEFAULT;
	m_actualStrokeType = DEFAULT_STROKE;

	m_actualCurveNumber = -1;
	m_actualCurveFrameCount = -1;
	m_actualCurveIsRight = true;
	m_actualServePlayerRight = true;
	m_actualServePos = -1;
	m_actualServeTry = 0;

	m_firstServeImpactWasOutside = false;
	m_firstServeImpactFrameCount = -1;
	m_insertPositionContinuationOfRally = -1;

	m_serveDetected = false;
	m_rallyIsRunning = false;
}

void EventDetection::detecEvents(const vector<Mat> &bufferRight, const vector<Mat> &bufferLeft){
	tm.reset();tm.start();
	updateActualCurve();
	cout<<"checkedRight: "<<m_alreadyEventCheckedCurveNumberRight<<" actualCurveNumber: "<< m_actualCurveNumber<<" m_actualCurveIsRight: "<<m_actualCurveIsRight<<endl;
	cout<<"checkedLeft: "<<m_alreadyEventCheckedCurveNumberLeft<<" actualCurveNumber: "<< m_actualCurveNumber<<" m_actualCurveIsRight: "<<m_actualCurveIsRight<<endl;
	if((m_alreadyEventCheckedCurveNumberRight < m_actualCurveNumber && m_actualCurveIsRight == true) || (m_alreadyEventCheckedCurveNumberLeft < m_actualCurveNumber && m_actualCurveIsRight == false)) {
		m_frameCountCurveOverNet = -1;
		detecServeEvents();
		detecChangeOfServiceEvents();
		if(m_rallyIsRunning == true) {
			categoriseStroke(bufferRight, bufferLeft);
			if(m_serveDetected == true) {
				detecImpactAfterServeEvents();
			}
			else{
				detecImpactEvents();
			}
			detecRallyEvents();
		}
	}
	detecInNetEvent();
	detecChangeOfSideEvent();
	// if(m_frameCount == 4600){
	// 	appendEndOfGameEvent(4600, -1, false);
	// }
	// if(m_frameCount == 9175){
	// 	appendEndOfGameEvent(9174, -1, true);
	// }
	// if(m_frameCount == 17150){
	// 	appendEndOfGameEvent(17150, -1, true);
	// }

	if(m_actualCurveIsRight == true && m_actualCurveNumber > m_alreadyEventCheckedCurveNumberRight) {
		m_alreadyEventCheckedCurveNumberRight = m_actualCurveNumber;
	}
	else if(m_actualCurveIsRight == false && m_actualCurveNumber > m_alreadyEventCheckedCurveNumberLeft) {
		m_alreadyEventCheckedCurveNumberLeft = m_actualCurveNumber;
	}
	m_frameCount++;
	printEvents();

	tm.stop();
	event_times.push_back(tm.getTimeMilli());
	std::sort(event_times.begin(), event_times.end());
	double event_avg = std::accumulate(event_times.begin(), event_times.end(), 0.0) / event_times.size();
	std::cout << "Event : Avg : " << event_avg << " ms FPS : " << 1000.0 / event_avg << std::endl;

}

void EventDetection::detecServeEvents(){
	//TODO integrate returnPlayerPositionCheck
	vector<vector<Point2f> > positionOfPlayerDuringCurve;
	Point2f impactPointOfActualCurve;

	if(m_actualCurveIsRight == true  && m_actualCurveNumber >= 0 && m_actualCurveCharacter == SERVE_RIGHT_PLAYER ){
		if(checkPlayerIsInsideCourt(true, m_actualCurveNumber, false) == true){
			float numberOfPositionInsideServeZone3 = 0;
			float numberOfPositionInsideServeZone4 = 0;
			float numberOfPositionInsideReturnZone1 = 0;
			float numberOfPositionInsideReturnZone2 = 0;
			vector<Point2f> positionsOfReturnPlayer;
			m_playerDetection->searchForPlayerPositionBevoreFrameCount(m_curveDetectionRight->getCurve(m_actualCurveNumber).getLastFrameCount(), false, positionsOfReturnPlayer);

			for(uint32_t i = 0; i < positionsOfReturnPlayer.size(); i++){
				cout<<"positionsOfReturnPlayer: "<<i<<" "<<positionsOfReturnPlayer[i]<<endl;
				if(imageObjects::isInsideQuadrangle(m_fieldDetectionRight->getReturnZoneLeft1(), positionsOfReturnPlayer[i], 0) == true) {
					numberOfPositionInsideReturnZone1++;
				}
				else if(imageObjects::isInsideQuadrangle(m_fieldDetectionRight->getReturnZoneLeft2(), positionsOfReturnPlayer[i], 0) == true) {
					numberOfPositionInsideReturnZone2++;
				}
			}
			int numbersOfPosition = (int)m_curveDetectionRight->getCurve(m_actualCurveNumber).getPositionsOfPlayerDuringCurveVecSize();
			for(int i = 0; i < numbersOfPosition; i++) {
				Point2f positionAtModel = m_curveDetectionRight->getCurve(m_actualCurveNumber).getPositionOfPlayerDuringCurve(i);
				if(imageObjects::isInsideQuadrangle(m_fieldDetectionRight->getServeZoneRight3(), positionAtModel, m_lineWidth) == true) {
					numberOfPositionInsideServeZone3++;
				}
				if(imageObjects::isInsideQuadrangle(m_fieldDetectionRight->getServeZoneRight4(), positionAtModel, m_lineWidth) == true) {
					numberOfPositionInsideServeZone4++;
				}
			}
			float percentageInsideServeZones = (float)(numberOfPositionInsideServeZone3 + numberOfPositionInsideServeZone4) / (float)numbersOfPosition;
			cout<<"numberOfPositionInsideServeZone3: "<<numberOfPositionInsideServeZone3<<" numberOfPositionInsideServeZone4: "<<numberOfPositionInsideServeZone4<<" percentageInsideServeZones: "<<percentageInsideServeZones<<endl;
			if(numberOfPositionInsideServeZone3 > numberOfPositionInsideServeZone4 && percentageInsideServeZones >= m_minPercentageInsideServeZones) {

				if(numberOfPositionInsideReturnZone1 > numberOfPositionInsideReturnZone2){
					appendServeEvent(m_actualCurveFrameCount, 4, m_curveDetectionRight->getCurve(m_actualCurveNumber).getSpeed());
					cout<<"Return player stands in ReturnZone 1 but was expected in ReturnZone 2"<<endl;
				}
				else{
					appendServeEvent(m_actualCurveFrameCount, 3, m_curveDetectionRight->getCurve(m_actualCurveNumber).getSpeed());
				}
				cout<<"Number of positions in returnZone 1: "<<numberOfPositionInsideReturnZone1<<endl;
				cout<<"Number of positions in returnZone 2: "<<numberOfPositionInsideReturnZone2<<endl;
			}
			else if(numberOfPositionInsideServeZone3 < numberOfPositionInsideServeZone4 && percentageInsideServeZones >= m_minPercentageInsideServeZones) {
				if(numberOfPositionInsideReturnZone2 > numberOfPositionInsideReturnZone1){
					appendServeEvent(m_actualCurveFrameCount, 3, m_curveDetectionRight->getCurve(m_actualCurveNumber).getSpeed());
					cout<<"Return player stands in ReturnZone 1 but was expected in ReturnZone 2"<<endl;
				}
				else{
					appendServeEvent(m_actualCurveFrameCount, 4, m_curveDetectionRight->getCurve(m_actualCurveNumber).getSpeed());
				}
				cout<<"Number of positions in returnZone 1: "<<numberOfPositionInsideReturnZone1<<endl;
				cout<<"Number of positions in returnZone 2: "<<numberOfPositionInsideReturnZone2<<endl;
			}
			else if(numberOfPositionInsideServeZone3 == numberOfPositionInsideServeZone4 && percentageInsideServeZones >= m_minPercentageInsideServeZones){
				if(numberOfPositionInsideReturnZone1 > numberOfPositionInsideReturnZone2){
					appendServeEvent(m_actualCurveFrameCount, 4, m_curveDetectionRight->getCurve(m_actualCurveNumber).getSpeed());
				}
				else{
					appendServeEvent(m_actualCurveFrameCount, 3, m_curveDetectionRight->getCurve(m_actualCurveNumber).getSpeed());
				}
			}
			else{
				cout<<"Serve right, but not in one zone."<<endl;
			}
			cout<<"Number of positions: "<<m_curveDetectionRight->getCurve(m_actualCurveNumber).getPositionsOfPlayerDuringCurveVecSize()<<endl;
			cout<<"Position in model: "<<m_curveDetectionRight->getCurve(m_actualCurveNumber).getPositionOfPlayerDuringCurve(0)<<endl;
		}
		else{
			cout<<"Detected Serve from right player on other Court"<<endl;
		}
	}
	else if(m_actualCurveIsRight == false && m_actualCurveNumber >= 0 && m_actualCurveCharacter == SERVE_LEFT_PLAYER ) {
		if(checkPlayerIsInsideCourt(false, m_actualCurveNumber, false) == true){
			float numberOfPositionInsideServeZone1 = 0;
			float numberOfPositionInsideServeZone2 = 0;
			float numberOfPositionInsideReturnZone3 = 0;
			float numberOfPositionInsideReturnZone4 = 0;
			vector<Point2f> positionsOfReturnPlayer;
			m_playerDetection->searchForPlayerPositionBevoreFrameCount(m_curveDetectionLeft->getCurve(m_actualCurveNumber).getLastFrameCount(), true, positionsOfReturnPlayer);

			for(uint32_t i = 0; i < positionsOfReturnPlayer.size(); i++){
				cout<<"positionsOfReturnPlayer: "<<i<<" "<<positionsOfReturnPlayer[i]<<endl;
				if(imageObjects::isInsideQuadrangle(m_fieldDetectionLeft->getReturnZoneRight3(), positionsOfReturnPlayer[i], 0) == true) {
					numberOfPositionInsideReturnZone3++;
				}
				else if(imageObjects::isInsideQuadrangle(m_fieldDetectionLeft->getReturnZoneRight4(), positionsOfReturnPlayer[i], 0) == true) {
					numberOfPositionInsideReturnZone4++;
				}
			}
			int numbersOfPosition = (int) m_curveDetectionLeft->getCurve(m_actualCurveNumber).getPositionsOfPlayerDuringCurveVecSize();
			for(uint32_t i = 0; i < numbersOfPosition; i++) {
				Point2f positionAtModel = m_curveDetectionLeft->getCurve(m_actualCurveNumber).getPositionOfPlayerDuringCurve(i);
				cout<<"PositionAtModel: "<<positionAtModel<<endl;
				if(imageObjects::isInsideQuadrangle(m_fieldDetectionLeft->getServeZoneLeft1(), positionAtModel, m_lineWidth) == true) {
					numberOfPositionInsideServeZone1++;
				}
				if(imageObjects::isInsideQuadrangle(m_fieldDetectionLeft->getServeZoneLeft2(), positionAtModel, m_lineWidth) == true) {
					numberOfPositionInsideServeZone2++;
				}
			}

			float percentageInsideServeZones = (float)(numberOfPositionInsideServeZone1 + numberOfPositionInsideServeZone2) / (float)numbersOfPosition;
			cout<<"NumberOfPositions: "<<numbersOfPosition<<endl;
			cout<<"numberOfPositionInsideServeZone1: "<<numberOfPositionInsideServeZone1<<" numberOfPositionInsideServeZone2: "<<numberOfPositionInsideServeZone2<<" percentageInsideServeZones: "<<percentageInsideServeZones<<endl;
			if(numberOfPositionInsideServeZone1 > numberOfPositionInsideServeZone2 && percentageInsideServeZones >= m_minPercentageInsideServeZones) {
				if(numberOfPositionInsideReturnZone3 > numberOfPositionInsideReturnZone4){
					appendServeEvent(m_actualCurveFrameCount, 2, m_curveDetectionLeft->getCurve(m_actualCurveNumber).getSpeed());
					cout<<"Return player stands in ReturnZone 4 but was expected in ReturnZone 3"<<endl;
				}
				else{
					appendServeEvent(m_actualCurveFrameCount, 1, m_curveDetectionLeft->getCurve(m_actualCurveNumber).getSpeed());
				}
				cout<<"Number of positions in returnZone 3: "<<numberOfPositionInsideReturnZone3<<endl;
				cout<<"Number of positions in returnZone 4: "<<numberOfPositionInsideReturnZone4<<endl;
			}
			else if(numberOfPositionInsideServeZone1 < numberOfPositionInsideServeZone2 && percentageInsideServeZones >= m_minPercentageInsideServeZones) {
				if(numberOfPositionInsideReturnZone4 > numberOfPositionInsideReturnZone3){
					appendServeEvent(m_actualCurveFrameCount, 1, m_curveDetectionLeft->getCurve(m_actualCurveNumber).getSpeed());
					cout<<"Return player stands in ReturnZone 4 but was expected in ReturnZone 3"<<endl;
				}
				else{
					appendServeEvent(m_actualCurveFrameCount, 2, m_curveDetectionLeft->getCurve(m_actualCurveNumber).getSpeed());
				}
				cout<<"Number of positions in returnZone 3: "<<numberOfPositionInsideReturnZone3<<endl;
				cout<<"Number of positions in returnZone 4: "<<numberOfPositionInsideReturnZone4<<endl;

			}
			else if(numberOfPositionInsideServeZone1 == numberOfPositionInsideServeZone2 && percentageInsideServeZones >= m_minPercentageInsideServeZones){
				if(numberOfPositionInsideReturnZone3 > numberOfPositionInsideReturnZone4){
					appendServeEvent(m_actualCurveFrameCount, 2, m_curveDetectionLeft->getCurve(m_actualCurveNumber).getSpeed());
				}
				else{
					appendServeEvent(m_actualCurveFrameCount, 1, m_curveDetectionLeft->getCurve(m_actualCurveNumber).getSpeed());
				}
			}
			else{
				cout<<"Serve left, but not in one zone."<<endl;
			}
			cout<<"Number of Positions: "<<m_curveDetectionLeft->getCurve(m_actualCurveNumber).getPositionsOfPlayerDuringCurveVecSize()<<endl;
			cout<<"Position in model: "<<m_curveDetectionLeft->getCurve(m_actualCurveNumber).getPositionOfPlayerDuringCurve(0)<<endl;
		}
		else{
			cout<<"Detected Serve from left player on other Court"<<endl;
		}
	}
}

void EventDetection::detecImpactAfterServeEvents(){
	Point2f impactPointOfActualCurve;
	cout<<"actualServePlayerRight: "<<m_actualServePlayerRight<<" m_actualStrokeIsRight: "<< m_actualCurveIsRight<<" getActualCurveCharacter: "<<m_actualCurveCharacter<<endl;
	if(m_actualServePlayerRight == false && m_actualCurveIsRight == true && m_actualCurveCharacter == IMPACT) {
		impactPointOfActualCurve = m_fieldDetectionRight->transformPointToModel(m_curveDetectionRight->getCurve(m_actualCurveNumber).getPointOfImpact());
		cout<<"impactPointOfActualCurve: "<<impactPointOfActualCurve<<endl;
		if(m_actualServePos == 2) {
			if(imageObjects::isInsideQuadrangle(m_fieldDetectionRight->getServeImpactZoneRight3(), impactPointOfActualCurve, m_lineWidth) == true) {
				appendImpactAfterServeEvent(true, 3, calculateProbabilityOfImpact(m_fieldDetectionRight->getServeImpactZoneRight3(), impactPointOfActualCurve, m_impactAfterServeProbabilityModulation));
			}
			else{
				appendImpactAfterServeEvent(false, 3, calculateProbabilityOfImpact(m_fieldDetectionRight->getServeImpactZoneRight3(), impactPointOfActualCurve, m_impactAfterServeProbabilityModulation));
				m_firstServeImpactWasOutside = true;
				m_firstServeImpactFrameCount = m_frameCount;
			}
		}
		else if(m_actualServePos == 1) {
			if(imageObjects::isInsideQuadrangle(m_fieldDetectionRight->getServeImpactZoneRight4(), impactPointOfActualCurve, m_lineWidth) == true) {
				appendImpactAfterServeEvent(true, 4, calculateProbabilityOfImpact(m_fieldDetectionRight->getServeImpactZoneRight4(), impactPointOfActualCurve, m_impactAfterServeProbabilityModulation));
			}
			else{
				appendImpactAfterServeEvent(false, 4, calculateProbabilityOfImpact(m_fieldDetectionRight->getServeImpactZoneRight4(), impactPointOfActualCurve, m_impactAfterServeProbabilityModulation));
				m_firstServeImpactWasOutside = true;
				m_firstServeImpactFrameCount = m_frameCount;
			}
		}
	}
	else if(m_actualServePlayerRight == true && m_actualCurveIsRight == false && m_actualCurveCharacter == IMPACT) {
		impactPointOfActualCurve = m_fieldDetectionLeft->transformPointToModel(m_curveDetectionLeft->getCurve(m_actualCurveNumber).getPointOfImpact());
		cout<<"impactPointOfActualCurve: "<<impactPointOfActualCurve<<endl;
		if(m_actualServePos == 3) {
			if(imageObjects::isInsideQuadrangle(m_fieldDetectionLeft->getServeImpactZoneLeft2(), impactPointOfActualCurve, m_lineWidth) == true) {
				appendImpactAfterServeEvent(true, 2, calculateProbabilityOfImpact(m_fieldDetectionLeft->getServeImpactZoneLeft2(), impactPointOfActualCurve, m_impactAfterServeProbabilityModulation));
			}
			else{
				appendImpactAfterServeEvent(false, 2, calculateProbabilityOfImpact(m_fieldDetectionLeft->getServeImpactZoneLeft2(), impactPointOfActualCurve, m_impactAfterServeProbabilityModulation));
				m_firstServeImpactWasOutside = true;
				m_firstServeImpactFrameCount = m_frameCount;
			}
		}
		else if(m_actualServePos == 4) {
			if(imageObjects::isInsideQuadrangle(m_fieldDetectionLeft->getServeImpactZoneLeft1(), impactPointOfActualCurve, m_lineWidth) == true) {
				appendImpactAfterServeEvent(true, 1, calculateProbabilityOfImpact(m_fieldDetectionLeft->getServeImpactZoneLeft1(), impactPointOfActualCurve, m_impactAfterServeProbabilityModulation));
			}
			else{
				appendImpactAfterServeEvent(false, 1, calculateProbabilityOfImpact(m_fieldDetectionLeft->getServeImpactZoneLeft1(), impactPointOfActualCurve, m_impactAfterServeProbabilityModulation));
				m_firstServeImpactWasOutside = true;
				m_firstServeImpactFrameCount = m_frameCount;
			}
		}
	}
}


void EventDetection::detecImpactEvents(){
	//TODO
	Point2f impactPointOfActualCurve;

	if(m_actualCurveIsRight == true && m_actualCurveCharacter == IMPACT) {
		impactPointOfActualCurve = m_fieldDetectionRight->transformPointToModel(m_curveDetectionRight->getCurve(m_actualCurveNumber).getPointOfImpact());
		cout<<"ImpactPoint: "<<impactPointOfActualCurve<<endl;
		//impact Right inside single field
		if(imageObjects::isInsideQuadrangle(m_fieldDetectionRight->getRightSingelFieldPoints(), impactPointOfActualCurve, m_lineWidth) == true) {
			appendImpactEvent(true, calculateProbabilityOfImpact(m_fieldDetectionRight->getRightSingelFieldPoints(), impactPointOfActualCurve, m_impactProbabilityModulation));
		}
		//impact Right outside single field
		else if(imageObjects::isInsideQuadrangle(m_fieldDetectionRight->getCourtBoundry(), impactPointOfActualCurve, m_lineWidth) == true){
			cout<<"Impact Outside Field"<<endl;
			appendImpactEvent(false, calculateProbabilityOfImpact(m_fieldDetectionRight->getRightSingelFieldPoints(), impactPointOfActualCurve, m_impactProbabilityModulation));
		}
	}
	else if(m_actualCurveIsRight == false && m_actualCurveCharacter == IMPACT) {
		impactPointOfActualCurve = impactPointOfActualCurve = m_fieldDetectionLeft->transformPointToModel(m_curveDetectionLeft->getCurve(m_actualCurveNumber).getPointOfImpact());
		cout<<"ImpactPoint: "<<impactPointOfActualCurve<<endl;
		if(imageObjects::isInsideQuadrangle(m_fieldDetectionLeft->getLeftSingelFieldPoints(), impactPointOfActualCurve, m_lineWidth) == true) {
			appendImpactEvent(true, calculateProbabilityOfImpact(m_fieldDetectionLeft->getLeftSingelFieldPoints(), impactPointOfActualCurve, m_impactProbabilityModulation));
		}
		//impact Right outside single field
		else if(imageObjects::isInsideQuadrangle(m_fieldDetectionLeft->getCourtBoundry(), impactPointOfActualCurve, m_lineWidth) == true){
			cout<<"Impact Outside Field"<<endl;
			appendImpactEvent(false, calculateProbabilityOfImpact(m_fieldDetectionLeft->getLeftSingelFieldPoints(), impactPointOfActualCurve, m_impactProbabilityModulation));
		}
	}
}

void EventDetection::detecRallyEvents(){

	detecStrokeEvent();
	detecOverNetEvent();

	bool curveBelongsToPlayerInsideCourt = false;
	if(m_actualCurveIsRight == true && m_actualCurveCharacter == RALLY_ENDING_RIGHT) {
		curveBelongsToPlayerInsideCourt = checkPlayerIsInsideCourt(true, m_actualCurveNumber, true);
	}
	else if(m_actualCurveIsRight == false && m_actualCurveCharacter == RALLY_ENDING_LEFT) {
		curveBelongsToPlayerInsideCourt = checkPlayerIsInsideCourt(false, m_actualCurveNumber, true);
	}
	if(curveBelongsToPlayerInsideCourt == true){
		appendRallyEndingEvent();
		m_serveDetected = false;
	}
}

void EventDetection::detecStrokeEvent(){
	if(m_actualCurveIsRight == true && m_actualCurveCharacter == STROKE_RIGHT_PLAYER) {
		if(checkPlayerIsInsideCourt(true, m_actualCurveNumber, true) == true){
			appendStrokeEvent(m_curveDetectionRight->getCurve(m_actualCurveNumber).getSpeed());
			m_serveDetected = false;
		}
	}
	else if(m_actualCurveIsRight == false && m_actualCurveCharacter == STROKE_LEFT_PLAYER) {
		if(checkPlayerIsInsideCourt(false, m_actualCurveNumber, true) == true){
			appendStrokeEvent(m_curveDetectionLeft->getCurve(m_actualCurveNumber).getSpeed());
			m_serveDetected = false;
		}
	}
}

void EventDetection::detecOverNetEvent(){
	int numberOfPositionsInsideField = 0;
	if(m_actualCurveIsRight == true && m_actualCurveCharacter == FROM_RIGHT_OVER_NET) {
		for(int i = 0; i < 4; i++){
			Point2f positionAtModel = m_curveDetectionRight->getCurve(m_actualCurveNumber).getPositionOfPlayerDuringCurve(i);
			if(imageObjects::isInsideQuadrangle(m_fieldDetectionRight->getCourtBoundry(), positionAtModel, m_lineWidth) == true) {
				numberOfPositionsInsideField++;
			}
		}
	}
	else if(m_actualCurveIsRight == false && m_actualCurveCharacter == FROM_LEFT_OVER_NET) {
		for(int i = 0; i < 4; i++){
			Point2f positionAtModel = m_curveDetectionLeft->getCurve(m_actualCurveNumber).getPositionOfPlayerDuringCurve(i);
			if(imageObjects::isInsideQuadrangle(m_fieldDetectionLeft->getCourtBoundry(), positionAtModel, m_lineWidth) == true) {
				numberOfPositionsInsideField++;
			}
		}
	}
	if(numberOfPositionsInsideField >= 2){
		m_frameCountCurveOverNet = m_frameCount;
		appendOverNetEvent();
	}
}

void EventDetection::detecInNetEvent(){
	if(m_frameCountCurveOverNet > 0 && m_frameCount - m_frameCountCurveOverNet > 130){
		appendRallyEndingEvent();
		m_serveDetected = false;
		m_frameCountCurveOverNet = -1;
	}
}


void EventDetection::detecChangeOfServiceEvents(){
	//TODO
	if(m_servesPerSide.size() > 1 && m_actualNumberOfServiceSides < m_servesPerSide.size()) {
		if(m_servesPerSide.back() >= m_minimumServesForSafeChangeOfServes) {
			m_actualNumberOfServiceSides = m_servesPerSide.size();
			appendEndOfGameEvent(m_frameCountChangeOfServe, m_insertPositionChangeOfServesEvent.back(), true);
		}
	}
}

void EventDetection::detecChangeOfSideEvent(){
	//TODO
	if(m_frameCountsChangeOfSides.empty() == false && m_events.empty() == false){
		if(m_events.back().getNumberOfFrame() > m_frameCountsChangeOfSides.front()){
			int insertIndex = 0;
			for(int i = 0; i <m_events.size(); i++){
				if(m_events[i].getNumberOfFrame() < m_frameCountsChangeOfSides.front()){
					insertIndex = i;
				}
			}
			insertIndex++;
			appendEndOfGameEvent(m_frameCountsChangeOfSides.front(), insertIndex, false);
			m_mutexChangeOfSides.lock();
			m_frameCountsChangeOfSides.erase(m_frameCountsChangeOfSides.begin());
			m_mutexChangeOfSides.unlock();
		}
	}
}

void EventDetection::categoriseStroke(const vector<Mat> &bufferRight, const vector<Mat> &bufferLeft){
	//TODO
	if(m_actualCurveCharacter == STROKE_LEFT_PLAYER || m_actualCurveCharacter == STROKE_RIGHT_PLAYER || m_actualCurveCharacter == SERVE_LEFT_PLAYER || m_actualCurveCharacter == SERVE_RIGHT_PLAYER) {

		vector<vector<Rect> > boundRects;
		vector<Mat> imgBuffer;
		uint32_t numberOfSnappedImages;
		m_actualStrokeNameFrameCount = m_actualCurveFrameCount;
		if(m_actualCurveIsRight == true) {
			imgBuffer = bufferRight;
			boundRects = m_playerSegmentationRight->getHumanMaskBoundRectsOfFrames();
		}
		else{
			imgBuffer = bufferLeft;
			boundRects = m_playerSegmentationLeft->getHumanMaskBoundRectsOfFrames();
		}

		if(m_actualCurveCharacter == SERVE_LEFT_PLAYER || m_actualCurveCharacter == SERVE_RIGHT_PLAYER) {
			numberOfSnappedImages = 15;
		}
		else{
			numberOfSnappedImages = 5;
		}

		Rect boundRect;
		boundRect.height = 0;
		vector<int> categoryCount;
		for(int i = 0; i < 6; i++) {
			categoryCount.push_back(0);
		}
		for(uint32_t i = 0; i < numberOfSnappedImages; i++) {
			if(i < imgBuffer.size()-1) {
				boundRect.height = 0;
				//TODO: fehleranfällig, da immer die größte roi nicht aber die die dem schlag am nächsten war gewertet wird.
				for(uint32_t j = 0; j <boundRects[i + m_frameCount - m_framesPrediction].size(); j++) {
					if(boundRect.height < boundRects[i + m_frameCount - m_framesPrediction][j].height) {
						boundRect = boundRects[i + m_frameCount - m_framesPrediction][j];
					}
				}
				Point2f humanStandingPosition = Point2f(boundRect.x + boundRect.width / 2, boundRect.y + boundRect.height);
				cout<<"HumanStandingPosition: "<<humanStandingPosition<<endl;
				float width = boundRect.height*1.0;
				float x = boundRect.x + boundRect.width/2 - width/2;
				if(x < 0) {
					x = 0;
				}
				if(x + width > m_frameWidth) {
					width = m_frameWidth - x-1;
				}
				boundRect.x = x;
				boundRect.width = width;

				if(boundRect.width > 0 && boundRect.height > 0) {
					Mat originalImgRoi = imgBuffer[i].clone();
					cout<<boundRect.x <<" "<<boundRect.y<<" "<<boundRect.height<<" "<<boundRect.width<<endl;
					Mat roi = originalImgRoi(boundRect);

					double sclHeight = static_cast<double>(48) / static_cast<double>(roi.rows);
					double sclWidth = static_cast<double>(48) / static_cast<double>(roi.cols);
					resize(roi, roi, cv::Size(0,0), sclWidth, sclHeight, cv::INTER_LINEAR);
					m_walter->forward(roi);

					m_walter->getFCLayer7()->printOutput();

					categoryCount[m_walter->getFCLayer7()->getDetection()]++;
				}
			}
		}
		int strokeCharacterNumber = distance(categoryCount.begin(), max_element(categoryCount.begin(), categoryCount.end()));
		string actualStrokeName = m_walter->getStrokeName(strokeCharacterNumber);

		cout<<"WalterCategoryNumber: "<<strokeCharacterNumber<<endl;
		cout<<"WalterCategorisation: "<<actualStrokeName<<endl;
		m_strokeEvents.push_back(StrokeEvent(strokeCharacterNumber, m_actualCurveFrameCount, m_actualCurveIsRight));
		m_actualStrokeType = strokeCharacterNumber;
	}
}

void EventDetection::updateActualCurve(){
	int curveNumberRight = -1;
	int curveNumberLeft = -1;

	m_actualCurveNumber = -1;

	int curveFrameCountRight = 0;
	int curveFrameCountLeft = 0;

	int curveCharacterRight;
	int curveCharacterLeft;

	const vector<Curve> &curvesRight = m_curveDetectionRight->getCurves();
	const vector<Curve> &curvesLeft = m_curveDetectionLeft->getCurves();

	if(curvesRight.size() > 0) {
		bool foundRight = false;
		int32_t i = (int32_t)curvesRight.size()-1;
		while(foundRight == false && i >= 0) {
			int curveFrameCountRightTmp = curvesRight[i].getLastFrameCount();
			curveCharacterRight = getCurveCharacter(curvesRight[i].getCharacterisation());
			if(curveCharacterRight!= DEFAULT && curveCharacterRight != STROKE_LEFT_PLAYER && curveFrameCountRightTmp < m_frameCount - m_framesPrediction) {
				curveNumberRight = i;
				curveFrameCountRight = curveFrameCountRightTmp;
				foundRight = true;
			}
			i--;
		}
	}
	if(curvesLeft.size() > 0) {
		bool foundLeft = false;
		int i = (int32_t)curvesLeft.size()-1;
		while(foundLeft == false && i >= 0) {
			int curveFrameCountLeftTmp = curvesLeft[i].getLastFrameCount();
			curveCharacterLeft = getCurveCharacter(curvesLeft[i].getCharacterisation());
			if(curveCharacterLeft != DEFAULT && curveCharacterLeft != STROKE_RIGHT_PLAYER && curveFrameCountLeftTmp < m_frameCount - m_framesPrediction) {
				curveNumberLeft = i;
				curveFrameCountLeft = curveFrameCountLeftTmp;
				foundLeft = true;
			}
			i--;
		}
	}
	cout<<"CurveCountRight "<<curveFrameCountRight<<" CurveCountLeft "<<curveFrameCountLeft<<" imgCount between: "<<(m_frameCount -140)<<" and: "<<(m_frameCount - m_framesPrediction)<<endl;
	if(((curveFrameCountLeft > m_frameCount -140 && curveFrameCountLeft < m_frameCount - m_framesPrediction)  || (curveFrameCountRight > m_frameCount - 140 && curveFrameCountRight < m_frameCount - m_framesPrediction))) {

		if(curveFrameCountRight >= curveFrameCountLeft && curveFrameCountRight < m_frameCount - m_framesPrediction) {
			m_actualCurveNumber = curveNumberRight;
			m_actualCurveIsRight = true;
			m_actualCurveFrameCount = curveFrameCountRight;
			m_actualCurveCharacter = curveCharacterRight;
		}
		else if(curveFrameCountLeft > curveFrameCountRight && curveFrameCountLeft < m_frameCount - m_framesPrediction) {
			m_actualCurveNumber = curveNumberLeft;
			m_actualCurveIsRight = false;
			m_actualCurveFrameCount = curveFrameCountLeft;
			m_actualCurveCharacter = curveCharacterLeft;
		}
	}
}

void EventDetection::updateServeCounter(int servePos){
	//TODO Anpassen, dass hier noch Doppelte serves mit gecountet werden.
	uint gameCount = m_servesPerSide.size();
	if(m_frameCountChangeOfServe == -1) {
		m_servesPerSide.push_back(1);
		m_insertPositionChangeOfServesEvent.push_back(0);
		m_frameCountsChangeOfServes.push_back(m_actualCurveFrameCount);
		m_frameCountChangeOfServe = m_actualCurveFrameCount;
		m_serveIsRight = m_actualCurveIsRight;
	}
	else if(m_serveIsRight == m_actualCurveIsRight) {
		m_servesPerSide.back()++;
	}
	else if(m_serveIsRight != m_actualCurveIsRight && (servePos == 2 || servePos == 3) && m_servesPerSide.back() >= 2) {
		m_servesPerSide.push_back(1);
		int insertPosition = ((int)m_events.size()-1) > 0 ? (int)m_events.size() : 0;
		m_insertPositionChangeOfServesEvent.push_back(insertPosition);
		m_frameCountsChangeOfServes.push_back(m_actualCurveFrameCount-1);
		m_frameCountChangeOfServe = m_frameCountsChangeOfServes.back();
		m_serveIsRight = m_actualCurveIsRight;
	}
	else if(m_serveIsRight != m_actualCurveIsRight && (servePos == 2 || servePos == 3) && gameCount > 2) {
		if(m_servesPerSide[gameCount-2] >= 2) {
			m_servesPerSide.pop_back();
			m_frameCountsChangeOfServes.pop_back();
			m_insertPositionChangeOfServesEvent.pop_back();
			m_servesPerSide.back()++;
			m_frameCountChangeOfServe = m_frameCountsChangeOfServes.back();
		}
		else{
			m_servesPerSide.push_back(1);
			int insertPosition = ((int)m_events.size()-1) > 0 ? (int)m_events.size()-1 : 0;
			m_insertPositionChangeOfServesEvent.push_back(insertPosition);
			m_frameCountChangeOfServe = m_frameCountsChangeOfServes.back();
		}
		m_serveIsRight = m_actualCurveIsRight;
	}
}

void EventDetection::appendServeEvent(int frameCount, int servePos, float speed){
	if(m_events.size() > 0){
		if(m_events.back().getEventType() == SERVE && frameCount - m_events.back().getNumberOfFrame() < m_minServeFrameCountDiff){
			m_events.pop_back();
		}
		else{
			updateServeCounter(servePos);

			if(m_firstServeImpactWasOutside == true && servePos != m_actualServePos && m_actualServeTry == 1){
				appendContinuationOfRallyEvent();
			}
			if(servePos == m_actualServePos){
				m_actualServeTry++;
			}
			else{
				m_actualServeTry = 1;
			}
		}
	}
	else{
		m_actualServeTry = 1;
	}
	m_actualServePos = servePos;
	m_serveDetected = true;
	m_rallyIsRunning = true;
	m_actualServePlayerRight = m_actualCurveIsRight;
	m_firstServeImpactWasOutside = false;
	Event serveEvent = Event(SERVE, frameCount, m_actualCurveIsRight);
	serveEvent.setServeZoneNumber(servePos);
	serveEvent.setStrokeType(SERVE_STROKE);
	serveEvent.setStrokeSpeed(speed);
	serveEvent.setProbabilityEventHappendToSafe();
	m_events.push_back(serveEvent);
}

void EventDetection::appendImpactAfterServeEvent(bool isInsideServeImpactZone, int serveImpactZone, float probabilityEventHappend){
	Event impactAfterServeEvent = Event(IMPACT_AFTER_SERVE, m_actualCurveFrameCount, m_actualCurveIsRight);
	impactAfterServeEvent.setServeImpactZone(serveImpactZone);
	impactAfterServeEvent.setIsInsideServeImpactZone(isInsideServeImpactZone);
	impactAfterServeEvent.setProbabilityEventHappend(probabilityEventHappend);
	m_events.push_back(impactAfterServeEvent);
	m_insertPositionContinuationOfRally = m_events.size();

	m_serveDetected = false;
}

void EventDetection::appendStrokeEvent(float speed){
	Event strokeEvent = Event(STROKE, m_actualCurveFrameCount, m_actualCurveIsRight);
	strokeEvent.setProbabilityEventHappendToSafe();
	strokeEvent.setStrokeType(m_actualStrokeType);
	strokeEvent.setStrokeSpeed(speed);

	m_events.push_back(strokeEvent);
}

void EventDetection::appendOverNetEvent(){
	Event overNetEvent = Event(OVER_NET, m_actualCurveFrameCount, m_actualCurveIsRight);
	overNetEvent.setProbabilityEventHappendToSafe();
	m_events.push_back(overNetEvent);
}

void EventDetection::appendImpactEvent(bool isInsideSingleField, float probabilityEventHappend){
	Event impactEvent = Event(CRITICAL_IMPACT, m_actualCurveFrameCount, m_actualCurveIsRight);
	impactEvent.setIsInsideSingleField(isInsideSingleField);
	impactEvent.setProbabilityEventHappend(probabilityEventHappend);
	m_events.push_back(impactEvent);
}

void EventDetection::appendRallyEndingEvent(){
	Event rallyEndingEvent = Event(END_OF_RALLY, m_actualCurveFrameCount, m_actualCurveIsRight);
	rallyEndingEvent.setProbabilityEventHappendToSafe();
	m_events.push_back(rallyEndingEvent);

	m_rallyIsRunning = false;
}

void EventDetection::appendContinuationOfRallyEvent(){
	cout<<"Appending continuationOfRallyEvent"<<endl;
	Event continuationOfRallyEvent = Event(CONTINUATION_OF_RALLY, m_firstServeImpactFrameCount + 1, m_actualCurveIsRight);
	continuationOfRallyEvent.setProbabilityEventHappendToSafe();
	m_events.insert(m_events.begin() + m_insertPositionContinuationOfRally, continuationOfRallyEvent);
}

void EventDetection::appendEndOfGameEvent(int frameCount, int positionInEventVec, bool isChangeOfServe){
	Event gameEndingEvent = Event(END_OF_GAME, frameCount, m_actualCurveIsRight);
	gameEndingEvent.setIsChangeOfServe(isChangeOfServe);
	gameEndingEvent.setProbabilityEventHappendToSafe();
	if(positionInEventVec >= 0){
		m_events.insert(m_events.begin() + positionInEventVec, gameEndingEvent);
	}
	else{
		m_events.push_back(gameEndingEvent);
	}
}

void EventDetection::appendEndOfMatchEvent(int frameCount){
	Event matchEndingEvent = Event(END_OF_MATCH, frameCount, m_actualCurveIsRight);
	matchEndingEvent.setProbabilityEventHappendToSafe();
	m_events.push_back(matchEndingEvent);
	cout<<"ActualServeTry: "<<m_actualServeTry<<" FirstServeImpactWasOutside: "<<m_firstServeImpactWasOutside<<endl;
	if(m_firstServeImpactWasOutside == true && m_actualServeTry == 1){
		appendContinuationOfRallyEvent();
	}
}

void EventDetection::addChangeOfSideEvent(int frameCount){
	m_mutexChangeOfSides.lock();
	m_frameCountsChangeOfSides.push_back(frameCount);
	m_mutexChangeOfSides.unlock();
}

bool EventDetection::checkPlayerIsInsideCourt(bool curveIsRight, int numberOfCurve, bool checkLastPoints){
	cout<<"checkPlayerIsInsideCourt"<<endl;
	int numberOfPositionsInsideField = 0;
	int begin = 0;
	int end = 4;
	if(curveIsRight == true) {
		if(checkLastPoints == true){
			begin = (int)m_curveDetectionRight->getCurve(m_actualCurveNumber).getPositionsOfPlayerDuringCurveVecSize() - 4;
			end = (int)m_curveDetectionRight->getCurve(m_actualCurveNumber).getPositionsOfPlayerDuringCurveVecSize();
		}
		for(int i = begin; i < end; i++){
			Point2f positionAtModel = m_curveDetectionRight->getCurve(m_actualCurveNumber).getPositionOfPlayerDuringCurve(i);
			cout<<"positionAtModel: "<<positionAtModel<<endl;
			if(imageObjects::isInsideQuadrangle(m_fieldDetectionRight->getCourtBoundry(), positionAtModel, m_lineWidth) == true) {
				numberOfPositionsInsideField++;
			}
		}
		if(numberOfPositionsInsideField >= 2){
			return true;
		}
		else{
			return false;
		}
	}
	else{
		if(checkLastPoints == true){
			begin = (int)m_curveDetectionLeft->getCurve(m_actualCurveNumber).getPositionsOfPlayerDuringCurveVecSize() - 4;
			end = (int)m_curveDetectionLeft->getCurve(m_actualCurveNumber).getPositionsOfPlayerDuringCurveVecSize();
		}
		for(int i = begin; i < end; i++){
			Point2f positionAtModel = m_curveDetectionLeft->getCurve(m_actualCurveNumber).getPositionOfPlayerDuringCurve(i);
			cout<<"positionAtModel: "<<positionAtModel<<endl;
			if(imageObjects::isInsideQuadrangle(m_fieldDetectionLeft->getCourtBoundry(), positionAtModel, m_lineWidth) == true) {
				numberOfPositionsInsideField++;
			}
		}
		if(numberOfPositionsInsideField >= 2){
			return true;
		}
		else{
			return false;
		}
	}
}

float EventDetection::calculateProbabilityOfImpact(const vector<Point2f> &impactField, Point2f impactPoint, float modulation){
	float probability = imageMath::calculateTanh(abs(pointPolygonTest(impactField, impactPoint, true)), modulation);
	if(probability > 0.99){
		return 0.99f;
	}
	else{
		return probability;
	}
}

void EventDetection::printEvents(){
	for(int i = 0; i < m_events.size(); i++) {
		cout<<"Event "<<i<<": frameCount: "<<m_events[i].getNumberOfFrame()<<" Type: "<<getEventName(m_events[i].getEventType())<<"         probability: "<<m_events[i].getProbabilityEventHappend()<<" ServeZone: "<<m_events[i].getServeZoneNumber()<<" inServeImpactZone: "<<m_events[i].getIsInsideServeImpactZone()<<endl;
	}
	cout<<endl;
	for(int i = 0; i < m_servesPerSide.size(); i++) {
		cout<<"Game: "<<i<<" Serves: "<<m_servesPerSide[i]<<endl;
	}
}

int EventDetection::getCurveCharacter(int curveCharacterisationNumber){
	int curveCharacter;

	switch(curveCharacterisationNumber) {
	case 7: curveCharacter = IMPACT; break;
	case 8: curveCharacter = IMPACT; break;
	case 9: curveCharacter = STROKE_LEFT_PLAYER; break;
	case 10: curveCharacter = STROKE_RIGHT_PLAYER; break;
	case 11: curveCharacter = STROKE_LEFT_PLAYER; break;
	case 12: curveCharacter = STROKE_RIGHT_PLAYER; break;
	case 13: curveCharacter = STROKE_LEFT_PLAYER; break;
	case 14: curveCharacter = STROKE_RIGHT_PLAYER; break;
	case 15: curveCharacter = STROKE_LEFT_PLAYER; break;
	case 16: curveCharacter = STROKE_RIGHT_PLAYER; break;
	case 17: curveCharacter = FROM_RIGHT_OVER_NET; break;
	case 18: curveCharacter = FROM_LEFT_OVER_NET; break;
	case 19: curveCharacter = RALLY_ENDING_RIGHT; break;
	case 20: curveCharacter = RALLY_ENDING_LEFT; break;
	case 21: curveCharacter = SERVE_LEFT_PLAYER; break;
	case 22: curveCharacter = SERVE_RIGHT_PLAYER; break;
	default: curveCharacter = DEFAULT; break;
	}
	return curveCharacter;
}

string EventDetection::getEventName(int eventNumber){
	string eventCharacter;
	switch(eventNumber) {
	case STROKE: eventCharacter = "Stroke"; break;
	case SERVE: eventCharacter = "Serve"; break;
	case IMPACT_AFTER_SERVE: eventCharacter = "Impact after serve"; break;
	case OVER_NET: eventCharacter = "Over net"; break;
	case CRITICAL_IMPACT: eventCharacter = "Critical impact"; break;
	case END_OF_RALLY: eventCharacter = "End of rally"; break;
	case CONTINUATION_OF_RALLY: eventCharacter = "Continuation of Rally"; break;
	case END_OF_GAME: eventCharacter = "End of game"; break;
	case END_OF_SET: eventCharacter = "End of set"; break;
	case END_OF_MATCH: eventCharacter = "End of match"; break;
	default: eventCharacter = "Default"; break;
	}
	return eventCharacter;
}
