//
//  curveDetection.cpp
//
//
//  Created by Henri Kuper on 23.06.17.
//
//

#include "curveDetection.hpp"

using namespace cv;
using namespace std;
using namespace imgProc;
//#define GPU_ON

/*
    TODO:
    //Character
    - serves nicht also solche erkennen, wenn der spieler unter halb des kopfes aufschlägt.
    - integration von senkrecht in die luft fliegenden aufschlagkurven in eine einzige Senkrecht steigende oder fallende kurve.
    - Einführung von Aufschlagerkennung, wenn nur die Flugbahn des schon geschlagenen Balles erkannt wird.
    - Einführen von Aufschlagerkennung, wenn nur der hochgeworfene Ball über dem Kopf erkannt wird, als Potenzieller Aufschlag, der vom Netz verifiziert weden soll.
    - Winkelberechnung so anpassen, das nicht die vorletzten beiden Punkte sondern, ein durchschnitt der Kurve einbezogen wird.

    //Speed:
    - Bei Kurven, die erst einige Bilder nach dem abschlag punkt erkannt werden, nimmt er trotzdem die Spieler position an, die der Spieler beim ersten Punkt der Kurve hat. Dies führt dazu, dass der Ball schon einiges an weg zurückgelegt hat,wenn die Zeitmessung beginnt -> erhöhte Geschwindigkeiten möglich.

    //Impact Point:
    - better calculation of the time of impact points mit quadratischer regression
    - quadratische Regression für bessere gesamtflugbahnbestimmung(geringere Abweichung-> passende flugbahn)

    //Curve Verwaltung:
    - Testen ob die funktion kurvenentfernen zum mittelpunkt und dann löschen wirklich nötig ist, oder ob sie nur schaden anrichtet, weil die meisten falschen kurven von abstand im schnitt zur maske abgedeckt ist.
    - durchlaufen der Curven Vectoren muss optimiert werden. Definitv abgeschlossene Kurven sollen nicht mehr mit einbezogen werden in die Berechnungen.
    - Rausfiltern von zeitlich überlappend verlaufenden Kurven.

    //Merging
    - Kurven mit entgegengesetzter flugrichtung nicht mergen. Ebenso wie entgegengesetzter höhenkurve

 */

CurveDetection::CurveDetection(int height, int width, FieldDetection *field, PlayerSegmentation *player, BallDetection *ball) : m_frameHeight(height), m_frameWidth(width), m_fieldDetection(field), m_playerSegmentation(player), m_ballDetection(ball){
	curveDetectionInit();
}

CurveDetection::~CurveDetection(){

}

void CurveDetection::curveDetectionInit(){
	m_inputVideoResizingFactor = (float) m_frameWidth/1280.0f ;
	cout<<"CurveDetection m_inputVideoResizingFactor: "<<m_inputVideoResizingFactor<<endl;

	//m_ballDetection = new BallDetection(m_frameHeight, m_frameWidth, m_fieldDetection, m_playerSegmentation);
	//m_fieldModel = m_fieldDetection->getFieldModel();
	m_fieldModel = m_fieldDetection->getFieldModelSmall();
	m_isRightFieldSide = m_fieldDetection->getIsRightFieldSide();

	m_resizingFactor = 1.0f;

	m_maxFrameCountDiffForNewPoint = 7;//7
	m_minimumCurvePoints = 4;
	m_frameCountForErronousCurveDelete = 4; //9
	m_ballRadiusFactor = 0.29; //0.28

	m_framesPredictionCount = 40;//40

	m_yDiffWhichLeadsToImpact = 45 * m_inputVideoResizingFactor;//50
	m_frameCountWhichLeadsToFieldChanging = 30;//40
	m_frameCountWhichLeadsToEndCurve = 30;//40

	m_avarageDistMultMaxDistToPlayer = 120 * 140 * m_inputVideoResizingFactor;
	m_avarageDistMaxToPlayerContour = 20 * m_inputVideoResizingFactor; //20
	m_maxCurveDeleteIndex = 400.0f; //40

	m_alreadyCharacterisedCurve = 0;
	m_alreadyFinishedCurve = 0;

	m_frameCount = 0;

	m_calculatingFlightPathTime = 0;
	m_deleteErrorCurvesTime = 0;
	m_deleteDoublicatedCurvesTime = 0;
	m_characterisCurvesTime = 0;
	m_updateCurvesTime = 0;
	m_drawCurvesTime = 0;

	vector<Point2f> fourCornersOfCenterField = m_fieldDetection->getFourCornersOfCenterField();
	if(fourCornersOfCenterField.size() == 4) {
		float serveHeight = 80.0f * m_inputVideoResizingFactor;
		float impactHeight = 35.0f * m_inputVideoResizingFactor;
		if(m_isRightFieldSide == true && fourCornersOfCenterField[1].y - serveHeight > 0.0) {
			m_minHeightOfServe = fourCornersOfCenterField[1].y -serveHeight;
			m_yMaxWhichLeadsToImpact = fourCornersOfCenterField[1].y - impactHeight;
			cout<<"m_minHeightOfServe: "<<m_minHeightOfServe<<endl;
		}
		else if(m_isRightFieldSide == false && fourCornersOfCenterField[0].y - serveHeight > 0.0) {
			m_minHeightOfServe = fourCornersOfCenterField[0].y - serveHeight;
			m_yMaxWhichLeadsToImpact = fourCornersOfCenterField[1].y - impactHeight;
			cout<<"m_minHeightOfServe: "<<m_minHeightOfServe<<endl;
		}
		else{
			cout<<"check if the fourCornersOfCenterField are correctly localised"<<endl;
			m_minHeightOfServe = m_frameHeight/2;
			m_yMaxWhichLeadsToImpact = m_frameHeight/2;
		}
	}
	else{
		cout<<"FourCornersOfCenterField are not calibrated."<<endl;
		m_minHeightOfServe = m_frameHeight;
		m_yMaxWhichLeadsToImpact = m_frameHeight;
	}
	m_drawnCurves = Mat::zeros((int)(m_frameHeight*m_resizingFactor), (int)(m_frameWidth*m_resizingFactor), CV_8UC3);
}

void CurveDetection::updateFinishedCurves(){
	for(int i = m_alreadyFinishedCurve; i < m_curves.size(); i++){
		if(m_curves[i].getIsFinished() == false && m_curves[i].getLastFrameCount() < m_frameCount - m_maxFrameCountDiffForNewPoint){
			m_curves[i].setFinished();
		}
	}
	int i = m_alreadyFinishedCurve;
	bool foundUnfinishedCurve = false;
	while(i < m_curves.size() && foundUnfinishedCurve == false){
		if(m_curves[i].getIsFinished() == false){
			foundUnfinishedCurve = true;
		}
		m_alreadyFinishedCurve = i;
		i++;
	}
	cout<<"Curvesize"<<m_curves.size()<<endl;
	cout<<"AlreadyFinishedCurve: "<<m_alreadyFinishedCurve<<endl;
}

void CurveDetection::updateCharacterisedCurves(){
	int i = m_alreadyCharacterisedCurve;
	bool foundUncharacterisedCurve = false;
	while(i < m_curves.size() && foundUncharacterisedCurve == false){
		if(m_curves[i].getCharacterisation() == -1){
			foundUncharacterisedCurve = true;
		}
		m_alreadyCharacterisedCurve = i;
		i++;
	}
	cout<<"AlreadyCharacterisedCurve: "<<m_alreadyCharacterisedCurve<<endl;
}

float CurveDetection::calculateDerivativeWithRespectToImageCount(float x0, float x1, int imgCount0, int imgCount1){
	return (x1-x0)/(float)(imgCount1-imgCount0);
}

void CurveDetection::detecCurves(){
#ifdef GPU_ON
	cout<<"detecBallWithDiffGPU"<<endl;
	m_ballDetection->detecBallWithDiffGPU(originalFrameBuffer);
#endif
	cout<<"CurveDetection"<<endl;

	//TODO Richtig einbinden
	//m_drawnBallCenters.push_back(Mat::zeros((int)(m_frameHeight*m_resizingFactor),(int)(m_frameWidth*m_resizingFactor), CV_8UC3));
	m_drawnBallCenters.push_back(vector<Point2f>());
	m_drawnBallCentersRadius.push_back(vector<float>());

	if(m_frameCount > 2) {
		m_tmCalculatingFlightPath.reset();m_tmCalculatingFlightPath.start();
		calculatingFlightPath();
		m_tmCalculatingFlightPath.stop();

		m_tmDeleteErrorCurves.reset();m_tmDeleteErrorCurves.start();
		deleteErronousFlightCurves();
		m_tmDeleteErrorCurves.stop();

		m_tmDeleteDoublicatedCurves.reset();m_tmDeleteDoublicatedCurves.start();
		deleteDublicatedFlightCurves();
		m_tmDeleteDoublicatedCurves.stop();

		m_tmCharacteriseCurves.reset();m_tmCharacteriseCurves.start();
		characteriseCurve();
		m_tmCharacteriseCurves.stop();

		m_tmUpdateCurves.reset(); m_tmUpdateCurves.start();
		updateFinishedCurves();
		updateCharacterisedCurves();
		m_tmUpdateCurves.stop();

		//printCurves();
		m_tmDrawCurves.reset();m_tmDrawCurves.start();
		drawDetectedCurves();
		m_tmDrawCurves.stop();

		//printFPS();
	}
	m_frameCount++;
}

void CurveDetection::calculatingFlightPath(){
	cout<<"calculatingFlightPath"<<endl;
	//vector<Point2f> ballCentersOfActualImg =  m_possibleBallCenterOfFrames.back();
	//const vector<Point2f>& ballCentersOfActualImgROI =  m_ballDetection->getActualPossibleBallCenterROI();

	//Bevore adding the Points as new Curves to the flightCurves we check if the points fit to another existing curve.
	if(m_curves.size() > 0) {
		uint32_t originalFligthCurvesLength = m_curves.size();
		//
		vector<int> alreadyAddedNewPoint;
		for(uint32_t i = 0; i < originalFligthCurvesLength; i++) {
			alreadyAddedNewPoint.push_back(0);
		}

		//checking all FlightCurves if the actual point j fits to their curve.
		//if the flight curves length is only 1, it will be dublicated.
		cout<<"Default and HumanRoi FlightCurveDetection"<<endl;
		for(uint32_t i = m_alreadyFinishedCurve; i < m_curves.size(); i++) {
			// uint32_t ballCentersOfActualImgSize = ballCentersOfActualImg.size();
			Point2f ballCenterPrediction;
			if(m_curves[i].getBallPointsVecSize() > 1 && m_frameCount - m_curves[i].getLastFrameCount() <= m_maxFrameCountDiffForNewPoint){
				ballCenterPrediction = makeBallCenterPrediction(i);
			}
			//First check normal Area
			for(uint32_t j = 0; j < m_ballDetection->getActualBallCentersSize(); j++) {
				if(m_curves[i].getBallPointsVecSize() == 1) {
					//its important to stop at j == getActualBallCentersSize()-1 to add to all dublicates a point
					//so that no single Points of previous curves are left over
					if(j < m_ballDetection->getActualBallCentersSize()-1) {
						//dublicate values
						m_curves.insert(m_curves.begin() + i+1, m_curves[i]);
						alreadyAddedNewPoint.insert(alreadyAddedNewPoint.begin() + i+1, 0);
						m_curves[i].appendData(m_ballDetection->getActualPointDistToHumanMask(j), m_ballDetection->getActualPointPlayerPosition(j), m_ballDetection->getActualBallBoundRect(j), m_ballDetection->getActualBallCenter(j), m_frameCount);
						alreadyAddedNewPoint[i] = 1;
						i++;
					}
					//add new Value
					else{
						m_curves[i].appendData(m_ballDetection->getActualPointDistToHumanMask(j), m_ballDetection->getActualPointPlayerPosition(j), m_ballDetection->getActualBallBoundRect(j), m_ballDetection->getActualBallCenter(j), m_frameCount);
						alreadyAddedNewPoint[i] = 1;
					}
				}
				//--checked
				else if(m_curves[i].getBallPointsVecSize() > 1 && alreadyAddedNewPoint[i] != 1 && (m_frameCount - m_curves[i].getLastFrameCount()) < m_maxFrameCountDiffForNewPoint) {
					//TODO zu ende bringen
					bool successfullyAdded = proofBallDetection(m_ballDetection->getActualBallCenter(j), j, i, ballCenterPrediction, false);
					if(successfullyAdded){
						alreadyAddedNewPoint[i] = 2;	//case 2: temporary added new point
					}
				}
			}
			//Then check Human ROI Area
			for(uint32_t j = 0; j < m_ballDetection->getActualBallCentersROISize(); j++) {
				if(m_curves[i].getBallPointsVecSize() > 1 && alreadyAddedNewPoint[i] == 0 && m_curves[i].getIsFinished() == false) {
					//Filtering out Curves coming from above (abs. slope > 4) in Human Mask, that lead to cause troubles in serve detection
					if(abs(m_curves[i].getSlopeOfLastTwoBallPoints()) < 4){
						proofBallDetection(m_ballDetection->getActualBallCenterROI(j), j, i, ballCenterPrediction, true);
					}
				}
			}
		}
	}
	cout<<"NewFlightCurvesAdding"<<endl;
	//adding the Points as new Curves to the flightCurves
	for(uint32_t i = 0; i < m_ballDetection->getActualBallCentersSize(); i++) {
		//TODO kann man sich das kopiern mit einem direkten push in m_curves sparen?
		Curve curve(m_ballDetection->getActualBallCenter(i), m_frameCount, m_ballDetection->getActualPointDistToHumanMask(i), m_ballDetection->getActualPointPlayerPosition(i), m_ballDetection->getActualBallBoundRect(i));
		m_curves.push_back(curve);
	}
}

bool CurveDetection::proofBallDetection(Point2f ballCenter, int numberOfBallCenter,int curveNumber, Point2f ballCenterPrediction, bool inHumanMaskROI){

	//Point2f ballCenterPrediction = makeBallCenterPrediction(curveNumber);

	bool ballDetectionIsCorrect = detectedBallIsInPredictedBallCenterRadius(ballCenterPrediction, ballCenter, inHumanMaskROI);
	//if a ballcenter was already added, check which center fits better the prediction
	if(ballDetectionIsCorrect == true && m_curves[curveNumber].getLastFrameCount() == m_frameCount) {
		float differenceP0ToPrediction;
		imgProc::imageMath::calculateDistanceBetween(ballCenterPrediction, m_curves[curveNumber].getLastBallPoint(), differenceP0ToPrediction);
		float differenceP1ToPrediction;
		imgProc::imageMath::calculateDistanceBetween(ballCenterPrediction, ballCenter, differenceP1ToPrediction);
		if(differenceP1ToPrediction < differenceP0ToPrediction) {
			m_curves[curveNumber].deleteLastData();
			if(inHumanMaskROI){
				m_curves[curveNumber].appendData(m_ballDetection->getActualPointsDistToHumanMaskROI(numberOfBallCenter), m_ballDetection->getActualPointsPlayerPositionROI(numberOfBallCenter), m_ballDetection->getActualBallBoundRectsROI(numberOfBallCenter), ballCenter, m_frameCount);
			}
			else{
				m_curves[curveNumber].appendData(m_ballDetection->getActualPointDistToHumanMask(numberOfBallCenter), m_ballDetection->getActualPointPlayerPosition(numberOfBallCenter), m_ballDetection->getActualBallBoundRect(numberOfBallCenter), ballCenter, m_frameCount);
			}
		}
	}
	else if(ballDetectionIsCorrect == true) {
		if(inHumanMaskROI){
			m_curves[curveNumber].appendData(m_ballDetection->getActualPointsDistToHumanMaskROI(numberOfBallCenter), m_ballDetection->getActualPointsPlayerPositionROI(numberOfBallCenter), m_ballDetection->getActualBallBoundRectsROI(numberOfBallCenter), ballCenter, m_frameCount);
		}
		else{
			m_curves[curveNumber].appendData(m_ballDetection->getActualPointDistToHumanMask(numberOfBallCenter), m_ballDetection->getActualPointPlayerPosition(numberOfBallCenter), m_ballDetection->getActualBallBoundRect(numberOfBallCenter), ballCenter, m_frameCount);
		}
	}
	return ballDetectionIsCorrect;
}

Point2f CurveDetection::makeBallCenterPrediction(int curveNumber){
	//TODO: 1. Konzept und Umsetzung überarbeiten
	//Do linear regression if curve contains less than 3 points
	Point2f p0 = m_curves[curveNumber].getLastBallPoint();
	Point2f p1 = m_curves[curveNumber].getSecondLastBallPoint();
	Point2f p2;
	int imgCountP0 = m_curves[curveNumber].getLastFrameCount();
	int imgCountP1 = m_curves[curveNumber].getSecondLastFrameCount();
	int imgCountP2;

	float xPreviousDifference;
	//float previousDifference;
	float previousFrameDifference;
	float xAcceleration;
	//float Acceleration;

	float xDifference = p0.x-p1.x;
	float Difference = sqrt((p0.x-p1.x)*(p0.x-p1.x) + (p0.y-p1.y)*(p0.y-p1.y));
	float frameDifference = imgCountP0 - imgCountP1;

	if(m_curves[curveNumber].getBallPointsVecSize() > 2){
		p2 = m_curves[curveNumber].getThirdLastBallPoint();
		imgCountP2  = m_curves[curveNumber].getThirdLastFrameCount();

		xPreviousDifference = p1.x-p2.x;
		//previousDifference = sqrt((p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y));

		previousFrameDifference = imgCountP1 - imgCountP2;

		//Acceleration = (Difference/(float)frameDifference)  / (PreviousDifference/(float)previousFrameDifference);

		//xAcceleration = (xDifference/(float)frameDifference)  / (xPreviousDifference/(float)previousFrameDifference);
		//xAcceleration = Acceleration;

		xAcceleration = (xDifference/(float)frameDifference)  / 	(xPreviousDifference/(float)previousFrameDifference);
		xAcceleration = pow(xAcceleration, (m_frameCount - imgCountP0));
	}

	float xDistanceToPrediction;
	float xPrimaryDistanceToPrediction;

	//cout<<"Curve Frame Count: "<<m_curves[curveNumber].getFrameCountVecSize()<<endl;
	//cout<<"Curve Ball Points: "<<m_curves[curveNumber].getBallPointsVecSize()<<endl;

	if(m_curves[curveNumber].getFrameCountVecSize()>2 && xDifference > 2 * m_inputVideoResizingFactor && xPreviousDifference > 2 * m_inputVideoResizingFactor){
		xDistanceToPrediction = xDifference*xAcceleration*(m_frameCount - imgCountP0)/frameDifference;
		xPrimaryDistanceToPrediction = xDifference*xAcceleration/frameDifference;
		//cout<<"Used Acceleration for Regression "<<xAcceleration<<endl;
	}
	else{
		xDistanceToPrediction = xDifference*(m_frameCount - imgCountP0)/frameDifference;
		xPrimaryDistanceToPrediction = xDifference/frameDifference;
		//cout<<"Didnt use Acceleration for Regression "<<xAcceleration<<endl;
	}

	float yDistanceToPrediction;
	float distanceToPrediction;
	Point2f ballCenterPrediction;

	ballCenterPrediction.x = p0.x + xDistanceToPrediction;
	//linear regression
	Point2f slopeInterceptValues;
	imgProc::imageMath::calculateLinearRegression(p0, p1, slopeInterceptValues);
	ballCenterPrediction.y = slopeInterceptValues.x * ballCenterPrediction.x + slopeInterceptValues.y;
	yDistanceToPrediction = ballCenterPrediction.y - p0.y;

	//primaryBallCenterPrediction only used for size of m_predictedBallCenterRadius
	Point2f primaryBallCenterPrediction;
	primaryBallCenterPrediction.x = p0.x + xPrimaryDistanceToPrediction;
	primaryBallCenterPrediction.y = slopeInterceptValues.x * primaryBallCenterPrediction.x + slopeInterceptValues.y;
	float yPrimaryDistanceToPrediction = primaryBallCenterPrediction.y - p0.y;

	//Do quadratic regression if curve contains at least 3 points and absolut value of slope is less than 5
	//extreme steep sloves during serves often cause troubles in quad regression.
	/*if(m_curves[curveNumber].getBallPointsVecSize() > 3 && abs(slopeInterceptValues.x) < 5){
		xDistanceToPrediction = xDifference*(xAcceleration*(m_frameCount - imgCountP0));
		vector<float> quadRegressionParameters;
		vector<Point2f> curvePoints = m_curves[curveNumber].getBallPoints();
		imgProc::imageMath::calculateQuadRegression(curvePoints, quadRegressionParameters, 1);
		//Only use quad Regression if parabola is downwardly opened
		if(quadRegressionParameters[0] > 0){
			ballCenterPrediction.y = quadRegressionParameters[0]*(ballCenterPrediction.x)*(ballCenterPrediction.x) + quadRegressionParameters[1]*ballCenterPrediction.x + quadRegressionParameters[2];
			yDistanceToPrediction = ballCenterPrediction.y - p0.y;
		}
	}*/

	distanceToPrediction = sqrt(xPrimaryDistanceToPrediction * xPrimaryDistanceToPrediction + yPrimaryDistanceToPrediction * yPrimaryDistanceToPrediction);
	//TODO: Mathematische funktion die den ballCenterRadius dynamisch anpasst

	m_predictedBallCenterRadius = distanceToPrediction * (0.4 + 60 / (100 + pow(distanceToPrediction, 2.5)));

	/*
	if(distanceToPrediction <= 5 * m_inputVideoResizingFactor){
		m_predictedBallCenterRadius = distanceToPrediction * 1.00f;
	}
	else if(distanceToPrediction <= 10 * m_inputVideoResizingFactor){
		//m_predictedBallCenterRadius = (distanceToPrediction * 5/distanceToPrediction);
		m_predictedBallCenterRadius = distanceToPrediction * 0.80f;
	}
	else if(distanceToPrediction <= 20){
		m_predictedBallCenterRadius = distanceToPrediction * 0.50f;
	}
	else{
		m_predictedBallCenterRadius = distanceToPrediction * 0.40f;
	}
	*/

	// if(m_curves[curveNumber].getBallPointsVecSize() > 1){
	// 	cout<<"curveNumber: "<<curveNumber<<" getSecondLastBallPoint: "<<m_curves[curveNumber].getSecondLastBallPoint()<<" getLastBallPoint: "<<m_curves[curveNumber].getLastBallPoint()<<endl;
	// 	cout<<"ballCenterPrediction: "<< ballCenterPrediction <<" m_predictedBallCenterRadius: "<<m_predictedBallCenterRadius<<endl;
	// }

	//Draw BallCenterPrediction into m_drawBallCenters
	if(ballCenterPrediction.x < m_frameWidth && ballCenterPrediction.y < m_frameHeight && m_curves[curveNumber].getBallPointsVecSize() > 2){
		//circle( m_drawnBallCenters[m_frameCount], ballCenterPrediction*m_resizingFactor, (int)m_predictedBallCenterRadius, Scalar(0,0,255), 1, 8, 0 );
		m_drawnBallCenters[m_frameCount].push_back(ballCenterPrediction);
		m_drawnBallCentersRadius[m_frameCount].push_back(m_predictedBallCenterRadius);
	}
	return ballCenterPrediction;
}
/*
Point2f CurveDetection::makeBallCenterPrediction(int curveNumber){
	//TODO checken ob hier alles stimmt, vor allem calculateLinearRegression
	//Do linear regression if curve contains less than 3 points
	Point2f p0 = m_curves[curveNumber].getLastBallPoint();
	Point2f p1 = m_curves[curveNumber].getSecondLastBallPoint();
	int imgCountP0 = m_curves[curveNumber].getLastFrameCount();
	int imgCountP1 = m_curves[curveNumber].getSecondLastFrameCount();
	float xDifference = p0.x-p1.x;
	float yDistanceToPrediction;
	float xDistanceToPrediction = xDifference * (float)(m_frameCount - imgCountP0) / (float)(imgCountP0 - imgCountP1);
	float distanceToPrediction;
	Point2f ballCenterPrediction;

	ballCenterPrediction.x = p0.x + xDistanceToPrediction;

	Point2f slopeInterceptValues;
	imgProc::imageMath::calculateLinearRegression(p0, p1, slopeInterceptValues);

	//extreme steep sloves during serves often cause troubles in quad regression.
	//if(m_curves[curveNumber].getBallPointsVecSize() < 3 || abs(slopeInterceptValues.x) > 5){
		yDistanceToPrediction = slopeInterceptValues.x * xDifference * (float)(m_frameCount - imgCountP0) / (float)(imgCountP0 - imgCountP1);
		ballCenterPrediction.y = p0.y + yDistanceToPrediction;
	//}
	//Do quadratic regression if curve contains at least 3 points and absolut value of slope is less than 5
	else{
		vector<float> quadRegressionParameters;
		vector<Point2f> curvePoints = m_curves[curveNumber].getBallPoints();
		imgProc::imageMath::calculateQuadRegression(curvePoints, quadRegressionParameters, 1);
		ballCenterPrediction.y = quadRegressionParameters[0](ballCenterPrediction.x)(ballCenterPrediction.x) + quadRegressionParameters[1]*ballCenterPrediction.x + quadRegressionParameters[2];
		yDistanceToPrediction = ballCenterPrediction.y - p1.y;
	}
	distanceToPrediction = sqrt(xDistanceToPrediction * xDistanceToPrediction + yDistanceToPrediction * yDistanceToPrediction);
	m_predictedBallCenterRadius = distanceToPrediction * 0.20f;

	//Draw BallCenterPrediction into m_drawBallCenters
	if(ballCenterPrediction.x < m_frameWidth && ballCenterPrediction.y < m_frameHeight && m_curves[curveNumber].getBallPointsVecSize() > 3){
		circle( m_drawnBallCenters[m_frameCount], ballCenterPrediction*m_resizingFactor, (int)m_predictedBallCenterRadius, Scalar(0,0,255), 1, 8, 0 );
	}
	return ballCenterPrediction;
}
*/
bool CurveDetection::detectedBallIsInPredictedBallCenterRadius(Point2f ballCenterPrediction, Point2f detectedBallCenter, bool isInHumanMaskROI){
	bool isLocatedInsideRadius = false;
	if(isInHumanMaskROI){
		m_predictedBallCenterRadius = m_predictedBallCenterRadius * 0.75; // 0.5
	}
	int xmin = ballCenterPrediction.x - m_predictedBallCenterRadius;
	int xmax = ballCenterPrediction.x + m_predictedBallCenterRadius;
	int ymin = ballCenterPrediction.y - m_predictedBallCenterRadius/4;
	int ymax = ballCenterPrediction.y + m_predictedBallCenterRadius;

	if(detectedBallCenter.x > xmin && detectedBallCenter.x < xmax && detectedBallCenter.y > ymin && detectedBallCenter.y < ymax) {
		isLocatedInsideRadius = true;
	}
	return isLocatedInsideRadius;
}

bool CurveDetection::detectedBallIsInPredictedBallCenterRadiusCircle(Point2f ballCenterPrediction, Point2f detectedBallCenter, bool isInHumanMaskROI){
	bool isLocatedInsideRadius = false;
	float distanceToBallCenterPrediction;
	imgProc::imageMath::calculateDistanceBetween(ballCenterPrediction, detectedBallCenter, distanceToBallCenterPrediction);

	if(distanceToBallCenterPrediction < m_predictedBallCenterRadius){
		isLocatedInsideRadius = true;
	}
	return isLocatedInsideRadius;
}

void CurveDetection::characteriseCurve(){
	cout<<"CharacteriseCurve"<<endl;
	if(m_curves.size() > 1){
		for(int i = m_alreadyCharacterisedCurve; i < m_curves.size(); i++){
			if(m_frameCount - m_curves[i].getLastFrameCount() > m_frameCountWhichLeadsToEndCurve && m_curves[i].getIsCompleted() == 1 && m_curves[i].getCharacterisation() == -1){
				cout<<"FrameCountDiff: "<<(m_frameCount - m_curves[i].getLastFrameCount())<<endl;

				detecBestFittingFollowingCurve(i);
				int numberOfBestFittingFollowingCurve = m_curves[i].getFollowingCurveIndex();
				cout<<"###### Curve ["<<i<< "]'s follower is ["<<numberOfBestFittingFollowingCurve<<"]"<<endl;
				if(numberOfBestFittingFollowingCurve == -1){
					characteriseSingleCurve(i);
				}
				else{
					int numberOfFollowingsBestFittingPreviousCurve = m_curves[numberOfBestFittingFollowingCurve].getPreviousCurveIndex();
					cout<<"###### Curve ["<<numberOfBestFittingFollowingCurve<< "]'s previous is ["<<numberOfFollowingsBestFittingPreviousCurve<<"]"<<endl;
					//following and previous curvenumber match with each other
					if(i == numberOfFollowingsBestFittingPreviousCurve || numberOfFollowingsBestFittingPreviousCurve == -1){
						characteriseCurveWithBestFittingCurve(i, numberOfBestFittingFollowingCurve);
						cout<<"###### Succeeded Match of two curves"<<endl;
					}
					else{
						m_curves[i].setCharacterisation(-2);
					}
						// if(checkSimultaneity(i, numberOfBestFittingCurve) == true){
						// 	if(mergeSimultanCurves(i, numberOfBestFittingCurve) == false){
						// 		characteriseCurveWithBestFittingCurve(i, numberOfBestFittingCurve);
						// 	}
						// }
						// else{
						// 	characteriseCurveWithBestFittingCurve(i, numberOfBestFittingCurve);
						// }
				}
			}
		}
	}
}

void CurveDetection::characteriseSingleCurve(int curveNumber){
	cout<<"CharacteriseSingleCurve"<<endl;
	int prevCurveIsNotOutOfImage = -1;
	//If the prev Curve leads to a stroke, the actual curve should fly over the net.
	if(curveNumber-1 >= 0) {
		if(m_curves[curveNumber-1].getCharacterisation() < 17 || m_curves[curveNumber-1].getCharacterisation() > 20) {
			prevCurveIsNotOutOfImage = 1;
		}
	}

	if(m_curves[curveNumber].getIsCompleted() == 1 && m_curves[curveNumber].getCharacterisation() == -1) {

		int frameCountDiff = m_frameCount - m_curves[curveNumber].getLastFrameCount();
		float dx = m_curves[curveNumber].getSecondLastBallPoint().x - m_curves[curveNumber].getThirdLastBallPoint().x;

		//17: curve leads to way out of the right Image over the net
		if(frameCountDiff > m_frameCountWhichLeadsToFieldChanging && m_isRightFieldSide == true && dx < 0.0f ) {
			m_curves[curveNumber].setCharacterisation(17);
		}
		//18: curve leads to way out of the left Image over the net
		else if(frameCountDiff > m_frameCountWhichLeadsToFieldChanging && m_isRightFieldSide == false && dx > 0.0f) {
			m_curves[curveNumber].setCharacterisation(18);
		}
		//19: curve leads to right player and is stopped there or right out of image right
		else if(frameCountDiff > m_frameCountWhichLeadsToEndCurve && m_isRightFieldSide == true && dx > 0.0f && prevCurveIsNotOutOfImage == 1) {
			m_curves[curveNumber].setCharacterisation(19);
		}
		//20: curve leads to left player and is stopped there or left out of image left
		else if(frameCountDiff > m_frameCountWhichLeadsToEndCurve && m_isRightFieldSide == false && dx < 0.0f && prevCurveIsNotOutOfImage == 1) {
			m_curves[curveNumber].setCharacterisation(20);
		}
		else{
			cout<<"DEFAULT CHARACTERISATION CURVE SINGLE CURVE: "<<curveNumber<<endl;
			m_curves[curveNumber].setCharacterisation(-2);
		}
	}
}

void CurveDetection::characteriseCurveWithBestFittingCurve(int curveNumber, int curveNumberBestFittingCurve){
	cout<<"CharacteriseCurveWithBestFittingCurve"<<endl;
	if(m_curves[curveNumber].getIsCompleted() == 1 && m_curves[curveNumberBestFittingCurve].getIsCompleted() == 1 && m_curves[curveNumber].getCharacterisation() == -1) {

		int frameCountCurve0 = m_curves[curveNumber].getLastFrameCount();
		int frameCountCurve1 = m_curves[curveNumberBestFittingCurve].getFirstFrameCount();

		Point2f curve0PEnd  = m_curves[curveNumber].getLastBallPoint();
		Point2f curve1PStart  = m_curves[curveNumberBestFittingCurve].getFirstBallPoint();
		Point2f curve1PEnd  = m_curves[curveNumberBestFittingCurve].getLastBallPoint();

		Point2f curve0DirectionVector = m_curves[curveNumber].getThirdLastBallPoint() - m_curves[curveNumber].getSecondLastBallPoint();
		Point2f verticalVector = Point2f(0, -1);
		Point2f curve1DirectionVector = m_curves[curveNumberBestFittingCurve].getBallPoint(2) - m_curves[curveNumberBestFittingCurve].getBallPoint(1);

		float angleBetweenCurve0andVertical;
		imgProc::imageMath::angelBetween(curve0DirectionVector, verticalVector, angleBetweenCurve0andVertical);
		float angleBetweenCurve1andVertical;
		imgProc::imageMath::angelBetween(curve1DirectionVector, verticalVector, angleBetweenCurve1andVertical);

		cout<<"Curve: "<< curveNumber <<" angleBetweenCurve0andVertical: "<< angleBetweenCurve0andVertical<<" angleBetweenCurve1andVertical: "<<angleBetweenCurve1andVertical<<endl;

		float yDifference = curve0PEnd.y - curve1PStart.y;

		float dx0 = m_curves[curveNumber].getCurveEndDX();
		float dx1 = m_curves[curveNumberBestFittingCurve].getCurveStartDX();
		float dy0 = m_curves[curveNumber].getCurveEndDY();
		float dy1 = m_curves[curveNumberBestFittingCurve].getCurveStartDY();

		//21: curve leads to serve from left to right
		if(m_isRightFieldSide == false && ((angleBetweenCurve0andVertical < 30.0 || angleBetweenCurve0andVertical > 140.0) && angleBetweenCurve1andVertical > 35.0 && angleBetweenCurve1andVertical < 120.0) && m_minHeightOfServe > curve1PStart.y && m_minHeightOfServe > curve0PEnd.y && dx1 > 0.0f) {
			m_curves[curveNumber].setCharacterisation(21);
		}
		//22: curve leads to serve from right to left
		else if(m_isRightFieldSide == true && ((angleBetweenCurve0andVertical < 30.0 || angleBetweenCurve0andVertical > 140.0) && angleBetweenCurve1andVertical > 35.0 && angleBetweenCurve1andVertical < 120.0) && m_minHeightOfServe > curve1PStart.y && m_minHeightOfServe > curve0PEnd.y && dx1 < 0.0f) {
			m_curves[curveNumber].setCharacterisation(22);
		}
		//1: curve leads to another curve with the same flight direction: rigth up and left down
		else if(dx0 < 0.0f && dy0 <= 0.0f && dx1 < 0.0f && dy1 >= 0.0f && (curve0PEnd.x > curve1PStart.x || frameCountCurve0 >= frameCountCurve1)) {
			m_curves[curveNumber].setCharacterisation(1);
		}
		//2: curve leads to another curve with the same flight direction: left up and right down
		else if(dx0 > 0.0f && dy0 <= 0.0f && dx1 > 0.0f && dy1 >= 0.0f && (curve0PEnd.x < curve1PStart.x || frameCountCurve0>= frameCountCurve1)) {
			m_curves[curveNumber].setCharacterisation(2);
		}
		//3: curve leads to another curve with the same flight direction: right to left up
		else if(dx0 < 0.0f && dy0 < 0.0f && dx1 < 0.0f && dy1 < 0.0f && (curve0PEnd.x > curve1PStart.x || frameCountCurve0 >= frameCountCurve1)) {
			m_curves[curveNumber].setCharacterisation(3);
		}
		//4: curve leads to another curve with the same flight direction: left to right up
		else if(dx0 > 0.0f && dy0 < 0.0f && dx1 > 0.0f && dy1 < 0.0f && (curve0PEnd.x < curve1PStart.x || frameCountCurve0 >= frameCountCurve1)) {
			m_curves[curveNumber].setCharacterisation(4);
		}
		//5: curve leads to another curve with the same flight direction: right to left down
		else if(dx0 < 0.0f && dy0 > 0.0f && dx1 < 0.0f && dy1 > 0.0f && (curve0PEnd.x > curve1PStart.x || frameCountCurve0 >= frameCountCurve1)) {
			m_curves[curveNumber].setCharacterisation(5);
		}
		//6: curve leads to another curve with the same flight direction: left to right down
		else if(dx0 > 0.0f && dy0 > 0.0f && dx1 > 0.0f && dy1 > 0.0f && (curve0PEnd.x < curve1PStart.x || frameCountCurve0 >= frameCountCurve1)) {
			m_curves[curveNumber].setCharacterisation(6);
		}
		//7: curve leads to impact: from right to left
		else if(dx0 < 0.0f && dy0 > 0.0f && dx1 < 0.0f && dy1 < 0.0f && m_curves[curveNumberBestFittingCurve].getBallPoint(1).x <= m_curves[curveNumber].getSecondLastBallPoint().x) {

			m_curves[curveNumber].setCharacterisation(7);

			//cout<<"0P0: "<<curve0P0<<" 0p1: "<<m_curves[curveNumber].getSecondLastBallPoint()<<" 1P0: "<<curve1P0<<" 1P1: "<<curve1P1<<endl;

			calculatingPointOfImpact(curveNumber, curveNumberBestFittingCurve);

			cout<<"Impact: "<<m_curves[curveNumber].getPointOfImpact()<<"imgCount: "<<m_curves[curveNumber].getFrameCountOfImpact()+m_framesPredictionCount<<endl;
			if(m_curves[curveNumber].getPointOfImpact().y < m_yMaxWhichLeadsToImpact) {
				cout<<"Impact is to high"<<endl;
			}
		}
		//7: curve leads to impact: from right to left
		else if(dx0 < 0.0f && dy0 > 0.0f && dx1 > 0.0f && dy1 < 0.0f && yDifference > m_yDiffWhichLeadsToImpact && curve0PEnd.y > m_yMaxWhichLeadsToImpact) {
			m_curves[curveNumber].setCharacterisation(7);
			int frameCountCurve0 = m_curves[curveNumber].getLastFrameCount();
			Point2f pointOfImpact = m_curves[curveNumber].getLastBallPoint();
			pointOfImpact.y += (float)m_curves[curveNumber].getLastBallsBoundRect().height * m_ballRadiusFactor;
			cout<<"ImgCountCurve"<< curveNumber <<": "<<frameCountCurve0<<" with yDiff"<<endl;

			cout<<"ImgCountImpact: "<< frameCountCurve0<<endl;

			m_curves[curveNumber].setPointOfImpact(pointOfImpact);
			m_curves[curveNumber].setFrameCountOfImpact(frameCountCurve0);
		}
		//8: curve leads to impact: from left to right
		else if(dx0 > 0.0f && dy0 > 0.0f && dx1 > 0.0f && dy1 < 0.0f && m_curves[curveNumberBestFittingCurve].getBallPoint(1).x >= m_curves[curveNumber].getSecondLastBallPoint().x) {

			m_curves[curveNumber].setCharacterisation(8);

			calculatingPointOfImpact(curveNumber, curveNumberBestFittingCurve);

			cout<<"Impact: "<<m_curves[curveNumber].getPointOfImpact()<<"imgCount: "<<m_curves[curveNumber].getFrameCountOfImpact()+m_framesPredictionCount<<endl;
			if(m_curves[curveNumber].getPointOfImpact().y < m_yMaxWhichLeadsToImpact) {
				cout<<"Impact is to high"<<endl;
			}
		}
		//8: curve leads to impact: from left to right
		else if(dx0 > 0.0f && dy0 > 0.0f && dx1 < 0.0f && dy1 < 0.0f && yDifference > m_yDiffWhichLeadsToImpact && curve0PEnd.y > m_yMaxWhichLeadsToImpact) {
			m_curves[curveNumber].setCharacterisation(8);
			int frameCountCurve0 = m_curves[curveNumber].getLastFrameCount();
			Point2f pointOfImpact = m_curves[curveNumber].getLastBallPoint();
			pointOfImpact.y += (float)m_curves[curveNumber].getLastBallsBoundRect().height * m_ballRadiusFactor;
			cout<<"ImgCountCurve"<< curveNumber <<": "<<frameCountCurve0<<" with yDiff"<<endl;
			cout<<"ImgCountImpact: "<< frameCountCurve0<<endl;

			m_curves[curveNumber].setPointOfImpact(pointOfImpact);
			m_curves[curveNumber].setFrameCountOfImpact( frameCountCurve0);
		}
		//9: curve leads to stroke: from up right to right up
		else if(dx0 < 0.0f && dy0 < 0.0f && dx1 > 0.0f && dy1 < 0.0f && curve0PEnd.x < curve1PEnd.x) {
			m_curves[curveNumber].setCharacterisation(9);
		}
		//10: curve leads to stroke: from up left to left up
		else if(dx0 > 0.0f && dy0 < 0.0f && dx1 < 0.0f && dy1 < 0.0f && curve0PEnd.x > curve1PEnd.x) {
			m_curves[curveNumber].setCharacterisation(10);
		}
		//11: curve leads to stroke: from right to right down
		else if(dx0 < 0.0f && dy0 > 0.0f && dx1 > 0.0f && dy1 > 0.0f && curve0PEnd.x < curve1PEnd.x) {
			m_curves[curveNumber].setCharacterisation(11);
		}
		//12: curve leads to stroke: from left to left down
		else if(dx0 > 0.0f && dy0 > 0.0f && dx1 < 0.0f && dy1 > 0.0f && curve0PEnd.x > curve1PEnd.x) {
			m_curves[curveNumber].setCharacterisation(12);
		}
		//13: curve leads to stroke: from right down to right up
		else if(dx0 < 0.0f && dy0 > 0.0f && dx1 > 0.0f && dy1 < 0.0f && curve0PEnd.x < curve1PEnd.x) {
			m_curves[curveNumber].setCharacterisation(13);
		}
		//14: curve leads to stroke: from left down to left up
		else if(dx0 > 0.0f && dy0 > 0.0f && dx1 < 0.0f && dy1 < 0.0f && curve0PEnd.x > curve1PEnd.x) {
			m_curves[curveNumber].setCharacterisation(14);
		}
		//15: curve leads to stroke: from right up to right down
		else if(dx0 < 0.0f && dy0 < 0.0f && dx1 > 0.0f && dy1 > 0.0f && curve0PEnd.x < curve1PEnd.x) {
			m_curves[curveNumber].setCharacterisation(15);
		}
		//16: curve leads to stroke: from left up to left down
		else if(dx0 > 0.0f && dy0 < 0.0f && dx1 < 0.0f && dy1 > 0.0f && curve0PEnd.x > curve1PEnd.x) {
			m_curves[curveNumber].setCharacterisation(16);
		}
		else{
			cout<<"DEFAULT CHARACTERISATION CURVE: "<<curveNumber<<endl;
			m_curves[curveNumber].setCharacterisation(-2);
		}
		if(((m_curves[curveNumber].getCharacterisation() >= 9 && m_curves[curveNumber].getCharacterisation() <= 16 ) || m_curves[curveNumber].getCharacterisation() == 21 || m_curves[curveNumber].getCharacterisation() == 22)) {
			calculateCurveSpeed(curveNumber, curveNumberBestFittingCurve);
		}
	}
}

void CurveDetection::detecBestFittingFollowingCurve(int curveNumber){
	int numberOfBestFittingCurve = -1;
	if(curveNumber + 1 < m_curves.size()){
		float fittingIndex = 10000.0f;
		float dx0 = m_curves[curveNumber].getCurveEndDX();
		float curve0PEndX = m_curves[curveNumber].getLastBallPoint().x;
		int curve0FrameCountEnd = m_curves[curveNumber].getLastFrameCount();

		for(int i = curveNumber + 1; i < m_curves.size(); i++){
			if(m_curves[i].getIsCompleted() == true){
				float dx1 = m_curves[i].getCurveEndDX();
				float curve1PEndX = m_curves[i].getLastBallPoint().x;
				int curve1FrameCountEnd = m_curves[i].getLastFrameCount();
				//dx0 < 0.0f && dx1 > 0.0f && curve0PEndX < curve1PEndX
				//dx0 > 0.0f && dx1 < 0.0f && curve0PEndX > curve1PEndX
				//dx0 < 0.0f && dx1 < 0.0f && curve0PEndX > curve1PEndX
				//dx0 > 0.0f && dx1 > 0.0f && curve0PEndX < curve1PEndX
				if((((dx0 < 0.0f && dx1 > 0.0f) || (dx0 > 0.0f && dx1 > 0.0f)) && curve0PEndX < curve1PEndX) || (((dx0 > 0.0f && dx1 < 0.0f) || (dx0 < 0.0f && dx1 < 0.0f)) && curve0PEndX > curve1PEndX)){
					cout<<"Criterium is complied between: "<<curveNumber<<" and "<<i<<endl;
					float distance;
					imageMath::calculateDistanceBetween(m_curves[curveNumber].getLastBallPoint(), m_curves[i].getFirstBallPoint(), distance);
					int frameCountDiff = abs(curve0FrameCountEnd - curve1FrameCountEnd) < 1 ? 1 : abs(curve0FrameCountEnd - curve1FrameCountEnd);
					float actualFittingIndex = distance * frameCountDiff;
					cout<<"FittingIndex: "<<actualFittingIndex<<endl;
					if(actualFittingIndex < fittingIndex){
						fittingIndex = distance;
						numberOfBestFittingCurve = i;
					}
				}
			}
		}
		m_curves[curveNumber].setFollowingCurveIndex(numberOfBestFittingCurve);
		//m_curves[numberOfBestFittingCurve].setCurveIndexWhosNextIsMe(curveNumber);
	}
	cout<<"BestFittingFollowingCurveEnd"<<endl;
}

void CurveDetection::detecBestFittingPreviousCurve(int curveNumber){
	int numberOfBestFittingCurve = -1;
	if(curveNumber - 1 > 0){
		float fittingIndex = 10000.0f;
		float dx1 = m_curves[curveNumber].getCurveEndDX();
		float curve1PEndX = m_curves[curveNumber].getLastBallPoint().x;
		int curve1FrameCountEnd = m_curves[curveNumber].getLastFrameCount();

		for(int i = curveNumber -1; i >= m_alreadyCharacterisedCurve; i--){
			if(m_curves[i].getLastFrameCount() - m_frameCount > 30){
				continue;
			}
			if(m_curves[i].getIsCompleted() == true){
				float dx0 = m_curves[i].getCurveEndDX();
				float curve0PEndX = m_curves[i].getLastBallPoint().x;
				int curve0FrameCountEnd = m_curves[i].getLastFrameCount();
				//dx0 < 0.0f && dx1 > 0.0f && curve0PEndX < curve1PEndX
				//dx0 > 0.0f && dx1 < 0.0f && curve0PEndX > curve1PEndX
				//dx0 < 0.0f && dx1 < 0.0f && curve0PEndX > curve1PEndX
				//dx0 > 0.0f && dx1 > 0.0f && curve0PEndX < curve1PEndX
				if((((dx0 < 0.0f && dx1 > 0.0f) || (dx0 > 0.0f && dx1 > 0.0f)) && curve0PEndX < curve1PEndX) || (((dx0 > 0.0f && dx1 < 0.0f) || (dx0 < 0.0f && dx1 < 0.0f)) && curve0PEndX > curve1PEndX)){
					//cout<<"Criterium is complied between: "<<curveNumber<<" and "<<i<<endl;
					float distance;
					imageMath::calculateDistanceBetween(m_curves[curveNumber].getFirstBallPoint(), m_curves[i].getLastBallPoint(), distance);
					int frameCountDiff = abs(curve0FrameCountEnd - curve1FrameCountEnd) < 1 ? 1 : abs(curve0FrameCountEnd - curve1FrameCountEnd);
					float actualFittingIndex = distance * frameCountDiff;
					//cout<<"FittingIndex: "<<distance<<endl;
					if(actualFittingIndex < fittingIndex){
						fittingIndex = distance;
						numberOfBestFittingCurve = i;
					}
				}
			}
		}
		m_curves[curveNumber].setPreviousCurveIndex(numberOfBestFittingCurve);
	}
	cout<<"BestFittingPreviousCurveEnd"<<endl;
}

void CurveDetection::deleteDublicatedFlightCurves(){
	cout<<"deleteDublicatedFlightCurves"<<endl;
	int approxFlightCurveImgCountSize = (int)m_curves.size()-1;
	for(int curve0 = approxFlightCurveImgCountSize; curve0 >= m_alreadyFinishedCurve + 1; curve0--) {

		for(int curve1 = curve0 - 1; curve1 >= m_alreadyFinishedCurve; curve1--) {
			int dublicateCounter = 0;
			vector<Point2f> ballCenter0 = m_curves[curve0].getBallPoints();
			vector<Point2f> ballCenter1 = m_curves[curve1].getBallPoints();
			for(uint32_t i = 0; i < ballCenter0.size(); i++) {

				float x0 = ballCenter0[i].x;
				float y0 = ballCenter0[i].y;
				for(uint32_t j = 0; j < ballCenter1.size(); j++) {
					float x1 = ballCenter1[j].x;
					float y1 = ballCenter1[j].y;
					if(x0 == x1 && y0 == y1) {
						if(m_curves[curve0].getFrameCount(i) == m_curves[curve1].getFrameCount(j)) {
							dublicateCounter++;
						}
					}
				}
			}
			if(dublicateCounter > 2) {
				if(m_curves[curve0].getBallPointsVecSize() > m_curves[curve1].getBallPointsVecSize()) {
					deleteFalseDetectedCurve(curve1);
					//resetting the curve characterisation which was created with the deleted curve
					//TODO do the reset of the character in the deleteFalseDetectedCurve funcion!!!
					if(curve1 > 0) {
						m_curves[curve1-1].resetCharacterisation();
					}
				}
				else{
					deleteFalseDetectedCurve(curve0);
					if(curve0 > 0) {
						m_curves[curve0-1].resetCharacterisation();
					}
				}
				curve1 = -1;
			}
		}
	}
}

bool CurveDetection::mergeSimultanCurves(int curveNumber0, int curveNumber1){
	//proof if able to mergeSimultanCurves
	cout<<"MergeSimultanCureve"<<endl;
	//int mergedCurveNumber = curveNumber1 + 1;
	float mergeAverageDistance = 0.0f;
	float mergeAverageYDistance = 0.0f;
	int simultaneityCounter = 0;
	for(uint i = 0; i < m_curves[curveNumber0].getFrameCountVecSize(); i++) {
		for(uint j = 0; j < m_curves[curveNumber1].getFrameCountVecSize(); j++) {
			if(m_curves[curveNumber0].getFrameCount(i) == m_curves[curveNumber1].getFrameCount(j)) {
				simultaneityCounter++;
				float dist;
				imgProc::imageMath::calculateDistanceBetween(m_curves[curveNumber0].getBallPoint(i), m_curves[curveNumber1].getBallPoint(j), dist);
				mergeAverageDistance += dist;
				mergeAverageYDistance += abs(m_curves[curveNumber0].getBallPoint(i).y - m_curves[curveNumber1].getBallPoint(j).y);
			}
		}
	}
	mergeAverageDistance /=(float)simultaneityCounter;
	mergeAverageYDistance /=(float)simultaneityCounter;
	float avarageYCurve0 = 0;
	float avarageYCurve1 = 0;
	for(uint i = 0; i < m_curves[curveNumber0].getFrameCountVecSize(); i++) {
		Point2f p0 = m_curves[curveNumber0].getBallPoint(i);
		avarageYCurve0 += p0.y;
	}

	for(uint i = 0; i < m_curves[curveNumber1].getFrameCountVecSize(); i++) {
		Point2f p0 = m_curves[curveNumber1].getBallPoint(i);
		avarageYCurve1 += p0.y;
	}
	avarageYCurve0 /= (float)m_curves[curveNumber0].getFrameCountVecSize();
	avarageYCurve1 /= (float)m_curves[curveNumber1].getFrameCountVecSize();


	int curve0Iterator = 0;
	int curve1Iterator = 0;
	if(mergeAverageDistance < 40.0f * m_inputVideoResizingFactor && mergeAverageYDistance < 20.0f * m_inputVideoResizingFactor) {

		//insert new curve with first point of curveNumber0 as the start point
		Curve mergedCurve(m_curves[curveNumber0].getFirstBallPoint(), m_curves[curveNumber0].getFirstFrameCount(), m_curves[curveNumber0].getDistanceToPlayer(0), m_curves[curveNumber0].getPositionOfPlayerDuringCurve(0), m_curves[curveNumber0].getBallsBoundRect(0));
		curve0Iterator++;
		while(curve0Iterator < m_curves[curveNumber0].getFrameCountVecSize() && curve1Iterator < m_curves[curveNumber1].getFrameCountVecSize()) {
			if(m_curves[curveNumber0].getFrameCount(curve0Iterator) < m_curves[curveNumber1].getFrameCount(curve1Iterator)) {
				mergedCurve.appendData( m_curves[curveNumber0].getDistanceToPlayer(curve0Iterator), m_curves[curveNumber0].getPositionOfPlayerDuringCurve(curve0Iterator), m_curves[curveNumber0].getBallsBoundRect(curve0Iterator), m_curves[curveNumber0].getBallPoint(curve0Iterator), m_curves[curveNumber0].getFrameCount(curve0Iterator));
				curve0Iterator++;
			}
			else if(m_curves[curveNumber0].getFrameCount(curve0Iterator) == m_curves[curveNumber1].getFrameCount(curve1Iterator)) {
				if(m_curves[curveNumber0].getBallPoint(curve0Iterator).y < m_curves[curveNumber1].getBallPoint(curve1Iterator).y) {
					mergedCurve.appendData( m_curves[curveNumber0].getDistanceToPlayer(curve0Iterator), m_curves[curveNumber0].getPositionOfPlayerDuringCurve(curve0Iterator), m_curves[curveNumber0].getBallsBoundRect(curve0Iterator), m_curves[curveNumber0].getBallPoint(curve0Iterator), m_curves[curveNumber0].getFrameCount(curve0Iterator));
				}
				else{
					mergedCurve.appendData( m_curves[curveNumber1].getDistanceToPlayer(curve1Iterator), m_curves[curveNumber1].getPositionOfPlayerDuringCurve(curve0Iterator), m_curves[curveNumber1].getBallsBoundRect(curve1Iterator), m_curves[curveNumber1].getBallPoint(curve1Iterator), m_curves[curveNumber1].getFrameCount(curve1Iterator));
				}

				curve0Iterator++;
				curve1Iterator++;
			}
			else{
				mergedCurve.appendData( m_curves[curveNumber1].getDistanceToPlayer(curve1Iterator), m_curves[curveNumber1].getPositionOfPlayerDuringCurve(curve1Iterator), m_curves[curveNumber1].getBallsBoundRect(curve1Iterator), m_curves[curveNumber1].getBallPoint(curve1Iterator), m_curves[curveNumber1].getFrameCount(curve1Iterator));
				curve1Iterator++;
			}
		}
		//merge leftover points of the curves
		while(curve0Iterator < m_curves[curveNumber0].getFrameCountVecSize()) {
			mergedCurve.appendData( m_curves[curveNumber0].getDistanceToPlayer(curve0Iterator), m_curves[curveNumber0].getPositionOfPlayerDuringCurve(curve0Iterator), m_curves[curveNumber0].getBallsBoundRect(curve0Iterator), m_curves[curveNumber0].getBallPoint(curve0Iterator), m_curves[curveNumber0].getFrameCount(curve0Iterator));
			curve0Iterator++;
		}
		while(curve1Iterator < m_curves[curveNumber1].getFrameCountVecSize()) {
			mergedCurve.appendData( m_curves[curveNumber1].getDistanceToPlayer(curve1Iterator), m_curves[curveNumber1].getPositionOfPlayerDuringCurve(curve1Iterator), m_curves[curveNumber1].getBallsBoundRect(curve1Iterator), m_curves[curveNumber1].getBallPoint(curve1Iterator), m_curves[curveNumber1].getFrameCount(curve1Iterator));
			curve1Iterator++;
		}
		deleteFalseDetectedCurve(curveNumber1);
		deleteFalseDetectedCurve(curveNumber0);
		m_curves.insert(m_curves.begin() + curveNumber0, mergedCurve);

		return true;
	}
	else{
		return false;
		//TODO wieder aufnehmen, wenn nötig
		//deleteLessFittingFlightCurve(curveNumber0, curveNumber1);
	}
}

bool CurveDetection::checkSimultaneity(int curveNumber0, int curveNumber1){
	//TODO find another criterum to delete simultan curves. Not that one is nearer to the human. That leads to mistakes when the double curve is the shadow curve
	bool isSimultan = false;
	int endImgCurve0 = m_curves[curveNumber0].getLastFrameCount();
	int startImgCurve1 = m_curves[curveNumber1].getFirstFrameCount();
	int simultanCounterCurve0 = 0;
	int simultanCounterCurve1 = 0;

	for(uint32_t i = 0; i < m_curves[curveNumber1].getFrameCountVecSize(); i++) {
		int imgCurve1 = m_curves[curveNumber1].getFrameCount(i);
		if(endImgCurve0 >= imgCurve1) {
			simultanCounterCurve0++;
		}
	}
	for(uint32_t i = 0; i < m_curves[curveNumber0].getFrameCountVecSize(); i++) {
		int imgCurve0 = m_curves[curveNumber0].getFrameCount(i);
		if(startImgCurve1 <= imgCurve0) {
			simultanCounterCurve1++;
		}
	}

	if(simultanCounterCurve0 > 1 && simultanCounterCurve1 > 1) {
		isSimultan = true;
		//mergeSimultanCurves(curveNumber0, curveNumber1);
	}
	return isSimultan;
}

void CurveDetection::deleteLessFittingFlightCurve(int curveNumber0, int curveNumber1){
	cout<<"deleteLessFittingFlightCurve"<<endl;
	//Neues Konzept:
	/* Alt: Wenn keine Kurve nah am spieler ist, wird die untere der beiden kurven gelöscht.
	   Dies dienst dazu, vom ball erzeugte schattenkurven zu löschen.

	 */
	float indexCurve0;
	float indexCurve1;

	float avarageDistanceToHumanCountourCurve0 = calcAvarageDistanceToHumanContour(curveNumber0);
	float avarageDistanceToHumanCountourCurve1 = calcAvarageDistanceToHumanContour(curveNumber1);

	indexCurve0 = (float) m_curves[curveNumber0].getFrameCountVecSize() * avarageDistanceToHumanCountourCurve0;
	indexCurve1 = (float) m_curves[curveNumber1].getFrameCountVecSize() * avarageDistanceToHumanCountourCurve1;
	cout<<"Index"<<curveNumber0<<": "<< indexCurve0<<" Index1"<<curveNumber1<<": "<<indexCurve1<<endl;

	if(indexCurve0 > m_maxCurveDeleteIndex && indexCurve1 > m_maxCurveDeleteIndex) {
		float avarageYCurve0 = 0;
		float avarageYCurve1 = 0;
		for(uint i = 0; i < m_curves[curveNumber0].getFrameCountVecSize(); i++) {
			Point2f p0 = m_curves[curveNumber0].getBallPoint(i);
			avarageYCurve0 += p0.y;
		}

		for(uint i = 0; i < m_curves[curveNumber1].getFrameCountVecSize(); i++) {
			Point2f p0 = m_curves[curveNumber1].getBallPoint(i);
			avarageYCurve1 += p0.y;
		}
		avarageYCurve0 /= (float)m_curves[curveNumber0].getFrameCountVecSize();
		avarageYCurve1 /= (float)m_curves[curveNumber1].getFrameCountVecSize();

		cout<<"avarageYCurve0: "<<avarageYCurve0<<endl;
		cout<<"avarageYCurve1: "<<avarageYCurve1<<endl;

		if(avarageYCurve1 > avarageYCurve0) {
			deleteFalseDetectedCurve(curveNumber1);
		}
		else{
			deleteFalseDetectedCurve(curveNumber0);
			if(curveNumber0 > 0) {
				m_curves[curveNumber0-1].resetCharacterisation();
			}
		}
	}
	else{
		if(indexCurve0 > indexCurve1) {
			deleteFalseDetectedCurve(curveNumber1);
		}
		else{
			deleteFalseDetectedCurve(curveNumber0);
			if(curveNumber0 > 0) {
				m_curves[curveNumber0-1].resetCharacterisation();
			}
		}
	}
}

void CurveDetection::deleteErronousFlightCurves(){
	cout<<"DeleteErronousFlightCurves"<<endl;
	if(m_curves.size() > 0) {
		for(int32_t i = (int32_t)m_curves.size()-1; i >= m_alreadyFinishedCurve; i--) {

			if((int32_t)m_curves[i].getFrameCountVecSize() < m_minimumCurvePoints && (m_frameCount - m_curves[i].getLastFrameCount()) > m_frameCountForErronousCurveDelete) {
				//cout<<"Delete short curve: "<< m_curves[i].getFrameCountVecSize() << " points"<<endl;
				deleteFalseDetectedCurve(i);
			}
			else if((int32_t)m_curves[i].getFrameCountVecSize() >= m_minimumCurvePoints && m_curves[i].getIsCompleted()!=1) {
				m_curves[i].setCompletion();
				detecBestFittingPreviousCurve(i);
				//checkFlightCurveDistanceToPlayer(i);
			}
		}
	}
}

void CurveDetection::deleteFalseDetectedCurve(int curveNumber){
	//TODO curve zeigt auf die kurve die gelöscht werden soll, dann sagt der curve die auf sie zeigt, dass sie nicht mehr auf die gelöschte kurve zeigen soll
	//Ints für curve.cpp: 1. welche zeigt auf mich 	2. eigener Index 3, index next, 4. index before


	m_curves.erase(m_curves.begin() + curveNumber);
}

float CurveDetection::calcAvarageDistanceToHumanContour(int curveNumber){

	float avarageDistanceToPlayerContour = 0.0;
	for(uint32_t points = 0; points < m_curves[curveNumber].getDistanceToPlayerVecSize(); points++) {
		avarageDistanceToPlayerContour += m_curves[curveNumber].getDistanceToPlayer(points);
	}
	avarageDistanceToPlayerContour /= m_curves[curveNumber].getDistanceToPlayerVecSize();

	return avarageDistanceToPlayerContour;
}

void CurveDetection::calculatingPointOfImpact(int curveNumber0, int curveNumber1){
	vector<Point2f> curve0 = m_curves[curveNumber0].getBallPoints();
	vector<Point2f> curve1 = m_curves[curveNumber1].getBallPoints();
	vector<float> quadRegressionParameters0;
	vector<float> quadRegressionParameters1;

	Point2f pointOfImpact;
	int imgCountOfImpact;

	Point2f curve0P0  = curve0[curve0.size()-3];
	Point2f curve0P1 = curve0[curve0.size()-2];
	Point2f curve1P0 = curve1[1];
	Point2f curve1P1 = curve1[2];

	imgProc::imageMath::calculateQuadRegression(curve0, quadRegressionParameters0, 1);
	imgProc::imageMath::calculateQuadRegression(curve1, quadRegressionParameters1, 0);

	//impact calculated by equalling the two quadratic regressions
	//Quad Regression Parameters: ax^2+bx+c = dx^2+ex+f
	//LGS solved  x = - (b-e)/(2(a-d)) +- sqrt( ((b-e)/(2(a-d)))^2 - (c-f)/(a-d) )
	//						x = -p/2 +- sqrt( p^2/4 - q )
	float p = (quadRegressionParameters0[1]-quadRegressionParameters1[1]) / (quadRegressionParameters0[0]-quadRegressionParameters1[0]);
	float q = (quadRegressionParameters0[2]-quadRegressionParameters1[2]) / (quadRegressionParameters0[0]-quadRegressionParameters1[0]);

	if(p*p/4 - q < 0){
		cout<<"calculatingPointOfImpact: Quad. functions don´t intersect!"<<endl;
	}
	else{
		float x1 = -p/2 - sqrt(p*p/4 - q);
		float x2 = -p/2 + sqrt(p*p/4 - q);
		if (abs(x2 - curve0[curve0.size()-2].x) < abs(x1 - curve0[curve0.size()-2].x)){
			x1=x2;
		}

		float y1 = quadRegressionParameters0[0]*x1*x1 + quadRegressionParameters0[1]*x1 + quadRegressionParameters0[2];

		if(x1>0 && y1>0){
			cout<<"Point of Impact calculated by quad. Regression"<<endl;
			pointOfImpact.x = x1;
			pointOfImpact.y = y1;
		}
		//If quad. regression doesn't calculate a correct Point of impact, use linear regerssion
		else{
			cout<<"calculatingPointOfImpact: Quad. functions intersect in negative Point!"<<endl;
			cout<<"Point of Impact calculated by lin. Regression"<<endl;
			Point2f slopeInterceptValues0;
			Point2f slopeInterceptValues1;
			imgProc::imageMath::calculateLinearRegression(curve0P0, curve0P1, slopeInterceptValues0);
			imgProc::imageMath::calculateLinearRegression(curve1P0, curve1P1, slopeInterceptValues1);

			float slope0 = slopeInterceptValues0.x; //m0
			float intercept0 = slopeInterceptValues0.y;	//b0
			float slope1 = slopeInterceptValues1.x;	//m1
			float intercept1 = slopeInterceptValues1.y;	//b1

			pointOfImpact.x = (intercept1 - intercept0) / (slope0 - slope1);
			pointOfImpact.y = slope0 * pointOfImpact.x + intercept0;
		}
	}

	if(pointOfImpact.y < m_curves[curveNumber0].getLastBallPoint().y) {
		pointOfImpact = m_curves[curveNumber0].getLastBallPoint();
	}
	else if(pointOfImpact.y < m_curves[curveNumber1].getLastBallPoint().y) {
		pointOfImpact = m_curves[curveNumber1].getLastBallPoint();
	}
	//pointsOfImpact.y correction
	pointOfImpact.y += (float)m_curves[curveNumber0].getLastBallsBoundRect().height * m_ballRadiusFactor;

	int frameCountCurve0 = m_curves[curveNumber0].getLastFrameCount();
	int frameCountCurve1 = m_curves[curveNumber1].getFirstFrameCount();
	cout<<"ImgCountCurve"<< curveNumber0 <<": "<<frameCountCurve0<<" ImgCountCurve"<< curveNumber1 <<": "<<frameCountCurve1<<endl;

	imgCountOfImpact = frameCountCurve0 + (frameCountCurve1 - frameCountCurve0 + 1);
	cout<<"ImgCountImpact: "<< imgCountOfImpact<<endl;

	m_curves[curveNumber0].setPointOfImpact(pointOfImpact);
	m_curves[curveNumber0].setFrameCountOfImpact(imgCountOfImpact);
}

void CurveDetection::calculatePointOfStroke(int curveNumber0, int curveNumber1, Point2f &pointOfStroke, float &frameCountOfStroke){
	Point2f slopeInterceptValues0;
	Point2f slopeInterceptValues1;

	imgProc::imageMath::calculateLinearRegression(m_curves[curveNumber0].getLastBallPoint(), m_curves[curveNumber0].getSecondLastBallPoint(), slopeInterceptValues0);
	imgProc::imageMath::calculateLinearRegression(m_curves[curveNumber1].getBallPoint(0), m_curves[curveNumber1].getBallPoint(1), slopeInterceptValues1);

	float slope0 = slopeInterceptValues0.x; //m0
	float intercept0 = slopeInterceptValues0.y;	//b0
	float slope1 = slopeInterceptValues1.x;	//m1
	float intercept1 = slopeInterceptValues1.y;	//b1

	pointOfStroke.x = (intercept1 - intercept0) / (slope0 - slope1);
	pointOfStroke.y = slope0 * pointOfStroke.x + intercept0;

	float frameCountDiff = (pointOfStroke.x - m_curves[curveNumber0].getLastBallPoint().x) / slope0;
	frameCountOfStroke = m_curves[curveNumber0].getLastFrameCount() + frameCountDiff;

	if((frameCountOfStroke != frameCountOfStroke) || frameCountOfStroke > m_curves[curveNumber1].getFirstFrameCount()){
		frameCountOfStroke = m_curves[curveNumber0].getLastFrameCount() + (m_curves[curveNumber0].getFirstFrameCount() - m_curves[curveNumber0].getLastFrameCount() + 1) / 2;
	}
}

void CurveDetection::calculateCurveSpeed(int curveNumber, int curveNumberBestFittingCurve){
	//TODO improve curveSpeed calculation for the new recordings. Maybe match flight direction by following impact point.
	//TODO zwischengespeicherte vectoren löschen und direkt über die get methoden arbeiten.
	Point2f pointOfStroke;
	float frameCountOfStroke;
	calculatePointOfStroke(curveNumber, curveNumberBestFittingCurve, pointOfStroke, frameCountOfStroke);

	Point2f playerPosition = m_curves[curveNumber].getLastPositionOfPlayerDuringCurve();
	vector<Point2f> actualCurve = m_curves[curveNumberBestFittingCurve].getBallPoints();
	vector<int> actualCurveImgCount = m_curves[curveNumberBestFittingCurve].getFrameCounts();

	Point2f actualCurveP0 = actualCurve[actualCurve.size()-3];
	Point2f actualCurveP1 = actualCurve[actualCurve.size()-1];

	//int actualCurveImgCountStart = actualCurveImgCount[0];
	int actualCurveImgCountP0 = actualCurveImgCount[actualCurveImgCount.size()-3];
	int actualCurveImgCountP1 = actualCurveImgCount[actualCurveImgCount.size()-1];

	float dx = (actualCurveP1.x - actualCurveP0.x) / (actualCurveImgCountP1 - actualCurveImgCountP0);
	float imgCountLeavingWindow;
	float speed;
	if(m_isRightFieldSide == true) {
		imgCountLeavingWindow = (0.0f - actualCurveP1.x)/ dx + actualCurveImgCountP1;
		float dTime = imgCountLeavingWindow - frameCountOfStroke;
		float dDistance = abs(1587.0f - playerPosition.x);
		speed = dDistance / dTime * (35.0f * 3.6f / 100.0f);
	}
	else{
		imgCountLeavingWindow = (m_frameWidth - actualCurveP1.x)/ dx + actualCurveImgCountP1;
		float dTime = imgCountLeavingWindow - frameCountOfStroke;
		float dDistance = abs(1587.0f - playerPosition.x);
		speed = dDistance / dTime * (35.0f * 3.6f / 100.0f);

	}
	//cout<<"PLAYER POSITION: "<<playerPosition.x<<endl;
	cout<<"CurveCharacter: "<<m_curves[curveNumber].getCharacterisation()<<" CURVE SPEED: "<<speed<<endl;
	//m_curves[curveNumber-1].setSpeed(speed);
	m_curves[curveNumber].setSpeed(speed);
}

void CurveDetection::drawDetectedCurves(){
	cout<<"drawDetectedCurves"<<endl;
	int radius = 3;//5
	m_drawnCurves = Mat::zeros((int)(m_frameHeight*m_resizingFactor), (int)(m_frameWidth*m_resizingFactor), CV_8UC3);
	for( uint32_t i = 0; i< m_curves.size(); i++ ) {
		//Rot Gruen Blau
		Scalar color = Scalar( 0, 255, 0 );
		if(i%4 ==0) {
			color = Scalar( 255, 255, 0 );
		}
		else if(i%4 ==1) {
			color = Scalar( 0, 255, 255 );
		}
		else if(i%4 ==2) {
			color = Scalar( 255, 0, 255 );
		}
		else{
			color = Scalar( 0, 255,0 );
		}

		if(m_curves[i].getIsCompleted() == 1 && m_curves[i].getCharacterisation()) {
			//if(m_curves[i].getIsCompleted() == 1){
			for(uint32_t j = 0; j < m_curves[i].getBallPointsVecSize(); j++) {
				if(m_curves[i].getFrameCount(j) < m_frameCount - m_framesPredictionCount && m_curves[i].getFrameCount(j) > m_frameCount - 150) {//150/2
					circle( m_drawnCurves, m_curves[i].getBallPoint(j)*m_resizingFactor, radius, color, -1, 8, 0 );
				}
			}
		}
	}
	for(uint32_t i = 0; i < m_curves.size(); i++) {
		if(m_curves[i].getFrameCountOfImpact() < m_frameCount - m_framesPredictionCount && m_curves[i].getFrameCountOfImpact() >= 0) {
			Point2f pointOfImpact = m_curves[i].getPointOfImpact();

			ellipse(m_drawnCurves, pointOfImpact, Size((int)(5* m_resizingFactor), (int)(3* m_resizingFactor)), 0, 0, 360, CV_RGB(255, 0, 0), 2, 8);
			//nicht effizient gelöst
			transformAndDrawImpactPoints(m_curves[i].getPointOfImpact());
		}
		else if(m_curves[i].getFrameCountOfImpact() > 0) {
		}
	}

	//Draw BallcenterPredictions
	//TODO Segfault gefahr!!!
	//da m_drawnBallCenters.size() - 2*m_framesPredictionCount -2 < 0 sein kann, bei uint ist es dann aber 4.294.967.295.
	if(m_frameCount - 2*m_framesPredictionCount -2>= 0){
		for(int i = m_drawnBallCenters.size() - 2*m_framesPredictionCount -2; i<m_drawnBallCenters.size()-m_framesPredictionCount-1; i++){
			for(int pointcounter=0; pointcounter<m_drawnBallCenters[i].size(); pointcounter++){
				//m_drawnCurves += m_drawnBallCenters[i];
				circle( m_drawnCurves, m_drawnBallCenters[i][pointcounter]*m_resizingFactor, (int)(m_drawnBallCentersRadius[i][pointcounter]*m_resizingFactor), Scalar(0, 0, 255), 1, 8, 0 );	//default radius
				circle( m_drawnCurves, m_drawnBallCenters[i][pointcounter]*m_resizingFactor, (int)(m_drawnBallCentersRadius[i][pointcounter]*m_resizingFactor * 0.75), Scalar(255, 0, 0), 1, 8, 0 ); //ROI radius
			}
		}
	}
}

void CurveDetection::transformAndDrawImpactPoints(Point2f impactPoint){

	Point2f transformedPoint = m_fieldDetection->transformPointToModel(impactPoint);
	Scalar color = Scalar(0,0,255);
	m_transformedPointsOfImpact.push_back(transformedPoint);

	vector<Point2f> singleFieldPoints = m_fieldDetection->getSingleFieldPoints();
	//Changing of size to fit to the smaller fieldModel
	Point2f transformedPointResized;
	transformedPointResized.x = transformedPoint.x*0.2f;
	transformedPointResized.y = transformedPoint.y*0.2f;
	//Maybe add some pixel to fieldPoints to get better in/out results
	if(singleFieldPoints[0].x < transformedPoint.x && singleFieldPoints[2].x > transformedPoint.x && singleFieldPoints[0].y < transformedPoint.y && singleFieldPoints[2].y > transformedPoint.y) {
		color = Scalar(0,255,0);
		circle( m_fieldModel, transformedPointResized, 3, color, -1, 8, 0 );
	}
	else{
		if(transformedPointResized.x < 0) {transformedPointResized.x = 0.0;}
		if(transformedPointResized.x > m_fieldModel.cols-1) {transformedPointResized.x = m_fieldModel.cols-5;}//eig 1 aber schlecht zu erkennen
		if(transformedPointResized.y < 0) {transformedPointResized.y = 0.0;}
		if(transformedPointResized.y > m_fieldModel.rows-1) {transformedPointResized.y = m_fieldModel.rows-5;}//

		color = Scalar(0,0,255);
		circle( m_fieldModel, transformedPointResized, 3, color, -1, 8, 0 );
	}
}

void CurveDetection::printCurves(){

	cout<<endl;
	if(m_isRightFieldSide == true){
		cout<<"Curves Right"<<endl;
	}
	else{
		cout<<"Curves Left"<<endl;
	}
	cout<<"m_curves.size() "<<m_curves.size()<<endl;
	for(uint32_t i = 0; i < m_curves.size(); i++) {
		if(m_curves[i].getIsCompleted() == 1) {
			cout<<"Curve: "<<i<<" Length: "<< m_curves[i].getBallPointsVecSize()<<" Completed: "<<m_curves[i].getIsCompleted()<< " Finished: "<<m_curves[i].getIsFinished()<<" Character: "<< m_curves[i].getCharacterisation()<<" StartImg: "<<m_curves[i].getFirstFrameCount()<<" EndImg: "<<m_curves[i].getLastFrameCount()<<endl;

		}
		else if(m_curves[i].getBallPointsVecSize() > 3) {
			cout<<"Curve: "<<i<<" Length: "<< m_curves[i].getBallPointsVecSize()<<" Completed: "<<m_curves[i].getIsCompleted()<< " Finished: "<<m_curves[i].getIsFinished()<<" Character: "<< m_curves[i].getCharacterisation()<<" StartImg: "<<m_curves[i].getFirstFrameCount()<<" EndImg: "<<m_curves[i].getLastFrameCount()<<endl;
		}
		else{
			cout<<"Curve: "<<i<<" Length: "<< m_curves[i].getBallPointsVecSize()<<" Completed: "<<m_curves[i].getIsCompleted()<< " Finished: "<<m_curves[i].getIsFinished()<<" StartImg: "<<m_curves[i].getFirstFrameCount()<<" EndImg: "<<m_curves[i].getLastFrameCount()<<endl;
		}
	}

	/*for(uint32_t i = 0; i < m_curves.size(); i++){
	            if(m_curves[i].getIsCompleted() == 1 && (m_curves[i].getCharacterisation() == 7 || m_curves[i].getCharacterisation() == 8)){
	                     cout<<"Curve: "<<i<<" Length: "<< m_curves[i].getBallPointsVecSize()<<" Completed: "<<m_curves[i].getIsCompleted()<<" Character: "<< m_curves[i].getCharacterisation()<<" StartImg: "<<m_curves[i].getFirstFrameCount()<<" EndImg: "<<m_curves[i].getLastFrameCount()<<endl;
	            }*/
	/*for(uint32_t i = 0; i < m_curves.size(); i++){
	    if(m_curves[i].getBallPointsVecSize() == 6 ){
	      for(uint32_t j = 0; j < m_curves[i].getBallPointsVecSize(); j++){
	          cout<<m_curves[i].getBallPoint(j)<<" ";
	      }
	      for(uint32_t j = 0; j < m_curves[i].getFrameCountVecSize(); j++){
	          cout<<m_curves[i].getFrameCount(j)<<" ";
	      }
	      cout<<endl;
	    }
	   }*/
}

void CurveDetection::printFPS(){
	cout<<endl;
	double numberOfTimes = (double)(m_frameCount - 2);

	m_calculatingFlightPathTime += m_tmCalculatingFlightPath.getTimeMilli();
	double flightPathAvarage = m_calculatingFlightPathTime / numberOfTimes;
	std::cout << "FlightPathCalculation : Avg : " << flightPathAvarage << " ms FPS : " << 1000.0 / flightPathAvarage << std::endl;

	m_deleteErrorCurvesTime += m_tmDeleteErrorCurves.getTimeMilli();
	double deleteErrorAvarage = m_deleteErrorCurvesTime / numberOfTimes;
	std::cout << "Delete Error Curves : Avg : " << deleteErrorAvarage << " ms FPS : " << 1000.0 / deleteErrorAvarage << std::endl;

	m_deleteDoublicatedCurvesTime += m_tmDeleteDoublicatedCurves.getTimeMilli();
	double deleteDoublicateAvarage = m_deleteDoublicatedCurvesTime / numberOfTimes;
	std::cout << "Delete Doublicated Curves : Avg : " << deleteDoublicateAvarage << " ms FPS : " << 1000.0 / deleteDoublicateAvarage << std::endl;

	m_characterisCurvesTime += m_tmCharacteriseCurves.getTimeMilli();
	double characterAvarage = m_characterisCurvesTime / numberOfTimes;
	std::cout << "Characterise Curves : Avg : " << characterAvarage << " ms FPS : " << 1000.0 / characterAvarage << std::endl;

	m_updateCurvesTime += m_tmUpdateCurves.getTimeMilli();
	double updateCurvesAvarage = m_updateCurvesTime / numberOfTimes;
	std::cout << "Update Curves : Avg : " << updateCurvesAvarage << " ms FPS : " << 1000.0 / updateCurvesAvarage << std::endl;

	m_drawCurvesTime += m_tmDrawCurves.getTimeMilli();
	double drawCurvesAvarage = m_drawCurvesTime / numberOfTimes;
	std::cout << "Draw Curves : Avg : " << drawCurvesAvarage << " ms FPS : " << 1000.0 / drawCurvesAvarage << std::endl;
	cout<<endl;
}
