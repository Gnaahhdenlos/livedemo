//
//  playerDetection.cpp
//
//
//  Created by Henri Kuper on 06.09.17.
//
//


#include "playerDetection.hpp"

using namespace std;
using namespace cv;


//--------constructor/destructor-----------------
PlayerDetection::PlayerDetection(FieldDetection *fieldDetectionRight, FieldDetection *fieldDetectionLeft, int frameWidth, int frameHeight) : m_fieldDetectionRight(fieldDetectionRight), m_fieldDetectionLeft(fieldDetectionLeft), m_frameWidth(frameWidth), m_frameHeight(frameHeight){

	playerDetectionInit();
}

PlayerDetection::~PlayerDetection(){

}
//--------CPU - functions----------------
#ifndef GPU_ON
void PlayerDetection::playerDetectionInit(){

	m_inputVideoResizingFactor = (float) m_frameWidth/1280.0f;
	cout<<"Playerdetection: m_inputVideoResizingFactor: "<<m_inputVideoResizingFactor<<endl;

	m_isInitialMergeDone = false;
	m_frameCount = 0;
	m_maxDistanceToPreviusDetection = 60.0f * m_inputVideoResizingFactor;
	m_maxXDistanceToPreviusDetection = 40.0f * m_inputVideoResizingFactor;
	m_maxYDistanceToPreviusDetection = 70.0f * m_inputVideoResizingFactor;

	m_frameCountDiff = 10;
	m_histSize = 12;
	m_numberOfDataNeeded = 130;
	m_numberOfDataNeededForHistComparison = 200;

	m_minVerticalMovement = 50.0f * m_inputVideoResizingFactor;
	m_minHorizontalMovement = 60.0f * m_inputVideoResizingFactor;

	m_numberOfLeftPlayer = 0;
	m_numberOfRightPlayer = 0;
	cout<<"PlayerSegmentationInit"<<endl;
	m_playerSegmentationRight = new PlayerSegmentation(m_fieldDetectionRight, m_frameWidth, m_frameHeight);
	m_playerSegmentationLeft = new PlayerSegmentation(m_fieldDetectionLeft, m_frameWidth, m_frameHeight);
}

void PlayerDetection::initBackgroundModel(Mat& frameRight, Mat& frameLeft){
	m_playerSegmentationRight->initBackgroundModel(frameRight);
	m_playerSegmentationLeft->initBackgroundModel(frameLeft);
}

void PlayerDetection::detecPlayer(vector<Mat>& frameBufferGrayRight, vector<Mat>& frameBufferGrayLeft){

	if(frameBufferGrayRight.size() >= 2 && frameBufferGrayLeft.size() >= 2) {

		// m_playerSegmentationRight->createForegroundMask(frameBufferGrayRight[1]);
		// m_playerSegmentationLeft->createForegroundMask(frameBufferGrayLeft[1]);
		m_tmCreateForegroundMask.reset();m_tmCreateForegroundMask.start();
		// thread playerSegmentationRightThread(&PlayerSegmentation::createForegroundMaskWithMog2, m_playerSegmentationRight, std::ref(frameBufferGrayRight[1]));
		// thread playerSegmentationLeftThread(&PlayerSegmentation::createForegroundMaskWithMog2, m_playerSegmentationLeft, std::ref(frameBufferGrayLeft[1]));
		// playerSegmentationRightThread.join();
		// playerSegmentationLeftThread.join();
		m_playerSegmentationRight->createForegroundMaskWithMog2(frameBufferGrayRight[1]);
		m_playerSegmentationLeft->createForegroundMaskWithMog2(frameBufferGrayLeft[1]);

		m_tmCreateForegroundMask.stop();

		m_tmTrackPlayer.reset();m_tmTrackPlayer.start();
		trackPlayer(m_playerSegmentationRight, frameBufferGrayRight[1]);
		trackPlayer(m_playerSegmentationLeft, frameBufferGrayLeft[1]);
		m_tmTrackPlayer.stop();
		m_tmDetecFalseDetectedPlayer.reset();m_tmDetecFalseDetectedPlayer.start();
		deleteFalseDetectedPlayer();
		m_tmDetecFalseDetectedPlayer.stop();
		for(int i = 0; i < m_players.size(); i++) {
			if(m_players[i].getHistogramRight().rows == 0 && m_players[i].getHasRightSideData() == true) {
				cout<<"Player: "<<i<<" has RightData but no RightHistogram"<<endl;
			}
			if(m_players[i].getHistogramLeft().rows == 0 && m_players[i].getHasLeftSideData() == true) {
				cout<<"Player: "<<i<<" has Leftdata but no LeftHistogram"<<endl;
			}
		}
		m_tmMergeCorrespondingPlayer.reset();m_tmMergeCorrespondingPlayer.start();
		mergeCorrespondingPlayer();
		m_tmMergeCorrespondingPlayer.stop();
		printFPS();
		//printPlayer();
	}
	else{
		cout<<"PlayerDetection: FrameBuffer Gray has less than 3 images"<<endl;
	}

	//showTracking(frameBufferGrayRight[1]);

	m_frameCount++;
}

void PlayerDetection::trackPlayer(PlayerSegmentation *playerSeg, Mat &frameGray){
	cout<<"TrackPlayer"<<endl;
	vector<float> contourAreas;
	vector<Mat> contourHistograms;
	vector<Point2f> contourMomentsCenter;
	vector<int> contourAlreadyUsed;

	//Mat histogram;
	const vector<vector<Point> > &playerContours = playerSeg->getMainHumanMaskContours();
	const std::vector<cv::Moments> &playerMoments  = playerSeg->getLastHumanMaskMoments();
	const std::vector<cv::Point2f> &playerPosition  = playerSeg->getMainPlayerPositionInModel();
	const std::vector<cv::Rect> &playerRects = playerSeg->getHumanMaskBoundRects();

	for(uint32_t i = 0; i < playerContours.size(); i++) {
		contourAreas.push_back(contourArea(playerContours[i]));
		contourMomentsCenter.push_back(Point2f());
		imgProc::imageMath::calculateMomentMassCenter(playerMoments[i], contourMomentsCenter.back());
		contourHistograms.push_back(Mat());
		createHistogramOfContour(frameGray, playerContours, i, contourHistograms.back(), playerRects, playerMoments);
		contourAlreadyUsed.push_back(0);
	}

	for(uint j = 0; j < m_players.size(); j++) {
		if(m_players[j].getIsRight() == playerSeg->getIsRightFieldSide() && m_players[j].getIsOutOfScope() == false) {
			int contourNumber = -1;
			float minDistanceToPreviusDetection = 10000.0;
			float maxContourAreaDividedDistance = 0;
			for(uint32_t i = 0; i < playerContours.size(); i++) {
				float dist;
				Point2f lastCenter = m_players[j].getLastCenterOfPlayer();
				//calculate lastCenter abs difference to contourCenter x and y
				float xDiff = abs(contourMomentsCenter[i].x - lastCenter.x);
				float yDiff = abs(contourMomentsCenter[i].y - lastCenter.y);
				imgProc::imageMath::calculateDistanceBetween(contourMomentsCenter[i], lastCenter, dist);
				float contourAreaDividedDistance = contourAreas[i] / sqrt(dist);

				if((dist < m_maxDistanceToPreviusDetection || (xDiff < m_maxXDistanceToPreviusDetection && yDiff < m_maxYDistanceToPreviusDetection)) && maxContourAreaDividedDistance < contourAreaDividedDistance) {
					minDistanceToPreviusDetection = dist;
					maxContourAreaDividedDistance = contourAreaDividedDistance;
					contourNumber = i;
				}
			}
			if(contourNumber != -1) {
				m_players[j].appendPlayerData(m_frameCount, contourMomentsCenter[contourNumber], playerPosition[contourNumber], contourHistograms[contourNumber]);
				contourAlreadyUsed[contourNumber] = 1;
			}
		}
	}
	for(uint i = 0; i < playerContours.size(); i++) {
		if(contourAlreadyUsed[i] == 0) {
			m_players.push_back(Player(playerSeg->getIsRightFieldSide(), m_frameCount, contourMomentsCenter[i], playerPosition[i], contourHistograms[i]));
		}
	}
}

void PlayerDetection::createHistogramOfContour(Mat &frameGray, const vector<vector<Point> > &humanMaskContours, int contourNumber, Mat &histogram, const vector<Rect> &playerRects, const vector<Moments> &playerMoments){
	Mat playerMask = Mat::zeros(frameGray.size(), frameGray.type());
	//Mat playerMaskNew = Mat::zeros(frameGray.size(), frameGray.type());
	/// Set the ranges ( for B,G,R) )
	float range[] = { 0, 256 }; //the upper boundary is exclusive
	const float* histRange = { range };
	int channels[] = { 0 };
	bool uniform = true; bool accumulate = false;
	vector<vector< Point > > reshapeContour;
	reshapeContour.push_back(vector< Point >());

	imgProc::imageObjects::reshapeContourWithMassCenter(humanMaskContours[contourNumber], reshapeContour[0], 3);
	drawContours( playerMask, reshapeContour, 0, Scalar(255), CV_FILLED, 8, noArray() );

	calcHist( &frameGray, 1, channels, playerMask, histogram, 1, &m_histSize, &histRange, uniform, accumulate );
	normalize(histogram, histogram, 1.0f, 0, NORM_MINMAX, -1, Mat() ); //NORM_MINMAX
	for(uint i = 0; i < histogram.rows; i++) {
		histogram.at<float>(i) = abs(histogram.at<float>(i));
	}
}

void PlayerDetection::searchForPlayerPositionBevoreFrameCount(int frameCount, bool isRight, vector<Point2f> &dstPosition){
	cout<<"Search for player position bevore frameCount"<<endl;
	int numberOfBestFittingPlayer = -1;
	float maxHorizontalMovement = 0.0f;
	for(int i = 0; i < m_players.size(); i++){
		if(isRight == true && m_players[i].getHasRightSideData() == true){
			if(m_players[i].getFrameCountOfDisappearingRightIsEmpty() == false && m_players[i].getFrameCountOfAppearingRightIsEmpty() == false){
				if(m_players[i].getLastFrameCountOfAppearingRight() < frameCount && m_players[i].getLastFrameCountOfDisappearingRight() >= frameCount){
					if(m_players[i].getHorizontalMovement() > maxHorizontalMovement){
						maxHorizontalMovement = m_players[i].getHorizontalMovement();
						numberOfBestFittingPlayer = i;
					}
				}
			}
		}
		else if(isRight == false && m_players[i].getHasLeftSideData() == true){
			if(m_players[i].getFrameCountOfDisappearingLeftIsEmpty() == false && m_players[i].getFrameCountOfAppearingLeftIsEmpty() == false){
				if(m_players[i].getLastFrameCountOfAppearingLeft() < frameCount && m_players[i].getLastFrameCountOfDisappearingLeft() >= frameCount){
					if(m_players[i].getHorizontalMovement() > maxHorizontalMovement){
						maxHorizontalMovement = m_players[i].getHorizontalMovement();
						numberOfBestFittingPlayer = i;
					}
				}
			}
		}
	}
	cout<<"numberOfBestFittingPlayer: "<<numberOfBestFittingPlayer<<endl;
	if(numberOfBestFittingPlayer > -1){
		const vector<int> &frameCounts = m_players[numberOfBestFittingPlayer].getFrameCounts();
		const vector<Point2f> & positionsOfPlayerInModel = m_players[numberOfBestFittingPlayer].getPositionsOfPlayerInModel();
		int i = (int)frameCounts.size() - 1;
		cout<<"FrameCountsSize: "<<i<<endl;
		cout<<"positionsOfPlayerInModelSize: "<<positionsOfPlayerInModel.size()<<endl;
		cout<<"FrameCount: "<<frameCount<<endl;
		int iFrameCountWasFound = 0;
		while(i >= 0 && i >= iFrameCountWasFound - 10){
			if(frameCounts[i] <= frameCount){
				if(iFrameCountWasFound == 0){
					iFrameCountWasFound = i;
				}
				dstPosition.push_back(m_players[numberOfBestFittingPlayer].getPositionOfPlayerInModel(i));
			}
			i--;
		}
	}
	else{
		cout<<"COULD NOT FIND RETURN PLAYER"<<endl;
	}
}

void PlayerDetection::deleteFalseDetectedPlayer(){
	cout<<"DeleteFalseDetectedPlayer"<<endl;
	for(uint i = 0; i < m_players.size(); i++) {
		int lastFrameCount = m_players[i].getLastFrameCount();
		if(lastFrameCount < m_frameCount - m_frameCountDiff && m_players[i].getFrameCounts().size() < m_numberOfDataNeeded) {
			m_players.erase(m_players.begin()+i);
		}
		else if(m_players[i].getFrameCounts().size() > m_numberOfDataNeeded && m_players[i].getHorizontalMovement() < m_minHorizontalMovement && m_players[i].getVerticalMovement() < m_minVerticalMovement ) {
			m_players.erase(m_players.begin()+i);
		}
		else if(lastFrameCount < m_frameCount - m_frameCountDiff) {
			m_players[i].setIsOutOfScope(true);
		}
	}

	for(uint i = 0; i < m_players.size(); i++) {
		for(uint j = i + 1; j < m_players.size(); j++) {
			if(m_players[i].getLastCenterOfPlayer() ==  m_players[j].getLastCenterOfPlayer()) {
				if(m_players[i].getDetectionCounter() < m_players[j].getDetectionCounter()) {
					deletePlayer(i);
				}
				else if(m_players[i].getDetectionCounter() >= m_players[j].getDetectionCounter()) {
					deletePlayer(j);
				}
			}
		}
	}
}

void PlayerDetection::mergeCorrespondingPlayer(){
	//TODO bestes match suchen, wenn unter 0.2 dann mergen.

	if(m_players.size() > 0) {
		for(int32_t i = ((int)m_players.size()-1); i > 0; i--) {
			if(m_players[i].getIsOutOfScope() == true && !m_players[i].getIsComparedToOtherHistograms() && i >= 0) {
				int mergeWithPlayerNumber = -1;
				float minCompareDist = 10.0f * m_inputVideoResizingFactor;
				for(int32_t j = i -1; j >= 0; j--) {
					if(m_players[i].getHasRightSideData() == true && m_players[j].getHasRightSideData() == true) {
						float compareDist = compareHist(m_players[i].getHistogramRight(), m_players[j].getHistogramRight(), CV_COMP_BHATTACHARYYA);
						// cout<<"HISTCOMPARE right between: "<<i<<" and "<<j<<" "<<compareDist<<endl;
						if(minCompareDist > compareDist){
							mergeWithPlayerNumber = j;
							minCompareDist = compareDist;
						}
					}
					else if(m_players[i].getHasLeftSideData() == true && m_players[j].getHasLeftSideData() == true) {
						float compareDist = compareHist(m_players[i].getHistogramLeft(), m_players[j].getHistogramLeft(), CV_COMP_BHATTACHARYYA);
						// cout<<"HISTCOMPARE left between: "<<i<<" and "<<j<<" "<<compareDist<<endl;
						if(minCompareDist > compareDist){
							mergeWithPlayerNumber = j;
							minCompareDist = compareDist;
						}
					}
				}
				if(minCompareDist < 0.2f){
					mergePlayer(mergeWithPlayerNumber,i);
				}
			}
		}
	}
	//Not good enough. And for the moment not necessary.
	//mergeDifferentSidePlayer();
}

void PlayerDetection::mergePlayer(int playerNumber0, int playerNumber1){

	m_players[playerNumber0].mergeWithOtherPlayer(m_players[playerNumber1]);
	deletePlayer(playerNumber1);
}

void PlayerDetection::mergeDifferentSidePlayer(){
	//TODO Nur mergen, wenn alle 4 sich von der image count nicht überlappen.
	if(m_isInitialMergeDone == false) {
		if(m_players.size() >= 4) {
			int numberOfPlayersOutOfScope = 0;
			for(uint i = 0; i < m_players.size(); i++) {
				if(m_players[i].getIsOutOfScope() == true) {
					numberOfPlayersOutOfScope++;
				}
			}
			if(numberOfPlayersOutOfScope >= 4) {
				int numberOfFirstLeftPlayer = -1;
				int numberOfSecondLeftPlayer = -1;
				int numberOfFirstRightPlayer = -1;
				int numberOfSecondRightPlayer = -1;

				for(int i = 0; i < m_players.size(); i++) {
					if(numberOfFirstLeftPlayer == -1 && m_players[i].getHasLeftSideData() == true) {
						numberOfFirstLeftPlayer = i;
					}
					else if(numberOfFirstLeftPlayer > -1 && numberOfSecondLeftPlayer == -1 && m_players[i].getHasLeftSideData() == true) {
						numberOfSecondLeftPlayer = i;
					}
					if(numberOfFirstRightPlayer == -1 && m_players[i].getHasRightSideData() == true) {
						numberOfFirstRightPlayer = i;
					}
					else if(numberOfFirstRightPlayer > -1 && numberOfSecondRightPlayer == -1 && m_players[i].getHasRightSideData() == true) {
						numberOfSecondRightPlayer = i;
					}
				}

				if(numberOfFirstLeftPlayer > -1 && numberOfSecondLeftPlayer > -1 && numberOfFirstRightPlayer > -1 && numberOfSecondRightPlayer > -1) {
					m_players[numberOfFirstLeftPlayer].mergeWithOtherPlayer(m_players[numberOfSecondRightPlayer]);
					m_players[numberOfFirstRightPlayer].mergeWithOtherPlayer(m_players[numberOfSecondLeftPlayer]);
					if(numberOfSecondRightPlayer > numberOfSecondLeftPlayer) {
						deletePlayer(numberOfSecondRightPlayer);
						deletePlayer(numberOfSecondLeftPlayer);
					}
					else{
						deletePlayer(numberOfSecondLeftPlayer);
						deletePlayer(numberOfSecondRightPlayer);
					}
					cout<<"Initial Merge is Done"<<endl;
					cout<<"Player"<< numberOfFirstRightPlayer <<" 0 hasRightSideData: "<<m_players[numberOfFirstRightPlayer].getHasRightSideData()<<endl;
					cout<<"Player"<<numberOfFirstRightPlayer<<" 0 hasLeftSideData: "<<m_players[numberOfFirstRightPlayer].getHasLeftSideData()<<endl;
					cout<<"Player"<<numberOfFirstLeftPlayer<<" 0 hasRightSideData: "<<m_players[numberOfFirstLeftPlayer].getHasRightSideData()<<endl;
					cout<<"Player"<<numberOfFirstLeftPlayer<<" 0 hasLeftSideData: "<<m_players[numberOfFirstLeftPlayer].getHasLeftSideData()<<endl;
					m_isInitialMergeDone = true;
				}
			}
		}
	}
}

void PlayerDetection::deletePlayer(int position){

	m_players.erase(m_players.begin() + position);
}

void PlayerDetection::showHistogram(int playerNumber){

	if(m_players.size() > playerNumber) {
		string imgName = "calcHist Demo";

		imgName += to_string(playerNumber);
		Mat histogram;
		if(m_players[playerNumber].getIsRight() == true) {
			histogram = m_players[playerNumber].getHistogramRight();
			cout<<"BatCoeffizient of Player Left"<<imgName<<": "<< m_players[playerNumber].getAvarageVariationOfHistogramsRight()<<endl;
			//imgName += " Right ";
		}
		else{
			histogram = m_players[playerNumber].getHistogramLeft();
			cout<<"BatCoeffizient of Player Left"<<imgName<<": "<< m_players[playerNumber].getAvarageVariationOfHistogramsLeft()<<endl;
			//imgName += " Left ";
		}

		int hist_w = 512; int hist_h = 400;
		int bin_w = cvRound( (double) hist_w/m_histSize );
		Mat histImage( hist_h, hist_w, CV_8UC1, Scalar(0) );

		for( int i = 0; i < m_histSize; i++ )
		{
			int val = saturate_cast<int>(histogram.at<float>(i)*histImage.rows);
			rectangle( histImage, Point(i*bin_w,histImage.rows),
			           Point((i+1)*bin_w,histImage.rows - val),
			           Scalar(255), -1, 8 );
		}
		imshow(imgName, histImage );
	}
}

void PlayerDetection::showTracking(Mat &frameGray){

	Point start;
	Mat trackingRight = Mat::zeros(frameGray.size(), CV_8UC1);
	for(uint i = 0; i < m_players.size(); i++) {
		if(m_players[i].getIsRight() == true) {
			start = m_players[i].getCentersOfPlayer()[0];
			for(uint j = 1; j < m_players[i].getCentersOfPlayer().size(); j++) {
				int thickness = 2;
				int lineType = 8;
				Point end = m_players[i].getCentersOfPlayer()[j];
				line( trackingRight, start, end, Scalar( (int)(255/m_players.size()*(m_players.size()-i)) ), thickness, lineType );
				start = end;
			}
		}
	}

	Mat trackingLeft = Mat::zeros(frameGray.size(), CV_8UC1);
	for(uint i = 0; i < m_players.size(); i++) {
		if(m_players[i].getIsRight() == false) {
			start = m_players[i].getCentersOfPlayer()[0];
			for(uint j = 1; j < m_players[i].getCentersOfPlayer().size(); j++) {
				int thickness = 2;
				int lineType = 8;
				Point end = m_players[i].getCentersOfPlayer()[j];
				line( trackingLeft, start, end, Scalar( (int)(255/m_players.size()*(m_players.size()-i))), thickness, lineType );
				start = end;
			}
		}
	}

	//Just for debugging
	// if(m_frameCount%60 == 0) {
	// 	imshow("trackingRight", trackingRight);
	// 	imshow("trackingLeft", trackingLeft);
	//
	// 	for(int i = 0; i < 8; i++) {
	// 		//showHistogram(i);
	// 	}
	// 	waitKey(30);
	//}
}

void PlayerDetection::printPlayer(){
	cout<<endl;
	for(int i = 0; i < m_players.size(); i++) {
		for(int j = 0; j < m_players[i].getFrameCountOfAppearingRight().size(); j++) {
			cout<<"AppearingRight: "<<i<<"    "<<m_players[i].getFrameCountOfAppearingRight()[j]<<endl;
			cout<<"DisappearingRight: "<<i<<" "<<m_players[i].getFrameCountOfDisappearingRight()[j]<<endl;
		}
		for(int j = 0; j < m_players[i].getFrameCountOfAppearingLeft().size(); j++) {
			cout<<"AppearingLeft: "<<i<<"    "<<m_players[i].getFrameCountOfAppearingLeft()[j]<<endl;
			cout<<"DisappearingLeft: "<<i<<" "<<m_players[i].getFrameCountOfDisappearingLeft()[j]<<endl;
		}
	}
	cout<<endl;
}

void PlayerDetection::printFPS(){
	cout<<endl;
	m_foreGroundMaskTimes.push_back(m_tmCreateForegroundMask.getTimeMilli());
	std::sort(m_foreGroundMaskTimes.begin(), m_foreGroundMaskTimes.end());
	double foregroundAvarage = std::accumulate(m_foreGroundMaskTimes.begin(), m_foreGroundMaskTimes.end(), 0.0) / m_foreGroundMaskTimes.size();
	std::cout << "Foreground : Avg : " << foregroundAvarage << " ms FPS : " << 1000.0 / foregroundAvarage << std::endl;

	m_trackPlayerTimes.push_back(m_tmTrackPlayer.getTimeMilli());
	std::sort(m_trackPlayerTimes.begin(), m_trackPlayerTimes.end());
	double trackPlayerAvarage = std::accumulate(m_trackPlayerTimes.begin(), m_trackPlayerTimes.end(), 0.0) / m_trackPlayerTimes.size();
	std::cout << "TrackPlayer : Avg : " << trackPlayerAvarage << " ms FPS : " << 1000.0 / trackPlayerAvarage << std::endl;

	m_deleteFalseDetectedPlayerTimes.push_back(m_tmDetecFalseDetectedPlayer.getTimeMilli());
	std::sort(m_deleteFalseDetectedPlayerTimes.begin(), m_deleteFalseDetectedPlayerTimes.end());
	double deleteFalsePlayerAvarage = std::accumulate(m_deleteFalseDetectedPlayerTimes.begin(), m_deleteFalseDetectedPlayerTimes.end(), 0.0) / m_deleteFalseDetectedPlayerTimes.size();
	std::cout << "DeleteFalsePlayer : Avg : " << deleteFalsePlayerAvarage << " ms FPS : " << 1000.0 / deleteFalsePlayerAvarage << std::endl;

	m_mergeCorrespondingPlayer.push_back(m_tmMergeCorrespondingPlayer.getTimeMilli());
	std::sort(m_mergeCorrespondingPlayer.begin(), m_mergeCorrespondingPlayer.end());
	double mergePlayerAvarage = std::accumulate(m_mergeCorrespondingPlayer.begin(), m_mergeCorrespondingPlayer.end(), 0.0) / m_mergeCorrespondingPlayer.size();
	std::cout << "MergePlayer : Avg : " << mergePlayerAvarage << " ms FPS : " << 1000.0 / mergePlayerAvarage << std::endl;
	cout<<endl;
}

#endif
