//
//  backWardscoring.hpp
//
//
//  Created by Henri Kuper on 16.10.17.
//
//

#ifndef backwardScoring_hpp
#define backwardScoring_hpp

#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <queue>

#include "configData.hpp"
#include "scoringNode.hpp"
#include "eventDetection.hpp"
#include "event.hpp"
#include "../statisticsLib/match.hpp"


class BackwardScoring {
private:

EventDetection *m_eventDetection;

ScoringNode m_scoringTree;

std::vector<ScoringNode *> m_newestScoringNodes;

Match *m_match;

int m_actualEventNumber;

int m_actualServeTry;
int m_playerWinsRally;						//Number of player that would win if the rally ends now. 0 undefined, 1 player on the left side, 2 player on the right side
float m_actualProbabilityOfScore;
int m_numberOfImpactsInside;
bool m_faultDetected;
bool m_updatedProbability;
bool m_secondServeIsReasonable;
bool m_hasDetectedDoubleFault;
bool m_hasDetectedImpactAfterServe;
bool m_serveIsAce;
int m_isWinnerForSide;
int m_isErrorForSide;

bool m_firstServeDetected;
bool m_newEventDetected;
enum m_eventTypes {STROKE, SERVE, IMPACT_AFTER_SERVE, OVER_NET, CRITICAL_IMPACT, END_OF_RALLY, END_OF_GAME, END_OF_SET, END_OF_MATCH, CONTINUATION_OF_RALLY};

void backwardScoringInit();
void updateEvent();
void printNewestScoringNodes();
void printScoringNode(ScoringNode *scoringNode);
float calculateProbabilityOfPoint(float eventProbability);

// bool wayToSortScoringNodes(ScoringNode *node0, ScoringNode *node1) { return node0->getProbabilityScoreIsCorrect() > node1->getProbabilityScoreIsCorrect();}

public:
BackwardScoring(EventDetection *eventDetection, Match *match);
~BackwardScoring();

void scoring();
void scoringDebug();
void processStrokeEvent();
void processServeEvent();
void processImpactAfterServeEvent();
void processOverNetEvent();
void processCriticalImpactEvent();
void processEndOfRallyEvent();
void processContinuationOfRallyEvent();
void processEndOfGameEvent();
void processEndOfMatchEvent();
void recalculateScoringHistory(ScoringNode *mostLikelyScoringNode, bool &foundCorrectScoring);

void updateScoreProbabilites();
void updateStatistics();
void updateNewestScoringNodesAfterSecondServe(int startFrameCount, float speed);
void updateNewestScoringNodesAfterFirstServe(int newServePos, int startFrameCount, bool serveSideIsRight, float speed);
bool updateNewestScoringNodesAfterFirstAndSecondServe(int newServePos, int startFrameCount, bool serveSideIsRight, float speed);
void updateStrokeSpeed(float strokeSpeed, bool isRight);
void updateStroke(int strokeType, bool isRight);

void markPossibleScoringPath(ScoringNode* startNode);

bool mergeEqualScoringNodes(int nodeNumber);
void mergeEqualGameNodes();
void deleteFalseScoringPath();
bool checkIfPossbibleScoringPathExist(bool isChangeOfServe);
void restockScoringPaths();
void adjustProbabilities();


Match *getMatch() {return m_match;}

};

#endif
