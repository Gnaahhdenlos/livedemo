//beau
//  ballDetection.cpp
//
//
//  Created by Henri Kuper on 23.06.17.
//
//

/*TODO

   - playerClassification implementieren mit histogrammanalyse

 */

//#define GPU_ON
#include "ballDetection.hpp"


using namespace cv;
using namespace std;

//--------CPU-GPU shared private functions--------

void BallDetection::ballDetectionInit(){
	//if m_field is not calibrated the function does not throw an exception.
	m_isRightFieldSide = m_field->getIsRightFieldSide();

	m_inputVideoResizingFactor = (float)m_frameWidth/1280.0f;
	cout<<"BallDetection m_inputVideoResizingFactor: "<<m_inputVideoResizingFactor<<endl;

	m_frameCount = 0;
	m_framesPredictionCount = 42;

	m_lowH = 37; //28
	m_highH = 44; //42

	m_lowS = 55;//60
	m_highS = 255;

	m_lowV = 60;//60
	m_highV = 255;

	m_resizing0 = 1.0f; //0.5
	m_resizing1 = 1.0f/(m_resizing0);

	m_differenceImgThreshold = 10.0f;//13//12

	m_erodeDilateSize = 3;//4	//too small to be multiplied with inputVideoResizingFactor

	m_maxHeightOfContour = 90.0*m_inputVideoResizingFactor;

	m_avarageDistMaxToContour = 20.0*m_inputVideoResizingFactor;

	m_maxBallArea = 2000.0f*m_inputVideoResizingFactor*m_inputVideoResizingFactor;
}

//--------constructor/destructor-----------------

BallDetection::BallDetection(int height, int width, FieldDetection *field, PlayerSegmentation *player) : m_frameHeight(height), m_frameWidth(width), m_field(field), m_playerSegmentation(player){
	ballDetectionInit();
}

BallDetection::BallDetection() : m_frameHeight(1080),m_frameWidth(1920){
	ballDetectionInit();
}

BallDetection::~BallDetection(){

}

#ifndef GPU_ON
//--------CPU - functions----------------

void BallDetection::updateHumanMask(){
	m_humanMaskROI = m_playerSegmentation->getHumanMaskRoi();
	m_humanMaskPositionInModel = m_playerSegmentation->getPlayerPositionInModel();
	//m_humanMask = m_playerSegmentation->getForegroundMask();
	m_humanMaskContours = m_playerSegmentation->getHumanMaskContours();
}


void BallDetection::detecBallWithDiff(const vector<Mat> &originalImgBuffer, const vector<Mat> &grayFrameBuffer){


	//grayFrameBuffer.push_back(Mat());
	Mat resizedImg;
	//cout<<"originalImgBuffer: "<<originalImgBuffer.size()<<endl;
	//resize(originalImgBuffer[m_framesPredictionCount], resizedImg, Size(), m_resizing0, m_resizing0, INTER_LINEAR);//INTER_NEAREST
	//cvtColor(resizedImg, grayFrameBuffer.back(), COLOR_BGR2GRAY);
	//vector<vector<Point> > possibleBallContours;
	//vector<vector<Point> > possibleBallContoursROI;
	//vector<Vec4i> hierarchy;

	if(grayFrameBuffer.size() > 2) {

		m_erodeDilateSize = 4; //eig 6
		//Mat HSVImgResized;

		//cout<<"Balldetection: "<<originalImgBuffer[m_framesPredictionCount-1].size().width<<endl;
		//cout<<"Balldetection: "<<originalImgBuffer[m_framesPredictionCount-1].size().height<<endl;

		resize(originalImgBuffer[m_framesPredictionCount-1], m_HSVImg, Size(), m_resizing0, m_resizing0, INTER_LINEAR);//INTER_NEAREST
		cvtColor(m_HSVImg, m_HSVImg, COLOR_BGR2HSV);
		//uneffizient implementiert
		cv::absdiff(grayFrameBuffer[0], grayFrameBuffer[1], m_diffImage0);
		cv::absdiff(grayFrameBuffer[1], grayFrameBuffer[2], m_diffImage1);

		updateHumanMask();

		if(m_frameCount > 0) {
			createMovementMask();

			checkPossibleBallPositions();
			checkPossibleBallPositionsROI();
			//drawPossibleBallContours();
		}

		m_possibleBallCenterOfFrames.push_back(m_actualBallCenters);
		//printPossibleBallCenters();

		m_frameCount++;
		//grayFrameBuffer.erase(grayFrameBuffer.begin());
	}
}

void BallDetection::createMovementMask(){

	Mat humanMaskPoints, yellowHumanPoints;
	Mat movementMask(m_diffImage0.size(), m_diffImage0.type());
	Mat movementMaskWithoutHumanROI(m_diffImage0.size(), m_diffImage0.type());
	Mat movementMaskHumanROI(m_diffImage0.size(), m_diffImage0.type());
	Mat movementMask0(m_diffImage0.size(), m_diffImage0.type());
	Mat movementMask1(m_diffImage1.size(), m_diffImage1.type());
	vector<Vec4i> hierarchy;

	m_actualPossibleBallContours.clear();
	m_actualPossibleBallContoursROI.clear();

	threshold(m_diffImage0, movementMask0, m_differenceImgThreshold, 255.0f, THRESH_BINARY);
	threshold(m_diffImage1, movementMask1, m_differenceImgThreshold, 255.0f, THRESH_BINARY);

	bitwise_and(movementMask0, movementMask1, movementMask, noArray());
	// if(m_isRightFieldSide == true){
	//   imshow("DiffRight", movementMask);
	//   waitKey(30);
	// }

	//To cut off the ventilator movement
	Mat blackBox = Mat::zeros(130*m_inputVideoResizingFactor, 90*m_inputVideoResizingFactor, CV_8UC1);
	blackBox.copyTo(movementMask(Rect(0,0, blackBox.cols, blackBox.rows)));
	blackBox.copyTo(movementMask(Rect(movementMask.cols - blackBox.cols, 0, blackBox.cols, blackBox.rows)));

	//filters huge movements from image 0 to image 2
	Mat colorFilteredBinaryImg;
	tm.reset(); tm.start();
	// inRange(m_HSVImg, Scalar(m_lowH, m_lowS, m_lowV), Scalar(m_highH, m_highS, m_highV), colorFilteredBinaryImg); //Threshold the image
	// //calculates a mask only containing points which are inside the human contour
	// bitwise_and(movementMask, m_humanMaskROI, humanMaskPoints, noArray());
	//
	// //calculates a mask only containing points which are inside the human contour and yellow
	// bitwise_and(humanMaskPoints, colorFilteredBinaryImg, yellowHumanPoints, noArray());
	//
	// //calculates a mask only containing points which are outside the human contour
	// Mat pointsWithoutHuman = movementMask - m_humanMaskROI;
	//
	// Mat test = m_humanMaskROI - yellowHumanPoints;
	// //calculates a mask only containing points which are outside the human contour or yellow
	// bitwise_or(yellowHumanPoints, pointsWithoutHuman, movementMask, noArray());

	m_erodeDilateSize = 3;//4

	//morphological opening (removes small objects from the foreground)
	erode( movementMask, movementMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );
	//Checking how many possibleBallCenters are Detected. If there are too many, we erode again.
	Mat movementMaskTmp = movementMask - m_humanMaskROI;
	findContours( movementMaskTmp, m_actualPossibleBallContours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	cout<<"NumberOfBallCenterBevore: "<<m_actualPossibleBallContours.size()<<endl;
	if(m_actualPossibleBallContours.size() > 50){
		cout<<"Another erode"<<endl;
		erode( movementMask, movementMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)), Point(-1,-1), 2);
		dilate( movementMask, movementMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)), Point(-1,-1), 2);
	}
	dilate( movementMask, movementMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)));
	//morphological closing (removes small holes from the foreground)
	dilate( movementMask, movementMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );
	erode(movementMask, movementMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );
	resize(movementMask, movementMask, Size(), m_resizing1, m_resizing1, INTER_NEAREST);

	m_movementMask = movementMask - m_humanMaskROI;
	m_movementMaskHumanROI = movementMask - m_movementMask;

	m_actualPossibleBallContours.clear();

	// Find contours
	findContours( m_movementMask, m_actualPossibleBallContours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	findContours( m_movementMaskHumanROI, m_actualPossibleBallContoursROI, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	cout<<"NumberOfBallCenterAfter: "<<m_actualPossibleBallContours.size()<<endl;
	if(m_actualPossibleBallContours.size() > 20){
		cout<<"NUMBER OF CURVES MAYBE TO BIG"<<endl;
	}

	// if(m_isRightFieldSide == true){
	// 	imshow("m_movementMaskRight", m_movementMask);
	// 	imshow("m_humanMaskROI", m_humanMaskROI);
	// }
	// if(m_isRightFieldSide == false){
	// 	imshow("m_movementMaskLeft", m_movementMask);
	// }
	// if(m_frameCount > 1640){
	// 	waitKey(0);
	// }
	// else{
	// 	waitKey(30);
	// }
	/*
	imshow("MovementMaskHumanROI", m_movementMaskHumanROI);
	imshow("MovementMask", m_movementMask);
	waitKey(1);
	*/

	tm.stop();

	erode_times0.push_back(tm.getTimeMilli());
	std::sort(erode_times0.begin(), erode_times0.end());
	double erode0_avg = std::accumulate(erode_times0.begin(), erode_times0.end(), 0.0) / erode_times0.size();
	std::cout << "Color : Avg : " << erode0_avg << " ms FPS : " << 1000.0 / erode0_avg << std::endl;
}


void BallDetection::checkPossibleBallPositions(){

	m_actualPointsDistToHumanMask.clear();
	m_actualPointsPlayerPosition.clear();
	m_actualBallBoundRects.clear();
	m_actualBallCenters.clear();

	for(uint32_t i = 0; i < m_actualPossibleBallContours.size(); i++) {
		double area = contourArea(m_actualPossibleBallContours[i], false);
		//filtering all elements that are to big to be a ball
		if(area < m_maxBallArea) {
			Rect boundRect = boundingRect( Mat(m_actualPossibleBallContours[i]) );
			if(boundRect.height < m_maxHeightOfContour) {
				Moments ballMoment = moments(m_actualPossibleBallContours[i], false);
				Point2f ballMomentsCenter = Point2f(ballMoment.m10/ballMoment.m00, ballMoment.m01/ballMoment.m00);
				Rect boundRect = boundingRect( Mat(m_actualPossibleBallContours[i]) );

				if(ballMomentsCenter == ballMomentsCenter) {
					//to catch points out of range
					if(ballMomentsCenter.x >= m_frameWidth || ballMomentsCenter.y >= m_frameHeight || ballMomentsCenter.x <= 0 || ballMomentsCenter.y <= 0) {
						cout<<ballMomentsCenter<<endl;
					}
					else{
						m_actualBallCenters.push_back(ballMomentsCenter);
						m_actualBallBoundRects.push_back(boundRect);
						//no distance should be longer than the imgWidth + imgHeight
						float minDistToHumanMask = m_frameWidth + m_frameHeight;
						Point2f positionOfHumanMask = Point2f(-10000,-10000);
						for(uint32_t j = 0; j < m_humanMaskContours.size(); j++) {
							for(uint32_t k = 0; k <  m_actualPossibleBallContours[i].size(); k++) {
								float distToHumanMask = abs(pointPolygonTest( m_humanMaskContours[j], m_actualPossibleBallContours[i][k], true ));
								if(minDistToHumanMask > distToHumanMask) {
									minDistToHumanMask = distToHumanMask;
									//TODO: Checken, wie man das besser lösen kann, sodass nur für den ersten Punkt einer Kurve die Position berechnet wird.
									positionOfHumanMask = m_humanMaskPositionInModel[j];
								}
							}
						}
						m_actualPointsPlayerPosition.push_back(positionOfHumanMask);
						if(minDistToHumanMask == m_frameWidth + m_frameHeight) {
							cout<<"Did not find a humanMask"<<endl;
							minDistToHumanMask = m_avarageDistMaxToContour;
						}
						m_actualPointsDistToHumanMask.push_back(minDistToHumanMask);
					}
				}
			}
		}
	}
}

void BallDetection::checkPossibleBallPositionsROI(){
	//TODO: Mergen mit checkPossibleBallPositions

	m_actualPointsDistToHumanMaskROI.clear();
	m_actualPointsPlayerPositionROI.clear();
	m_actualBallBoundRectsROI.clear();
	m_actualBallCentersROI.clear();
	//vector<Point2f> possibleBallMomentsCenter;

	for(uint32_t i = 0; i < m_actualPossibleBallContoursROI.size(); i++) {
		double area = contourArea(m_actualPossibleBallContoursROI[i], false);
		//filtering all elements that are to big to be a ball
		if(area < m_maxBallArea) {
			Rect boundRect = boundingRect( Mat(m_actualPossibleBallContoursROI[i]) );
			if(boundRect.height < m_maxHeightOfContour) {
				Moments ballMoment = moments(m_actualPossibleBallContoursROI[i], false);
				Point2f ballMomentsCenter = Point2f(ballMoment.m10/ballMoment.m00, ballMoment.m01/ballMoment.m00);
				Rect boundRect = boundingRect( Mat(m_actualPossibleBallContoursROI[i]) );

				if(ballMomentsCenter == ballMomentsCenter) {
					//to catch points out of range
					if(ballMomentsCenter.x >= m_frameWidth || ballMomentsCenter.y >= m_frameHeight || ballMomentsCenter.x <= 0 || ballMomentsCenter.y <= 0) {
						cout<<ballMomentsCenter<<endl;
					}
					else{
						m_actualBallCentersROI.push_back(ballMomentsCenter);
						m_actualBallBoundRectsROI.push_back(boundRect);
						//no distance should be longer than the imgWidth + imgHeight
						float minDistToHumanMask = m_frameWidth + m_frameHeight;
						Point2f positionOfHumanMask = Point2f(-10000,-10000);
						for(uint32_t j = 0; j < m_humanMaskContours.size(); j++) {
							for(uint32_t k = 0; k <  m_actualPossibleBallContoursROI[i].size(); k++) {
								float distToHumanMask = abs(pointPolygonTest( m_humanMaskContours[j], m_actualPossibleBallContoursROI[i][k], true ));
								if(minDistToHumanMask > distToHumanMask) {
									minDistToHumanMask = distToHumanMask;
									//TODO: Checken, wie man das besser lösen kann, sodass nur für den ersten Punkt einer Kurve die Position berechnet wird.
									positionOfHumanMask = m_humanMaskPositionInModel[j];
								}
							}
						}
						m_actualPointsPlayerPositionROI.push_back(positionOfHumanMask);
						if(minDistToHumanMask == m_frameWidth + m_frameHeight) {
							cout<<"Did not find a humanMask"<<endl;
							minDistToHumanMask = m_avarageDistMaxToContour;
						}
						m_actualPointsDistToHumanMaskROI.push_back(minDistToHumanMask);
					}
				}
			}
		}
	}
	//m_possibleBallCenterOfFramesROI.push_back(possibleBallMomentsCenter);
}

void BallDetection::drawPossibleBallContours(){
	Mat movementDebug = m_movementMask.clone();
	// cout<<"Size: "<<movementDebug.size()<<endl;
	// cout<<"ballCenter: "<<m_possibleBallCenterOfFrames.back().size()<<endl;
	for(int i = 0; i < m_actualBallCenters.size(); i++) {
		if(m_actualPointsDistToHumanMask[i] < 40.0f* m_inputVideoResizingFactor) {
			circle(movementDebug, m_actualBallCenters[i], 5, Scalar(150), 4, LINE_8, 0);
		}
		else{
			circle(movementDebug, m_actualBallCenters[i], 5, Scalar(50), 4, LINE_8, 0);
		}
	}
	if(m_isRightFieldSide == true) {
		imshow("Right bug",movementDebug);
	}
	else{
		imshow("Left bug",movementDebug);
	}
	waitKey(30);
}

void BallDetection::printPossibleBallCenters(){
	cout<<endl;
	cout<<"PossibleBallCenters: "<<endl;

	for(int i=0; i<m_possibleBallCenterOfFrames.size(); i++){
		cout<<"BallCenterFrameCount: "<<i<<endl;
		for(int j=0; j<m_possibleBallCenterOfFrames[i].size(); j++){
			cout<<m_possibleBallCenterOfFrames[i]<<endl;
		}
	}
}

#else
//--------GPU - functions----------------
void BallDetection::updateHumanMaskGPU(){
	m_humanMaskROIGPU = m_playerSegmentation->getHumanMaskRoiGPU();
	m_humanMaskPositionInModel = m_playerSegmentation->getPlayerPositionInModel();
	m_humanMaskContours = m_playerSegmentation->getMainHumanMaskContours();
}


void BallDetection::detecBallWithDiffGPU(const vector<Mat> &originalImgBuffer){

	m_grayFrameBufferGPU.push_back(gpu::GpuMat());

	gpu::GpuMat originalImgGPU;
	originalImgGPU.upload(originalImgBuffer.back());

	gpu::cvtColor(originalImgGPU, m_grayFrameBufferGPU.back(), COLOR_BGR2GRAY);


	//m_grayFrameBuffer.push_back(originalImg.clone());

	vector<vector<Point> > possibleBallContours;
	vector<vector<Point> > possibleBallContoursROI;
	vector<Vec4i> hierarchy;

	if(m_grayFrameBufferGPU.size() > 2) {
		m_erodeDilateSize = 4; //eig 6

		cvtColor(originalImgBuffer[originalImgBuffer.size()-2], m_HSVImg, COLOR_BGR2HSV);

//        gpu::GpuMat imgBuffer0GPU, imgBuffer1GPU, imgBuffer2GPU;

//        imgBuffer0GPU.upload(m_grayFrameBuffer[0]);
//        imgBuffer1GPU.upload(m_grayFrameBuffer[1]);
//        imgBuffer2GPU.upload(m_grayFrameBuffer[2]);

		//uneffizient implementiert
		gpu::absdiff(m_grayFrameBufferGPU[0], m_grayFrameBufferGPU[1], m_diffImage0GPU);
		gpu::absdiff(m_grayFrameBufferGPU[1], m_grayFrameBufferGPU[2], m_diffImage1GPU);
		gpu::absdiff(m_grayFrameBufferGPU[0], m_grayFrameBufferGPU[2], m_diffImage2GPU);


		m_playerSegmentation->createHumanMaskGPU(m_grayFrameBufferGPU[1]);

		//hier wird die HumanMask benötigt!
		//humanMaskRoi updaten!!
		updateHumanMaskGPU();


		if(m_frameCount > 0) {
			createMovementMaskGPU();

			// Find contours
			findContours( m_movementMask, possibleBallContours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
			findContours( m_movementMaskHumanROI, possibleBallContoursROI, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

			checkPossibleBallPositions(possibleBallContours);
			checkPossibleBallPositionsROI(possibleBallContoursROI);
		}
		m_frameCount++;
		m_grayFrameBufferGPU.erase(m_grayFrameBufferGPU.begin());
	}
}

void BallDetection::createMovementMaskGPU(){

	gpu::GpuMat humanMaskPointsGPU, yellowHumanPointsGPU, movementMaskGPU, movementMask2GPUtmp, movementMaskGPUtmp;

	Mat colorFilteredBinaryImg;

	gpu::GpuMat movementMask0GPU(m_diffImage0GPU.size(), m_diffImage0GPU.type());
	gpu::GpuMat movementMask1GPU(m_diffImage1GPU.size(), m_diffImage1GPU.type());
	gpu::GpuMat movementMask2GPU(m_diffImage2GPU.size(), m_diffImage2GPU.type());

	gpu::threshold(m_diffImage0GPU, movementMask0GPU, m_differenceImgThreshold, 255.0f, THRESH_BINARY);
	gpu::threshold(m_diffImage1GPU, movementMask1GPU, m_differenceImgThreshold, 255.0f, THRESH_BINARY);
	gpu::threshold(m_diffImage2GPU, movementMask2GPU, m_differenceImgThreshold, 255.0f, THRESH_BINARY);


	gpu::bitwise_and(movementMask0GPU, movementMask1GPU, movementMaskGPU);

	m_erodeDilateSize = 7;//7

	//Mat element = getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize), Point(-1,-1));
	//Ptr<gpu::FilterEngine_GPU> erodeFilter = gpu::createMorphologyFilter_GPU(MORPH_ERODE, movementMask2GPU.type(), element);
	//filters huge movements from image 0 to image 2
	Mat movementMask2;
	movementMask2GPU.download(movementMask2);

	//gpu::erode(movementMask2GPU, movementMask2GPUtmp, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize), Point(-1,-1)), Point(-1,-1), 1 );
	erode(movementMask2, movementMask2, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize), Point(-1,-1)), Point(-1,-1), 1 );

	//erodeFilter->apply(movementMask2GPU, movementMask2GPU);

	movementMask2GPUtmp.upload(movementMask2);

	gpu::subtract(movementMaskGPU, movementMask2GPUtmp, movementMaskGPU);


	gpu::GpuMat lowerColorFilteredImgGPU(m_diffImage0GPU.size(), m_diffImage0GPU.type());
	gpu::GpuMat higherColorFilteredImgGPU(m_diffImage0GPU.size(), m_diffImage0GPU.type());
	gpu::GpuMat lowerSFilteredImgGPU(m_diffImage0GPU.size(), m_diffImage0GPU.type());
	gpu::GpuMat lowerVFilteredImgGPU(m_diffImage0GPU.size(), m_diffImage0GPU.type());
	gpu::GpuMat colorFilteredBinaryImgGPU(m_diffImage0GPU.size(), m_diffImage0GPU.type());

	gpu::GpuMat hsvGPU[3];
	gpu::GpuMat HSVImgGPU;
	HSVImgGPU.upload(m_HSVImg);
	gpu::split(HSVImgGPU,hsvGPU);

	gpu::threshold(hsvGPU[0], lowerColorFilteredImgGPU, m_lowH, 255.0f, THRESH_BINARY);
	gpu::threshold(hsvGPU[0], higherColorFilteredImgGPU, m_highH, 255.0f, THRESH_BINARY_INV);
	gpu::threshold(hsvGPU[1], lowerSFilteredImgGPU, m_lowS, 255.0f, THRESH_BINARY);
	gpu::threshold(hsvGPU[2], lowerVFilteredImgGPU, m_lowV, 255.0f, THRESH_BINARY);

	gpu::bitwise_and(lowerColorFilteredImgGPU, higherColorFilteredImgGPU, colorFilteredBinaryImgGPU);
	gpu::bitwise_and(colorFilteredBinaryImgGPU, lowerSFilteredImgGPU, colorFilteredBinaryImgGPU);
	gpu::bitwise_and(colorFilteredBinaryImgGPU, lowerVFilteredImgGPU, colorFilteredBinaryImgGPU);


	//TODO selbstständig für GPU implementieren
	//inRange(m_HSVImg, Scalar(m_lowH, m_lowS, m_lowV), Scalar(m_highH, m_highS, m_highV), colorFilteredBinaryImg); //Threshold the image
	//colorFilteredBinaryImgGPU.upload(colorFilteredBinaryImg);


	//calculates a mask only containing points which are inside the human contour

	gpu::bitwise_and(movementMaskGPU, m_humanMaskROIGPU, humanMaskPointsGPU);

	//calculates a mask only containing points which are inside the human contour and yellow
	gpu::bitwise_and(humanMaskPointsGPU, colorFilteredBinaryImgGPU, yellowHumanPointsGPU);


	//calculates a mask only containing points which are outside the human contour
	gpu::GpuMat pointsWithoutHumanGPU;
	gpu::subtract(movementMaskGPU, m_humanMaskROIGPU, pointsWithoutHumanGPU);

	//calculates a mask only containing points which are outside the human contour or yellow
	gpu::bitwise_or(yellowHumanPointsGPU, pointsWithoutHumanGPU, movementMaskGPU);

	Mat movementMask;
	movementMaskGPU.download(movementMask);
	m_erodeDilateSize = 4;
	tm.reset(); tm.start();

	//morphological opening (removes small objects from the foreground)
	// gpu::erode(movementMaskGPU, movementMaskGPUtmp, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );
	//gpu::dilate( movementMaskGPUtmp, movementMaskGPU, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );
	erode(movementMask, movementMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );
	dilate( movementMask, movementMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );
	//morphological closing (removes small holes from the foreground)
	//gpu::dilate( movementMaskGPU, movementMaskGPUtmp, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );
	//gpu::erode(movementMaskGPUtmp, movementMaskGPU, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );

	dilate( movementMask, movementMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );
	erode(movementMask, movementMask, getStructuringElement(MORPH_ELLIPSE, Size(m_erodeDilateSize, m_erodeDilateSize)) );

	tm.stop();
	erode_times0.push_back(tm.getTimeMilli());
	std::sort(erode_times0.begin(), erode_times0.end());
	double erode0_avg = std::accumulate(erode_times0.begin(), erode_times0.end(), 0.0) / erode_times0.size();
	std::cout << "Erode0 : Avg : " << erode0_avg << " ms FPS : " << 1000.0 / erode0_avg << std::endl;

	m_movementMask = movementMask;
	//movementMaskGPU.download(m_movementMask);
}

#endif
