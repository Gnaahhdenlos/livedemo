//
//  eventDetectionOld.cpp
//
//
//  Created by Henri Kuper on 17.05.17.
//
//
//#define GPU_ON

#include "eventDetectionOld.hpp"


using namespace std;
using namespace cv;
/* TODO:

    - fehlerdetektion, wenn der ball ins netz geht verbessern
    - zwei direkt aufeinanderfolgende serves abfangen (als beispiel, wenn sie innerhalb von 50 bildern detectiert werden.)(wurde zum Teil gelöst du durch verhinderung des Auftretens in der Klasse curveDetection)


    - Funktion schreiben, die unnütze Bilder zwischen Punkten rausschneidet.

 */

EventDetectionOld::EventDetectionOld(){
	eventDetectionOldInit();
}
EventDetectionOld::EventDetectionOld(struct ConfigData &config) : m_config(config){
	eventDetectionOldInit();
}
EventDetectionOld::~EventDetectionOld(){

}

int32_t EventDetectionOld::eventDetectionOldInit(){
	//TODO
	m_alreadyEventCheckedCurveNumberRight = -1;
	m_alreadyEventCheckedCurveNumberLeft = -1;

	m_videoImgOffset = 400;//1400//400
	m_framesPrediction = 42;//12

	m_actualServeTry = 0;
	m_actualServePos = 0;

	m_videoStreamStatus = 0;
	m_numberOfImpactsInside = 0;
	m_playerWonLastRally = 0;
	m_playerHasToServer = 0;
	m_frameCountCurveOverNet = -1;
	m_actualCurveName = "";
	m_actualStrokeName = "";
	m_faultDetected = false;

	m_actualStrokeCurveNumber = -1;

	//alreadyDetectedCurveNumber = -1;
	m_lineWidth = 4.0;

	videoStreamsInit();
	calibrationInit();

	m_match = new Match();
	m_statistics = new Statistics();

	walter = new network();

	walter->initWeights(m_config.getFilePathToWeights());

	m_videoCreator = new VideoCreator(m_config.getSafePathOutputVideo(), m_frameWidth, m_frameHeight, m_videoImgOffset, m_framesPrediction, m_curveDetectionRight, m_curveDetectionLeft, m_statistics, m_match);

	m_frameCount = 0;

	return EXIT_SUCCESS;
}

int32_t EventDetectionOld::videoStreamsInit(){

	bool success;
	success = m_rightCalibrationVideoStream.open(m_config.getFilePathToCalibrationVideoRight());
	if ( !m_rightCalibrationVideoStream.isOpened() && success) {
		cout << "Cannot open " << m_config.getFilePathToCalibrationVideoRight() << endl;
		return EXIT_FAILURE;
	}
	success = m_leftCalibrationVideoStream.open(m_config.getFilePathToCalibrationVideoLeft());
	if ( !m_leftCalibrationVideoStream.isOpened() && success) {
		cout << "Cannot open " << m_config.getFilePathToCalibrationVideoLeft() << endl;
		return EXIT_FAILURE;
	}

	success = m_rightVideoStream.open(m_config.getFilePathToVideoRight());
	if ( !m_rightVideoStream.isOpened() && success) {
		cout << "Cannot open " << m_config.getFilePathToVideoRight() << endl;
		return EXIT_FAILURE;
	}
	success = m_leftVideoStream.open(m_config.getFilePathToVideoLeft());
	if ( !m_leftVideoStream.isOpened() && success) {
		cout << "Cannot open " << m_config.getFilePathToVideoLeft() << endl;
		return EXIT_FAILURE;
	}

	int frameWidthRightCalibrationVideo = m_rightCalibrationVideoStream.get(CV_CAP_PROP_FRAME_WIDTH);
	int frameHeightRightCalibrationVideo = m_rightCalibrationVideoStream.get(CV_CAP_PROP_FRAME_HEIGHT);

	int frameWidthLeftCalibrationVideo = m_leftCalibrationVideoStream.get(CV_CAP_PROP_FRAME_WIDTH);
	int frameHeightLeftCalibrationVideo = m_leftCalibrationVideoStream.get(CV_CAP_PROP_FRAME_HEIGHT);

	int frameWidthRightVideo = m_rightVideoStream.get(CV_CAP_PROP_FRAME_WIDTH);
	int frameHeightRightVideo = m_rightVideoStream.get(CV_CAP_PROP_FRAME_HEIGHT);

	int frameWidthLeftVideo = m_leftVideoStream.get(CV_CAP_PROP_FRAME_WIDTH);
	int frameHeightLeftVideo = m_leftVideoStream.get(CV_CAP_PROP_FRAME_HEIGHT);

	if((frameWidthRightCalibrationVideo == frameWidthRightVideo && frameWidthLeftCalibrationVideo == frameWidthLeftVideo) && (frameHeightRightCalibrationVideo == frameHeightRightVideo && frameHeightLeftCalibrationVideo == frameHeightLeftVideo)) {
		m_frameWidth = frameWidthRightCalibrationVideo;
		m_frameHeight = frameHeightRightCalibrationVideo;
	}
	else{
		cout<<"Videos have different dimensions"<<endl;
		return EXIT_FAILURE;
	}
	m_imageSize = Size(m_frameWidth, m_frameHeight);

	return EXIT_SUCCESS;
}

int32_t EventDetectionOld::calibrationInit(){
	FileStorage fileStorage(m_config.getFilePathToCameraCalibrationMatrix(), FileStorage::READ);
	if( !fileStorage.isOpened() ) {
		return EXIT_FAILURE;
	}
	fileStorage["Camera_Matrix"]>> m_cameraMatrix;
	fileStorage["Distortion_Coefficients"]>> m_distortionCoefficients;
	cout<<"Camera_Matrix: "<<m_cameraMatrix<<endl;
	cout<<"Distortion_Coefficients: "<<m_distortionCoefficients<<endl;

	//Mat::eye(3,3, m_cameraMatrix.type()
	initUndistortRectifyMap(m_cameraMatrix, m_distortionCoefficients, Mat(),
	                        m_cameraMatrix,
	                        m_imageSize, CV_16SC2, m_undistortionRemap1, m_undistortionRemap2);

	m_fieldDetectionRight = new FieldDetection(m_frameHeight, m_frameWidth, m_cameraMatrix, m_distortionCoefficients, true, m_config);
	m_fieldDetectionLeft = new FieldDetection(m_frameHeight, m_frameWidth, m_cameraMatrix, m_distortionCoefficients, false, m_config);

	m_fieldDetectionRight->calibrateFieldLinesWithCalibrationVideo(m_rightCalibrationVideoStream);
	m_fieldDetectionLeft->calibrateFieldLinesWithCalibrationVideo(m_leftCalibrationVideoStream);

	m_rightCalibrationVideoStream.release();
	m_leftCalibrationVideoStream.release();

	m_rightVideoPlayerSegmentation = new PlayerSegmentation(m_fieldDetectionRight);
	m_leftVideoPlayerSegmentation = new PlayerSegmentation(m_fieldDetectionLeft);

	m_rightBallDetection = new BallDetection(m_frameHeight, m_frameWidth, m_fieldDetectionRight, m_rightVideoPlayerSegmentation);
	m_leftBallDetection = new BallDetection(m_frameHeight, m_frameWidth, m_fieldDetectionLeft, m_leftVideoPlayerSegmentation);

	m_curveDetectionRight = new CurveDetection(m_frameHeight, m_frameWidth, m_fieldDetectionRight, m_rightVideoPlayerSegmentation, m_rightBallDetection);
	m_curveDetectionLeft = new CurveDetection(m_frameHeight, m_frameWidth, m_fieldDetectionLeft, m_leftVideoPlayerSegmentation, m_leftBallDetection);

	m_leftSingelFieldPoints = m_fieldDetectionLeft->getLeftSingelFieldPoints();
	m_rightSingelFieldPoints = m_fieldDetectionRight->getRightSingelFieldPoints();

	return EXIT_SUCCESS;
}

int32_t EventDetectionOld::analysingVideos(){

	Mat imgOriginalRight;
	Mat imgOriginalLeft;

	//skipping images to sync both streams
	for(int i = 0; i < 1; i++) {
		m_leftVideoStream.read(imgOriginalLeft); // read a new frame from video
	}

	while(true) {
		tm.reset(); tm.start();
		//grapImagesFromVideoStream();
		std::thread threadGrapImage(&EventDetectionOld::grapImagesFromVideoStream, this);

		//skipping the calibration, because the calibration video and the analysing videos containing the same start frames
		if(m_frameCount > m_videoImgOffset && (int32_t)m_imgBufferRight.size()>= m_framesPrediction) {
			//Threading

//            std::thread threadRightVideo(&CurveDetection::detecCurves, m_curveDetectionRight, m_imgBufferRight);
//            std::thread threadLeftVideo(&CurveDetection::detecCurves, m_curveDetectionLeft, m_imgBufferLeft);
//
//            threadRightVideo.join();
//            threadLeftVideo.join();

			//if((int32_t)m_imgBufferRight.size() >= m_framesPrediction){
			updateDataForVideoCreator();
			m_videoCreator->writeVideoOutput(m_imgBufferRight[0], m_imgBufferLeft[0]);
			//thread threadVideoWriter(&videoCreator::writeVideoOutput, m_videoCreator, m_imgBufferRight[0], m_imgBufferLeft[0]);
			//}
			m_curveDetectionRight->detecCurves();
			m_curveDetectionLeft->detecCurves();
			updateActualStrokeCurve();
			updateActualCurveCharacter();
			detecEvents();
			//threadVideoWriter.join();

			tm.stop();
			event_times.push_back(tm.getTimeMilli());
			std::sort(event_times.begin(), event_times.end());
			double event_avg = std::accumulate(event_times.begin(), event_times.end(), 0.0) / event_times.size();
			std::cout << "Event : Avg : " << event_avg << " ms FPS : " << 1000.0 / event_avg << std::endl;

		}
		threadGrapImage.join();
		if((int32_t)m_imgBufferRight.size() > m_framesPrediction+1) {
			m_imgBufferRight.erase(m_imgBufferRight.begin());
			m_imgBufferLeft.erase(m_imgBufferLeft.begin());
		}
		if(m_videoStreamStatus) {
			break;
		}
		/*if(m_frameCount > 1750) {//13900
		    //if(m_frameCount > 4600) {
		    break;
		   }*/
		m_frameCount++;
		cout<<"imgCount: "<<m_frameCount<<endl;

	}
	m_videoCreator->stopVideoCreation();
	return EXIT_SUCCESS;
}

int32_t EventDetectionOld::detecEvents(){

	cout<<"checkedRight: "<<m_alreadyEventCheckedCurveNumberRight<<" actualCurveNumber: "<< m_actualStrokeCurveNumber<<" m_actualStrokeIsRight: "<<m_actualStrokeIsRight<<endl;
	cout<<"checkedLeft: "<<m_alreadyEventCheckedCurveNumberLeft<<" actualCurveNumber: "<< m_actualStrokeCurveNumber<<" m_actualStrokeIsRight: "<<m_actualStrokeIsRight<<endl;
	if((m_alreadyEventCheckedCurveNumberRight < m_actualStrokeCurveNumber && m_actualStrokeIsRight == true) || (m_alreadyEventCheckedCurveNumberLeft < m_actualStrokeCurveNumber && m_actualStrokeIsRight == false)) {

		categoriseStroke();

		m_frameCountCurveOverNet = -1;
		detecServes();

		cout<<"actualServeTry: "<<m_actualServeTry<<endl;
		if(m_actualServeTry > 0) {
			cout<<"checkedImpactAfterServe: "<<m_checkedImpactAfterServe<<endl;
			cout<<"actualServeHitsServeField"<<m_actualServeHitsServeField<<" faultDetected: "<<m_faultDetected<<endl;
			if(m_checkedImpactAfterServe == false) {

				detecImpactAfterServe();
			}
			else if(m_checkedImpactAfterServe == true && m_faultDetected == false) {
				detecPointLoss();
			}
		}
	}
	//TODO: Parameter anpassen.
	if(m_frameCountCurveOverNet > 0 && m_frameCount - m_frameCountCurveOverNet > 130) {
		cout<<"Detected stroke into Net"<<endl;
		m_faultDetected = true;
		updateScore();
	}

	if(m_actualStrokeIsRight == true && m_actualStrokeCurveNumber > m_alreadyEventCheckedCurveNumberRight) {
		m_alreadyEventCheckedCurveNumberRight = m_actualStrokeCurveNumber;
	}
	else if(m_actualStrokeIsRight == false && m_actualStrokeCurveNumber > m_alreadyEventCheckedCurveNumberLeft) {
		m_alreadyEventCheckedCurveNumberLeft = m_actualStrokeCurveNumber;
	}


	return EXIT_SUCCESS;
}

int32_t EventDetectionOld::detecServes(){

	vector<vector<Point2f> > positionOfPlayerDuringCurve;
	Point2f impactPointOfActualCurve;
	int actualNumberOfPointsPlayed = m_match->m_sets.back().m_games.back().m_pointsPlayer1 + m_match->m_sets.back().m_games.back().m_pointsPlayer2 + (m_match->m_sets.back().m_games.back().m_advantageForPlayer+1)/2;
	m_actualServePos = actualNumberOfPointsPlayed%2 +1;

	cout<<"actualServePos: "<<m_actualServePos<<endl;
	if(m_actualStrokeIsRight == true  && m_actualStrokeCurveNumber >= 0 && getActualCurveCharacter() == "Serve right player" && (m_playerHasToServer == 0 || m_playerHasToServer == 2)) {
		m_playerHasToServer = 2;
		//new implementation

		float numberOfPositionInsideServeZone3 = 0;
		float numberOfPositionInsideServeZone4 = 0;
		vector<Point2f> serveZoneRight3 = m_fieldDetectionRight->getServeZoneRight3();
		vector<Point2f> serveZoneRight4 = m_fieldDetectionRight->getServeZoneRight4();
		for(uint32_t i = 0; i < m_curveDetectionRight->m_curves[m_actualStrokeCurveNumber].getPositionsOfPlayerDuringCurveVecSize(); i++) {
			Point2f positionAtModel = m_curveDetectionRight->m_curves[m_actualStrokeCurveNumber].m_positionsOfPlayerDuringCurve[i];
			if(isInsideZone(serveZoneRight3, positionAtModel) == true) {
				numberOfPositionInsideServeZone3++;
			}
			if(isInsideZone(serveZoneRight4, positionAtModel) == true) {
				numberOfPositionInsideServeZone4++;
			}
		}
		cout<<"numberOfPositionInsideServeZone3: "<<numberOfPositionInsideServeZone3<<" numberOfPositionInsideServeZone4: "<<numberOfPositionInsideServeZone4<<endl;
		if(numberOfPositionInsideServeZone3 > numberOfPositionInsideServeZone4 && numberOfPositionInsideServeZone3 >= 0.75) {
			m_actualServePlayerRight = true;
			if(m_actualServePos == 1) {
				m_actualServeTry++;
				updateServeStatistic();
			}
			else if(m_playerWinsRally == 0) {
				//TODO: letzter punkt wurde falsch gegeben?
				resetLastPoint();
				m_actualServeTry = 2;
				//updateServeStatistic();

			}
			else{
				//unsichere Punkt
				m_faultDetected = true;
				updateScore();
				m_actualServeTry++;
				updateServeStatistic();
			}
			m_playerWinsRally = 1;
			m_numberOfImpactsInside = 0;

			m_actualServeHitsServeField = false;
			m_checkedImpactAfterServe = false;
			m_faultDetected = false;
		}
		else if(numberOfPositionInsideServeZone3 < numberOfPositionInsideServeZone4 && numberOfPositionInsideServeZone4 >= 0.75) {
			m_actualServePlayerRight = true;
			if(m_actualServePos == 2) {
				m_actualServeTry++;
				updateServeStatistic();
			}
			else if(m_playerWinsRally == 0) {
				//TODO: letzter punkt wurde falsch gegeben?
				resetLastPoint();
				m_actualServeTry = 2;
				//updateServeStatistic();
			}
			else{
				//unsichere Punkt
				m_faultDetected = true;
				updateScore();
				m_actualServeTry++;
				updateServeStatistic();
			}
			m_playerWinsRally = 1;
			m_numberOfImpactsInside = 0;

			m_actualServeHitsServeField = false;
			m_checkedImpactAfterServe = false;
			m_faultDetected = false;
		}
		else{
			cout<<"Serve right, but not in one zone."<<endl;
		}
		cout<<"Number of positions: "<<m_curveDetectionRight->m_curves[m_actualStrokeCurveNumber].m_positionsOfPlayerDuringCurve.size()<<endl;
		cout<<"Position in model: "<<m_curveDetectionRight->m_curves[m_actualStrokeCurveNumber].m_positionsOfPlayerDuringCurve[0]<<endl;
	}

	else if(m_actualStrokeIsRight == false && m_actualStrokeCurveNumber >= 0 && getActualCurveCharacter() == "Serve left player" && (m_playerHasToServer == 0 || m_playerHasToServer == 1)) {
		m_playerHasToServer = 1;
		//new implementation

		float numberOfPositionInsideServeZone1 = 0;
		float numberOfPositionInsideServeZone2 = 0;
		vector<Point2f> serveZoneLeft1 = m_fieldDetectionLeft->getServeZoneLeft1();
		vector<Point2f> serveZoneLeft2 = m_fieldDetectionLeft->getServeZoneLeft2();

		for(uint32_t i = 0; i < m_curveDetectionLeft->m_curves[m_actualStrokeCurveNumber].getPositionsOfPlayerDuringCurveVecSize(); i++) {
			Point2f positionAtModel = m_curveDetectionLeft->m_curves[m_actualStrokeCurveNumber].m_positionsOfPlayerDuringCurve[i];
			if(isInsideZone(serveZoneLeft1, positionAtModel) == true) {
				numberOfPositionInsideServeZone1++;
			}
			if(isInsideZone(serveZoneLeft2, positionAtModel) == true) {
				numberOfPositionInsideServeZone2++;
			}
		}
		cout<<"numberOfPositionInsideServeZone1: "<<numberOfPositionInsideServeZone1<<" numberOfPositionInsideServeZone2: "<<numberOfPositionInsideServeZone2<<endl;
		if(numberOfPositionInsideServeZone1 > numberOfPositionInsideServeZone2 && numberOfPositionInsideServeZone1 >= 0.75) {
			m_actualServePlayerRight = false;
			if(m_actualServePos == 2) {
				m_actualServeTry++;
				updateServeStatistic();
			}
			else if(m_playerWinsRally == 0) {
				//TODO: letzter punkt wurde falsch gegeben?
				resetLastPoint();
				m_actualServeTry = 2;
				//updateServeStatistic();
			}
			else{
				//unsichere Punkt
				m_faultDetected = true;
				updateScore();
				m_actualServeTry++;
				updateServeStatistic();
			}
			m_playerWinsRally = 2;
			m_numberOfImpactsInside = 0;

			m_actualServeHitsServeField = false;
			m_checkedImpactAfterServe = false;
			m_faultDetected = false;
		}
		else if(numberOfPositionInsideServeZone1 < numberOfPositionInsideServeZone2 && numberOfPositionInsideServeZone2 >= 0.75) {
			m_actualServePlayerRight = false;
			if(m_actualServePos == 1) {
				m_actualServeTry++;
				updateServeStatistic();
			}
			else if(m_playerWinsRally == 0) {
				//TODO: letzter punkt wurde falsch gegeben?
				resetLastPoint();
				m_actualServeTry = 2;
				//updateServeStatistic();
			}
			else{
				//unsichere Punkt
				m_faultDetected = true;
				updateScore();
				m_actualServeTry++;
				updateServeStatistic();
			}
			m_playerWinsRally = 2;
			m_numberOfImpactsInside = 0;

			m_actualServeHitsServeField = false;
			m_checkedImpactAfterServe = false;
			m_faultDetected = false;
		}
		else{
			cout<<"Serve left, but not in one zone."<<endl;
		}
		cout<<"Number of Positions: "<<m_curveDetectionLeft->m_curves[m_actualStrokeCurveNumber].m_positionsOfPlayerDuringCurve.size()<<endl;
		cout<<"Position in model: "<<m_curveDetectionLeft->m_curves[m_actualStrokeCurveNumber].m_positionsOfPlayerDuringCurve[0]<<endl;
	}

	return EXIT_SUCCESS;
}

int32_t EventDetectionOld::detecImpactAfterServe(){

	Point2f impactPointOfActualCurve;

	cout<<"actualServePlayerRight: "<<m_actualServePlayerRight<<" m_actualStrokeIsRight: "<< m_actualStrokeIsRight<<" getActualCurveCharacter: "<<getActualCurveCharacter()<<endl;
	if(m_actualServePlayerRight == false && m_actualStrokeIsRight == true && getActualCurveCharacter() == "Impact") {

		impactPointOfActualCurve = m_fieldDetectionRight->transformPointToModel(m_curveDetectionRight->m_curves[m_actualStrokeCurveNumber].getPointOfImpact());

		cout<<"actualServePos: "<<m_actualServePos<<endl;
		cout<<"impactPointOfActualCurve: "<<impactPointOfActualCurve<<endl;
		if(m_actualServePos == 1) {
			vector<Point2f> serveImpactZoneRight3 = m_fieldDetectionRight->getServeImpactZoneRight3();
			if(isInsideZone(serveImpactZoneRight3, impactPointOfActualCurve) == true) {
				m_actualServeHitsServeField = true;
				m_playerWinsRally = 1;
				m_numberOfImpactsInside = 1;
			}
			else if(m_actualServeTry == 1) {
				m_faultDetected = true;
				m_playerWinsRally = 0;
			}
			else{
				m_faultDetected = true;
				m_playerWinsRally = 2;
				m_actualServeTry = 0;
				updateDoubleFaultStatistic();
				updateScore();
			}
		}
		else if(m_actualServePos == 2) {
			vector<Point2f> serveImpactZoneRight4 = m_fieldDetectionRight->getServeImpactZoneRight4();
			if(isInsideZone(serveImpactZoneRight4, impactPointOfActualCurve) == true) {
				m_actualServeHitsServeField = true;
				m_playerWinsRally = 1;
				m_numberOfImpactsInside = 1;
			}
			else if(m_actualServeTry == 1) {
				m_faultDetected = true;
				m_playerWinsRally = 0;
			}
			else{
				m_faultDetected = true;
				m_playerWinsRally = 2;
				m_actualServeTry = 0;
				updateDoubleFaultStatistic();
				updateScore();
			}
		}
		m_checkedImpactAfterServe = true;
	}
	else if(m_actualServePlayerRight == true && m_actualStrokeIsRight == false && getActualCurveCharacter() == "Impact") {
		impactPointOfActualCurve = m_fieldDetectionLeft->transformPointToModel(m_curveDetectionLeft->m_curves[m_actualStrokeCurveNumber].getPointOfImpact());

		cout<<"impactPointOfActualCurve: "<<impactPointOfActualCurve<<endl;
		if(m_actualServePos == 1) {
			//TODO: Check ob curveimpact im serveaufkommfeld ist.
			//Point2f impactPoint = impactPointOfActualCurve;
			vector<Point2f> serveImpactZoneLeft2 = m_fieldDetectionRight->getServeImpactZoneLeft2();
			if(isInsideZone(serveImpactZoneLeft2, impactPointOfActualCurve) == true) {
				m_actualServeHitsServeField = true;
				m_playerWinsRally = 2;
				m_numberOfImpactsInside = 1;
			}
			else if(m_actualServeTry == 1) {
				m_faultDetected = true;
				m_playerWinsRally = 0;
			}
			else{
				m_faultDetected = true;
				m_playerWinsRally = 1;
				updateDoubleFaultStatistic();
				updateScore();
			}
		}
		else if(m_actualServePos == 2) {
			vector<Point2f> serveImpactZoneLeft1 = m_fieldDetectionRight->getServeImpactZoneLeft1();
			if(isInsideZone(serveImpactZoneLeft1, impactPointOfActualCurve) == true) {
				m_actualServeHitsServeField = true;
				m_playerWinsRally = 2;
				m_numberOfImpactsInside = 1;
			}
			else if(m_actualServeTry == 1) {
				m_faultDetected = true;
				m_playerWinsRally = 0;
			}
			else{
				m_faultDetected = true;
				m_playerWinsRally = 2;
				updateDoubleFaultStatistic();
				updateScore();
			}
		}
		m_checkedImpactAfterServe = true;
	}
	//TODO:Nur übergangsweise nutzen
	else if(m_actualStrokeIsRight == true && getActualCurveCharacter() == "Stroke right player") {
		m_playerWinsRally = 1;
		m_numberOfImpactsInside = 0;
		m_checkedImpactAfterServe = true;
	}
	else if(m_actualStrokeIsRight == false && getActualCurveCharacter() == "Stroke left player") {
		m_playerWinsRally = 2;
		m_numberOfImpactsInside = 0;
		m_checkedImpactAfterServe = true;
		cout<<"Stroke Player Right "<<" playerWinsRally: "<<m_playerWinsRally<<endl;
	}

	return EXIT_SUCCESS;
}

int32_t EventDetectionOld::detecPointLoss(){

	Point2f impactPointOfActualCurve;

	if(m_actualStrokeIsRight == true && getActualCurveCharacter() == "Impact") {
		impactPointOfActualCurve = m_fieldDetectionRight->transformPointToModel(m_curveDetectionRight->m_curves[m_actualStrokeCurveNumber].getPointOfImpact());

		cout<<"ImpactPoint: "<<impactPointOfActualCurve<<endl;
		//impact Right inside single field
		if(isInsideZone(m_rightSingelFieldPoints, impactPointOfActualCurve) == true) {

			//Player 1 already had an impact
			if(m_playerWinsRally == 1) {
				m_numberOfImpactsInside++;
			}
			//Player 1 had no impact
			else{
				m_playerWinsRally = 1;
				m_numberOfImpactsInside = 1;
			}

			cout<<"Impact Inside Field"<<endl;
			cout<<"playerWillWin: "<<m_playerWinsRally<<endl;
			if(m_numberOfImpactsInside > 1) {
				m_faultDetected = true;
				//m_playerWinsRally = 1;
				updateScore();
			}
			//Player 1 had no impact
			/*else{
			    m_playerWinsRally = 2;
			   }*/
		}
		//impact Right outside single field
		else{
			cout<<"Impact Outside Field"<<endl;
			cout<<"playerWillWin: "<<m_playerWinsRally<<endl;
			if(m_playerWinsRally == 1) {
				m_faultDetected = true;
				//m_playerWinsRally = 1;
				updateScore();
			}
			//Player 1 had no impact
			else{
				m_faultDetected = true;
				m_playerWinsRally = 2;
				updateScore();
			}
		}
	}

	else if(m_actualStrokeIsRight == false && getActualCurveCharacter() == "Impact") {

		impactPointOfActualCurve = m_fieldDetectionLeft->transformPointToModel(m_curveDetectionLeft->m_curves[m_actualStrokeCurveNumber].getPointOfImpact());

		//impact Right inside single field
		cout<<"ImpactPoint: "<<impactPointOfActualCurve<<endl;
		if(isInsideZone(m_leftSingelFieldPoints, impactPointOfActualCurve) == true) {

			if(m_playerWinsRally == 2) {
				m_numberOfImpactsInside++;
			}
			//Player 2 had no impact
			else{
				m_playerWinsRally = 2;
				m_numberOfImpactsInside = 1;
			}

			cout<<"Impact Inside Field"<<endl;
			cout<<"playerWillWin: "<<m_playerWinsRally<<endl;
			if(m_numberOfImpactsInside > 1) {
				m_faultDetected = true;
				//m_playerWinsRally = 1;
				updateScore();
			}
		}
		//impact Right outside single field
		else{
			cout<<"Impact Outside Field"<<endl;
			cout<<"playerWillWin: "<<m_playerWinsRally<<endl;
			if(m_playerWinsRally == 2) {
				m_faultDetected = true;
				//m_playerWinsRally = 2;
				updateScore();
			}
			//Player 2 had no impact
			else{
				m_faultDetected = true;
				m_playerWinsRally = 1;
				updateScore();
			}
		}
	}

	else if(m_actualStrokeIsRight == true && getActualCurveCharacter() == "Stroke right player") {
		m_playerWinsRally = 1;
		m_numberOfImpactsInside = 0;
		cout<<"Stroke Player Right "<<" playerWinsRally: "<<m_playerWinsRally<<endl;
	}
	else if(m_actualStrokeIsRight == false && getActualCurveCharacter() == "Stroke left player") {
		m_playerWinsRally = 2;
		m_numberOfImpactsInside = 0;
		cout<<"Stroke Player Right "<<" playerWinsRally: "<<m_playerWinsRally<<endl;
	}
	else if(m_actualStrokeIsRight == true && getActualCurveCharacter() == "Rally ending right") {
		m_faultDetected = true;
		updateScore();
	}
	else if(m_actualStrokeIsRight == false && getActualCurveCharacter() == "Rally ending left") {
		m_faultDetected = true;
		updateScore();
	}
	else if(m_actualStrokeIsRight == true && getActualCurveCharacter() == "From right over net") {
		m_playerWinsRally = 1;
		cout<<"From right over Net"<<endl;
		m_frameCountCurveOverNet = m_frameCount;
	}
	else if(m_actualStrokeIsRight == false && getActualCurveCharacter() == "From left over net") {
		m_playerWinsRally = 2;
		cout<<"From left over Net"<<endl;
		m_frameCountCurveOverNet = m_frameCount;
	}
	return EXIT_SUCCESS;
}



int32_t EventDetectionOld::categoriseStroke(){

	if(getActualCurveCharacter() == "Stroke left player" || getActualCurveCharacter() == "Stroke right player" || getActualCurveCharacter() == "Serve left player" || getActualCurveCharacter() == "Serve right player") {

		//curveDetection *curveDetection;
		vector<vector<Rect> > boundRects;
		vector<Mat> imgBuffer;
		uint32_t numberOfSnappedImages;
		m_actualStrokeNameImgCount = m_actualCurveImgCount;
		if(m_actualStrokeIsRight == true) {
			imgBuffer = m_imgBufferRight;

			boundRects = m_rightVideoPlayerSegmentation->getHumanMaskBoundRectsOfFrames();
		}
		else{
			imgBuffer = m_imgBufferLeft;

			boundRects = m_leftVideoPlayerSegmentation->getHumanMaskBoundRectsOfFrames();
		}

		if(getActualCurveCharacter() == "Serve left player" || getActualCurveCharacter() == "Serve right player") {
			numberOfSnappedImages = 15;
		}
		else{
			numberOfSnappedImages = 5;
		}

		Rect boundRect;
		boundRect.height = 0;
		vector<int> categoryCount;
		for(int i = 0; i < 6; i++) {
			categoryCount.push_back(0);
		}
		//cout<<"DebugStroke1"<<endl;
		for(uint32_t i = 0; i < numberOfSnappedImages; i++) {
			if(i < imgBuffer.size()-1) {
				boundRect.height = 0;
				//TODO: fehleranfällig, da immer die größte roi nicht aber die die dem schlag am nächste war gewertet wird.
				//        cout<<"Number of Human Rois per img: "<<boundRects[i + m_frameCount - m_videoImgOffset - m_framesPrediction].size()<<endl;
				for(uint32_t j = 0; j <boundRects[i + m_frameCount - m_videoImgOffset - m_framesPrediction].size(); j++) {
					if(boundRect.height < boundRects[i + m_frameCount - m_videoImgOffset - m_framesPrediction][j].height) {
						boundRect = boundRects[i + m_frameCount - m_videoImgOffset - m_framesPrediction][j];
					}
				}
				Point2f humanStandingPosition = Point2f(boundRect.x + boundRect.width / 2, boundRect.y + boundRect.height);
				cout<<"HumanStandingPosition: "<<humanStandingPosition<<endl;
				float width = boundRect.height*1.0;
				float x = boundRect.x + boundRect.width/2 - width/2;
				if(x < 0) {
					x = 0;
				}
				if(x + width > m_frameWidth) {
					width = m_frameWidth - x-1;
				}
				boundRect.x = x;
				boundRect.width = width;

				if(boundRect.width > 0 && boundRect.height > 0) {
					Mat originalImgRoi = imgBuffer[i].clone();
					cout<<boundRect.x <<" "<<boundRect.y<<" "<<boundRect.height<<" "<<boundRect.width<<endl;
					Mat roi = originalImgRoi(boundRect);
					//imshow("stroke", roi);
					//waitKey(30);
					// roi skalieren.

					double sclHeight = static_cast<double>(48) / static_cast<double>(roi.rows);
					double sclWidth = static_cast<double>(48) / static_cast<double>(roi.cols);
					resize(roi, roi, cv::Size(0,0), sclWidth, sclHeight, cv::INTER_LINEAR);
					walter->forward(roi);

					walter->getFCLayer7()->printOutput();

					categoryCount[walter->getFCLayer7()->getDetection()]++;
				}
			}
		}

		m_actualStrokeName = walter->getStrokeName(distance(categoryCount.begin(), max_element(categoryCount.begin(), categoryCount.end())));

		cout<<"WalterCategoryNumber: "<<distance(categoryCount.begin(), max_element(categoryCount.begin(), categoryCount.end()))<<endl;
		cout<<"WalterCategorisation: "<<m_actualStrokeName<<endl;
		updateStrokeStatistics();
	}

	return EXIT_SUCCESS;
}

void EventDetectionOld::grapImagesFromVideoStream(){
	tm2.reset(); tm2.start();
	m_imgBufferRight.push_back(Mat());
	m_imgBufferLeft.push_back(Mat());
	Mat imgRight, imgLeft;
	bool canReadRight = m_rightVideoStream.read(imgRight); // read a new frame from video
	bool canReadLeft = m_leftVideoStream.read(imgLeft); // read a new frame from video
	tm2.reset(); tm2.start();
	if (!canReadRight || !canReadLeft) {
		cout << "Cannot read a frame from video stream" << endl;
		m_videoStreamStatus = EXIT_FAILURE;
	}
	else{
		remap(imgRight, m_imgBufferRight.back(), m_undistortionRemap1, m_undistortionRemap2, INTER_LINEAR);
		remap(imgLeft, m_imgBufferLeft.back(), m_undistortionRemap1, m_undistortionRemap2, INTER_LINEAR);
		//undistort(imgRight, m_imgBufferRight.back(), m_cameraMatrix, m_distortionCoefficients);
		//undistort(imgLeft,m_imgBufferLeft.back(), m_cameraMatrix, m_distortionCoefficients);
	}
	/*imshow("BufferRight", m_imgBufferRight.back());
	   imshow("BufferLeft", m_imgBufferLeft.back());
	   waitKey(30);*/
	//cout<<"testimgSize: "<<m_imgBufferRight[42].rows<<endl;

	tm2.stop();
	frame_times.push_back(tm2.getTimeMilli());
	std::sort(frame_times.begin(), frame_times.end());
	double frame_avg = std::accumulate(frame_times.begin(), frame_times.end(), 0.0) / frame_times.size();
	std::cout << "Grap : Avg : " << frame_avg << " ms FPS : " << 1000.0 / frame_avg << std::endl;

}

int32_t EventDetectionOld::updateScore(){
	cout<<"Update Score"<<endl;

	cout<<"m_faultDetected: "<<m_faultDetected<<" m_playerWinsRally: "<<m_playerWinsRally<<endl;
	Game *actualGame = &(m_match->m_sets.back().m_games.back());

	if( m_playerWinsRally == 1) {

		if(actualGame->m_pointsPlayer1 < 3 && actualGame->m_pointsPlayer2 < 4) {

			actualGame->m_pointsPlayer1 += 1;
		}
		else if(actualGame->m_pointsPlayer1 == 3 && actualGame->m_pointsPlayer2 == 3 && actualGame->m_advantageForPlayer == 0) {
			actualGame->m_advantageForPlayer = 1;
		}
		else if(actualGame->m_pointsPlayer1 == 3 && actualGame->m_pointsPlayer2 == 3 && actualGame->m_advantageForPlayer == 1) {
			m_match->m_sets.back().m_games.push_back(Game());
			m_match->m_sets.back().m_gamesWonByPlayer1 +=1;
			m_match->m_sets.back().m_gameWonByPlayer.push_back(1);
			m_playerHasToServer = (m_playerHasToServer-2) * -1 + 1;
		}
		else if(actualGame->m_pointsPlayer1 == 3 && actualGame->m_pointsPlayer2 == 3 && actualGame->m_advantageForPlayer == 2) {
			actualGame->m_advantageForPlayer = 0;
		}
		else if(actualGame->m_pointsPlayer1 == 3 && actualGame->m_pointsPlayer2 < 3) {
			m_match->m_sets.back().m_games.push_back(Game());
			m_match->m_sets.back().m_gamesWonByPlayer1 +=1;
			m_match->m_sets.back().m_gameWonByPlayer.push_back(1);
			m_playerHasToServer = (m_playerHasToServer-2) * -1 + 1;
		}
		else{
			cout<<"Please check Update Score function"<<endl;
			cout<<"pointsPlayer2: "<<actualGame->m_pointsPlayer2<<" pointsPlayer1: "<<actualGame->m_pointsPlayer1<<" Adv: "<<actualGame->m_advantageForPlayer<<endl;
		}
		m_playerWonLastRally = 1;
	}

	else if(m_playerWinsRally == 2) {
		if(actualGame->m_pointsPlayer2 < 3 && actualGame->m_pointsPlayer1 < 4) {

			actualGame->m_pointsPlayer2 += 1;

		}
		else if(actualGame->m_pointsPlayer2 == 3 && actualGame->m_pointsPlayer1 == 3 && actualGame->m_advantageForPlayer == 0) {
			actualGame->m_advantageForPlayer = 2;
		}
		else if(actualGame->m_pointsPlayer2 == 3 && actualGame->m_pointsPlayer1 == 3 && actualGame->m_advantageForPlayer == 2) {
			m_match->m_sets.back().m_games.push_back(Game());
			m_match->m_sets.back().m_gamesWonByPlayer2 +=1;
			m_match->m_sets.back().m_gameWonByPlayer.push_back(2);
			m_playerHasToServer = (m_playerHasToServer-2) * -1 + 1;
		}
		else if(actualGame->m_pointsPlayer2 == 3 && actualGame->m_pointsPlayer1 == 3 && actualGame->m_advantageForPlayer == 1) {
			actualGame->m_advantageForPlayer = 0;
		}
		else if(actualGame->m_pointsPlayer2 == 3 && actualGame->m_pointsPlayer1 < 3) {
			m_match->m_sets.back().m_games.push_back(Game());
			m_match->m_sets.back().m_gamesWonByPlayer2 +=1;
			m_match->m_sets.back().m_gameWonByPlayer.push_back(2);
			m_playerHasToServer = (m_playerHasToServer-2) * -1 + 1;
		}
		else{
			cout<<"Please check Update Score function"<<endl;
			cout<<"pointsPlayer2: "<<actualGame->m_pointsPlayer2<<" pointsPlayer1: "<<actualGame->m_pointsPlayer1<<" Adv: "<<actualGame->m_advantageForPlayer<<endl;
		}
		m_playerWonLastRally = 2;
	}

	m_statistics->updateStatistics();

	m_playerWinsRally = 0;
	m_actualServeTry = 0;
	m_numberOfImpactsInside = 0;
	m_frameCountCurveOverNet = -1;

	return EXIT_SUCCESS;
}

int32_t EventDetectionOld::resetLastPoint(){
	cout<<"Reset Last Point Player: "<< m_playerWonLastRally <<endl;
	Game *actualGame = &(m_match->m_sets.back().m_games.back());
	if(m_playerWonLastRally == 1) {
		actualGame->m_pointsPlayer1 -= 1;
	}
	else if(m_playerWonLastRally == 2) {
		actualGame->m_pointsPlayer2 -= 1;
	}

	return EXIT_SUCCESS;
}

int32_t EventDetectionOld::updateActualStrokeCurve(){

	int curveNumberRight = -1;
	int curveNumberLeft = -1;

	m_actualStrokeCurveNumber = -1;

	int curveImgCountRight = 0;
	int curveImgCountLeft = 0;

	vector<Curve> curvesRight = m_curveDetectionRight->m_curves;
	vector<Curve> curvesLeft = m_curveDetectionLeft->m_curves;

	if(curvesRight.size() > 0) {
		bool foundRight = false;
		int32_t i = (int32_t)curvesRight.size()-1;
		while(foundRight == false && i >= 0) {
			int curveImgCountRightTmp = curvesRight[i].getLastFrameCount();
			if(getCurveCharacter(curvesRight[i].getCharacterisation()) != "default" && getCurveCharacter(curvesRight[i].getCharacterisation()) != "Stroke left player" && curveImgCountRightTmp < m_frameCount - m_videoImgOffset - m_framesPrediction) {
				curveNumberRight = i;
				curveImgCountRight = curveImgCountRightTmp;
				foundRight = true;
			}
			i--;
		}
	}
	if(curvesLeft.size() > 0) {
		bool foundLeft = false;
		int i = (int32_t)curvesLeft.size()-1;
		while(foundLeft == false && i >= 0) {
			int curveImgCountLeftTmp = curvesLeft[i].getLastFrameCount();
			if(getCurveCharacter(curvesLeft[i].getCharacterisation()) != "default" && getCurveCharacter(curvesLeft[i].getCharacterisation()) != "Stroke right player" && curveImgCountLeftTmp < m_frameCount - m_videoImgOffset - m_framesPrediction) {
				curveNumberLeft = i;
				curveImgCountLeft = curveImgCountLeftTmp;
				foundLeft = true;
			}
			i--;
		}
	}
	cout<<"CurveCountRight "<<curveImgCountRight<<" CurveCountLeft "<<curveImgCountLeft<<" imgCount between: "<<(m_frameCount - m_videoImgOffset -140)<<" and: "<<(m_frameCount - m_videoImgOffset - m_framesPrediction)<<endl;
	if(((curveImgCountLeft > m_frameCount - m_videoImgOffset -140 && curveImgCountLeft < m_frameCount - m_videoImgOffset - m_framesPrediction)  || (curveImgCountRight > m_frameCount - m_videoImgOffset - 140 && curveImgCountRight < m_frameCount - m_videoImgOffset - m_framesPrediction))) {

		if(curveImgCountRight >= curveImgCountLeft && curveImgCountRight < m_frameCount - m_videoImgOffset - m_framesPrediction) {
			m_actualStrokeCurveNumber = curveNumberRight;
			m_actualStrokeIsRight = true;
			m_actualCurveImgCount = curveImgCountRight;
		}
		else if(curveImgCountLeft > curveImgCountRight && curveImgCountLeft < m_frameCount - m_videoImgOffset - m_framesPrediction) {
			m_actualStrokeCurveNumber = curveNumberLeft;
			m_actualStrokeIsRight = false;
			m_actualCurveImgCount = curveImgCountLeft;
		}
	}
	else{
		m_actualStrokeCurveNumber = -1;
	}

	return EXIT_SUCCESS;
}


string EventDetectionOld::getActualCurveCharacter(){

	string curveCharacter;

	if(m_actualStrokeCurveNumber >= 0) {
		if(m_actualStrokeIsRight == true) {
			curveCharacter = m_curveDetectionRight->m_curves[m_actualStrokeCurveNumber].getCharacterisationString();
		}
		else if (m_actualStrokeIsRight == false) {
			curveCharacter = m_curveDetectionLeft->m_curves[m_actualStrokeCurveNumber].getCharacterisationString();
		}
	}

	return curveCharacter;
}

string EventDetectionOld::getCurveCharacter(int curveCharacterisationNumber){
	string curveCharacter;

	switch(curveCharacterisationNumber) {
	case 7: curveCharacter = "Impact"; break;
	case 8: curveCharacter = "Impact"; break;
	case 9: curveCharacter = "Stroke left player"; break;
	case 10: curveCharacter = "Stroke right player"; break;
	case 11: curveCharacter = "Stroke left player"; break;
	case 12: curveCharacter = "Stroke right player"; break;
	case 13: curveCharacter = "Stroke left player"; break;
	case 14: curveCharacter = "Stroke right player"; break;
	case 15: curveCharacter = "Stroke left player"; break;
	case 16: curveCharacter = "Stroke right player"; break;
	case 17: curveCharacter = "From right over net"; break;
	case 18: curveCharacter = "From left over net"; break;
	case 19: curveCharacter = "Rally ending right"; break;
	case 20: curveCharacter = "Rally ending left"; break;
	case 21: curveCharacter = "Serve left player"; break;
	case 22: curveCharacter = "Serve right player"; break;
	default: curveCharacter = "default"; break;
	}
	return curveCharacter;
}


bool EventDetectionOld::isInsideZone(vector<Point2f>& zone, Point2f positionAtModel){
	bool isInsideZone = false;

	if(zone[0].x - m_lineWidth < positionAtModel.x && zone[2].x + m_lineWidth > positionAtModel.x && zone[0].y - m_lineWidth < positionAtModel.y && zone[2].y + m_lineWidth > positionAtModel.y) {
		isInsideZone = true;
	}
	return isInsideZone;
}

int32_t EventDetectionOld::updateActualCurveCharacter(){

	string actualCurveName = getActualCurveCharacter();
	//TODO: wieder entkommentieren, nur für debugzwecke
	if(actualCurveName == "Stroke left player" || actualCurveName == "Stroke right player" || actualCurveName == "Serve left player" || actualCurveName == "Serve right player") {
		//if(actualCurveName != "Sdefault" && actualCurveName != "Impact"){
		m_actualCurveName = actualCurveName;
		m_actualCurveNameImgCount = m_actualCurveImgCount;
	}
	if(m_actualCurveNameImgCount < m_frameCount - m_videoImgOffset -140 || m_actualCurveNameImgCount > m_frameCount - m_videoImgOffset - m_framesPrediction) {
		m_actualCurveName = "";
	}

	return EXIT_SUCCESS;
}

int32_t EventDetectionOld::updateStrokeStatistics(){

	string actualStrokeName = getActualCurveCharacter();

	if(actualStrokeName == "Serve left player" && m_actualStrokeName == "serve") {
		m_statistics->player1->m_numberOfServes += 1;
	}
	else if(actualStrokeName == "Serve right player" && m_actualStrokeName == "serve") {
		m_statistics->player2->m_numberOfServes += 1;
	}
	else if(actualStrokeName == "Stroke right player" && m_actualStrokeName == "forehand-spin") {
		m_statistics->player2->m_numberOfForehandSpin += 1;
	}
	else if(actualStrokeName == "Stroke right player" && m_actualStrokeName == "forehand-slice") {
		m_statistics->player2->m_numberOfForehandSlice += 1;
	}
	else if(actualStrokeName == "Stroke right player" && m_actualStrokeName == "backhand-spin") {
		m_statistics->player2->m_numberOfBackhandSpin += 1;
	}
	else if(actualStrokeName == "Stroke right player" && m_actualStrokeName == "backhand-slice") {
		m_statistics->player2->m_numberOfBackhandSlice += 1;
	}
	else if(actualStrokeName == "Stroke left player" && m_actualStrokeName == "forehand-spin") {
		m_statistics->player1->m_numberOfForehandSpin += 1;
	}
	else if(actualStrokeName == "Stroke left player" && m_actualStrokeName == "forehand-slice") {
		m_statistics->player1->m_numberOfForehandSlice += 1;
	}
	else if(actualStrokeName == "Stroke left player" && m_actualStrokeName == "backhand-spin") {
		m_statistics->player1->m_numberOfBackhandSpin += 1;
	}
	else if(actualStrokeName == "Stroke left player" && m_actualStrokeName == "backhand-slice") {
		m_statistics->player1->m_numberOfBackhandSlice += 1;
	}

	return EXIT_SUCCESS;
}

int32_t EventDetectionOld::updateServeStatistic(){

	if(m_actualServeTry == 1 && m_actualServePlayerRight == false) {
		m_statistics->player1->m_numberOfFirstServes += 1;
	}
	else if(m_actualServeTry == 1 && m_actualServePlayerRight == true) {
		m_statistics->player2->m_numberOfFirstServes += 1;
	}
	else if(m_actualServeTry == 2 && m_actualServePlayerRight == false) {
		m_statistics->player1->m_numberOfSecondServes += 1;
	}
	else if(m_actualServeTry == 2 && m_actualServePlayerRight == true) {
		m_statistics->player2->m_numberOfSecondServes += 1;
	}
	return EXIT_SUCCESS;
}

int32_t EventDetectionOld::updateDoubleFaultStatistic(){

	if(m_actualServePlayerRight == false) {
		m_statistics->player1->m_numberOfDoubleFaults += 1;
	}
	else if(m_actualServePlayerRight == true) {
		m_statistics->player2->m_numberOfDoubleFaults += 1;
	}

	return EXIT_SUCCESS;
}

int32_t EventDetectionOld::updateDataForVideoCreator(){

	m_videoCreator->setActualCurveName(m_actualCurveName);
	m_videoCreator->setActualStrokeName(m_actualStrokeName);
	m_videoCreator->setActualCurveImgCount(m_actualCurveImgCount);
	m_videoCreator->setActualStrokeNameImgCount(m_actualStrokeNameImgCount);
	m_videoCreator->setActualStrokeCurveNumber(m_actualStrokeCurveNumber);
	m_videoCreator->setActualStrokeIsRight(m_actualStrokeIsRight);
	m_videoCreator->setFrameCount(m_frameCount);

	return EXIT_SUCCESS;
}
