//
//  event.cpp
//
//
//  Created by Henri Kuper on 05.10.17.
//
//


#include "event.hpp"

using namespace std;

//---------------StrokeEvent------------
StrokeEvent::StrokeEvent(){
	strokeEventInit();
}
StrokeEvent::StrokeEvent(int strokeType, int frameCount, bool isRight) : m_strokeType(strokeType), m_numberOfFrame(frameCount), m_isRight(isRight){
	strokeEventInit();
}
StrokeEvent::~StrokeEvent(){

}

void StrokeEvent::strokeEventInit(){

}

//---------------Event------------
Event::Event(){
	eventInit();
}

Event::Event(int eventType, int frameCount, bool isRight) : m_eventType(eventType), m_numberOfFrame(frameCount), m_isRight(isRight){
	eventInit();
}

Event::~Event(){

}

void Event::eventInit(){
	m_strokeType = DEFAULT_STROKE;
	m_strokeSpeed = 0.0f;
	m_serveZoneNumber = -1;
	m_serveImpactZoneNumber = -1;
	m_isInsideServeImpactZone = false;
	m_isInsideSingleField = false;
}
