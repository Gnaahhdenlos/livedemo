//
//  curve.hpp
//
//
//  Created by Henri Kuper on 11.07.17.
//
//

#ifndef curve_hpp
#define curve_hpp

#include <stdio.h>
#include <iostream>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>


class Curve {
private:
int m_characterisation;                       //number of the characterisation of the Curve
int m_isCompleted;                            //if value is 1, the Curve length is >= 4 else the value is 0
bool m_isFinished;														//true if curve length >=4 and no new ballPoint since 7 frames
int m_isNearPlayer;                           //if value is 1, the Curve is close to the player else the value is 0
float m_speed;                                //the speed(kmh) of the Curve if the following Curve is over the net.
cv::Point2f m_pointOfImpact;                  //point of impact, if the Curve leads to an impact else -10000,-10000
int m_pointOfImpactFrameCount;                //frameCount of impact, if the Curve leads to an impact else -1

//int m_curveIndex;															//index of curve in the curve Vector that contains all curves
int m_followingCurveIndex;													//index of following curve that fits best to actual curve
int m_previousCurveIndex;											//index of precursor curve that fits best to actual curve
//std::vector<int> m_curveIndexWhosNextIsMe;									//index of curve whos nextCurveIndex is me

/*
   initialises the Curve object. Sets every private variable to a default value.
 */
void curveInit();

public:
std::vector<cv::Point2f> m_ballPoints;        //contains every ball point of the Curve. The first point is the oldest.
std::vector<int>  m_frameCounts;              //contains the frameCount when the corresponding CurvePoint was detected.
std::vector<float> m_distancesToPlayer;       //contains the distance of every CurvePoint to the player
std::vector<cv::Rect> m_ballsBoundRects;      //contains the bounding rect of every ballPoint
std::vector<cv::Point2f> m_positionsOfPlayerDuringCurve;    //contains for everey ballPoint the actual position of the player

Curve();
Curve(cv::Point2f ballCenter, int imageCount, float distToHumanMask, cv::Point2f playerPosition, cv::Rect ballBoundRect);
~Curve();

/*
   appends the data distToHumanMask, playerPosition, ballBoundRect, ballCenter, imageCount to the vectors of the class.
 */
void appendData(float distToHumanMask, cv::Point2f playerPosition, cv::Rect ballBoundRect, cv::Point2f ballCenter, int imageCount);
/*
   deletes the data distToHumanMask, playerPosition, ballBoundRect, ballCenter, imageCount
   from the vectors of this class at the position "position".
 */
void deleteData(int position);
/*
   deletes the last data distToHumanMask, playerPosition, ballBoundRect, ballCenter, imageCount
   from the vectors of this class.
 */
void deleteLastData();
/*
   inserts the data distToHumanMask, playerPosition, ballBoundRect, ballCenter, imageCount
   to the vectors of the class at positon "position".
 */
void insertData(int position, float distToHumanMask, cv::Point2f playerPosition, cv::Rect ballBoundRect, cv::Point2f ballCenter, int imageCount);

//set and reset functions for the private Vriables
void setCharacterisation(int character){
	m_characterisation = character;
}
void resetCharacterisation(){
	m_characterisation = -1;
}

void setCompletion(){
	m_isCompleted = 1;
}
void resetCompletion(){
	m_isCompleted = 0;
}

void setFinished(){
	m_isFinished = true;
}
void resetFinished(){
	m_isFinished = false;
}

void setIsNearPlayer(){
	m_isNearPlayer = 1;
}
void resetIsNearPlayer(){
	m_isNearPlayer = 0;
}

void setSpeed(float speed){
	m_speed = speed;
}

void setPointOfImpact(cv::Point2f pointOfImpact){
	m_pointOfImpact = pointOfImpact;
}
void setFrameCountOfImpact(int frameCount){
	m_pointOfImpactFrameCount = frameCount;
}
void resetImpact(){
	m_pointOfImpact = cv::Point2f(-10000,-10000);
	m_pointOfImpactFrameCount = -1;
}

//void setCurveIndex(int index){
//	m_curveIndex = index;
//}
void setFollowingCurveIndex(int index){
	m_followingCurveIndex = index;
}
void setPreviousCurveIndex(int index){
	m_previousCurveIndex = index;
}
//void setCurveIndexWhosNextIsMe(int index){
//	m_curveIndexWhosNextIsMe.push_back(index);
//}

cv::Point2f getLastBallPoint(){
	return m_ballPoints.back();
}
cv::Point2f getSecondLastBallPoint(){
	return m_ballPoints[m_ballPoints.size()-2];
}
cv::Point2f getThirdLastBallPoint(){
	return m_ballPoints[m_ballPoints.size()-3];
}
cv::Point2f getFirstBallPoint(){
	return m_ballPoints[0];
}
cv::Point2f getBallPoint(int position){
	return m_ballPoints[position];
}
float getSlopeOfLastTwoBallPoints(){
	return (m_ballPoints[m_ballPoints.size()-2].y - m_ballPoints.back().y) / (m_ballPoints[m_ballPoints.size()-2].x - m_ballPoints.back().x);
}

int getLastFrameCount() const {
	return m_frameCounts.back();
}
int getSecondLastFrameCount() const {
	return m_frameCounts[m_frameCounts.size()-2];
}

int getThirdLastFrameCount() const {
	return m_frameCounts[m_frameCounts.size()-3];
}

int getFirstFrameCount() const {
	return m_frameCounts[0];
}
int getFrameCount(int position) const {
	return m_frameCounts[position];
}

float getDistanceToPlayer(int positon) const {
	return m_distancesToPlayer[positon];
}

cv::Point2f getPositionOfPlayerDuringCurve(int positon) const {
	return m_positionsOfPlayerDuringCurve[positon];
}

cv::Point2f getLastPositionOfPlayerDuringCurve() const {
	return m_positionsOfPlayerDuringCurve.back();
}

cv::Rect getBallsBoundRect(int positon) const {
	return m_ballsBoundRects[positon];
}
cv::Rect getLastBallsBoundRect() const {
	return m_ballsBoundRects.back();
}

int getIsNearPlayer() const {
	return m_isNearPlayer;
}
int getIsCompleted() const {
	return m_isCompleted;
}
bool getIsFinished() const {return m_isFinished;}
int getCharacterisation() const {
	return m_characterisation;
}
std::string getCharacterisationString();
float getSpeed() const {
	return m_speed;
}

cv::Point2f getPointOfImpact() const {
	return m_pointOfImpact;
}
int getFrameCountOfImpact() const {
	return m_pointOfImpactFrameCount;
}

//int getCurveIndex(){
//	return m_curveIndex;
//}
int getFollowingCurveIndex(){
	return m_followingCurveIndex;
}
int getPreviousCurveIndex(){
	return m_previousCurveIndex;
}
//std::vector<int> getCurveIndexWhosNextIsMe(){
//	return m_curveIndexWhosNextIsMe;
//}


float getCurveEndDX() const {
	return (float)(m_ballPoints[m_ballPoints.size()-2].x - m_ballPoints[m_ballPoints.size()-3].x);
}
float getCurveStartDX() const {
	return (float)(m_ballPoints[2].x - m_ballPoints[1].x);
}

float getCurveEndDY() const {
	return (float)(m_ballPoints[m_ballPoints.size()-2].y - m_ballPoints[m_ballPoints.size()-3].y);
}
float getCurveStartDY() const {
	return (float)(m_ballPoints[2].y - m_ballPoints[1].y);
}

//get-function returning the size of the vectors
uint32_t getBallPointsVecSize(){
	return m_ballPoints.size();
}
uint32_t getFrameCountVecSize(){
	return m_frameCounts.size();
}
uint32_t getDistanceToPlayerVecSize(){
	return m_distancesToPlayer.size();
}
uint32_t getBallsBoundRectVecSize(){
	return m_ballsBoundRects.size();
}
uint32_t getPositionsOfPlayerDuringCurveVecSize() const {
	return m_positionsOfPlayerDuringCurve.size();
}
std::vector<cv::Point2f> getBallPoints(){
	return m_ballPoints;
}
std::vector<int> getFrameCounts(){
	return m_frameCounts;
}
};


#endif /* curve_hpp */
