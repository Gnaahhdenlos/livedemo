//
//  videoCreator.cpp
//
//
//  Created by Henri Kuper on 13.07.17.
//
//

#include "videoCreator.hpp"
using namespace std;
using namespace cv;

/*
 TODO: Funktion schreiben, die unwichtige Bildsequenzen rausschneidet.
 -  Speed up erzeugen indem die punkte schon in das kleine Modell eingezeichnet werden und nicht erst in das 2700 pixel modell, was bei jedem aufruf geresized wird

 */


VideoCreator::VideoCreator(string safePathOutputVideo, int frameWidth, int frameHeight, int videoImgOffset, int framesPrediction, CurveDetection* curveDetectionRight, CurveDetection* curveDetectionLeft, Statistics* stats, Match* match): m_safePathOutputVideo(safePathOutputVideo), m_frameWidth(frameWidth), m_frameHeight(frameHeight), m_videoImgOffset(videoImgOffset), m_framesPrediction(framesPrediction), m_curveDetectionRight(curveDetectionRight), m_curveDetectionLeft(curveDetectionLeft), m_statistics(stats), m_match(match){

    videoCreatorInit();
}

VideoCreator::~VideoCreator(){

}

void VideoCreator::videoCreatorInit(){

    m_frameCount = 0;
    m_resizingFactor = 1.0f;//0.5

    int fps = 35;

    cout<<"m_frameWidth "<<m_frameWidth<<endl;
    cout<<"m_frameHeight "<<m_frameHeight<<endl;

    bool success = m_videoOutputWriter.open(m_safePathOutputVideo,CV_FOURCC('m', 'p', '4', 'v'), fps, Size(m_frameWidth*2, m_frameHeight * m_resizingFactor),true);
    cout<<"VideoWriter Dimension: Width: "<<m_frameWidth<<" Height: "<< (m_frameHeight * m_resizingFactor)<<endl;

    if (!success){
        std::cout << "!!! Output video could not be opened" << std::endl;
    }
}

void VideoCreator::stopVideoCreation(){

    m_videoOutputWriter.release();
}

void VideoCreator::writeVideoOutput(Mat& rightImg, Mat& leftImg){
    //cout<<"rightImg "<<rightImg.size()<<endl;
    //cout<<"leftImg "<<leftImg.size()<<endl;
    tm.reset(); tm.start();
    Mat videoImg;
    Mat videoImgRight;
    Mat videoImgLeft;
    Mat imgCurveDetectionRight;
    Mat imgCurveDetectionLeft;

    //cout<<"Debug1.1"<<endl;

    // Mat actualCurveNameDisplay = createActualCurveNameDisplay();
    // //cout<<"Debug1.2"<<endl;
    // Mat actualeStrokeSpeedDisplay = createActualeStrokeSpeedDisplay();
    // //cout<<"Debug1.3"<<endl;
    // Mat actualScoreDisplay = createActualScoreDisplay();
    // //cout<<"Debug1.4"<<endl;
    // Mat actualStrokeNameDisplay = createActualStrokeNameDisplay();
    // //cout<<"Debug1.5"<<endl;
    // Mat actualStatisticsDisplay = createActualStatisticsDisplay();
    //cout<<"Debug1.6"<<endl;
    Mat fieldModel = m_curveDetectionRight->getFieldModel() + m_curveDetectionLeft->getFieldModel();
    resize(fieldModel, fieldModel, Size(), 0.5, 0.5, INTER_LINEAR);
    //cout<<"Debug1.7"<<endl;
    resize(rightImg, rightImg, Size(), m_resizingFactor, m_resizingFactor, INTER_LINEAR);
    //cout<<"Debug1.8"<<endl;
    resize(leftImg, leftImg, Size(), m_resizingFactor, m_resizingFactor, INTER_LINEAR);
    //cout<<"Debug1.9"<<endl;
    videoImgRight = rightImg + m_curveDetectionRight->getDrawnCurves();
    videoImgLeft = leftImg + m_curveDetectionLeft->getDrawnCurves();
    //cout<<"Debug1.10"<<endl;
    hconcat(videoImgLeft,videoImgRight,videoImg);
    //cout<<"hconcat: "<<videoImg.rows<<" "<<videoImg.cols<<endl;

    fieldModel.copyTo(videoImg(Rect(videoImg.cols/2 - fieldModel.cols/2, videoImg.rows - fieldModel.rows, fieldModel.cols, fieldModel.rows)));

    /*actualCurveNameDisplay.copyTo(videoImg(Rect(videoImg.cols/2 - actualCurveNameDisplay.cols/2, 0, actualCurveNameDisplay.cols, actualCurveNameDisplay.rows)));
    actualStrokeNameDisplay.copyTo(videoImg(Rect(videoImg.cols/2 - actualStrokeNameDisplay.cols/2, actualCurveNameDisplay.rows , actualStrokeNameDisplay.cols, actualStrokeNameDisplay.rows)));
    actualeStrokeSpeedDisplay.copyTo(videoImg(Rect(videoImg.cols/2 - actualeStrokeSpeedDisplay.cols/2, actualCurveNameDisplay.rows + actualStrokeNameDisplay.rows, actualeStrokeSpeedDisplay.cols, actualeStrokeSpeedDisplay.rows)));
    actualScoreDisplay.copyTo(videoImg(Rect(videoImg.cols/2 - actualScoreDisplay.cols/2, actualCurveNameDisplay.rows + actualStrokeNameDisplay.rows + actualeStrokeSpeedDisplay.rows, actualScoreDisplay.cols, actualScoreDisplay.rows)));
    actualStatisticsDisplay.copyTo(videoImg(Rect(videoImg.cols/2 - actualStatisticsDisplay.cols/2, actualCurveNameDisplay.rows + actualStrokeNameDisplay.rows + actualeStrokeSpeedDisplay.rows + actualScoreDisplay.rows, actualStatisticsDisplay.cols, actualStatisticsDisplay.rows)));
    */

    //resize(videoImg, videoImg, Size(), 0.5, 0.5, INTER_LINEAR);
    // if(m_frameCount%60 == 0){
    //
    //     imshow("OrresizedVideoImgiginal", videoImg);
    //     waitKey(30);
    // }

    m_videoOutputWriter.write(videoImg);
    tm.stop();
    video_times.push_back(tm.getTimeMilli());
    std::sort(video_times.begin(), video_times.end());
    double video_avg = std::accumulate(video_times.begin(), video_times.end(), 0.0) / video_times.size();
    std::cout << "InternVideo : Avg : " << video_avg << " ms FPS : " << 1000.0 / video_avg << std::endl;
}

/*string VideoCreator::getActualScore(int playerNumber){
    string actualScore = "";

    if(playerNumber == 1){
        actualScore = to_string(m_match->setsWonByPlayer1);
        actualScore += "  ";
        actualScore += to_string(m_match->m_sets.back().m_gamesWonByPlayer1);
        actualScore += "  ";
        if(m_match->m_sets.back().m_games.back().m_advantageForPlayer == 1){
            actualScore += "A ";
        }

        else if(m_match->m_sets.back().m_games.back().m_pointsPlayer1 <= 2){
            actualScore += to_string(m_match->m_sets.back().m_games.back().m_pointsPlayer1 * 15);
            if(m_match->m_sets.back().m_games.back().m_pointsPlayer1 == 0){
                actualScore += " ";
            }
        }
        else{
            actualScore += "40";
        }
    }
    else if(playerNumber == 2){
        actualScore = to_string(m_match->setsWonByPlayer2);
        actualScore += "  ";
        actualScore += to_string(m_match->m_sets.back().m_gamesWonByPlayer2);
        actualScore += "  ";
        if(m_match->m_sets.back().m_games.back().m_advantageForPlayer == 2){
            actualScore += "A ";
        }
        else if(m_match->m_sets.back().m_games.back().m_pointsPlayer2 <= 2){
            actualScore += to_string(m_match->m_sets.back().m_games.back().m_pointsPlayer2 * 15);
            if(m_match->m_sets.back().m_games.back().m_pointsPlayer2 == 0){
                actualScore += " ";
            }
        }
        else{
            actualScore += "40";
        }
    }
    else{
        actualScore = "";
        cout<<"Not a valid player"<<endl;
    }
    return actualScore;
}*/


Mat VideoCreator::createActualCurveNameDisplay(){
    int fontFace = FONT_HERSHEY_PLAIN;
    double fontScale = 2 * m_resizingFactor;
    int thickness = 2  * m_resizingFactor;
    int baseline=0;

    Mat actualCurveNameDisplay = Mat::zeros(50 * m_resizingFactor, 550 * m_resizingFactor, CV_8UC3);

    Size textSizeCurveName = getTextSize(m_actualCurveName, fontFace, fontScale, thickness, &baseline);
    Point textOrgCurveName((actualCurveNameDisplay.cols- textSizeCurveName.width)/2, (actualCurveNameDisplay.rows + textSizeCurveName.height)/2);
    putText(actualCurveNameDisplay, m_actualCurveName, textOrgCurveName, fontFace, fontScale, Scalar(255,255,255), thickness, CV_AA);

    return actualCurveNameDisplay;
}

Mat VideoCreator::createActualStrokeNameDisplay(){
    int fontFace = FONT_HERSHEY_PLAIN;
    double fontScale = 2 * m_resizingFactor;
    int thickness = 2 * m_resizingFactor;
    int baseline=0;

    Mat actualStrokeNameDisplay = Mat::zeros(50 * m_resizingFactor, 550 * m_resizingFactor, CV_8UC3);

    if((m_actualCurveName == "Serve left player" || m_actualCurveName == "Serve right player") && m_actualStrokeName != "serve"){
        m_actualStrokeName = "";
    }
    else if((m_actualCurveName != "Serve left player" && m_actualCurveName != "Serve right player") && m_actualStrokeName == "serve"){
        m_actualStrokeName = "";
    }
    if(m_actualStrokeNameImgCount < m_frameCount - m_videoImgOffset -140 || m_actualStrokeNameImgCount > m_frameCount - m_videoImgOffset - m_framesPrediction){
        m_actualStrokeName = "";
    }

    Size textSizeStrokeName = getTextSize(m_actualStrokeName, fontFace, fontScale, thickness, &baseline);
    Point textOrgStrokeName((actualStrokeNameDisplay.cols- textSizeStrokeName.width)/2, (actualStrokeNameDisplay.rows + textSizeStrokeName.height)/2);
    putText(actualStrokeNameDisplay, m_actualStrokeName, textOrgStrokeName, fontFace, fontScale, Scalar(255,255,255), thickness, CV_AA);

    return actualStrokeNameDisplay;
}

Mat VideoCreator::createActualeStrokeSpeedDisplay(){
    int fontFace = FONT_HERSHEY_PLAIN;
    double fontScale = 2 * m_resizingFactor;
    int thickness = 2  * m_resizingFactor;
    int baseline=0;
    Mat actualeStrokeSpeedDisplay = Mat::zeros(50 * m_resizingFactor, 550 * m_resizingFactor, CV_8UC3);
    string actualeStrokeSpeed = to_string(getActualStrokeSpeed());
    Size textSizeCurveSpeed = getTextSize(actualeStrokeSpeed, fontFace, fontScale, thickness, &baseline);
    Point textOrgCurveSpeed((actualeStrokeSpeedDisplay.cols- textSizeCurveSpeed.width)/2, (actualeStrokeSpeedDisplay.rows + textSizeCurveSpeed.height)/2);
    putText(actualeStrokeSpeedDisplay, actualeStrokeSpeed, textOrgCurveSpeed, fontFace, fontScale, Scalar(255,255,255), thickness, CV_AA);
    return actualeStrokeSpeedDisplay;
}

Mat VideoCreator::createActualScoreDisplay(){
    //TODO
    int fontFace = FONT_HERSHEY_PLAIN;
    double fontScale = 2 * m_resizingFactor;
    int thickness = 2 * m_resizingFactor;
    int baseline=0;

    Mat actualScoreDisplay = Mat::zeros(100 * m_resizingFactor, 550 * m_resizingFactor, CV_8UC3);
    string actualeScorePlayer1 = "0";//getActualScore(1);
    string actualeScorePlayer2 = "0";//getActualScore(2);

    Size textSizeActualScorePlayer1 = getTextSize(actualeScorePlayer1, fontFace, fontScale, thickness, &baseline);
    Size textSizeActualScorePlayer2 = getTextSize(actualeScorePlayer2, fontFace, fontScale, thickness, &baseline);

    Point textOrgActualScorePlayer1((actualScoreDisplay.cols- textSizeActualScorePlayer1.width)/2, (actualScoreDisplay.rows/2 + textSizeActualScorePlayer1.height)/2);
    Point textOrgActualScorePlayer2((actualScoreDisplay.cols- textSizeActualScorePlayer2.width)/2, (actualScoreDisplay.rows/2 + textSizeActualScorePlayer2.height)/2 + actualScoreDisplay.rows/2);

    putText(actualScoreDisplay, actualeScorePlayer1, textOrgActualScorePlayer1, fontFace, fontScale, Scalar(255,255,255), thickness, CV_AA);
    putText(actualScoreDisplay, actualeScorePlayer2, textOrgActualScorePlayer2, fontFace, fontScale, Scalar(255,255,255), thickness, CV_AA);

    return actualScoreDisplay;
}

Mat VideoCreator::createActualStatisticsDisplay(){
    //TODO
    int fontFace = FONT_HERSHEY_PLAIN;
    double fontScale = 2 * m_resizingFactor;
    int thickness = 2 * m_resizingFactor;
    int baseline=0;
    int numberOfStats = 8;

    Mat actualStatisticsDisplay = Mat::zeros(400 * m_resizingFactor, 550 * m_resizingFactor, CV_8UC3);

    string playerNames = m_statistics->getActualStatistics(0);
    string statisticsAces = m_statistics->getActualStatistics(1);
    string statisticsDoubleFaults = m_statistics->getActualStatistics(2);
    string statisticsFirstServeRate = m_statistics->getActualStatistics(3);

    string statisticsForhandSpin = m_statistics->getActualStatistics(4);
    string statisticsForhandSlice = m_statistics->getActualStatistics(5);
    string statisticsBackhandSpin = m_statistics->getActualStatistics(6);
    string statisticsBackandSlice = m_statistics->getActualStatistics(7);

    Size textSizeActualStatisticsNames = getTextSize(playerNames, fontFace, fontScale, thickness, &baseline);
    Size textSizeActualStatisticsAces = getTextSize(statisticsAces, fontFace, fontScale, thickness, &baseline);
    Size textSizeActualStatisticsDoubleFaults = getTextSize(statisticsDoubleFaults, fontFace, fontScale, thickness, &baseline);
    Size textSizeActualStatisticsFirstServeRate = getTextSize(statisticsFirstServeRate, fontFace, fontScale, thickness, &baseline);

    Size textSizeActualStatisticsForhandSpin = getTextSize(statisticsForhandSpin, fontFace, fontScale, thickness, &baseline);
    Size textSizeActualStatisticsForhandSlice = getTextSize(statisticsForhandSpin, fontFace, fontScale, thickness, &baseline);
    Size textSizeActualStatisticsBackhandSpin = getTextSize(statisticsForhandSpin, fontFace, fontScale, thickness, &baseline);
    Size textSizeActualStatisticsBackhandSlice = getTextSize(statisticsForhandSpin, fontFace, fontScale, thickness, &baseline);


    Point textOrgActualStatisticsNames((actualStatisticsDisplay.cols- textSizeActualStatisticsNames.width)/2, (-textSizeActualStatisticsNames.height)/2 + actualStatisticsDisplay.rows/numberOfStats);
    Point textOrgActualStatisticsAces((actualStatisticsDisplay.cols- textSizeActualStatisticsAces.width)/2, (-textSizeActualStatisticsAces.height)/2 + actualStatisticsDisplay.rows/numberOfStats*2);
    Point textOrgActualStatisticsDoubleFaults((actualStatisticsDisplay.cols- textSizeActualStatisticsDoubleFaults.width)/2, (-textSizeActualStatisticsDoubleFaults.height)/2 + actualStatisticsDisplay.rows/numberOfStats*3);
    Point textOrgActualStatisticsFirstServeRate((actualStatisticsDisplay.cols- textSizeActualStatisticsFirstServeRate.width)/2, (-textSizeActualStatisticsFirstServeRate.height)/2 + actualStatisticsDisplay.rows/numberOfStats*4);

    Point textOrgActualStatisticsForhandSpin((actualStatisticsDisplay.cols- textSizeActualStatisticsForhandSpin.width)/2, (-textSizeActualStatisticsForhandSpin.height)/2 + actualStatisticsDisplay.rows/numberOfStats*5);
    Point textOrgActualStatisticsForhandSlice((actualStatisticsDisplay.cols- textSizeActualStatisticsForhandSlice.width)/2, (-textSizeActualStatisticsForhandSlice.height)/2 + actualStatisticsDisplay.rows/numberOfStats*6);
    Point textOrgActualStatisticsBackhandSpin((actualStatisticsDisplay.cols- textSizeActualStatisticsBackhandSpin.width)/2, (-textSizeActualStatisticsBackhandSpin.height)/2 + actualStatisticsDisplay.rows/numberOfStats*7);
    Point textOrgActualStatisticsBackhandSlice((actualStatisticsDisplay.cols- textSizeActualStatisticsBackhandSlice.width)/2, (-textSizeActualStatisticsBackhandSlice.height)/2 + actualStatisticsDisplay.rows/numberOfStats*8);


    putText(actualStatisticsDisplay, playerNames, textOrgActualStatisticsNames, fontFace, fontScale, Scalar(255,255,255), thickness, CV_AA);
    putText(actualStatisticsDisplay, statisticsAces, textOrgActualStatisticsAces, fontFace, fontScale, Scalar(255,255,255), thickness, CV_AA);
    putText(actualStatisticsDisplay, statisticsDoubleFaults, textOrgActualStatisticsDoubleFaults, fontFace, fontScale, Scalar(255,255,255), thickness, CV_AA);
    putText(actualStatisticsDisplay, statisticsFirstServeRate, textOrgActualStatisticsFirstServeRate, fontFace, fontScale, Scalar(255,255,255), thickness, CV_AA);

    putText(actualStatisticsDisplay, statisticsForhandSpin, textOrgActualStatisticsForhandSpin, fontFace, fontScale, Scalar(255,255,255), thickness, CV_AA);
    putText(actualStatisticsDisplay, statisticsForhandSlice, textOrgActualStatisticsForhandSlice, fontFace, fontScale, Scalar(255,255,255), thickness, CV_AA);
    putText(actualStatisticsDisplay, statisticsBackhandSpin, textOrgActualStatisticsBackhandSpin, fontFace, fontScale, Scalar(255,255,255), thickness, CV_AA);
    putText(actualStatisticsDisplay, statisticsBackandSlice, textOrgActualStatisticsBackhandSlice, fontFace, fontScale, Scalar(255,255,255), thickness, CV_AA);

    return actualStatisticsDisplay;
}

float VideoCreator::getActualStrokeSpeed(){
    //TODO: Die geschwindigkeit länger halten, da auch impacts und co analysiert werden, diese führt dazu, dass die zeit null gesetzt wird, sobald das auftritt.
    float curveSpeed = 0;
    if(m_actualStrokeCurveNumber >= 0){
        if(m_actualStrokeIsRight == true){
            curveSpeed = m_curveDetectionRight->m_curves[m_actualStrokeCurveNumber].getSpeed();
        }
        else if(m_actualStrokeIsRight == false){
            curveSpeed = m_curveDetectionLeft->m_curves[m_actualStrokeCurveNumber].getSpeed();
        }
    }
    return curveSpeed;
}
