//
//  eventDetectionOld.hpp
//
//
//  Created by Henri Kuper on 17.05.17.
//
//

#ifndef eventDetectionOld_hpp
#define eventDetectionOld_hpp

//#define GPU_ON

#include <iostream>
#include <vector>
#include <thread>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <stdio.h>

#include "fieldDetection.hpp"
#include "ballDetection.hpp"
#include "configData.hpp"
#include "match.hpp"
#include "curveDetection.hpp"
#include "playerSegmentation.hpp"
#include "statistics.hpp"
#include "videoCreator.hpp"
#include "../walterLib/network.hpp"

class EventDetectionOld {
private:
//--------CPU-GPU shared Variables--------
//structs
struct ConfigData m_config;
//Objects
FieldDetection *m_fieldDetectionRight;
FieldDetection *m_fieldDetectionLeft;

CurveDetection *m_curveDetectionRight;
CurveDetection *m_curveDetectionLeft;

PlayerSegmentation *m_rightVideoPlayerSegmentation;
PlayerSegmentation *m_leftVideoPlayerSegmentation;

BallDetection *m_rightBallDetection;
BallDetection *m_leftBallDetection;

cv::VideoCapture m_rightVideoStream;
cv::VideoCapture m_leftVideoStream;

cv::VideoCapture m_rightCalibrationVideoStream;
cv::VideoCapture m_leftCalibrationVideoStream;

std::vector<cv::Mat> m_imgBufferRight;
std::vector<cv::Mat> m_imgBufferLeft;

int m_frameWidth;
int m_frameHeight;

cv::Size m_imageSize;

int m_frameCount;
int m_alreadyEventCheckedCurveNumberRight;
int m_alreadyEventCheckedCurveNumberLeft;

int m_videoImgOffset;
int m_framesPrediction;
int m_videoStreamStatus;

float m_lineWidth;

std::vector<cv::Point2f> m_leftSingelFieldPoints;
std::vector<cv::Point2f> m_rightSingelFieldPoints;


std::string m_actualCurveName;
std::string m_actualStrokeName;
int m_actualCurveNameImgCount;
int m_actualStrokeNameImgCount;

int m_actualCurveImgCount;
int m_actualStrokeCurveNumber;
bool m_actualStrokeIsRight;

int m_actualServeTry;
int m_actualServePos;
bool m_actualServePlayerRight;
bool m_actualServeHitsServeField;
bool m_checkedImpactAfterServe;
bool m_faultDetected;

int m_playerHasToServer;
int m_playerWinsRally;
int m_numberOfImpactsInside;
int m_playerWonLastRally;
int m_frameCountCurveOverNet;

cv::Mat m_cameraMatrix;
cv::Mat m_distortionCoefficients;
cv::Mat m_undistortionRemap1;
cv::Mat m_undistortionRemap2;

Match *m_match;

Statistics *m_statistics;

network *walter;

VideoCreator *m_videoCreator;

cv::TickMeter tm;
cv::TickMeter tm2;
std::vector<double> event_times;
std::vector<double> frame_times;

//--------CPU-GPU shared private functions--------
int32_t updateActualCurveCharacter();
int32_t updateStrokeStatistics();
int32_t updateServeStatistic();
int32_t updateDoubleFaultStatistic();
int32_t updateDataForVideoCreator();

void grapImagesFromVideoStream();

int32_t eventDetectionOldInit();
int32_t videoStreamsInit();
int32_t calibrationInit();

public:
EventDetectionOld();
EventDetectionOld(struct ConfigData &config);
~EventDetectionOld();

int32_t analysingVideos();

int32_t detecEvents();
int32_t detecServes();
int32_t detecImpactAfterServe();
int32_t detecPointLoss();

int32_t categoriseStroke();

cv::Mat createVideoOutputImg();
int32_t updateActualStrokeCurve();
int32_t updateActualImpactCurve();
int32_t updateScore();
int32_t resetLastPoint();

bool isInsideZone(std::vector<cv::Point2f>&, cv::Point2f);

std::string getCurveCharacter(int);
std::string getActualCurveCharacter();
std::string getActualScore(int);

};

#endif /* eventDetectionOld_hpp */
