//
//  backWardscoring.hpp
//
//
//  Created by Henri Kuper on 16.10.17.
//
//

#include "backwardScoring.hpp"

using namespace std;

BackwardScoring::BackwardScoring(EventDetection *eventDetection, Match *match) : m_eventDetection(eventDetection), m_match(match){
	backwardScoringInit();
}

BackwardScoring::~BackwardScoring(){

}

void BackwardScoring::backwardScoringInit(){
	m_scoringTree = ScoringNode();
	m_scoringTree.setActualServePos(2);
	m_scoringTree.setIsMarkedAsPossibleScorePathNode(true);

	m_newestScoringNodes.push_back(&m_scoringTree);
	m_actualEventNumber = -1;
	m_actualServeTry = 0;
	m_playerWinsRally = 0;
	m_actualProbabilityOfScore = 0.5f;
	m_firstServeDetected = false;
	m_newEventDetected = false;
	m_updatedProbability = true;
	m_faultDetected = false;
	m_secondServeIsReasonable = false;
	m_hasDetectedDoubleFault = false;
	m_hasDetectedImpactAfterServe = false;
	m_serveIsAce = false;
	m_isWinnerForSide = 0;
	m_isErrorForSide = 0;
}

void BackwardScoring::updateEvent(){
	if((int)m_eventDetection->getEventsSize() - 1 > m_actualEventNumber){
		m_actualEventNumber = (int)m_eventDetection->getEventsSize() - 1;
		m_newEventDetected = true;
	}
}

void BackwardScoring::scoring(){
	cout<<"scoring"<<endl;

	//printNewestScoringNodes();
	for(int i = 0; i < m_eventDetection->getEventsSize(); i++){
		m_actualEventNumber	= i;
		int eventCharacterNumer = m_eventDetection->getEventTypeOfEvent(m_actualEventNumber);
		switch(eventCharacterNumer){
			case STROKE: processStrokeEvent(); break;
			case SERVE: processServeEvent(); break;
			case IMPACT_AFTER_SERVE: processImpactAfterServeEvent(); break;
			case OVER_NET: processOverNetEvent(); break;
			case CRITICAL_IMPACT: processCriticalImpactEvent(); break;
			case END_OF_RALLY: processEndOfRallyEvent(); break;
			case CONTINUATION_OF_RALLY: processContinuationOfRallyEvent(); break;
			case END_OF_GAME: processEndOfGameEvent(); break;
			case END_OF_MATCH: processEndOfMatchEvent(); break;
			default: cout<<"Something went wrong"<<endl; break;
		}
		if(m_faultDetected == true && m_updatedProbability == false){
			updateScoreProbabilites();
		}
	}
}

void BackwardScoring::scoringDebug(){
	cout<<"scoringDebug"<<endl;
	updateEvent();
	//printNewestScoringNodes();
	if(m_newEventDetected == true){
		m_newEventDetected = false;
		int eventCharacterNumer = m_eventDetection->getEventTypeOfEvent(m_actualEventNumber);
		switch(eventCharacterNumer){
			case STROKE: processStrokeEvent(); break;
			case SERVE: processServeEvent(); break;
			case IMPACT_AFTER_SERVE: processImpactAfterServeEvent(); break;
			case OVER_NET: processOverNetEvent(); break;
			case CRITICAL_IMPACT: processCriticalImpactEvent(); break;
			case END_OF_RALLY: processEndOfRallyEvent(); break;
			case CONTINUATION_OF_RALLY: processContinuationOfRallyEvent(); break;
			case END_OF_GAME: processEndOfGameEvent(); break;
			case END_OF_MATCH: processEndOfMatchEvent(); break;
			default: cout<<"Something went wrong"<<endl; break;
		}
		if(m_faultDetected == true && m_updatedProbability == false){
			updateScoreProbabilites();
		}
	}
}

void BackwardScoring::processServeEvent(){
	cout<<"Process serve Event"<<endl;
	if(m_updatedProbability == false){
		updateScoreProbabilites();
	}
	printNewestScoringNodes();
	const Event &serveEvent = m_eventDetection->getEvent(m_actualEventNumber);

	m_faultDetected = false;
	m_hasDetectedDoubleFault = false;
	m_hasDetectedImpactAfterServe = false;
	m_serveIsAce = true;
	m_updatedProbability = false;
	m_isWinnerForSide = 0;
	m_isErrorForSide = 0;
	m_actualProbabilityOfScore = 0.5f;

	int newestScoringNodesSize = (int)m_newestScoringNodes.size();
	cout<<"processServeEvent Number of Scoring nodes: "<<m_newestScoringNodes.size()<<endl;
	for(int i = 0; i < newestScoringNodesSize; i++){

		if(m_newestScoringNodes.front()->getServePosGameEnded() > 0){
			if(m_newestScoringNodes.front()->getServePosGameEnded() == 1){
				if(m_newestScoringNodes.front()->getServeIsRight() == serveEvent.getIsRight()){
					if(serveEvent.getServeZoneNumber() == 2 || serveEvent.getServeZoneNumber() == 3){
						updateNewestScoringNodesAfterFirstServe(1, serveEvent.getNumberOfFrame(), serveEvent.getIsRight(), serveEvent.getStrokeSpeed());
					}
					else if(serveEvent.getServeZoneNumber() == 1 || serveEvent.getServeZoneNumber() == 4){
						updateNewestScoringNodesAfterSecondServe(serveEvent.getNumberOfFrame(), serveEvent.getStrokeSpeed());
					}
					else{
						m_newestScoringNodes.erase(m_newestScoringNodes.begin());
					}
				}
				else{
					m_newestScoringNodes.erase(m_newestScoringNodes.begin());
				}
			}
			else if(m_newestScoringNodes.front()->getServePosGameEnded() == 2){
				if(m_newestScoringNodes.front()->getServeIsRight() != serveEvent.getIsRight()){
					if(serveEvent.getServeZoneNumber() == 2 || serveEvent.getServeZoneNumber() == 3){
						updateNewestScoringNodesAfterFirstServe(1, serveEvent.getNumberOfFrame(), serveEvent.getIsRight(), serveEvent.getStrokeSpeed());
					}
					else{
						m_newestScoringNodes.erase(m_newestScoringNodes.begin());
					}
				}
				else if(m_newestScoringNodes.front()->getServeIsRight() == serveEvent.getIsRight()){
					if(serveEvent.getServeZoneNumber() == 2 || serveEvent.getServeZoneNumber() == 3){
						//TODO update as second + update as first
						if(updateNewestScoringNodesAfterFirstAndSecondServe(1, serveEvent.getNumberOfFrame(), serveEvent.getIsRight(), serveEvent.getStrokeSpeed()) == true){
							i += 2;
						}
					}
					else{
						m_newestScoringNodes.erase(m_newestScoringNodes.begin());
					}
				}
				else{
					m_newestScoringNodes.erase(m_newestScoringNodes.begin());
				}
			}
			else{
				m_newestScoringNodes.erase(m_newestScoringNodes.begin());
			}
		}
		else{
			if(serveEvent.getServeZoneNumber() == 1 && m_newestScoringNodes.front()->getActualServePos() == 2){
				updateNewestScoringNodesAfterSecondServe(serveEvent.getNumberOfFrame(), serveEvent.getStrokeSpeed());
			}
			else if(serveEvent.getServeZoneNumber() == 2 && m_newestScoringNodes.front()->getActualServePos() == 1){
				updateNewestScoringNodesAfterSecondServe(serveEvent.getNumberOfFrame(), serveEvent.getStrokeSpeed());
			}
			else if(serveEvent.getServeZoneNumber() == 3 && m_newestScoringNodes.front()->getActualServePos() == 1){
				updateNewestScoringNodesAfterSecondServe(serveEvent.getNumberOfFrame(), serveEvent.getStrokeSpeed());
			}
			else if(serveEvent.getServeZoneNumber() == 4 && m_newestScoringNodes.front()->getActualServePos() == 2){
				updateNewestScoringNodesAfterSecondServe(serveEvent.getNumberOfFrame(), serveEvent.getStrokeSpeed());
			}
			else if(serveEvent.getServeZoneNumber() == 1 && m_newestScoringNodes.front()->getActualServePos() == 1){
				if(i+1 < newestScoringNodesSize){
					if(mergeEqualScoringNodes(0) == true){
						i++;
					}
				}
				updateNewestScoringNodesAfterFirstServe(2, serveEvent.getNumberOfFrame(), false, serveEvent.getStrokeSpeed());
			}
			else if(serveEvent.getServeZoneNumber() == 2 && m_newestScoringNodes.front()->getActualServePos() == 2){
				if(i+1 < newestScoringNodesSize){
					if(mergeEqualScoringNodes(0) == true){
						i++;
					}
				}
				updateNewestScoringNodesAfterFirstServe(1, serveEvent.getNumberOfFrame(), false, serveEvent.getStrokeSpeed());
			}
			else if(serveEvent.getServeZoneNumber() == 3 && m_newestScoringNodes.front()->getActualServePos() == 2){
				if(i+1 < newestScoringNodesSize){
					if(mergeEqualScoringNodes(0) == true){
						i++;
					}
				}
				updateNewestScoringNodesAfterFirstServe(1, serveEvent.getNumberOfFrame(), true, serveEvent.getStrokeSpeed());
			}
			else if(serveEvent.getServeZoneNumber() == 4 && m_newestScoringNodes.front()->getActualServePos() == 1){
				if(i+1 < newestScoringNodesSize){
					if(mergeEqualScoringNodes(0) == true){
						i++;
					}
				}
				updateNewestScoringNodesAfterFirstServe(2, serveEvent.getNumberOfFrame(), true, serveEvent.getStrokeSpeed());
			}
		}
	}
	printNewestScoringNodes();
}

void BackwardScoring::processImpactAfterServeEvent(){
	cout<<"Process impact after serve event"<<endl;
	const Event &impactAfterServeEvent = m_eventDetection->getEvent(m_actualEventNumber);
	m_hasDetectedImpactAfterServe = true;
	if(impactAfterServeEvent.getIsRight() == false){
		if(impactAfterServeEvent.getIsInsideServeImpactZone() == true){
			if(m_actualServeTry == 1){
				m_secondServeIsReasonable = false;
			}
			m_playerWinsRally = 2;
			m_numberOfImpactsInside = 1;
		}
		else if(m_actualServeTry == 1){
			m_faultDetected = true;
			m_playerWinsRally = 1;
			m_secondServeIsReasonable = true;
			m_serveIsAce = false;
		}
		else{
			cout<<"Has DoubleFautlt detected"<<endl;
			m_faultDetected = true;
			m_hasDetectedDoubleFault = true;
			m_serveIsAce = false;
			m_playerWinsRally = 1;
		}
	}
	if(impactAfterServeEvent.getIsRight() == true){
		if(impactAfterServeEvent.getIsInsideServeImpactZone() == true){
			if(m_actualServeTry == 1){
				m_secondServeIsReasonable = false;
			}
			m_playerWinsRally = 1;
			m_numberOfImpactsInside = 1;
		}
		else if(m_actualServeTry == 1){
			m_secondServeIsReasonable = true;
			m_faultDetected = true;
			m_playerWinsRally = 2;
			m_serveIsAce = false;
		}
		else{
			cout<<"Has DoubleFautlt detected"<<endl;
			m_faultDetected = true;
			m_hasDetectedDoubleFault = true;
			m_serveIsAce = false;
			m_playerWinsRally = 2;
		}
	}
	m_actualProbabilityOfScore = calculateProbabilityOfPoint(impactAfterServeEvent.getProbabilityEventHappend());
}

void BackwardScoring::processStrokeEvent(){
	cout<<"Process stroke Event"<<endl;
	m_serveIsAce = false;
	m_isWinnerForSide = 0;
	if(m_faultDetected == false){
		const Event &strokeEvent = m_eventDetection->getEvent(m_actualEventNumber);
		if(strokeEvent.getIsRight() == true){
			m_playerWinsRally = 1;
			m_isErrorForSide = 2;
		}
		else{
			m_playerWinsRally = 2;
			m_isErrorForSide = 1;
		}
		updateStrokeSpeed(strokeEvent.getStrokeSpeed(), strokeEvent.getIsRight());
		updateStroke(strokeEvent.getStrokeType(), strokeEvent.getIsRight());
		m_numberOfImpactsInside = 0;
		m_actualProbabilityOfScore = calculateProbabilityOfPoint(strokeEvent.getProbabilityEventHappend());
	}
}

void BackwardScoring::processOverNetEvent(){
	cout<<"Process over net Event"<<endl;
	if(m_faultDetected == false){
		const Event &overNetEvent = m_eventDetection->getEvent(m_actualEventNumber);
		if(overNetEvent.getIsRight() == true){
			m_playerWinsRally = 1;
		}
		else{
			m_playerWinsRally = 2;
		}
		m_numberOfImpactsInside = 0;
		m_actualProbabilityOfScore = calculateProbabilityOfPoint(overNetEvent.getProbabilityEventHappend());
	}
}

void BackwardScoring::processCriticalImpactEvent(){
	cout<<"Process critical impact Event"<<endl;
	if(m_faultDetected == false){
		const Event &impactEvent = m_eventDetection->getEvent(m_actualEventNumber);
		if(impactEvent.getIsRight() == true){
			if(impactEvent.getIsInsideSingleFieldZone() == true){
				if(m_playerWinsRally == 1) {
					m_numberOfImpactsInside++;
				}
				else{
					m_playerWinsRally = 1;
					m_numberOfImpactsInside = 1;
					m_actualProbabilityOfScore = calculateProbabilityOfPoint(impactEvent.getProbabilityEventHappend());
				}
				if(m_numberOfImpactsInside > 1){
					m_faultDetected = true;
				}
				m_isErrorForSide = 0;
				m_isWinnerForSide = 1;
			}
			else{
				m_faultDetected = true;
				m_actualProbabilityOfScore = calculateProbabilityOfPoint(impactEvent.getProbabilityEventHappend());
			}
		}
		else{
			if(impactEvent.getIsInsideSingleFieldZone() == true){
				if(m_playerWinsRally == 2) {
					m_numberOfImpactsInside++;
				}
				else{
					m_playerWinsRally = 2;
					m_numberOfImpactsInside = 1;
					m_actualProbabilityOfScore = calculateProbabilityOfPoint(impactEvent.getProbabilityEventHappend());
				}

				if(m_numberOfImpactsInside > 1){
					m_faultDetected = true;
				}
				m_isErrorForSide = 0;
				m_isWinnerForSide = 2;
			}
			else{
				m_faultDetected = true;
				m_actualProbabilityOfScore = calculateProbabilityOfPoint(impactEvent.getProbabilityEventHappend());
			}
		}
	}
}

void BackwardScoring::processEndOfRallyEvent(){
	cout<<"Process End of Rally Event"<<endl;
	m_faultDetected = true;
	if(m_hasDetectedImpactAfterServe == false){
		m_hasDetectedDoubleFault = true;
	}
}

void BackwardScoring::processContinuationOfRallyEvent(){
	//TODO check if this function works correct.
	cout<<"Precess continuationOfRallyEvent"<<endl;
	m_faultDetected = false;
	m_updatedProbability = false;
	for(uint i = 0; i < m_newestScoringNodes.size(); i++){
		m_newestScoringNodes[i]->resetLastRallyStopFrameCount();
		if(m_newestScoringNodes[i]->getPointWinForSide() == m_playerWinsRally){
			m_newestScoringNodes[i]->resetProbabilityActualPointIsCorrect(m_actualProbabilityOfScore);
		}
		else{
			m_newestScoringNodes[i]->resetProbabilityActualPointIsCorrect(1.0f - m_actualProbabilityOfScore);
		}
	}
}

void BackwardScoring::processEndOfGameEvent(){
	cout<<"Process End of Game Event"<<endl;
	cout<<"Unsorted"<<endl;
	printNewestScoringNodes();
	sort(m_newestScoringNodes.begin(), m_newestScoringNodes.end(), m_scoringTree);
	cout<<"Sorted"<<endl;
	printNewestScoringNodes();
	const Event &endOfGameEvent = m_eventDetection->getEvent(m_actualEventNumber);
	if(endOfGameEvent.getIsChangeOfServe() == true){
		int i = 0;
		while(checkIfPossbibleScoringPathExist(true) == false && i < 4){
			cout<<"Could not find a possible scoring path. Start filling up with points."<<endl;
			restockScoringPaths();
			i++;
		}
		int newestScoringNodesSize = (int)m_newestScoringNodes.size();
		for(int i = 0; i < newestScoringNodesSize; i++){
			int sumOfGames = m_newestScoringNodes.front()->getGamesWonByPlayer1() + m_newestScoringNodes.front()->getGamesWonByPlayer2();
			if(sumOfGames % 2 == 0 && m_newestScoringNodes.front()->getPointsWonLeft() == 0 && m_newestScoringNodes.front()->getPointsWonRight() == 0){
				markPossibleScoringPath(m_newestScoringNodes.front());
				m_newestScoringNodes.front()->setServePosGameEnded(0);
				m_newestScoringNodes.push_back(m_newestScoringNodes.front());
			}
			m_newestScoringNodes.erase(m_newestScoringNodes.begin());
		}
		deleteFalseScoringPath();
		mergeEqualGameNodes();
		adjustProbabilities();
	}
	else{
		int i = 0;
		while(checkIfPossbibleScoringPathExist(true) == false && i < 4){
			cout<<"Could not find a possible scoring path. Start filling up with points."<<endl;
			restockScoringPaths();
			i++;
		}
		int newestScoringNodesSize = (int)m_newestScoringNodes.size();
		for(int i = 0; i < newestScoringNodesSize; i++){
			int sumOfGames = m_newestScoringNodes.front()->getGamesWonByPlayer1() + m_newestScoringNodes.front()->getGamesWonByPlayer2();
			if(sumOfGames % 2 == 1 && m_newestScoringNodes.front()->getPointsWonLeft() == 0 && m_newestScoringNodes.front()->getPointsWonRight() == 0){
				markPossibleScoringPath(m_newestScoringNodes.front());
				m_newestScoringNodes.push_back(m_newestScoringNodes.front());
			}
			m_newestScoringNodes.erase(m_newestScoringNodes.begin());
		}
		deleteFalseScoringPath();
		mergeEqualGameNodes();
		adjustProbabilities();
	}
	cout<<"End of Game"<<endl;
	printNewestScoringNodes();
}

void BackwardScoring::processEndOfMatchEvent(){
	cout<<"Process End of match Event"<<endl;

	if(m_faultDetected == false){
		m_faultDetected = true;
		updateScoreProbabilites();
	}
	int iter = 0;
	while(checkIfPossbibleScoringPathExist(true) == false && checkIfPossbibleScoringPathExist(false) == false && iter < 4){
		cout<<"Could not find a possible scoring path. Start filling up with points."<<endl;
		restockScoringPaths();
		iter++;
	}
	int newestScoringNodesSize = (int)m_newestScoringNodes.size();
	printNewestScoringNodes();
	for(int i = 0; i < newestScoringNodesSize; i++){
		if(m_newestScoringNodes.front()->getPointsWonLeft() == 0 && m_newestScoringNodes.front()->getPointsWonRight() == 0){
			markPossibleScoringPath(m_newestScoringNodes.front());
			m_newestScoringNodes.push_back(m_newestScoringNodes.front());
		}
		m_newestScoringNodes.erase(m_newestScoringNodes.begin());
	}
	deleteFalseScoringPath();
	mergeEqualGameNodes();
	adjustProbabilities();
	cout<<"End of Match"<<endl;
	cout<<"Unsorted"<<endl;
	printNewestScoringNodes();
	sort(m_newestScoringNodes.begin(), m_newestScoringNodes.end(), ScoringNode())	;
	cout<<"Sorted"<<endl;
	printNewestScoringNodes();
	bool foundCorrectScoring = false;
	int i = 0;
	while(foundCorrectScoring == false && i < m_newestScoringNodes.size()){
		recalculateScoringHistory(m_newestScoringNodes[i], foundCorrectScoring);
		i++;
	}
	if(foundCorrectScoring == false){
		cout<<"Could not find a correct Score and take the score with most probability"<<endl;
		if(m_newestScoringNodes.empty() == false){
			recalculateScoringHistory(m_newestScoringNodes[0], foundCorrectScoring);
		}
	}
}

void BackwardScoring::recalculateScoringHistory(ScoringNode *mostLikelyScoringNode, bool &foundCorrectScoring){

	cout<<"Recalculation scoring history"<<endl;
	vector<ScoringNode*> scoringHistory;
	ScoringNode *backwardScoringNode = mostLikelyScoringNode;
	m_match->reset();
	cout<<"Starting backward scoring recalculation"<<endl;
	while(backwardScoringNode != NULL){
		cout<<*backwardScoringNode<<endl;
		scoringHistory.insert(scoringHistory.begin(), backwardScoringNode);
		backwardScoringNode = backwardScoringNode->getScoringNodeParent();
	}
	cout<<"Scoring History"<<endl;
	for(uint i = 0; i < scoringHistory.size(); i++){
		cout<<*scoringHistory[i]<<endl;
	}

	for(uint i = 1; i < scoringHistory.size(); i++){
		m_match->printActualScore();
		if(scoringHistory[i]->getPointsWonLeft() == 0 && scoringHistory[i]->getPointsWonRight() == 0){
			cout<<endl;
		}
		if(scoringHistory[i]->getScoringNodeParent() != NULL){
			if(scoringHistory[i]->getPlayerNumberRight() == 2){
				if(scoringHistory[i]->getPointWinForSide() == 2){
					m_match->addPointWinByPlayer(2, scoringHistory[i]);
				}
				else if(scoringHistory[i]->getPointWinForSide() == 1){
					m_match->addPointWinByPlayer(1, scoringHistory[i]);
				}
			}
			else if(scoringHistory[i]->getPlayerNumberRight() == 1){
				if(scoringHistory[i]->getPointWinForSide() == 2){
					m_match->addPointWinByPlayer(1, scoringHistory[i]);
				}
				else if(scoringHistory[i]->getPointWinForSide() == 1){
					m_match->addPointWinByPlayer(2, scoringHistory[i]);
				}
			}
		}
	}
	cout<<"PrintActualMatch"<<endl;
	m_match->printActualScore();

	if(m_match->getIsLastSetCompleted() == true){
		foundCorrectScoring = true;
		cout<<"Found Correct Score"<<endl;
		cout<<"Player1 Points Won: "<<m_match->getTotalPointsWon(1)<<" Player2 Points Won: "<<m_match->getTotalPointsWon(2)<<endl;
		cout<<"Player1 FirstServes: "<<m_match->getNumberOfFirstServes(1)<<" Player2 FirstServes: "<<m_match->getNumberOfFirstServes(2)<<endl;
		cout<<"Player1 SecondServes: "<<m_match->getNumberOfSecondServes(1)<<" Player2 SecondServes: "<<m_match->getNumberOfSecondServes(2)<<endl;
		cout<<"Player1 FirstServePercentage: "<<m_match->getFirstServePercentage(1)<<" Player2 FirstServePercentage: "<<m_match->getFirstServePercentage(2)<<endl;
		cout<<"Player1 DoubleFaults: "<<m_match->getDoubleFaults(1)<<" Player2 DoubleFaults: "<<m_match->getDoubleFaults(2)<<endl;
		cout<<"Player1 Aces: "<<m_match->getNumberOfAces(1)<<" Player2 Aces: "<<m_match->getNumberOfAces(2)<<endl;
		cout<<"Player1 FastesServeSpeed: "<<m_match->getFastesServeSpeed(1)<<" Player2 FastesServeSpeed: "<<m_match->getFastesServeSpeed(2)<<endl;
		cout<<"Player1 FastesStrokeSpeed: "<<m_match->getFastesStrokeSpeed(1)<<" Player2 FastesStrokeSpeed: "<<m_match->getFastesStrokeSpeed(2)<<endl;
		cout<<"Player1 Number Forehands: "<<m_match->getNumberOfForehand(1)<<" Player2 Number Forehands: "<<m_match->getNumberOfForehand(2)<<endl;
		cout<<"Player1 Number ForehandSpin: "<<m_match->getNumberOfForehandSpin(1)<<" Player2 Number ForehandSpin: "<<m_match->getNumberOfForehandSpin(2)<<endl;
		cout<<"Player1 Number ForehandSlice: "<<m_match->getNumberOfForehandSlice(1)<<" Player2 Number ForehandSlice: "<<m_match->getNumberOfForehandSlice(2)<<endl;
		cout<<"Player1 Number Backhands: "<<m_match->getNumberOfBackhand(1)<<" Player2 Number Backhand: "<<m_match->getNumberOfBackhand(2)<<endl;
		cout<<"Player1 Number BackhandSpin: "<<m_match->getNumberOfBackhandSpin(1)<<" Player2 Number Backhandspin: "<<m_match->getNumberOfBackhandSpin(2)<<endl;
		cout<<"Player1 Number BackhandSlice: "<<m_match->getNumberOfBackhandSlice(1)<<" Player2 Number Backhandslice: "<<m_match->getNumberOfBackhandSlice(2)<<endl;
		cout<<"Player1 Errors: "<<m_match->getNumberOfErrors(1)<<" Player2 Errors: "<<m_match->getNumberOfErrors(2)<<endl;
		cout<<"Player1 Winners: "<<m_match->getNumberOfWinners(1)<<" Player2 Winners: "<<m_match->getNumberOfWinners(2)<<endl;
	}
	else{
		foundCorrectScoring = false;
		cout<<"Found False Score"<<endl;
	}
}

void BackwardScoring::updateScoreProbabilites(){
	m_updatedProbability = true;
	printNewestScoringNodes();
	//m_probabilityActualPointIsCorrect;
	updateStatistics();
	cout<<"ProbabilityUpdate: "<<m_actualProbabilityOfScore<<endl;
	cout<<"ProbabilityUpdate: "<<(1.0f-m_actualProbabilityOfScore)<<endl;
	for(uint i = 0; i < m_newestScoringNodes.size(); i++){
		//Checks if the Scoring node contains the most likely score. If so it checks whether this serve is a second serve
		//and if it is reasonable(e.g the last impact after serve was a first serve an not inside serveImpactzone). Then it resets the last added Probability.
		if(m_newestScoringNodes[i]->getPointWinForSide() == m_playerWinsRally){
			if(m_secondServeIsReasonable == true){
				m_newestScoringNodes[i]->setProbabilityActualPointIsCorrect(m_actualProbabilityOfScore);
			}
			else{
				m_newestScoringNodes[i]->resetProbabilityActualPointIsCorrect(m_actualProbabilityOfScore);
			}
		}
		else{
			if(m_secondServeIsReasonable == true){
				m_newestScoringNodes[i]->setProbabilityActualPointIsCorrect(1.0f - m_actualProbabilityOfScore);
			}
			else{
				m_newestScoringNodes[i]->resetProbabilityActualPointIsCorrect(1.0f - m_actualProbabilityOfScore);
			}
		}
	}
	printNewestScoringNodes();
}

void BackwardScoring::updateStatistics(){
	for(uint i = 0; i < m_newestScoringNodes.size(); i++){
		m_newestScoringNodes[i]->addRallyStopFrameCount(m_eventDetection->getEvent(m_actualEventNumber).getNumberOfFrame());
		//TODO checken ob das doublefault für alle gesetzt werden müssen oder nur, da wo der Punkt verloren wird.
		m_newestScoringNodes[i]->setHasDetectedDoubleFault(m_hasDetectedDoubleFault);
		if((m_newestScoringNodes[i]->getPlayerNumberRight() == 2 && m_newestScoringNodes[i]->getPointWinForSide() == 2 && m_newestScoringNodes[i]->getPlayerNumberWhoServes() == 2) ||
				(m_newestScoringNodes[i]->getPlayerNumberRight() == 1 && m_newestScoringNodes[i]->getPointWinForSide() == 2 && m_newestScoringNodes[i]->getPlayerNumberWhoServes() == 1) ||
				(m_newestScoringNodes[i]->getPlayerNumberRight() == 2 && m_newestScoringNodes[i]->getPointWinForSide() == 1 && m_newestScoringNodes[i]->getPlayerNumberWhoServes() == 1) ||
				(m_newestScoringNodes[i]->getPlayerNumberRight() == 1 && m_newestScoringNodes[i]->getPointWinForSide() == 1 && m_newestScoringNodes[i]->getPlayerNumberWhoServes() == 2)){
			m_newestScoringNodes[i]->setServeIsAce(m_serveIsAce);
			//cout<<"SetServeIsAce"<<endl;
		}
		if(m_newestScoringNodes[i]->getPointWinForSide() == 1 && m_isWinnerForSide == 1){
			if(m_newestScoringNodes[i]->getPlayerNumberRight() == 2){
				m_newestScoringNodes[i]->setPlayerNumberCausedAWinner(1);
			}
			else if(m_newestScoringNodes[i]->getPlayerNumberRight() == 1){
				m_newestScoringNodes[i]->setPlayerNumberCausedAWinner(2);
			}
		}
		else if(m_newestScoringNodes[i]->getPointWinForSide() == 2 && m_isWinnerForSide == 2){
			if(m_newestScoringNodes[i]->getPlayerNumberRight() == 2){
				m_newestScoringNodes[i]->setPlayerNumberCausedAWinner(2);
			}
			else if(m_newestScoringNodes[i]->getPlayerNumberRight() == 1){
				m_newestScoringNodes[i]->setPlayerNumberCausedAWinner(1);
			}
		}

		if(m_newestScoringNodes[i]->getPointWinForSide() == 1 && m_isErrorForSide == 2){
			if(m_newestScoringNodes[i]->getPlayerNumberRight() == 2){
				m_newestScoringNodes[i]->setPlayerNumberCausedAnError(2);
			}
			else if(m_newestScoringNodes[i]->getPlayerNumberRight() == 1){
				m_newestScoringNodes[i]->setPlayerNumberCausedAnError(1);
			}
		}
		else if(m_newestScoringNodes[i]->getPointWinForSide() == 2 && m_isErrorForSide == 1){
			if(m_newestScoringNodes[i]->getPlayerNumberRight() == 2){
				m_newestScoringNodes[i]->setPlayerNumberCausedAnError(1);
			}
			else if(m_newestScoringNodes[i]->getPlayerNumberRight() == 1){
				m_newestScoringNodes[i]->setPlayerNumberCausedAnError(2);
			}
		}
	}
}

void BackwardScoring::updateStrokeSpeed(float strokeSpeed, bool isRight){
	for(int i = 0; i < m_newestScoringNodes.size(); i++){
		if((m_newestScoringNodes[i]->getPlayerNumberRight() == 1 && isRight == true)||(m_newestScoringNodes[i]->getPlayerNumberRight() == 2 && isRight == false)){
			m_newestScoringNodes[i]->addStrokeSpeed(0, strokeSpeed);
		}
		else if((m_newestScoringNodes[i]->getPlayerNumberRight() == 2 && isRight == true)||(m_newestScoringNodes[i]->getPlayerNumberRight() == 1 && isRight == false)){
			m_newestScoringNodes[i]->addStrokeSpeed(1, strokeSpeed);
		}
	}
}

void BackwardScoring::updateStroke(int strokeType, bool isRight){
	for(int i = 0; i < m_newestScoringNodes.size(); i++){
		if((m_newestScoringNodes[i]->getPlayerNumberRight() == 1 && isRight == true)||(m_newestScoringNodes[i]->getPlayerNumberRight() == 2 && isRight == false)){
			m_newestScoringNodes[i]->addStroke(0, strokeType);
		}
		else if((m_newestScoringNodes[i]->getPlayerNumberRight() == 2 && isRight == true)||(m_newestScoringNodes[i]->getPlayerNumberRight() == 1 && isRight == false)){
			m_newestScoringNodes[i]->addStroke(1, strokeType);
		}
	}
}

void BackwardScoring::updateNewestScoringNodesAfterSecondServe(int startFrameCount, float speed){
	m_newestScoringNodes.front()->increaseServeTry();
	m_newestScoringNodes.front()->addRallyStartFrameCount(startFrameCount);
	m_newestScoringNodes.front()->addServeSpeed(speed);
	m_newestScoringNodes.push_back(m_newestScoringNodes.front());
	m_newestScoringNodes.erase(m_newestScoringNodes.begin());
	m_actualServeTry++;
}

void BackwardScoring::updateNewestScoringNodesAfterFirstServe(int newServePos, int startFrameCount, bool serveSideIsRight, float speed){
	m_newestScoringNodes.front()->addScoringNodes(newServePos, startFrameCount, serveSideIsRight);
	//TODO check if servespeed should be added to the child scoring nodes
	m_newestScoringNodes.front()->addServeSpeed(speed);
	m_newestScoringNodes.push_back(m_newestScoringNodes.front()->getScoringNodeLeft());
	m_newestScoringNodes.push_back(m_newestScoringNodes.front()->getScoringNodeRight());
	m_newestScoringNodes.erase(m_newestScoringNodes.begin());
	m_actualServeTry = 1;
}

bool BackwardScoring::updateNewestScoringNodesAfterFirstAndSecondServe(int newServePos, int startFrameCount, bool serveSideIsRight, float speed){
	bool deletedChildren = false;
	m_newestScoringNodes.front()->increaseServeTry();
	m_newestScoringNodes.front()->addRallyStartFrameCount(startFrameCount);
	m_newestScoringNodes.front()->addServeSpeed(speed);
	if(m_newestScoringNodes.front()->getScoringNodeLeft() != NULL && m_newestScoringNodes.front()->getScoringNodeRight() != NULL){
		m_newestScoringNodes.erase(m_newestScoringNodes.begin() + 1);
		m_newestScoringNodes.erase(m_newestScoringNodes.begin() + 1);
		m_newestScoringNodes.front()->deleteChildNodes();
		deletedChildren = true;
	}
	m_newestScoringNodes.front()->addScoringNodes(newServePos, startFrameCount, serveSideIsRight);
	m_newestScoringNodes.push_back(m_newestScoringNodes.front());
	m_newestScoringNodes.push_back(m_newestScoringNodes.front()->getScoringNodeLeft());
	m_newestScoringNodes.push_back(m_newestScoringNodes.front()->getScoringNodeRight());
	m_newestScoringNodes.erase(m_newestScoringNodes.begin());
	m_actualServeTry = 1;
	return deletedChildren;
}

void BackwardScoring::markPossibleScoringPath(ScoringNode* startNode){
	startNode->setIsMarkedAsPossibleScorePathNode(true);
	ScoringNode *parentNode = startNode->getScoringNodeParent();
	while(parentNode != NULL){
		parentNode->setIsMarkedAsPossibleScorePathNode(true);
		parentNode = parentNode->getScoringNodeParent();
	}
}

bool BackwardScoring::mergeEqualScoringNodes(int nodeNumber){

	if(m_newestScoringNodes[nodeNumber]->getPointsWonLeft() == m_newestScoringNodes[nodeNumber + 1]->getPointsWonLeft() && m_newestScoringNodes[nodeNumber]->getPointsWonRight() == m_newestScoringNodes[nodeNumber + 1]->getPointsWonRight()){
		if(m_newestScoringNodes[nodeNumber]->getGamesWonByPlayer1() == m_newestScoringNodes[nodeNumber + 1]->getGamesWonByPlayer1() && m_newestScoringNodes[nodeNumber]->getGamesWonByPlayer2() == m_newestScoringNodes[nodeNumber + 1]->getGamesWonByPlayer2()){
			if(m_newestScoringNodes[nodeNumber]->getProbabilityScoreIsCorrect() > m_newestScoringNodes[nodeNumber + 1]->getProbabilityScoreIsCorrect()){
				m_newestScoringNodes[nodeNumber]->mergeWith(*m_newestScoringNodes[nodeNumber + 1]);
				delete m_newestScoringNodes[nodeNumber + 1];
				m_newestScoringNodes.erase(m_newestScoringNodes.begin() + nodeNumber + 1);
			}
			else{
				m_newestScoringNodes[nodeNumber + 1]->mergeWith(*m_newestScoringNodes[nodeNumber]);
				delete m_newestScoringNodes[nodeNumber];
				m_newestScoringNodes.erase(m_newestScoringNodes.begin() + nodeNumber);
			}
			return true;
		}
		else{
			return false;
		}
	}
	else{
		return false;
	}
}

void BackwardScoring::mergeEqualGameNodes(){
	for(int i = 0; i < (int)m_newestScoringNodes.size()-1; i++){
		for(int j = i + 1; j < (int)m_newestScoringNodes.size(); j++){
			if(m_newestScoringNodes[i]->getPointsWonLeft() == m_newestScoringNodes[j]->getPointsWonLeft() && m_newestScoringNodes[i]->getPointsWonRight() == m_newestScoringNodes[j]->getPointsWonRight()){
				//if(m_newestScoringNodes[i]->getGamesWonLeft() == m_newestScoringNodes[j]->getGamesWonLeft() && m_newestScoringNodes[i]->getGamesWonRight() == m_newestScoringNodes[j]->getGamesWonRight()){
				if(m_newestScoringNodes[i]->getGamesWonByPlayer1() == m_newestScoringNodes[j]->getGamesWonByPlayer1() && m_newestScoringNodes[i]->getGamesWonByPlayer2() == m_newestScoringNodes[j]->getGamesWonByPlayer2() && m_newestScoringNodes[i]->getSetsWonByPlayer1() == m_newestScoringNodes[j]->getSetsWonByPlayer1() && m_newestScoringNodes[i]->getSetsWonByPlayer2() == m_newestScoringNodes[j]->getSetsWonByPlayer2()){
					if(m_newestScoringNodes[i]->getProbabilityScoreIsCorrect() > m_newestScoringNodes[j]->getProbabilityScoreIsCorrect()){
						m_newestScoringNodes[i]->mergeWith(*m_newestScoringNodes[j]);
						delete m_newestScoringNodes[j];
						m_newestScoringNodes.erase(m_newestScoringNodes.begin() + j);
						j--;
					}
					else{
						m_newestScoringNodes[j]->mergeWith(*m_newestScoringNodes[i]);
						delete m_newestScoringNodes[i];
						m_newestScoringNodes.erase(m_newestScoringNodes.begin() + i);
						j = i + 1;
					}
				}
			}
		}
	}
}

void BackwardScoring::deleteFalseScoringPath(){
	queue<ScoringNode*> scoringQueue;
	scoringQueue.push(&m_scoringTree);
	while(!scoringQueue.empty()){
		ScoringNode *node = scoringQueue.front();
		//node->setIsMarkedAsPossibleScorePathNode(false);
		scoringQueue.pop();
		if(node->getScoringNodeLeft() != NULL){
			if(node->getScoringNodeLeft()->getIsMarkedAsPossibleScorePathNode() == true){
				scoringQueue.push(node->getScoringNodeLeft());
			}
			else{
				delete node->getScoringNodeLeft();
				node->resetScoringNodeLeft();
			}
		}
		if(node->getScoringNodeRight() != NULL){
			if(node->getScoringNodeRight()->getIsMarkedAsPossibleScorePathNode() == true){
				scoringQueue.push(node->getScoringNodeRight());
			}
			else{
				delete node->getScoringNodeRight();
				node->resetScoringNodeRight();
			}
		}
	}
}

bool BackwardScoring::checkIfPossbibleScoringPathExist(bool isChangeOfServe){
	int sumOfGamesRemainder = 0;
	if(isChangeOfServe == false){
		sumOfGamesRemainder = 1;
	}
	for(int i = 0; i < m_newestScoringNodes.size(); i++){
		int sumOfGames = m_newestScoringNodes[i]->getGamesWonByPlayer1() + m_newestScoringNodes[i]->getGamesWonByPlayer2();
		if(sumOfGames % 2 == sumOfGamesRemainder && m_newestScoringNodes[i]->getPointsWonLeft() == 0 && m_newestScoringNodes[i]->getPointsWonRight() == 0){
			return true;
		}
	}
	return false;
}

void BackwardScoring::restockScoringPaths(){
	int newestScoringNodesSize = (int)m_newestScoringNodes.size();
	for(int i = 0; i < newestScoringNodesSize; i++){
		if(m_newestScoringNodes.front()->getActualServePos() == 2){
			if(i+1 < newestScoringNodesSize){
				if(mergeEqualScoringNodes(0) == true){
					i++;
				}
			}
			updateNewestScoringNodesAfterFirstServe(1, -1, m_newestScoringNodes.front()->getServeIsRight(), 0);
		}
		else if(m_newestScoringNodes.front()->getActualServePos() == 1){
			if(i+1 < newestScoringNodesSize){
				if(mergeEqualScoringNodes(0) == true){
					i++;
				}
			}
			updateNewestScoringNodesAfterFirstServe(2, -1, m_newestScoringNodes.front()->getServeIsRight(), 0);
		}
	}
}

void BackwardScoring::adjustProbabilities(){
	float probabilitySum = 0;
	for(int i = 0; i < m_newestScoringNodes.size(); i++){
		probabilitySum += m_newestScoringNodes[i]->getProbabilityScoreIsCorrect();
	}
	for(int i = 0; i < m_newestScoringNodes.size(); i++){
		m_newestScoringNodes[i]->setProbabilityScoreIsCorrect(m_newestScoringNodes[i]->getProbabilityScoreIsCorrect() / probabilitySum);
	}
}

float BackwardScoring::calculateProbabilityOfPoint(float eventProbability){
	return (eventProbability + 1.0f)/2.0f;
}

void BackwardScoring::printNewestScoringNodes(){
	for(int i = 0; i < m_newestScoringNodes.size(); i++){
		cout<<"Node "<<i<<" ->"<<*m_newestScoringNodes[i]<<endl;
	}
}
void BackwardScoring::printScoringNode(ScoringNode *scoringNode){
	cout<<"GamesSide: "<<scoringNode->getGamesWonLeft()<<" : "<<scoringNode->getGamesWonRight()
	<<"Games: "<<scoringNode->getGamesWonByPlayer1()<<" : "<<scoringNode->getGamesWonByPlayer2()
	<<" Rallys: "<<scoringNode->getPointsWonLeft()<<" : "<<scoringNode->getPointsWonRight()
	<<" Prob: "<<scoringNode->getProbabilityScoreIsCorrect()
	<<" ServePos: "<<scoringNode->getActualServePos()
	<<" Marked: "<<scoringNode->getIsMarkedAsPossibleScorePathNode()
	<<" player right: "<<scoringNode->getPlayerNumberRight()
	<<" StartFrame: "<<scoringNode->getStartFrameCount()
	<<" StopFrame: "<<scoringNode->getStopFrameCount()
	<<endl;
}
