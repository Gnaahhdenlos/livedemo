//
//  curve.cpp
//
//
//  Created by Henri Kuper on 11.07.17.
//
//

#include <stdio.h>
#include "curve.hpp"

using namespace std;

Curve::Curve(){
	curveInit();
}

Curve::Curve(cv::Point2f ballCenter, int frameCount, float distToHumanMask, cv::Point2f playerPosition, cv::Rect ballBoundRect){
	curveInit();
	appendData(distToHumanMask, playerPosition, ballBoundRect, ballCenter, frameCount);
}

Curve::~Curve(){
	m_ballPoints.clear();
	m_frameCounts.clear();
	m_distancesToPlayer.clear();
	m_ballsBoundRects.clear();
	m_positionsOfPlayerDuringCurve.clear();
}

void Curve::curveInit(){
	m_characterisation = -1;
	m_isCompleted = 0;
	m_isFinished = false;
	m_isNearPlayer = 0;
	m_speed = 0.0;
	m_pointOfImpact = cv::Point2f(-10000,-10000);
	m_pointOfImpactFrameCount = -1;

	m_followingCurveIndex = -1;
	m_previousCurveIndex = -1;	
}

void Curve::appendData(float distToHumanMask, cv::Point2f playerPosition, cv::Rect ballBoundRect, cv::Point2f ballCenter, int frameCount){
	m_distancesToPlayer.push_back(distToHumanMask);
	m_positionsOfPlayerDuringCurve.push_back(playerPosition);
	m_ballsBoundRects.push_back(ballBoundRect);
	m_ballPoints.push_back(ballCenter);
	m_frameCounts.push_back(frameCount);
}

void Curve::deleteData(int position){

	m_distancesToPlayer.erase(m_distancesToPlayer.begin() + position);
	m_positionsOfPlayerDuringCurve.erase(m_positionsOfPlayerDuringCurve.begin() + position);
	m_ballsBoundRects.erase(m_ballsBoundRects.begin() + position);
	m_ballPoints.erase(m_ballPoints.begin() + position);
	m_frameCounts.erase(m_frameCounts.begin() + position);
}

void Curve::deleteLastData(){

	m_distancesToPlayer.pop_back();
	m_positionsOfPlayerDuringCurve.pop_back();
	m_ballsBoundRects.pop_back();
	m_ballPoints.pop_back();
	m_frameCounts.pop_back();
}

void Curve::insertData(int position, float distToHumanMask, cv::Point2f playerPosition, cv::Rect ballBoundRect, cv::Point2f ballCenter, int frameCount){

	m_distancesToPlayer.insert(m_distancesToPlayer.begin() + position, distToHumanMask);
	m_positionsOfPlayerDuringCurve.insert(m_positionsOfPlayerDuringCurve.begin() + position, playerPosition);
	m_ballsBoundRects.insert(m_ballsBoundRects.begin() + position, ballBoundRect);
	m_ballPoints.insert(m_ballPoints.begin() + position, ballCenter);
	m_frameCounts.insert(m_frameCounts.begin() + position, frameCount);
}

string Curve::getCharacterisationString(){
	string curveCharacter;

	switch(m_characterisation) {
	case 7: curveCharacter = "Impact"; break;
	case 8: curveCharacter = "Impact"; break;
	case 9: curveCharacter = "Stroke left player"; break;
	case 10: curveCharacter = "Stroke right player"; break;
	case 11: curveCharacter = "Stroke left player"; break;
	case 12: curveCharacter = "Stroke right player"; break;
	case 13: curveCharacter = "Stroke left player"; break;
	case 14: curveCharacter = "Stroke right player"; break;
	case 15: curveCharacter = "Stroke left player"; break;
	case 16: curveCharacter = "Stroke right player"; break;
	case 17: curveCharacter = "From right over net"; break;
	case 18: curveCharacter = "From left over net"; break;
	case 19: curveCharacter = "Rally ending right"; break;
	case 20: curveCharacter = "Rally ending left"; break;
	case 21: curveCharacter = "Serve left player"; break;
	case 22: curveCharacter = "Serve right player"; break;
	default: curveCharacter = "default"; break;
	}
	return curveCharacter;
}
