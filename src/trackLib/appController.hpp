//
//  appController.hpp
//
//
//  Created by Henri Kuper on 05.09.17.
//
//

#ifndef appController_hpp
#define appController_hpp

#define useCamera

#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <thread>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "../imgProcLib/imageProcessing.hpp"
#include "frameGrabber.hpp"
#include "configData.hpp"
#include "fieldDetection.hpp"
#include "playerDetection.hpp"
#include "ballDetection.hpp"
#include "curveDetection.hpp"
#include "eventDetection.hpp"
#include "statistics.hpp"
#include "videoCreator.hpp"
#include "backwardScoring.hpp"

#include "../communicationLib/communication.hpp"
#include "../communicationLib/jsonCreator.hpp"

#ifdef useCamera
#include "../cameraLib/camera.hpp"
#include "../cameraLib/qrCodeScanner.hpp"
#endif

class AppController {
private:

struct ConfigData m_config;
FrameGrabber *m_frameGrabber;
Camera *m_cameras;

Communication *m_communication;
JsonCreator *m_jsonCreator;

FieldDetection *m_fieldDetectionRight;
FieldDetection *m_fieldDetectionLeft;

PlayerDetection *m_playerDetection;

BallDetection *m_ballDetectionRight;                              //pointer to the coresponding BallDetection object
BallDetection *m_ballDetectionLeft;                               //pointer to the coresponding BallDetection object

CurveDetection *m_curveDetectionRight;
CurveDetection *m_curveDetectionLeft;

EventDetection *m_eventDetection;

BackwardScoring *m_backwardScoring;

Statistics *m_statistics;
Match *m_match;

VideoCreator *m_videoCreator;

cv::VideoCapture m_videoStreamRight;
cv::VideoCapture m_videoStreamLeft;

cv::VideoCapture m_calibrationVideoStreamRight;
cv::VideoCapture m_calibrationVideoStreamLeft;

std::vector<cv::Mat> m_frameBufferRight;
std::vector<cv::Mat> m_frameBufferLeft;
std::vector<cv::Mat> m_frameBufferGrayRight;
std::vector<cv::Mat> m_frameBufferGrayLeft;

std::string m_qrCodeLeft, m_qrCodeRight;

int m_frameHeight;
int m_frameWidth;

int m_frameCount;

int m_framesPrediction;
int m_videoFramesOffset;

float m_inputVideoResizingFactor;

bool m_camerasAreRunning;

cv::Mat m_cameraMatrix;
cv::Mat m_distortionCoefficients;
cv::Mat m_undistortionRemap1;
cv::Mat m_undistortionRemap2;

//Timetracking
cv::TickMeter m_tmController;
cv::TickMeter m_tmFrameGrabber;
cv::TickMeter m_tmPlayerDetection;
cv::TickMeter m_tmBallDetection;
cv::TickMeter m_tmCurveDetection;
cv::TickMeter m_tmVideoCreator;
cv::TickMeter m_tmEventDetection;
cv::TickMeter m_tmBufferErase;

double m_controlTime;
double m_frameGrabberTime;
double m_playerTime;
double m_ballTime;
double m_curveTime;
double m_videoTime;
double m_eventTime;
double m_bufferTime;

void appControllerInit();
void cameraInit();
void videoStreamsInit();
void calibrationInit();
void appObjectsInit();
void updateDataForVideoCreator();

void waitingForStop();

public:
AppController(struct ConfigData &config);
~AppController();

void control();
void controlWithCamera();
void controlWithCameraAndGUI();
void createJsonFile();

void stopSession();
void setChangeOfSidesEvent();

void setQRCodes(std::string qrCodeLeft, std::string qrCodeRight){m_qrCodeLeft = qrCodeLeft; m_qrCodeRight = qrCodeRight;}

void printFPS();

};

#endif /* appController_hpp */
