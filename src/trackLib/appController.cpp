//
//  appController.cpp
//
//
//  Created by Henri Kuper on 05.09.17.
//
//
//#define GPU_ON

#include "appController.hpp"

using namespace std;
using namespace cv;

void AppController::appControllerInit(){
	m_frameCount = 0;
	m_videoFramesOffset = 0;//16900;//18960;//1400//400
	m_framesPrediction = 42;
	m_inputVideoResizingFactor = 1.0f;

	FileStorage fileStorage(m_config.getFilePathToCameraCalibrationMatrix(), FileStorage::READ);
	if( !fileStorage.isOpened() ) {

	}
	fileStorage["Camera_Matrix"]>> m_cameraMatrix;
	fileStorage["Distortion_Coefficients"]>> m_distortionCoefficients;
	cout<<"Camera_Matrix: "<<m_cameraMatrix<<endl;
	cout<<"Distortion_Coefficients: "<<m_distortionCoefficients<<endl;

	if(m_config.getWithCamera() == true){
		cout<<"cameraInit"<<endl;
		cameraInit();
	}

	//reseting timers
	m_controlTime = 0;
	m_frameGrabberTime = 0;
	m_playerTime = 0;
	m_ballTime = 0;
	m_curveTime = 0;
	m_videoTime = 0;
	m_eventTime = 0;
	m_bufferTime = 0;
	//Set qrCodes to default values
	//m_qrCodeLeft = "662d8cdb-03c8-478e-b4e6-2c05554066b7";
	//m_qrCodeRight = "662d8cdb-03c8-478e-b4e6-2c05554676b8";
	m_qrCodeLeft = "aaa5a5cb-a3de-4068-ace0-18f9c8148274";
	m_qrCodeRight = "ce5f8cfd-50f5-431d-a9ef-121b9733c68e";


	m_communication = new Communication();
	m_jsonCreator = new JsonCreator();
	//Testing

	// QRCodeScanner scanner = QRCodeScanner();
	// int numberOfCodes = scanner.findQRCode();
	// cout<<"NumberOfCodes: "<<numberOfCodes<<endl;
	// cout<<"qrCodeRight: "<<scanner.getqrCodeRight()<<endl;
	// cout<<"qrCodeLeft: "<<scanner.getqrCodeLeft()<<endl;

	// m_jsonCreator->writeSessionData("3711");
	// m_jsonCreator->writeGameData(1, m_qrCodeLeft, 1, 2, 5, 7, 155, 191, 0.7, 21, 43, 55);
	// m_jsonCreator->writeGameData(2, m_qrCodeRight, 3, 1, 2, 10, 140, 194, 0.75, 31, 33, 40);
	// m_jsonCreator->writeSetData(1, m_qrCodeLeft, 6, 4, 6);
	// m_jsonCreator->writeSetData(2, m_qrCodeRight, 3, 6, 2);
	// m_jsonCreator->printJsonFile();
	//m_communication->sendRequest(m_jsonCreator->getMatchStatisticJsonAsString());
	//waitKey(0);
}

void AppController::cameraInit(){
	cout<<"Camera init"<<endl;
	m_cameras = new Camera(m_cameraMatrix, m_distortionCoefficients);
}

void AppController::videoStreamsInit(){

	bool success;
	success = m_videoStreamRight.open(m_config.getFilePathToVideoRight());
	if ( !m_videoStreamRight.isOpened() && success) {
		cout << "AppController: Cannot open " << m_config.getFilePathToVideoRight() << endl;
	}
	success = m_videoStreamLeft.open(m_config.getFilePathToVideoLeft());
	if ( !m_videoStreamLeft.isOpened() && success) {
		cout << "AppController: Cannot open " << m_config.getFilePathToVideoLeft() << endl;
	}

	int frameWidthRightVideo = m_videoStreamRight.get(CV_CAP_PROP_FRAME_WIDTH);
	int frameHeightRightVideo = m_videoStreamRight.get(CV_CAP_PROP_FRAME_HEIGHT);

	int frameWidthLeftVideo = m_videoStreamLeft.get(CV_CAP_PROP_FRAME_WIDTH);
	int frameHeightLeftVideo = m_videoStreamLeft.get(CV_CAP_PROP_FRAME_HEIGHT);

	m_inputVideoResizingFactor = (float)frameWidthRightVideo/1280.0f;

	cout<<"AppController m_inputVideoResizingFactor: "<<m_inputVideoResizingFactor<<endl;

	if(frameWidthLeftVideo == frameWidthRightVideo && frameHeightLeftVideo == frameHeightRightVideo) {
		m_frameWidth = frameWidthRightVideo;
		m_frameHeight = frameHeightRightVideo;
	}
	else{
		cout<<"AppController: Videos have different dimensions"<<endl;
	}
}

void AppController::calibrationInit(){

	initUndistortRectifyMap(m_cameraMatrix, m_distortionCoefficients, Mat(),
													m_cameraMatrix,
													Size(m_frameWidth, m_frameHeight), CV_16SC2, m_undistortionRemap1, m_undistortionRemap2);

	cout<<"FieldDetectionInit"<<endl;
	m_fieldDetectionRight = new FieldDetection(m_frameHeight, m_frameWidth, m_cameraMatrix, m_distortionCoefficients, true, m_config);
	m_fieldDetectionLeft = new FieldDetection(m_frameHeight, m_frameWidth, m_cameraMatrix, m_distortionCoefficients, false, m_config);

	if(m_config.getCalibrationType() <= 2){
		m_fieldDetectionRight->calibrateFieldLinesWithPresetPoints();
		m_fieldDetectionLeft->calibrateFieldLinesWithPresetPoints();
	}
	else{
		bool success;
		success = m_calibrationVideoStreamRight.open(m_config.getFilePathToCalibrationVideoRight());
		if ( !m_calibrationVideoStreamRight.isOpened() && success) {
			cout << "AppController: Cannot open " << m_config.getFilePathToCalibrationVideoRight() << endl;
		}
		success = m_calibrationVideoStreamLeft.open(m_config.getFilePathToCalibrationVideoLeft());
		if ( !m_calibrationVideoStreamRight.isOpened() && success) {
			cout << "AppController: Cannot open " << m_config.getFilePathToCalibrationVideoLeft() << endl;
		}

		m_fieldDetectionRight->calibrateFieldLinesWithCalibrationVideo(m_calibrationVideoStreamRight);
		m_fieldDetectionLeft->calibrateFieldLinesWithCalibrationVideo(m_calibrationVideoStreamLeft);

		m_calibrationVideoStreamRight.release();
		m_calibrationVideoStreamLeft.release();
	}
}

void AppController::appObjectsInit(){
	cout<<"PlayerDetectionInit"<<endl;
	m_playerDetection = new PlayerDetection(m_fieldDetectionRight, m_fieldDetectionLeft, m_frameWidth, m_frameHeight);

	cout<<"BallDetectionInit"<<endl;
	m_ballDetectionRight = new BallDetection(m_frameHeight, m_frameWidth, m_fieldDetectionRight, m_playerDetection->getPlayerSegmentationRight());
	m_ballDetectionLeft = new BallDetection(m_frameHeight, m_frameWidth, m_fieldDetectionLeft, m_playerDetection->getPlayerSegmentationLeft());

	cout<<"CurveDetectionInit"<<endl;
	m_curveDetectionRight = new CurveDetection(m_frameHeight, m_frameWidth, m_fieldDetectionRight, m_playerDetection->getPlayerSegmentationRight(), m_ballDetectionRight);
	m_curveDetectionLeft = new CurveDetection(m_frameHeight, m_frameWidth, m_fieldDetectionLeft, m_playerDetection->getPlayerSegmentationLeft(), m_ballDetectionLeft);

	cout<<"EventDetectionInit"<<endl;
	m_eventDetection = new EventDetection(m_config, m_fieldDetectionRight, m_fieldDetectionLeft, m_curveDetectionRight, m_curveDetectionLeft, m_playerDetection, m_frameWidth, m_frameHeight, m_videoFramesOffset, m_framesPrediction);

	cout<<"MatchInit"<<endl;
	m_match = new Match();

	cout<<"BackwardsScoringInit"<<endl;
	m_backwardScoring = new BackwardScoring(m_eventDetection, m_match);

	//m_frameGrabber = new FrameGrabber(m_videoStreamRight, m_videoStreamLeft, m_frameBufferRight, m_frameBufferLeft, m_frameBufferGrayRight, m_frameBufferGrayLeft, m_undistortionRemap1, m_undistortionRemap2, m_inputVideoResizingFactor);
	m_frameGrabber = new FrameGrabber(m_videoStreamRight, m_videoStreamLeft, m_undistortionRemap1, m_undistortionRemap2, m_framesPrediction);

	cout<<"VideoCreatorInit"<<endl;
	m_videoCreator = new VideoCreator(m_config.getSafePathOutputVideo(), m_frameWidth, m_frameHeight, m_videoFramesOffset, m_framesPrediction, m_curveDetectionRight, m_curveDetectionLeft, m_statistics, m_match);

}

void AppController::updateDataForVideoCreator(){

	// m_videoCreator->setActualCurveName(m_actualCurveName);
	// m_videoCreator->setActualStrokeName(m_actualStrokeName);
	// m_videoCreator->setActualCurveImgCount(m_actualCurveImgCount);
	// m_videoCreator->setActualStrokeNameImgCount(m_actualStrokeNameImgCount);
	// m_videoCreator->setActualStrokeCurveNumber(m_actualStrokeCurveNumber);
	// m_videoCreator->setActualStrokeIsRight(m_actualStrokeIsRight);
	m_videoCreator->setFrameCount(m_frameCount);
}


AppController::AppController(struct ConfigData &config) : m_config(config){
	appControllerInit();
}

AppController::~AppController(){
	cout<<"Deleting the AppController"<<endl;
	delete m_frameGrabber;
	delete m_fieldDetectionRight;
	delete m_fieldDetectionLeft;
	delete m_playerDetection;
	delete m_ballDetectionRight;
	delete m_ballDetectionLeft;
	delete m_curveDetectionRight;
	delete m_curveDetectionLeft;
	delete m_eventDetection;
	delete m_backwardScoring;
	delete m_videoCreator;

	m_frameGrabber = NULL;
	m_fieldDetectionRight = NULL;
	m_fieldDetectionLeft = NULL;
	m_playerDetection = NULL;
	m_ballDetectionRight = NULL;
	m_ballDetectionLeft = NULL;
	m_curveDetectionRight = NULL;
	m_curveDetectionLeft = NULL;
	m_eventDetection = NULL;
	m_backwardScoring = NULL;
	m_videoCreator = NULL;

}

void AppController::control(){
	//Starting the videoCaptureing with the cameras.
	cout<<"videoStreamsInit"<<endl;
	videoStreamsInit();
	cout<<"calibrationInit"<<endl;
	calibrationInit();
	appObjectsInit();

	while(true) {
		m_tmController.reset(); m_tmController.start();
		int frameBufferSize = m_frameGrabber->getFrameBufferSizeRight();
		cout<<"FrameBufferSize: "<<frameBufferSize<<endl;
		m_tmFrameGrabber.reset();m_tmFrameGrabber.start();
		m_frameGrabber->grabFramesFromVideoStream();
		m_tmFrameGrabber.stop();
		if(m_frameCount == 15){
			m_playerDetection->initBackgroundModel(m_frameGrabber->getFirstFrameBufferGrayRight(), m_frameGrabber->getFirstFrameBufferGrayLeft());
		}
		//skipping the calibration, because the calibration video and the analysing videos containing the same start frames
		if(m_frameCount > m_videoFramesOffset && frameBufferSize >= m_framesPrediction) {

			//Threading
			m_tmPlayerDetection.reset(); m_tmPlayerDetection.start();
			m_playerDetection->detecPlayer(m_frameGrabber->getBufferGrayRight(), m_frameGrabber->getBufferGrayLeft());
			m_tmPlayerDetection.stop();

			m_tmBallDetection.reset(); m_tmBallDetection.start();
			//thread grabFramesRight(&FrameGrabber::grabFramesFromVideoStreamRight, this);
			//thread detecBallRightThread(&BallDetection::detecBallWithDiff, m_ballDetectionRight, std::ref(m_frameGrabber->getBufferRight()), std::ref(m_frameGrabber->getBufferGrayRight()));
			//thread detecBallLeftThread(&BallDetection::detecBallWithDiff, m_ballDetectionLeft, std::ref(m_frameGrabber->getBufferLeft()), std::ref(m_frameGrabber->getBufferGrayLeft()));
			m_ballDetectionRight->detecBallWithDiff(m_frameGrabber->getBufferRight(), m_frameGrabber->getBufferGrayRight());
			m_ballDetectionLeft->detecBallWithDiff(m_frameGrabber->getBufferLeft(), m_frameGrabber->getBufferGrayLeft());
			//detecBallRightThread.join();
			//detecBallLeftThread.join();
			m_tmBallDetection.stop();


			m_tmCurveDetection.reset(); m_tmCurveDetection.start();
			m_curveDetectionRight->detecCurves();
			m_curveDetectionLeft->detecCurves();
			m_tmCurveDetection.stop();

			m_tmVideoCreator.reset(); m_tmVideoCreator.start();

			updateDataForVideoCreator();
			m_videoCreator->writeVideoOutput(m_frameGrabber->getFirstFrameBufferRight(), m_frameGrabber->getFirstFrameBufferLeft());
			// cout<<"m_videoCreator->writeVideoOutput(m_frameGrabber->getFirstFrameBufferRight(), m_frameGrabber->getFirstFrameBufferLeft());"<<endl;
			m_tmVideoCreator.stop();

			m_tmEventDetection.reset(); m_tmEventDetection.start();
			m_eventDetection->detecEvents(m_frameGrabber->getBufferRight(), m_frameGrabber->getBufferLeft());
			m_tmEventDetection.stop();

			// if(m_frameCount == 5000 || m_frameCount == 14800 || m_frameCount == 26800 ){
			// 	m_eventDetection->appendEndOfGameEvent(m_frameCount, -1,false);
			// }

			// if(m_frameCount == 4601 || m_frameCount == 9176 || m_frameCount == 17149 ){
			// 	m_eventDetection->appendEndOfMatchEvent(m_frameCount);
			// }
			//m_backwardScoring->scoringDebug();

		}

		m_tmBufferErase.reset(); m_tmBufferErase.start();
		m_frameGrabber->deleteFramesFromBuffer();

		m_tmBufferErase.stop();
		if(m_frameGrabber->getVideoStreamStatus()) {
			m_eventDetection->appendEndOfMatchEvent(m_frameCount);
			m_eventDetection->printEvents();
			m_backwardScoring->scoring();
			//m_backwardScoring->scoringDebug();
			break;
		}

		m_frameCount++;

		m_tmController.stop();
		if( m_frameGrabber->getFrameBufferSizeRight() >= m_framesPrediction){
			printFPS();
		}
		cout<<"frameCount: "<<m_frameCount<<endl;
	}
	cout<<"CreateJsonFile"<<endl;
	createJsonFile();
	cout<<"SendJsonFile"<<endl;
	//m_communication->sendRequest(m_jsonCreator->getMatchStatisticJsonAsString());
	m_videoCreator->stopVideoCreation();
}

void AppController::controlWithCamera(){
	cout<<"Start camera threads"<<endl;
	thread frameGrabbingThread(&Camera::frameGrabbing, m_cameras);
	thread frameWriterThread1(&Camera::writeVideoLeft, m_cameras, m_config.getFilePathToVideoLeft());
	thread frameWriterThread2(&Camera::writeVideoRight, m_cameras, m_config.getFilePathToVideoRight());
	thread waitingForStopThread(&AppController::waitingForStop, this);
	m_camerasAreRunning = true;
	while(m_cameras->getLowerFrameCountVideo() - 50 < m_frameCount && m_camerasAreRunning == true){
		cout<<"Video is to short: "<<m_cameras->getLowerFrameCountVideo()<<endl;
		this_thread::sleep_for(chrono::milliseconds(500));
	}
	cout<<"videoStreamsInit"<<endl;
	videoStreamsInit();
	cout<<"calibrationInit"<<endl;
	calibrationInit();
	cout<<"objectsInit"<<endl;
	appObjectsInit();
	while(true){
		if(m_cameras->getLowerFrameCountVideo() - 50 > m_frameCount || m_camerasAreRunning == false){
			m_tmController.reset(); m_tmController.start();
			//m_frameGrabber->grabFramesFromVideoStream(m_frameBufferRight, m_frameBufferLeft, m_frameBufferGrayRight, m_frameBufferGrayLeft);
			//std::thread threadGrapImage(&FrameGrabber::grabFramesFromVideoStream, m_frameGrabber, std::ref(m_frameBufferRight), std::ref(m_frameBufferLeft), std::ref(m_frameBufferGrayRight), std::ref(m_frameBufferGrayLeft));
			//std::thread threadGrapImage(&FrameGrabber::grabFramesFromVideoStream, m_frameGrabber);
			m_tmFrameGrabber.reset();m_tmFrameGrabber.start();
			m_frameGrabber->grabFramesFromVideoStream();
			m_tmFrameGrabber.stop();
			//skipping the calibration, because the calibration video and the analysing videos containing the same start frames
			if(m_frameCount > m_videoFramesOffset && m_frameGrabber->getFrameBufferSizeRight() >= m_framesPrediction) {
				//Threading
				m_tmPlayerDetection.reset(); m_tmPlayerDetection.start();
				m_playerDetection->detecPlayer(m_frameGrabber->getBufferGrayRight(), m_frameGrabber->getBufferGrayLeft());
				m_tmPlayerDetection.stop();

				m_tmBallDetection.reset(); m_tmBallDetection.start();
				m_ballDetectionRight->detecBallWithDiff(m_frameGrabber->getBufferRight(), m_frameGrabber->getBufferGrayRight());
				m_ballDetectionLeft->detecBallWithDiff(m_frameGrabber->getBufferLeft(), m_frameGrabber->getBufferGrayLeft());
				m_tmBallDetection.stop();

				m_tmCurveDetection.reset(); m_tmCurveDetection.start();
				m_curveDetectionRight->detecCurves();
				m_curveDetectionLeft->detecCurves();
				m_tmCurveDetection.stop();

				m_tmVideoCreator.reset(); m_tmVideoCreator.start();
				//updateDataForVideoCreator();
				//m_videoCreator->writeVideoOutput(m_frameGrabber->getFirstFrameBufferRight(), m_frameGrabber->getFirstFrameBufferLeft());
				// m_videoCreator->writeVideoOutput(m_frameBufferRight[0], m_frameBufferLeft[0]);
				m_tmVideoCreator.stop();

				m_tmEventDetection.reset(); m_tmEventDetection.start();
				m_eventDetection->detecEvents(m_frameGrabber->getBufferRight(), m_frameGrabber->getBufferLeft());
				m_tmEventDetection.stop();
			}

			m_tmBufferErase.reset(); m_tmBufferErase.start();
			m_frameGrabber->deleteFramesFromBuffer();

			m_tmBufferErase.stop();
			if(m_frameGrabber->getVideoStreamStatus()) {
				m_eventDetection->appendEndOfMatchEvent(m_frameCount);
				m_eventDetection->printEvents();
				m_backwardScoring->scoring();
				//m_backwardScoring->scoringDebug();
				break;
			}
			m_frameCount++;

			m_tmController.stop();

			if(m_frameGrabber->getFrameBufferSizeRight() >= m_framesPrediction){
				printFPS();
			}

			cout<<"frameCount: "<<m_frameCount<<endl;
			//cout<<"frameCountVideoWriting: "<<m_cameras->getLowerFrameCountVideo()<<endl;
		}
		else if(m_camerasAreRunning == true){
			this_thread::sleep_for(chrono::milliseconds(2));
		}
	}
	cout<<"Releasing videoCreatpor"<<endl;
	m_videoCreator->stopVideoCreation();
	frameGrabbingThread.join();
	frameWriterThread1.join();
	frameWriterThread2.join();
	waitingForStopThread.join();

	cout<<"CreateJsonFile"<<endl;
	createJsonFile();
	cout<<"SendJsonFile"<<endl;
	//m_communication->sendRequest(m_jsonCreator->getMatchStatisticJsonAsString());
	cout<<"End of Control"<<endl;
}

void AppController::controlWithCameraAndGUI(){
	cout<<"QRCodeLeft: "<<m_qrCodeLeft<<" QRCodeRight: "<<m_qrCodeRight<<endl;
	cout<<"Start camera threads"<<endl;
	thread frameGrabbingThread(&Camera::frameGrabbing, m_cameras);
	thread frameWriterThread1(&Camera::writeVideoLeft, m_cameras, m_config.getFilePathToVideoLeft());
	thread frameWriterThread2(&Camera::writeVideoRight, m_cameras, m_config.getFilePathToVideoRight());

	m_camerasAreRunning = true;
	while(m_cameras->getLowerFrameCountVideo() - 50 < m_frameCount && m_camerasAreRunning == true){
		cout<<"Video is to short: "<<m_cameras->getLowerFrameCountVideo()<<endl;
		this_thread::sleep_for(chrono::milliseconds(500));
	}
	cout<<"videoStreamsInit"<<endl;
	videoStreamsInit();
	cout<<"calibrationInit"<<endl;
	calibrationInit();
	cout<<"objectsInit"<<endl;
	appObjectsInit();
	while(true){
		if(m_cameras->getLowerFrameCountVideo() - 50 > m_frameCount || m_camerasAreRunning == false){
			m_tmController.reset(); m_tmController.start();
			//m_frameGrabber->grabFramesFromVideoStream(m_frameBufferRight, m_frameBufferLeft, m_frameBufferGrayRight, m_frameBufferGrayLeft);
			//std::thread threadGrapImage(&FrameGrabber::grabFramesFromVideoStream, m_frameGrabber, std::ref(m_frameBufferRight), std::ref(m_frameBufferLeft), std::ref(m_frameBufferGrayRight), std::ref(m_frameBufferGrayLeft));
			//std::thread threadGrapImage(&FrameGrabber::grabFramesFromVideoStream, m_frameGrabber);
			m_tmFrameGrabber.reset();m_tmFrameGrabber.start();
			m_frameGrabber->grabFramesFromVideoStream();
			m_tmFrameGrabber.stop();
			//skipping the calibration, because the calibration video and the analysing videos containing the same start frames
			if(m_frameCount > m_videoFramesOffset && m_frameGrabber->getFrameBufferSizeRight() >= m_framesPrediction) {
				//Threading
				m_tmPlayerDetection.reset(); m_tmPlayerDetection.start();
				m_playerDetection->detecPlayer(m_frameGrabber->getBufferGrayRight(), m_frameGrabber->getBufferGrayLeft());
				m_tmPlayerDetection.stop();

				m_tmBallDetection.reset(); m_tmBallDetection.start();
				m_ballDetectionRight->detecBallWithDiff(m_frameGrabber->getBufferRight(), m_frameGrabber->getBufferGrayRight());
				m_ballDetectionLeft->detecBallWithDiff(m_frameGrabber->getBufferLeft(), m_frameGrabber->getBufferGrayLeft());
				m_tmBallDetection.stop();

				m_tmCurveDetection.reset(); m_tmCurveDetection.start();
				m_curveDetectionRight->detecCurves();
				m_curveDetectionLeft->detecCurves();
				m_tmCurveDetection.stop();

				m_tmVideoCreator.reset(); m_tmVideoCreator.start();
				//updateDataForVideoCreator();
				//m_videoCreator->writeVideoOutput(m_frameGrabber->getFirstFrameBufferRight(), m_frameGrabber->getFirstFrameBufferLeft());
				// m_videoCreator->writeVideoOutput(m_frameBufferRight[0], m_frameBufferLeft[0]);
				m_tmVideoCreator.stop();

				m_tmEventDetection.reset(); m_tmEventDetection.start();
				m_eventDetection->detecEvents(m_frameGrabber->getBufferRight(), m_frameGrabber->getBufferLeft());
				m_tmEventDetection.stop();
				if(m_frameCount == 100 || m_frameCount == 500 || m_frameCount == 900 || m_frameCount == 1200 || m_frameCount == 1600){
					m_eventDetection->appendServeEvent(m_frameCount, 2, 100);
				}
				if(m_frameCount == 300 || m_frameCount == 700 || m_frameCount ==1400 || m_frameCount == 1800){
					m_eventDetection->appendServeEvent(m_frameCount, 1, 100);
				}
			}

			m_tmBufferErase.reset(); m_tmBufferErase.start();
			m_frameGrabber->deleteFramesFromBuffer();

			m_tmBufferErase.stop();
			if(m_frameGrabber->getVideoStreamStatus()) {
				m_eventDetection->appendEndOfMatchEvent(m_frameCount);
				m_eventDetection->printEvents();
				m_backwardScoring->scoring();
				//m_backwardScoring->scoringDebug();
				break;
			}
			m_frameCount++;

			m_tmController.stop();

			if(m_frameGrabber->getFrameBufferSizeRight() >= m_framesPrediction){
				printFPS();
			}

			cout<<"frameCount: "<<m_frameCount<<endl;
			cout<<"frameCountVideoWriting: "<<m_cameras->getLowerFrameCountVideo()<<endl;
		}
		else if(m_camerasAreRunning == true){
			this_thread::sleep_for(chrono::milliseconds(2));
		}
	}
	cout<<"Releasing videoCreator"<<endl;
	m_videoCreator->stopVideoCreation();
	cout<<"Join frameGrabber"<<endl;
	frameGrabbingThread.join();
	cout<<"join writerThread1"<<endl;
	frameWriterThread1.join();
	cout<<"join writerThread2"<<endl;
	frameWriterThread2.join();

	cout<<"CreateJsonFile"<<endl;
	createJsonFile();
	cout<<"SendJsonFile"<<endl;
	//m_communication->sendRequest(m_jsonCreator->getMatchStatisticJsonAsString());
	cout<<"End of Control"<<endl;
}

void AppController::waitingForStop(){
	cout<<"Type q and enter to quit video capturing"<<endl;
	while(cin.get() != 'q');
	m_cameras->setStopRunning(true);

	m_camerasAreRunning = false;
}

void AppController::stopSession(){
	cout<<"SetStopRunning"<<endl;
	m_cameras->setStopRunning(true);
	m_camerasAreRunning = false;
}
void AppController::setChangeOfSidesEvent(){
	cout<<"SetChangeOfSidesEvent"<<endl;
	int actualCameraFrameCount = m_cameras->getLowerFrameCountCamera();
	m_eventDetection->addChangeOfSideEvent(actualCameraFrameCount);
}

void AppController::createJsonFile(){
	cout<<"CreateJsonFile"<<endl;
	m_jsonCreator->writeSessionData("000000LabCourtDTV");
	m_jsonCreator->writeGameData(1, m_qrCodeLeft, m_match->getNumberOfAces(1), m_match->getDoubleFaults(1), m_match->getNumberOfErrors(1), m_match->getNumberOfWinners(1), m_match->getFastesStrokeSpeed(1), m_match->getFastesServeSpeed(1), m_match->getFirstServePercentage(1), m_match->getNumberOfBackhand(1), m_match->getNumberOfForehand(1), m_match->getTotalPointsWon(1));
	m_jsonCreator->writeGameData(2, m_qrCodeRight, m_match->getNumberOfAces(2), m_match->getDoubleFaults(2), m_match->getNumberOfErrors(2), m_match->getNumberOfWinners(2), m_match->getFastesStrokeSpeed(2), m_match->getFastesServeSpeed(2), m_match->getFirstServePercentage(2), m_match->getNumberOfBackhand(2), m_match->getNumberOfForehand(2), m_match->getTotalPointsWon(2));
	m_jsonCreator->writeSetData(1, m_qrCodeLeft, m_match->getGamesWonOfSet(1, 0), m_match->getGamesWonOfSet(1, 1), m_match->getGamesWonOfSet(1, 2));
	m_jsonCreator->writeSetData(2, m_qrCodeRight, m_match->getGamesWonOfSet(2, 0), m_match->getGamesWonOfSet(2, 1), m_match->getGamesWonOfSet(2, 2));
	m_jsonCreator->printJsonFile();
}

void AppController::printFPS(){
	cout<<endl;
	double numberOfTimes = (double)(m_frameCount - m_framesPrediction + 2);
	//cout<<"numberOfTimes: "<<numberOfTimes<<endl;

	m_controlTime += m_tmController.getTimeMilli();
	double controlAvarage = m_controlTime / numberOfTimes;
	std::cout << "Controller : Avg : " << controlAvarage << " ms FPS : " << 1000.0 / controlAvarage << std::endl;

	m_frameGrabberTime += m_tmFrameGrabber.getTimeMilli();
	double grabberAvarage = m_frameGrabberTime / numberOfTimes;
	std::cout << "Grabber : Avg : " << grabberAvarage << " ms FPS : " << 1000.0 / grabberAvarage << std::endl;

	m_playerTime += m_tmPlayerDetection.getTimeMilli();
	double playerAvarage = m_playerTime / numberOfTimes;
	std::cout << "Player : Avg : " << playerAvarage << " ms FPS : " << 1000.0 / playerAvarage << std::endl;

	m_ballTime += m_tmBallDetection.getTimeMilli();
	double ballAvarage = m_ballTime / numberOfTimes;
	std::cout << "Ball : Avg : " << ballAvarage << " ms FPS : " << 1000.0 / ballAvarage << std::endl;

	m_curveTime += m_tmCurveDetection.getTimeMilli();
	double curveAvarage = m_curveTime / numberOfTimes;
	std::cout << "Curve : Avg : " << curveAvarage << " ms FPS : " << 1000.0 / curveAvarage << std::endl;

	m_videoTime += m_tmVideoCreator.getTimeMilli();
	double videoAvarage = m_videoTime / numberOfTimes;
	std::cout << "Video : Avg : " << videoAvarage << " ms FPS : " << 1000.0 / videoAvarage << std::endl;

	m_eventTime += m_tmEventDetection.getTimeMilli();
	double eventAvarage = m_eventTime / numberOfTimes;
	std::cout << "Event : Avg : " << eventAvarage << " ms FPS : " << 1000.0 / eventAvarage << std::endl;

	m_bufferTime += m_tmBufferErase.getTimeMilli();
	double bufferAvarage = m_bufferTime / numberOfTimes;
	std::cout << "Buffer : Avg : " << bufferAvarage << " ms FPS : " << 1000.0 / bufferAvarage << std::endl;
	cout<<endl;
}
