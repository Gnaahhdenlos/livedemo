//
//  playerStatistics.hpp
//
//
//  Created by Henri Kuper on 3.11.17.
//
//

#ifndef playerStatistics_hpp
#define playerStatistics_hpp

#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>

class PlayerStatistics{
private:
	int m_totalPointsWon;

	int m_numberOfAces;
	int m_numberOfDoubleFaults;

	int m_numberOfServes;
	int m_numberOfFirstServes;
	int m_numberOfSecondServes;
	float m_firstServePercentage;

	float m_fastesServeSpeed;
	float m_fastesStrokeSpeed;

	int m_numberOfWinners;
	int m_numberOfErrors;

	int m_numberOfForehand;
	int m_numberOfForehandSlice;
	int m_numberOfForehandSpin;

	int m_numberOfBackhand;
	int m_numberOfBackhandSlice;
	int m_numberOfBackhandSpin;

	void playerStatisticsInit();

public:

	PlayerStatistics();
	~PlayerStatistics();


	// increase statistics functions
	void increaseTotalPointsWon();
	void increaseNumberOfAces();
	void increaseNumberOfDoubleFaults();
	void increaseNumberOfWinners();
	void increaseNumberOfErrors();

	void increaseNumberOfFirstServe();
	void increaseNumberOfSecondServe();
	void addNumberOfForehandSlice(int numberStrokes);
	void addNumberOfForehandSpin(int numberStrokes);
	void addNumberOfBackhandSlice(int numberStrokes);
	void addNumberOfBackhandSpin(int numberStrokes);

	// get functions
	int getTotalPointsWon(){return m_totalPointsWon;}
	int getNumberOfAces(){return m_numberOfAces;}
	int getNumberOfDoubleFaults(){return m_numberOfDoubleFaults;}
	float getFirstServePercentage(){return m_firstServePercentage;}
	float getFastesServeSpeed(){return m_fastesServeSpeed;}
	float getFastesStrokeSpeed(){return m_fastesStrokeSpeed;}
	int getNumberOfWinners(){return m_numberOfWinners;}
	int getNumberOfErrors(){return m_numberOfErrors;}

	int getNumberOfServes(){return m_numberOfServes;}
	int getNumberOfFirstServes(){return m_numberOfFirstServes;}
	int getNumberOfSecondServes(){return m_numberOfSecondServes;}

	int getNumberOfForehand(){return m_numberOfForehand;}
	int getNumberOfForehandSlice(){return m_numberOfForehandSlice;}
	int getNumberOfForehandSpin(){return m_numberOfForehandSpin;}
	int getNumberOfBackhand(){return m_numberOfBackhand;}
	int getNumberOfBackhandSlice(){return m_numberOfBackhandSlice;}
	int getNumberOfBackhandSpin(){return m_numberOfBackhandSpin;}

	void setFastesServeSpeed(float speed){m_fastesServeSpeed = speed;}
	void setFastesStrokeSpeed(float speed){m_fastesStrokeSpeed = speed;}
};

#endif
