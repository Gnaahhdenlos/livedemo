//
//  gamePoint.hpp
//
//
//  Created by Henri Kuper on 03.06.17.
//
//

#ifndef gamePoint_hpp
#define gamePoint_hpp

#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>

class GamePoint{
private:
	int m_wonByPlayer;
	int m_startFrameCount;
	int m_endFrameCount;
	int m_playerWhoServes;
	int m_numberOfServes;


	void gamePointInit();

public:
	GamePoint();
	~GamePoint();

	void addPointWinByPlayer(int playerNumber, int startFrameCount, int endFrameCount);
	void printScoreHistory();

	int getWonByPlayer(){return m_wonByPlayer;}
	int getStartFrameCount(){return m_startFrameCount;}
	int getEndFrameCount(){return m_endFrameCount;}

	void setWonByPlayer(int playerNumber){m_wonByPlayer = playerNumber;}
	void setStartFrameCount(int frameCount){m_startFrameCount = frameCount;}
	void setEndFrameCount(int frameCount){m_endFrameCount = frameCount;}
};

#endif
