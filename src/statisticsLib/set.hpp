//
//  set.hpp
//
//
//  Created by Henri Kuper on 03.06.17.
//
//

#ifndef set_hpp
#define set_hpp

#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>

#include "game.hpp"

class Set{
	private:
	int m_gamesWonByPlayer1;
	int m_gamesWonByPlayer2;
	int m_wonByPlayer;
	int m_startFrameCount;
	int m_endFrameCount;
	std::vector<Game> m_games;
	void setInit();

public:
	Set();
	Set(int startFrameCount);
	~Set();

	bool addPointWinByPlayer(int playerNumber, int startFrameCount, int endFrameCount);

	void printSet();
	void printScoreHistory();
	void printActualScore();

	int getWonByPlayer(){return m_wonByPlayer;}
	int getStartFrameCount(){return m_startFrameCount;}
	int getEndFrameCount(){return m_endFrameCount;}
	int getGamesWonByPlayer1(){return m_gamesWonByPlayer1;}
	int getGamesWonByPlayer2(){return m_gamesWonByPlayer2;}
	int getPointsWonByPlayer1(){return m_games.back().getPointsWonByPlayer1();}
	int getPointsWonByPlayer2(){return m_games.back().getPointsWonByPlayer2();}

	void setWonByPlayer(int playerNumber){m_wonByPlayer = playerNumber;}
	void setStartFrameCount(int frameCount){m_startFrameCount = frameCount;}
	void setEndFrameCount(int frameCount){m_endFrameCount = frameCount;}
};

#endif
