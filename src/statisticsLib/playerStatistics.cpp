//
//  playerStatistics.hpp
//
//
//  Created by Henri Kuper on 3.11.17.
//
//

#include "playerStatistics.hpp"

PlayerStatistics::PlayerStatistics(){
	playerStatisticsInit();
}
PlayerStatistics::~PlayerStatistics(){

}

void PlayerStatistics::playerStatisticsInit(){
	m_totalPointsWon = 0;

	m_numberOfAces = 0;
	m_numberOfDoubleFaults = 0;

	m_numberOfServes = 0;
	m_numberOfFirstServes = 0;
	m_numberOfSecondServes = 0;
	m_firstServePercentage = 1.0f;

	m_fastesServeSpeed = 0.0f;
	m_fastesStrokeSpeed = 0.0f;

	m_numberOfWinners = 0;
	m_numberOfErrors = 0;

	m_numberOfForehand = 0;
	m_numberOfForehandSlice = 0;
	m_numberOfForehandSpin = 0;

	m_numberOfBackhand = 0;
	m_numberOfBackhandSlice = 0;
	m_numberOfBackhandSpin = 0;
}

void PlayerStatistics::increaseTotalPointsWon(){
	m_totalPointsWon++;
}
void PlayerStatistics::increaseNumberOfAces(){
	m_numberOfAces++;
}
void PlayerStatistics::increaseNumberOfDoubleFaults(){
	m_numberOfDoubleFaults++;
}
void PlayerStatistics::increaseNumberOfWinners(){
	m_numberOfWinners++;
}
void PlayerStatistics::increaseNumberOfErrors(){
	m_numberOfErrors++;
}
void PlayerStatistics::increaseNumberOfFirstServe(){
	m_numberOfFirstServes++;
	m_numberOfServes++;
	m_firstServePercentage = (float)(m_numberOfFirstServes - m_numberOfSecondServes) / (float)m_numberOfFirstServes;
}
void PlayerStatistics::increaseNumberOfSecondServe(){
	m_numberOfSecondServes++;
	m_numberOfServes++;
	m_firstServePercentage = (float)(m_numberOfFirstServes - m_numberOfSecondServes) / (float)m_numberOfFirstServes;
}
void PlayerStatistics::addNumberOfForehandSlice(int numberStrokes){
	m_numberOfForehandSlice += numberStrokes;
	m_numberOfForehand+= numberStrokes;
}
void PlayerStatistics::addNumberOfForehandSpin(int numberStrokes){
	m_numberOfForehandSpin += numberStrokes;
	m_numberOfForehand += numberStrokes;
}
void PlayerStatistics::addNumberOfBackhandSlice(int numberStrokes){
	m_numberOfBackhandSlice += numberStrokes;
	m_numberOfBackhand += numberStrokes;
}
void PlayerStatistics::addNumberOfBackhandSpin(int numberStrokes){
	m_numberOfBackhandSpin += numberStrokes;
	m_numberOfBackhand += numberStrokes;
}
