//
//  game.hpp
//
//
//  Created by Henri Kuper on 03.06.17.
//
//

#ifndef game_hpp
#define game_hpp

#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>

#include "gamePoint.hpp"

class Game{
private:
	int m_pointsWonByPlayer1;
	int m_pointsWonByPlayer2;
	int m_wonByPlayer;
	int m_startFrameCount;
	int m_endFrameCount;
	std::vector<GamePoint> m_gamePoints;
	void gameInit();

public:
	Game();
	Game(int startFrameCount);
	~Game();

	bool addPointWinByPlayer(int playerNumber, int startFrameCount, int endFrameCount);
	void printScoreHistory();
	void printActualScore();

	int getWonByPlayer(){return m_wonByPlayer;}
	int getStartFrameCount(){return m_startFrameCount;}
	int getEndFrameCount(){return m_endFrameCount;}
	int getPointsWonByPlayer1(){return m_pointsWonByPlayer1;}
	int getPointsWonByPlayer2(){return m_pointsWonByPlayer2;}

	void setWonByPlayer(int playerNumber){m_wonByPlayer = playerNumber;}
	void setStartFrameCount(int frameCount){m_startFrameCount = frameCount;}
	void setEndFrameCount(int frameCount){m_endFrameCount = frameCount;}
};

#endif
