//
//  match.cpp
//
//
//  Created by Henri Kuper on 03.06.17.
//
//

#include "match.hpp"

using namespace std;

Match::Match(){
	matchInit();
}

Match::~Match(){
}

void Match::matchInit(){
	m_statisticsPlayer1 = new PlayerStatistics();
	m_statisticsPlayer2 = new PlayerStatistics();

	m_setsWonByPlayer1 = 0;
 	m_setsWonByPlayer2 = 0;
	m_wonByPlayer = 0;
	m_startFrameCount = 0;
	m_endFrameCount = 0;
	m_sets.push_back(Set(0));
}

void Match::addPointWinByPlayer(int playerNumber, ScoringNode *scoringNode){
	if(m_sets.back().addPointWinByPlayer(playerNumber, scoringNode->getStartFrameCount(), scoringNode->getStopFrameCount()) == true){
		m_endFrameCount = scoringNode->getStopFrameCount();
		m_sets.back().setWonByPlayer(playerNumber);
		if(playerNumber == 1){
			m_setsWonByPlayer1++;
			m_sets.push_back(Set(scoringNode->getStopFrameCount()));
		}
		else if(playerNumber == 2){
			m_setsWonByPlayer2++;
			m_sets.push_back(Set(scoringNode->getStopFrameCount()));
		}
	}
	updatePlayerStatistics(playerNumber, scoringNode);
}

void Match::updatePlayerStatistics(int playerNumber, ScoringNode *scoringNode){
	if(playerNumber == 1){
		m_statisticsPlayer1->increaseTotalPointsWon();
	}
	else if(playerNumber == 2){
		m_statisticsPlayer2->increaseTotalPointsWon();
	}
	if(scoringNode->getPlayerNumberWhoServes() == 1){
		if(scoringNode->getActualServeTry() > 1){
			m_statisticsPlayer1->increaseNumberOfFirstServe();
			m_statisticsPlayer1->increaseNumberOfSecondServe();
		}
		else if(scoringNode->getActualServeTry() == 1){
			m_statisticsPlayer1->increaseNumberOfFirstServe();
		}
		if(scoringNode->getHasDetectedDoubleFault() == true){
			m_statisticsPlayer1->increaseNumberOfDoubleFaults();
		}
		if(scoringNode->getServeIsAce() == true){
			m_statisticsPlayer1->increaseNumberOfAces();
		}
		for(int i = 0; i < scoringNode->getServeSpeedVec().size(); i++){
			if(scoringNode->getServeSpeed(i) > m_statisticsPlayer1->getFastesServeSpeed()){
				m_statisticsPlayer1->setFastesServeSpeed(scoringNode->getServeSpeed(i));
			}
		}
	}
	else if(scoringNode->getPlayerNumberWhoServes() == 2){
		if(scoringNode->getActualServeTry() > 1){
			m_statisticsPlayer2->increaseNumberOfFirstServe();
			m_statisticsPlayer2->increaseNumberOfSecondServe();
		}
		else if(scoringNode->getActualServeTry() == 1){
			m_statisticsPlayer2->increaseNumberOfFirstServe();
		}
		if(scoringNode->getHasDetectedDoubleFault() == true){
			m_statisticsPlayer2->increaseNumberOfDoubleFaults();
		}
		if(scoringNode->getServeIsAce() == true){
			m_statisticsPlayer2->increaseNumberOfAces();
		}
		for(int i = 0; i < scoringNode->getServeSpeedVec().size(); i++){
			if(scoringNode->getServeSpeed(i) > m_statisticsPlayer2->getFastesServeSpeed()){
				m_statisticsPlayer2->setFastesServeSpeed(scoringNode->getServeSpeed(i));
			}
		}
	}
	for(int i = 0; i < scoringNode->getStrokeSpeedVec(0).size(); i++){
		if(scoringNode->getStrokeSpeed(0, i) > m_statisticsPlayer1->getFastesStrokeSpeed()){
			m_statisticsPlayer1->setFastesStrokeSpeed(scoringNode->getStrokeSpeed(0, i));
		}
	}
	for(int i = 0; i < scoringNode->getStrokeSpeedVec(1).size(); i++){
		if(scoringNode->getStrokeSpeed(1, i) > m_statisticsPlayer2->getFastesStrokeSpeed()){
			m_statisticsPlayer2->setFastesStrokeSpeed(scoringNode->getStrokeSpeed(1, i));
		}
	}

	if(scoringNode->getPlayerNumberCausedAnError() == 1){
		m_statisticsPlayer1->increaseNumberOfErrors();
	}
	else if(scoringNode->getPlayerNumberCausedAnError() == 2){
		m_statisticsPlayer2->increaseNumberOfErrors();
	}

	if(scoringNode->getPlayerNumberCausedAWinner() == 1){
		m_statisticsPlayer1->increaseNumberOfWinners();
	}
	else if(scoringNode->getPlayerNumberCausedAWinner() == 2){
		m_statisticsPlayer2->increaseNumberOfWinners();
	}

	m_statisticsPlayer1->addNumberOfForehandSpin(scoringNode->getNumberOfForehandSpin(0));
	m_statisticsPlayer1->addNumberOfForehandSlice(scoringNode->getNumberOfForehandSlice(0));
	m_statisticsPlayer1->addNumberOfBackhandSpin(scoringNode->getNumberOfBackhandSpin(0));
	m_statisticsPlayer1->addNumberOfBackhandSlice(scoringNode->getNumberOfBackhandSlice(0));

	m_statisticsPlayer2->addNumberOfForehandSpin(scoringNode->getNumberOfForehandSpin(1));
	m_statisticsPlayer2->addNumberOfForehandSlice(scoringNode->getNumberOfForehandSlice(1));
	m_statisticsPlayer2->addNumberOfBackhandSpin(scoringNode->getNumberOfBackhandSpin(1));
	m_statisticsPlayer2->addNumberOfBackhandSlice(scoringNode->getNumberOfBackhandSlice(1));

}

void Match::printActualScore(){
	int numberOfSetsWonByPlayer1 = 0;
	int numberOfSetsWonByPlayer2 = 0;
	for(uint i = 0; i < m_sets.size(); i++){
		if(m_sets[i].getWonByPlayer() == 1){
			numberOfSetsWonByPlayer1++;
		}
		else if(m_sets[i].getWonByPlayer() == 2){
			numberOfSetsWonByPlayer2++;
		}
	}
	cout<<"Sets: "<<numberOfSetsWonByPlayer1<<" : "<<numberOfSetsWonByPlayer2;
	m_sets.back().printActualScore();
}

void Match::printScoreHistory(){
	for(uint i = 0; i < m_sets.size(); i++){
		m_sets[i].printScoreHistory();
	}
}

void Match::reset(){
	m_sets.clear();
	delete m_statisticsPlayer1;
	delete m_statisticsPlayer2;
	m_statisticsPlayer1 = NULL;
	m_statisticsPlayer2 = NULL;
	matchInit();
}

//---------------long get Methods---------------------

int Match::getTotalPointsWon(int playerNumber){
	if(playerNumber == 1){
		return m_statisticsPlayer1->getTotalPointsWon();
	}
	else if(playerNumber == 2){
		return m_statisticsPlayer2->getTotalPointsWon();
	}
	else {
		return -1;
	}
}

int Match::getNumberOfFirstServes(int playerNumber){
	if(playerNumber == 1){
		return m_statisticsPlayer1->getNumberOfFirstServes();
	}
	else if(playerNumber == 2){
		return m_statisticsPlayer2->getNumberOfFirstServes();
	}
	else {
		return -1;
	}
}

int Match::getNumberOfSecondServes(int playerNumber){
	if(playerNumber == 1){
		return m_statisticsPlayer1->getNumberOfSecondServes();
	}
	else if(playerNumber == 2){
		return m_statisticsPlayer2->getNumberOfSecondServes();
	}
	else {
		return -1;
	}
}

float Match::getFirstServePercentage(int playerNumber){
	if(playerNumber == 1){
		return m_statisticsPlayer1->getFirstServePercentage();
	}
	else if(playerNumber == 2){
		return m_statisticsPlayer2->getFirstServePercentage();
	}
	else {
		return -1;
	}
}

int Match::getDoubleFaults(int playerNumber){
	if(playerNumber == 1){
		return m_statisticsPlayer1->getNumberOfDoubleFaults();
	}
	else if(playerNumber == 2){
		return m_statisticsPlayer2->getNumberOfDoubleFaults();
	}
	else {
		return -1;
	}
}

int Match::getNumberOfAces(int playerNumber){
	if(playerNumber == 1){
		return m_statisticsPlayer1->getNumberOfAces();
	}
	else if(playerNumber == 2){
		return m_statisticsPlayer2->getNumberOfAces();
	}
	else {
		return -1;
	}
}

float Match::getFastesServeSpeed(int playerNumber){
	if(playerNumber == 1){
		return m_statisticsPlayer1->getFastesServeSpeed();
	}
	else if(playerNumber == 2){
		return m_statisticsPlayer2->getFastesServeSpeed();
	}
	else {
		return -1;
	}
}

float Match::getFastesStrokeSpeed(int playerNumber){
	if(playerNumber == 1){
		return m_statisticsPlayer1->getFastesStrokeSpeed();
	}
	else if(playerNumber == 2){
		return m_statisticsPlayer2->getFastesStrokeSpeed();
	}
	else {
		return -1;
	}
}

int Match::getNumberOfForehand(int playerNumber){
	if(playerNumber == 1){
		return m_statisticsPlayer1->getNumberOfForehand();
	}
	else if(playerNumber == 2){
		return m_statisticsPlayer2->getNumberOfForehand();
	}
	else {
		return -1;
	}
}

int Match::getNumberOfForehandSpin(int playerNumber){
	if(playerNumber == 1){
		return m_statisticsPlayer1->getNumberOfForehandSpin();
	}
	else if(playerNumber == 2){
		return m_statisticsPlayer2->getNumberOfForehandSpin();
	}
	else {
		return -1;
	}
}

int Match::getNumberOfForehandSlice(int playerNumber){
	if(playerNumber == 1){
		return m_statisticsPlayer1->getNumberOfForehandSlice();
	}
	else if(playerNumber == 2){
		return m_statisticsPlayer2->getNumberOfForehandSlice();
	}
	else {
		return -1;
	}
}

int Match::getNumberOfBackhand(int playerNumber){
	if(playerNumber == 1){
		return m_statisticsPlayer1->getNumberOfBackhand();
	}
	else if(playerNumber == 2){
		return m_statisticsPlayer2->getNumberOfBackhand();
	}
	else {
		return -1;
	}
}

int Match::getNumberOfBackhandSpin(int playerNumber){
	if(playerNumber == 1){
		return m_statisticsPlayer1->getNumberOfBackhandSpin();
	}
	else if(playerNumber == 2){
		return m_statisticsPlayer2->getNumberOfBackhandSpin();
	}
	else {
		return -1;
	}
}

int Match::getNumberOfBackhandSlice(int playerNumber){
	if(playerNumber == 1){
		return m_statisticsPlayer1->getNumberOfBackhandSlice();
	}
	else if(playerNumber == 2){
		return m_statisticsPlayer2->getNumberOfBackhandSlice();
	}
	else {
		return -1;
	}
}

int Match::getNumberOfErrors(int playerNumber){
	if(playerNumber == 1){
		return m_statisticsPlayer1->getNumberOfErrors();
	}
	else if(playerNumber == 2){
		return m_statisticsPlayer2->getNumberOfErrors();
	}
	else {
		return -1;
	}
}

int Match::getNumberOfWinners(int playerNumber){
	if(playerNumber == 1){
		return m_statisticsPlayer1->getNumberOfWinners();
	}
	else if(playerNumber == 2){
		return m_statisticsPlayer2->getNumberOfWinners();
	}
	else {
		return -1;
	}
}

int Match::getGamesWonOfSet(int playerNumber, int setNumber){
	if(setNumber < m_sets.size()){
		if(playerNumber == 1){
			return m_sets[setNumber].getGamesWonByPlayer1();
		}
		else if(playerNumber == 2){
			return m_sets[setNumber].getGamesWonByPlayer2();
		}
		else{
			return -1;
		}
	}
	else{
		return 0;
	}
}

vector<int> Match::getGamesWonOfPlayer(int playerNumber){
	vector<int> setsOfPlayer;
	if(playerNumber == 1){
		for(uint i = 0; i < m_sets.size(); i++){
			setsOfPlayer.push_back(m_sets[i].getGamesWonByPlayer1());
		}
	}
	else if(playerNumber == 2){
		for(uint i = 0; i < m_sets.size(); i++){
			setsOfPlayer.push_back(m_sets[i].getGamesWonByPlayer1());
		}
	}
	else{
		cout<<"getGamesWonOfPlayer of non existing Player"<<endl;
	}
	return setsOfPlayer;
}
