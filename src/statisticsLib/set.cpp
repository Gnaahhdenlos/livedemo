//
//  set.cpp
//
//
//  Created by Henri Kuper on 03.06.17.
//
//

#include "set.hpp"

using namespace std;

Set::Set(){
	setInit();
}
Set::Set(int startFrameCount) : m_startFrameCount(startFrameCount){
	setInit();
}

Set::~Set(){
}

void Set::setInit(){
	m_wonByPlayer = 0;
	m_gamesWonByPlayer1 = 0;
	m_gamesWonByPlayer2 = 0;
	m_endFrameCount = 0;
	m_games.push_back(Game(m_startFrameCount));
}

bool Set::addPointWinByPlayer(int playerNumber, int startFrameCount, int endFrameCount){

	if(m_games.back().addPointWinByPlayer(playerNumber, startFrameCount, endFrameCount) == true){
		m_endFrameCount = endFrameCount;
		m_games.back().setWonByPlayer(playerNumber);
		if(m_gamesWonByPlayer1 < 5 && m_gamesWonByPlayer2 < 5){
			if(playerNumber == 1){
				m_gamesWonByPlayer1++;
			}
			else if(playerNumber == 2){
				m_gamesWonByPlayer2++;
			}
			m_games.push_back(Game(endFrameCount));
			return false;
		}
		else if(m_gamesWonByPlayer1 == 5){
			if(playerNumber == 1){
				m_gamesWonByPlayer1++;
				return true;
			}
			else{
				m_gamesWonByPlayer2++;
				m_games.push_back(Game(endFrameCount));
				return false;
			}
		}
		else if(m_gamesWonByPlayer2 == 5){
			if(playerNumber == 2){
				m_gamesWonByPlayer2++;
				return true;
			}
			else{
				m_gamesWonByPlayer1++;
				m_games.push_back(Game(endFrameCount));
				return false;
			}
		}
	}
	return false;
}

void Set::printScoreHistory(){
	int gamesPlayer1 = 0;
	int gamesPlayer2 = 0;
	for(uint i = 0; i < m_games.size(); i++){
		m_games[i].printScoreHistory();
		cout<<"Games: "<<gamesPlayer1<<" : "<<gamesPlayer2<<endl;
		if(m_games[i].getWonByPlayer() == 1){
			gamesPlayer1++;
		}
		else if(m_games[i].getWonByPlayer() == 2){
			gamesPlayer2++;
		}
	}
}

void Set::printSet(){

}

void Set::printActualScore(){
	cout<<" Games: "<<m_gamesWonByPlayer1<<" : "<<m_gamesWonByPlayer2;
	m_games.back().printActualScore();
}
