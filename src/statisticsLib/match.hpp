
//
//  match.hpp
//
//
//  Created by Henri Kuper on 03.06.17.
//
//

#ifndef match_hpp
#define match_hpp

#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>

#include "playerStatistics.hpp"
#include "../trackLib/scoringNode.hpp"
#include "set.hpp"

class Match{
private:
	PlayerStatistics *m_statisticsPlayer1;
	PlayerStatistics *m_statisticsPlayer2;

	int m_setsWonByPlayer1;
	int m_setsWonByPlayer2;
	int m_wonByPlayer;
	int m_startFrameCount;
	int m_endFrameCount;
	std::vector<Set> m_sets;
	void matchInit();

public:
	Match();
	~Match();

	//void addPointWinByPlayer(int playerNumber, int startFrameCount, int endFrameCount, int playerNumberWhosServes, int serveTries, bool hasDetectedDoubleFault, bool isAce, const std::vector<float> &serveSpeedVec);
	void addPointWinByPlayer(int playerNumber, ScoringNode *scoringNode);
	void updatePlayerStatistics(int playerNumber, ScoringNode *scoringNode);
	void printActualScore();
	void printScoreHistory();

	int getWonByPlayer(){return m_wonByPlayer;}
	int getStartFrameCount(){return m_startFrameCount;}
	int getEndFrameCount(){return m_endFrameCount;}
	bool getIsLastSetCompleted(){return (m_sets.back().getGamesWonByPlayer1() == 0 && m_sets.back().getGamesWonByPlayer2() == 0 && m_sets.back().getPointsWonByPlayer1() == 0 && m_sets.back().getPointsWonByPlayer2() == 0);}

	int getTotalPointsWon(int playerNumber);
	int getNumberOfFirstServes(int playerNumber);
	int getNumberOfSecondServes(int playerNumber);
	float getFirstServePercentage(int playerNumber);
	int getDoubleFaults(int playerNumber);
	int getNumberOfAces(int playerNumber);
	float getFastesServeSpeed(int playerNumber);
	float getFastesStrokeSpeed(int playerNumber);

	int getNumberOfForehand(int playerNumber);
	int getNumberOfForehandSpin(int playerNumber);
	int getNumberOfForehandSlice(int playerNumber);
	int getNumberOfBackhand(int playerNumber);
	int getNumberOfBackhandSpin(int playerNumber);
	int getNumberOfBackhandSlice(int playerNumber);
	int getNumberOfErrors(int playerNumber);
	int getNumberOfWinners(int playerNumber);

	int getGamesWonOfSet(int playerNumber, int setNumber);
	std::vector<int> getGamesWonOfPlayer(int playerNumber);

	void setWonByPlayer(int playerNumber){m_wonByPlayer = playerNumber;}
	void setStartFrameCount(int frameCount){m_startFrameCount = frameCount;}
	void setEndFrameCount(int frameCount){m_endFrameCount = frameCount;}

	void reset();
};

#endif /* match_h */
