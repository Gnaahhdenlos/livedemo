//
//  game.cpp
//
//
//  Created by Henri Kuper on 03.06.17.
//
//

#include "game.hpp"

using namespace std;

Game::Game(){
	gameInit();
}

Game::Game(int startFrameCount) : m_startFrameCount(startFrameCount){
	gameInit();
}

Game::~Game(){

}

void Game::gameInit(){
	m_wonByPlayer = 0;
	m_pointsWonByPlayer1 = 0;
	m_pointsWonByPlayer2 = 0;
	m_endFrameCount = 0;
	m_gamePoints.push_back(GamePoint());
}

bool Game::addPointWinByPlayer(int playerNumber, int startFrameCount, int endFrameCount){

	m_endFrameCount = endFrameCount;
	m_gamePoints.back().setWonByPlayer(playerNumber);
	m_gamePoints.back().setStartFrameCount(startFrameCount);
	m_gamePoints.back().setEndFrameCount(endFrameCount);

	if(m_pointsWonByPlayer1 < 3 && m_pointsWonByPlayer2 < 3){
		if(playerNumber == 1){
			m_pointsWonByPlayer1++;
		}
		else if(playerNumber == 2){
			m_pointsWonByPlayer2++;
		}
		m_gamePoints.push_back(GamePoint());
		return false;
	}
	else if(m_pointsWonByPlayer1 < 3 && m_pointsWonByPlayer2 == 3){
		if(playerNumber == 1){
			m_pointsWonByPlayer1++;
			m_gamePoints.push_back(GamePoint());
			return false;
		}
		else if(playerNumber == 2){
			m_pointsWonByPlayer2++;
			return true;
		}
	}
	else if(m_pointsWonByPlayer1 == 3 && m_pointsWonByPlayer2 < 3){
		if(playerNumber == 1){
			m_pointsWonByPlayer1++;
			return true;
		}
		else if(playerNumber == 2){
			m_pointsWonByPlayer2++;
			m_gamePoints.push_back(GamePoint());
			return false;
		}
	}
	else if(m_pointsWonByPlayer1 == 3 && m_pointsWonByPlayer2 == 3){
		if(playerNumber == 1){
			m_pointsWonByPlayer1++;
		}
		else if(playerNumber == 2){
			m_pointsWonByPlayer2++;
		}
		m_gamePoints.push_back(GamePoint());
		return false;
	}
	else if(m_pointsWonByPlayer1 == 4 && m_pointsWonByPlayer2 == 3){
		if(playerNumber == 1){
			return true;
		}
		else if(playerNumber == 2){
			m_pointsWonByPlayer1--;
			m_gamePoints.push_back(GamePoint());
			return false;
		}
	}
	else if(m_pointsWonByPlayer1 == 3 && m_pointsWonByPlayer2 == 4){
		if(playerNumber == 1){
			m_pointsWonByPlayer2--;
			m_gamePoints.push_back(GamePoint());
			return false;
		}
		else if(playerNumber == 2){
			return true;
		}
	}
	return false;
}

void Game::printScoreHistory(){
	int pointsPlayer1 = 0;
	int pointsPlayer2 = 0;
	for(uint i = 0; i < m_gamePoints.size(); i++){
		cout<<"Points: "<<pointsPlayer1<<" : "<<pointsPlayer2<<endl;
		if(m_gamePoints[i].getWonByPlayer() == 1){
			pointsPlayer1++;
		}
		else if(m_gamePoints[i].getWonByPlayer() == 2){
			pointsPlayer2++;
		}
	}
}

void Game::printActualScore(){
	cout<<" Points: "<<m_pointsWonByPlayer1<<" : "<<m_pointsWonByPlayer2<<endl;
}
