//
//  communication.hpp
//
//
//  Created by Henri Kuper on 10.11.17.
//
//

#include <cstdlib>
#include <cerrno>
#include <iostream>
#include <sstream>
#include <string>
#include <string.h>
#include <stdio.h>
#include <locale>
#include <ctime>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Exception.hpp>

// #include <cryptopp/cryptlib.h>
// #include <cryptopp/hmac.h>
// #include <cryptopp/sha.h>
// #include <cryptopp/base64.h>
// #include <cryptopp/hex.h>
// #include <cryptopp/filters.h>


class Communication{
private:
	std::string m_url;
	std::string m_cacheControl;
	std::string m_contentType;
	std::string m_apiKey;

	void communicationInit();
public:
	Communication();
	~Communication();

	void sendRequest(std::string post);
	// std::string hexEncoding(std::string str);
	// std::string createHMAC(std::string key, std::string plain);
	// std::string createSHA256(std::string value);

};
