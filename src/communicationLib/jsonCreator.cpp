//
//  jsonCreator.cpp
//
//
//  Created by Henri Kuper on 11.11.17.
//
//

#include "jsonCreator.hpp"

using json = nlohmann::json;
using namespace std;

JsonCreator::JsonCreator(){
	jsonCreatorInit();
}
JsonCreator::~JsonCreator(){

}

void JsonCreator::jsonCreatorInit(){
	writeMatchFileHeader();
	cout<<"matchStatisitc"<<endl;
	cout<<m_matchStatistic<<endl;
	cout<<"matchStatisitc string"<<endl;
}

void JsonCreator::writeMatchFileHeader(){

}

void JsonCreator::writeSessionData(string courtID){
	cout<<"writeSessionData"<<endl;
	time_t now = time(NULL);
	tm *gmtm = gmtime(&now);
	string date;
	date.clear();
	date.append(to_string(1900 + gmtm->tm_year));
	date.append(to_string(1 + gmtm->tm_mon));
	date.append(to_string(gmtm->tm_mday));
	date.append(to_string(1 + gmtm->tm_hour));
	date.append(to_string(1 + gmtm->tm_min));

	m_matchStatistic["court_id"] = courtID;
	m_matchStatistic["date"] = date;
}

void JsonCreator::writeGameData(int playerNumber, string playerID, int aces, int doubleFaults, int errors, int winners, int fastestGroundstroke, int fastestServe, float firstServePercentage, int numberOfBackhands, int numberOfForehands, int totalPoints){
	cout<<"writeGameData"<<endl;
	if(playerNumber == 1){
		//Player1
		m_matchStatistic["playerData"][playerID]["aces"] = aces;
		m_matchStatistic["playerData"][playerID]["doubleFaults"] = doubleFaults;
		m_matchStatistic["playerData"][playerID]["errors"] = errors;
		m_matchStatistic["playerData"][playerID]["winners"] = winners;
		m_matchStatistic["playerData"][playerID]["fastestGroundstroke"] = fastestGroundstroke;
		m_matchStatistic["playerData"][playerID]["fastestServe"] = fastestServe;
		m_matchStatistic["playerData"][playerID]["firstServePercentage"] = (int)(firstServePercentage * 100.0f);
		m_matchStatistic["playerData"][playerID]["numberOfBackhands"] = numberOfBackhands;
		m_matchStatistic["playerData"][playerID]["numberOfForehands"] = numberOfForehands;
		m_matchStatistic["playerData"][playerID]["totalPoints"] = totalPoints;
	}
	else if(playerNumber == 2){
		//Player2
		m_matchStatistic["playerData"][playerID]["aces"] = aces;
		m_matchStatistic["playerData"][playerID]["doubleFaults"] = doubleFaults;
		m_matchStatistic["playerData"][playerID]["errors"] = errors;
		m_matchStatistic["playerData"][playerID]["winners"] = winners;
		m_matchStatistic["playerData"][playerID]["fastestGroundstroke"] = fastestGroundstroke;
		m_matchStatistic["playerData"][playerID]["fastestServe"] = fastestServe;
		m_matchStatistic["playerData"][playerID]["firstServePercentage"] = (int)(firstServePercentage * 100.0f);
		m_matchStatistic["playerData"][playerID]["numberOfBackhands"] = numberOfBackhands;
		m_matchStatistic["playerData"][playerID]["numberOfForehands"] = numberOfForehands;
		m_matchStatistic["playerData"][playerID]["totalPoints"] = totalPoints;
	}
	else{
		cout<<"JsonCreator: write Game Data to non existing player."<<endl;
	}
}

void JsonCreator::writeSetData(int playerNumber, string playerID, int gamesWonSet1, int gamesWonSet2, int gamesWonSet3){
	cout<<"writeSetData"<<endl;
	if(playerNumber == 1){
		json player1Set1, player1Set2, player1Set3;
		player1Set1["position"] = 1;
		player1Set1["score"] = gamesWonSet1;
		player1Set2["position"] = 2;
		player1Set2["score"] = gamesWonSet2;
		player1Set3["position"] = 3;
		player1Set3["score"] = gamesWonSet3;

		m_matchStatistic["playerData"][playerID]["sets"] = {player1Set1, player1Set2, player1Set3};
	}
	else if(playerNumber == 2){
		json player2Set1, player2Set2, player2Set3;
		player2Set1["position"] = 1;
		player2Set1["score"] = gamesWonSet1;
		player2Set2["position"] = 2;
		player2Set2["score"] = gamesWonSet2;
		player2Set3["position"] = 3;
		player2Set3["score"] = gamesWonSet3;

		m_matchStatistic["playerData"][playerID]["sets"] = {player2Set1, player2Set2, player2Set3};
	}
}

void JsonCreator::convertJsonToString(json jsonFile, std::string &jsonString){

}

void JsonCreator::printJsonFile(){
	cout<<"matchStatisitc string"<<endl;
	std::cout << m_matchStatistic.dump(4) << std::endl;
}

void JsonCreator::testing(){

	// json file;
	// file["pi"] = 3.141;
	// file["happy"] = true;
	// file["name"] = "Nils";
	// file["pi"] = 3.142;
	// cout<<file<<endl;
	// std::cout << file.dump(4) << std::endl;
	// json j_boolean = true;
	// json j_number_integer = 17;
  // json j_number_float = 23.42;
  // json j_object = {{"one", 1}, {"two", 2}};
  // json j_object_empty(json::value_t::object);
  // json j_array = {1, 2, 4, 8, 16};
  // json j_array_empty(json::value_t::array);
  // json j_string = "Hello, world";
	//
	// std::cout << j_boolean.is_string() << '\n';
	// std::cout << j_number_integer << '\n';
	// std::cout << j_number_float << '\n';
	// std::cout << j_object << '\n';
	// std::cout << j_array << '\n';
	// std::cout << j_string.is_string() << '\n';
}

/*
m_matchStatistic =
{
{"swagger", "2.0"},
{"info", {
	{"description", "Dataformat to transfer session data."},
	{"version", "1.0.0"},
	{"title", "Wingfield"}
}},
{"paths", {}},
{"definitions", {
	{"Session", {
		{"type", "object"},
		{"properties", {
			{"id", {
				{"type", "string"}
			}},
			{"court_id", {
				{"type", "string"}
			}},
			{"date", {
				{"type", "string"},
				{"format", "date-time"}
			}},
			{"playerData", {
				{"type", "object"}
			}}
		}},
		{"required", {
			"id",
			"court_id",
			"date",
			"playerData"
		}}
	}},
	{"Game Data", {
		{"type", "object"},
		{"properties", {
			{"sets", {
				{"type", "array"},
				{"items", {
					{"$ref", "#/definitions/Set"}
				}}
			}},
			{"totalPoints", {
				{"type", "integer"}
			}},
			{"aces", {
				{"type", "integer"}
			}},
			{"doubleFaults", {
				{"type", "integer"}
			}},
			{"firstServePercentage", {
				{"type", "integer"}
			}},
			{"numberOfForehands", {
				{"type", "integer"}
			}},
			{"numberOfBackhands", {
				{"type", "integer"}
			}},
			{"fastestServe", {
				{"type", "integer"}
			}},
			{"fastestGroundstroke", {
				{"type", "integer"}
			}},
			{"winners", {
				{"type", "integer"}
			}},
			{"errors", {
				{"type", "integer"}
			}}
		}}
	}},
	{"Set", {
		{"type", "object"},
		{"properties", {
			{"position", {
				{"type", "integer"}
			}},
			{"score", {
				{"type", "integer"}
			}}
		}},
		{"required", {
			"position",
			"score"
		}}
	}}
}}
};
*/

/*
{
{"swagger", "2.0"},
{"info", {
	{"description", "Dataformat to transfer session data."},
	{"version", "1.0.0"},
	{"title", "Wingfield"}
}},
{"paths", {}},
{"definitions", {
	{"Session", {
		{"type", "object"},
		{"properties", {
			{"id", {
				{"type", "string"}
			}},
			{"court_id", {
				{"type", "string"}
			}},
			{"date", {
				{"type", "string"},
				{"format", "date-time"}
			}},
			{"playerData", {
				{"type", "object"}
			}}
		}},
		{"required", {
			"id",
			"court_id",
			"date",
			"playerData"
		}}
	}},
	{"Game Data", {
		{"type", "object"},
		{"properties", {
			{"sets", {
				{"type", "array"},
				{"items", {
					{"$ref", "#/definitions/Set"}
				}}
			}},
			{"totalPoints", {
				{"type", "integer"}
			}},
			{"aces", {
				{"type", "integer"}
			}},
			{"doubleFaults", {
				{"type", "integer"}
			}},
			{"firstServePercentage", {
				{"type", "integer"}
			}},
			{"numberOfForehands", {
				{"type", "integer"}
			}},
			{"numberOfBackhands", {
				{"type", "integer"}
			}},
			{"fastestServe", {
				{"type", "integer"}
			}},
			{"fastestGroundstroke", {
				{"type", "integer"}
			}},
			{"winners", {
				{"type", "integer"}
			}},
			{"errors", {
				{"type", "integer"}
			}}
		}}
	}},
	{"Set", {
		{"type", "object"},
		{"properties", {
			{"position", {
				{"type", "integer"}
			}},
			{"score", {
				{"type", "integer"}
			}}
		}},
		{"required", {
			"position",
			"score"
		}}
	}}
}}
};
*/
