//
//  communication.cpp
//
//
//  Created by Henri Kuper on 10.11.17.
//
//

#include "communication.hpp"
using namespace std;


Communication::Communication(){
	communicationInit();
}

Communication::~Communication(){

}

void Communication::communicationInit(){
	cout<<"communicationInit"<<endl;
	m_url = "https://bvs3qq9sr7.execute-api.eu-central-1.amazonaws.com/V1/sessions";

	m_cacheControl = "no-cache";
	m_contentType = "application/json";
	m_apiKey = "WXbhr2cETuaK8NG0ZpJ4l186nonpH9Sey7SvNOUa";
}

void Communication::sendRequest(string post){
	cout<<"Post: "<<post<<endl;
	try {
    curlpp::Cleanup cleaner;
    curlpp::Easy request;

    request.setOpt(new curlpp::options::Url(m_url));
    request.setOpt(new curlpp::options::Verbose(true));

    std::list<std::string> httpHeader;

		httpHeader.push_back("Cache-Control: " + m_cacheControl);
		httpHeader.push_back("Content-Type: " + m_contentType);
		httpHeader.push_back("x-api-key: " + m_apiKey);

    request.setOpt(new curlpp::options::HttpHeader(httpHeader));
    request.setOpt(new curlpp::options::PostFields(post));
    request.setOpt(new curlpp::options::PostFieldSize(post.size()));

    request.perform();
  }
  catch ( curlpp::LogicError & e ) {
    std::cout << e.what() << std::endl;
  }
  catch ( curlpp::RuntimeError & e ) {
    std::cout << e.what() << std::endl;
  }
}

// string Communication::createHMAC(string key, string plain){
// 	string mac, encoded;
// 	try
// 	{
// 		CryptoPP::HMAC< CryptoPP::SHA256 > hmac((unsigned char*)key.c_str(), key.size());
//
// 		CryptoPP::StringSource(plain, true, new CryptoPP::HashFilter(hmac, new CryptoPP::StringSink(mac)));//Hashfilter // StringSource
// 	}
// 	catch(const CryptoPP::Exception& e)
// 	{
// 		cerr << e.what() << endl;
// 	}
//
// 	return mac;
// }
//
// string Communication::createSHA256(string value){
// 	string mac;
// 	CryptoPP::SHA256 sha256;
// 	CryptoPP::StringSource(value, true, new CryptoPP::HashFilter(sha256, new CryptoPP::StringSink(mac)));
// 	return mac;
// }
//
// std::string Communication::hexEncoding(std::string str){
// 	string encoded, lowerEncoded;
// 	encoded.clear();
// 	lowerEncoded.clear();
// 	CryptoPP::StringSource(str, true, new CryptoPP::HexEncoder(new CryptoPP::StringSink(encoded))); // HexEncoder// StringSource
//
// 	locale loc;
// 	for(int i = 0; i < encoded.size(); i++){
// 		lowerEncoded.push_back(tolower(encoded[i], loc));
// 	}
// 	return lowerEncoded;
// }

// const char *data = post.c_str();
// CURL *hnd = curl_easy_init();
//
// curl_easy_setopt(hnd, CURLOPT_CUSTOMREQUEST, "POST");
// curl_easy_setopt(hnd, CURLOPT_URL, "https://bvs3qq9sr7.execute-api.eu-central-1.amazonaws.com/V1/sessions");
//
// struct curl_slist *headers = NULL;
// //headers = curl_slist_append(headers, "postman-token: 07122bf3-1cdb-b69f-15f0-9d2e4a69fc7e");
// headers = curl_slist_append(headers, "cache-control: no-cache");
// headers = curl_slist_append(headers, "content-type: application/json");
// headers = curl_slist_append(headers, "x-api-key: WXbhr2cETuaK8NG0ZpJ4l186nonpH9Sey7SvNOUa");
// curl_easy_setopt(hnd, CURLOPT_HTTPHEADER, headers);
// curl_easy_setopt(hnd, CURLOPT_VERBOSE, true);
//
// curl_easy_setopt(hnd, CURLOPT_POSTFIELDS, data);
//
// CURLcode ret = curl_easy_perform(hnd);
// int errorcode = ret;
// cout<<"CURLcode: "<<ret<<endl;
