//
//  jsonCreator.hpp
//
//
//  Created by Henri Kuper on 11.11.17.
//
//

#include <iostream>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

class JsonCreator{
private:
	json m_matchStatistic;

	void jsonCreatorInit();
public:
	JsonCreator();
	~JsonCreator();


	void convertJsonToString(json jsonFile, std::string &jsonString);

	std::string getMatchStatisticJsonAsString(){return m_matchStatistic.dump();}

	void writeMatchFileHeader();

	void writeSessionData(std::string courtID);
	void writeGameData(int playerNumber, std::string playerID, int aces, int doubleFaults, int errors, int winners, int fastestGroundstroke, int fastesServe, float firstServePercentage, int numberOfBackhands, int numberOfForehands, int totalPoints);
	void writeSetData(int playerNumber, std::string playerID, int gamesWonSet1, int gamesWonSet2, int gamesWonSet3);

	void printJsonFile();

	void testing();
};
