#include "convolutionLayer.hpp"

#include <random>
#include <iomanip>
#include <fstream>
#include <string>



#define BNN_NORMAL_DISTRIBUTION_MEAN    0.05f
#define BNN_NORMAL_DISTRIBUTION_STDDEV  -0.05f

int32_t convolutionLayer::convInit() {
	if ((m_fSize > 0) && (m_stride > 0) && (m_oDepth > 0)) {

		//sets random filter weights
		m_nKUnit = m_fSize*m_fSize;
		m_nFUnit = m_nKUnit*m_iDepth;
		m_nIPixel = m_iPixelSize*m_iPixelSize;
		m_nWeights = m_fSize*m_fSize*m_oDepth*m_iDepth;

		m_fWeightsDelta = new float[m_fSize*m_fSize*m_oDepth*m_iDepth];
		m_fWeights = new float[m_fSize*m_fSize*m_oDepth*m_iDepth];
		m_flippedFilterWeights = new float[m_fSize*m_fSize*m_oDepth*m_iDepth];
		m_errorLowerLayer = new float[m_iDepth*m_iPixelSize*m_iPixelSize];
		//std::cout<< "errors: "<< (m_iDepth*m_iPixelSize*m_iPixelSize)<<std::endl;
		//std::cout<< "fWeights"<< (m_fSize*m_fSize*m_oDepth*m_iDepth)<<std::endl;
		m_bias = new float[m_oDepth];
		m_biasDelta = new float[m_oDepth];
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_real_distribution<> d(BNN_NORMAL_DISTRIBUTION_MEAN, BNN_NORMAL_DISTRIBUTION_STDDEV);

		for(uint32_t i = 0; i < m_fSize*m_fSize*m_oDepth*m_iDepth; ++i) {
			m_fWeights[i] = d(gen);
		}
		for(uint32_t i = 0; i < m_oDepth; i++) {
			m_bias[i] = d(gen);
		}

		setZeroPadding();
		//calculates the hight(equals width) of the output Volume (Pixel-FilterGröße+2*ZeroPadding)/Schrittweite+1
		m_oSize =(m_iPixelSize-m_fSize+2*m_zeroPadding)/m_stride+1;
		m_nOUnit = m_oSize*m_oSize*m_oDepth;
		m_nIUnit = m_iPixelSize*m_iPixelSize*m_iDepth;
		m_nOPixel = m_oSize*m_oSize;

		m_oVolume = new float[m_nOUnit];
		m_oVolumeNet = new float [m_nOUnit];

		//std::cout << "Zero Padding: " <<m_zeroPadding << std::endl;
		//std::cout << "Filter Size: " <<m_fSize << std::endl;
		//std::cout << "Stride: " <<m_stride << std::endl;
		//std::cout << "Pixel: " <<m_iPixelSize << std::endl;
		//std::cout<< "Output Size: " << m_oSize << std::endl;




	} else {
		std::cout << "convolutionLayer::fInit: nOUnit and nIUnit must be positive and real." << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

int32_t convolutionLayer::setZeroPadding(){
	int32_t _zeroPadding = 0;
	int32_t _paddingIsFound = -1;
	while(_paddingIsFound == -1) {
		//std::cout<< "Modulo: " << (m_iPixelSize-m_fSize+2*_zeroPadding)%m_stride<<std::endl;
		if( (m_iPixelSize-m_fSize+2*_zeroPadding)%m_stride==0 ) {
			_paddingIsFound = 0;
		}
		else{
			_zeroPadding++;
		}
		//exception muss eingebaut werden!!!
		if(_zeroPadding == 10) {
			std::cout<< "The filter size in combination with the stride is not possible"<<std::endl;
			return EXIT_FAILURE;
		}
	}

	m_zeroPadding = _zeroPadding;
	return EXIT_SUCCESS;
}
int32_t convolutionLayer::forward1(float* _inputVolume){
	float* inpVolume = _inputVolume;
	float count = 0;


	//clock_t timeStart = clock();
	for(uint32_t z = 0; z < m_oDepth/2; z++) {
		for(uint32_t y = 0; y <= m_iPixelSize-m_fSize; y = y + m_stride) {
			for(uint32_t x = 0; x <= m_iPixelSize-m_fSize; x = x + m_stride) {
				for(uint32_t i = 0; i < m_iDepth; i++) {
					for(uint32_t yfilter = 0; yfilter < m_fSize; yfilter++) {
						for(uint32_t xfilter = 0; xfilter < m_fSize; xfilter++) {
							count = count + inpVolume[i*m_nIPixel+(y + yfilter) * m_iPixelSize + x + xfilter]
							        * m_fWeights[z*m_nFUnit + i*m_nKUnit + yfilter*m_fSize + xfilter];
							//std::cout<< "input: " << i*m_iPixelSize*m_iPixelSize+(y + yfilter) * m_iPixelSize + x + xfilter<<" Filter: "<< z*m_fSize*m_fSize*m_iDepth+yfilter*m_fSize+ i*m_fSize*m_fSize+xfilter<<std::endl;

						}
					}
				}
				count += m_bias[z];
				//std::cout<<"Net: "<<count<<" Vol: " << (m_actFunc->f(count)) <<std::endl;

				m_oVolumeNet[z*m_nOPixel+(y/m_stride)*m_oSize+(x/m_stride)] = count;
				m_oVolume[z*m_nOPixel+(y/m_stride)*m_oSize+(x/m_stride)] = m_actFunc->f(count);
				count = 0;
			}
		}
	}
	return EXIT_SUCCESS;
}

int32_t convolutionLayer::forward2(float* _inputVolume){
	float* inpVolume = _inputVolume;
	float count = 0;
	//clock_t timeStart = clock();
	for(uint32_t z = m_oDepth/2; z < m_oDepth; z++) {
		for(uint32_t y = 0; y <= m_iPixelSize-m_fSize; y = y + m_stride) {
			for(uint32_t x = 0; x <= m_iPixelSize-m_fSize; x = x + m_stride) {
				for(uint32_t i = 0; i < m_iDepth; i++) {
					for(uint32_t yfilter = 0; yfilter < m_fSize; yfilter++) {
						for(uint32_t xfilter = 0; xfilter < m_fSize; xfilter++) {
							count = count + inpVolume[i*m_nIPixel+(y + yfilter) * m_iPixelSize + x + xfilter]
							        * m_fWeights[z*m_nFUnit + i*m_nKUnit + yfilter*m_fSize + xfilter];
						}
					}
				}
				count += m_bias[z];
				/*if(m_layerNumber == 0){
				    if(count != count){
				        std::cout<<"Error count";
				    }
				   }*/
				m_oVolumeNet[z*m_nOPixel+(y/m_stride)*m_oSize+(x/m_stride)] = count;
				m_oVolume[z*m_nOPixel+(y/m_stride)*m_oSize+(x/m_stride)] = m_actFunc->f(count);
				count = 0;
			}
		}
	}
	return EXIT_SUCCESS;
}


int32_t convolutionLayer::forward(float* _inputVolume){

	//Comment if you want to use 2 threads
	std::thread t1(&convolutionLayer::forward1,this,_inputVolume);
	std::thread t2(&convolutionLayer::forward2,this,_inputVolume);

	t1.join();
	t2.join();


	//uncomment if you want to use only one thread

	/*float* inpVolume = _inputVolume;
	   float count = 0;
	   //clock_t timeStart = clock();
	   for(int32_t z = m_oDepth/2; z < m_oDepth; z++){
	        for(int32_t y = 0;  y <= m_iPixelSize-m_fSize; y = y + m_stride){
	                for(int32_t x = 0; x <= m_iPixelSize-m_fSize; x = x + m_stride){
	                        for(int32_t i = 0; i < m_iDepth; i++){
	                                for(int32_t yfilter = 0; yfilter < m_fSize; yfilter++){
	                                        for(int32_t xfilter = 0; xfilter < m_fSize; xfilter++){
	                                                count = count + inpVolume[i*m_nIPixel+(y + yfilter) * m_iPixelSize + x + xfilter]
	 * m_fWeights[z*m_nFUnit + i*m_nKUnit + yfilter*m_fSize + xfilter];
	                                        }
	                                }
	                        }
	                        count += m_bias[z];
	                        m_oVolumeNet[z*m_nOPixel+(y/m_stride)*m_oSize+(x/m_stride)] = count;
	                        m_oVolume[z*m_nOPixel+(y/m_stride)*m_oSize+(x/m_stride)] = m_actFunc->f(count);
	                        count = 0;
	                }
	        }
	   }*/



	return EXIT_SUCCESS;
}
//m_iDepth has to be even!!
void convolutionLayer::calcErrLowerLayer1(const float *_OVolume){
	if(m_layerNumber != 0) {
		for(uint i = 0; i < m_iDepth/2; i++) {
			for(uint z = 0; z < m_oDepth; z++) {
				for(uint yErr = 0; yErr < m_oSize; yErr++) {
					for(uint xErr = 0; xErr < m_oSize; xErr++) {
						for(uint y = 0; y < m_fSize; y++) {
							for(uint x = 0; x < m_fSize; x++) {
								m_errorLowerLayer[i*m_nIPixel + (yErr+y)*m_iPixelSize + xErr+x] +=
									(m_fWeights[z*m_nFUnit + i*m_nKUnit + y*m_fSize + x] *
									 m_error[z*m_nOPixel + yErr*m_oSize +xErr]);
							}
						}
					}
				}
			}
		}
	}
}

void convolutionLayer::calcErrLowerLayer2(const float *_OVolume){
	if(m_layerNumber != 0) {
		for(uint32_t i = m_iDepth/2; i < m_iDepth; i++) {
			for(uint32_t z = 0; z < m_oDepth; z++) {
				for(uint32_t yErr = 0; yErr < m_oSize; yErr++) {
					for(uint32_t xErr = 0; xErr < m_oSize; xErr++) {
						for(uint32_t y = 0; y < m_fSize; y++) {
							for(uint32_t x = 0; x < m_fSize; x++) {
								m_errorLowerLayer[i*m_nIPixel + (yErr+y)*m_iPixelSize + xErr+x] +=
									(m_fWeights[z*m_nFUnit + i*m_nKUnit + y*m_fSize + x] * m_error[z*m_nOPixel + yErr*m_oSize +xErr]);
							}
						}
					}
				}
			}
		}
	}
}
void convolutionLayer::calcErrLowerLayer(const float *_OVolume){
	if(m_layerNumber != 0) {
		for(uint32_t i = 0; i < m_iDepth; i++) {
			for(uint32_t z = 0; z < m_oDepth; z++) {
				for(uint32_t yErr = 0; yErr < m_oSize; yErr++) {
					for(uint32_t xErr = 0; xErr < m_oSize; xErr++) {
						for(uint32_t y = 0; y < m_fSize; y++) {
							for(uint32_t x = 0; x < m_fSize; x++) {
								m_errorLowerLayer[i*m_iPixelSize*m_iPixelSize + (yErr+y)*m_iPixelSize + xErr+x] +=
									(m_fWeights[z*m_fSize*m_fSize*m_iDepth + i*m_fSize*m_fSize + y*m_fSize + x] *
									 m_error[z*m_oSize*m_oSize + yErr*m_oSize +xErr]);
							}
						}
					}
				}
			}
		}
	}
}



void convolutionLayer::calcfWeightsDelta(const float *_OVolume){
	float count = 0;
	for(uint32_t z = 0; z < m_oDepth; z++) {
		for(uint32_t i = 0; i < m_iDepth; i++) {
			for(uint32_t y = 0; y <= m_iPixelSize - m_oSize; y++) {
				for(uint32_t x = 0; x <= m_iPixelSize - m_oSize; x++) {
					for(uint32_t yfilter = 0; yfilter < m_oSize; yfilter++) {
						for(uint32_t xfilter = 0; xfilter < m_oSize; xfilter++) {
							count += _OVolume[i*m_nIPixel + (y + yfilter) * m_iPixelSize + x + xfilter]
							         * m_error[z*m_nOPixel+yfilter*m_oSize+xfilter];
						}
					}

//					m_fWeightsDelta[i*m_nKUnit + z*m_nFUnit+y*m_fSize+x] = - m_learningRate * count/(m_oSize*m_oSize);
					m_fWeightsDelta[i*m_nKUnit + z*m_nFUnit+y*m_fSize+x] = -m_learningRate * count/(m_oSize*m_oSize);
					count = 0;
				}
			}
		}
	}

	count = 0;
	for(uint32_t z = 0; z < m_oDepth; z++) {
		for(uint32_t y = 0; y < m_oSize; y++) {
			for(uint32_t x = 0; x < m_oSize; x++) {
				count += m_error[z*m_nOPixel+y*m_oSize+x];
			}
		}
		m_biasDelta[z] = -m_learningRate * count/(m_oSize*m_oSize);
		count = 0;
	}
}

int32_t convolutionLayer::backward(float *prev_error, const float *_OVolume, float *_iVolumeNet){

	m_error = prev_error;
	//------preparation-----------
	for(uint32_t i = 0; i < m_iDepth*m_iPixelSize*m_iPixelSize; i++) {
		m_errorLowerLayer[i] = 0.0f;
	}
	for(uint32_t i = 0; i < m_fSize*m_fSize*m_oDepth*m_iDepth; i++) {
		m_fWeightsDelta[i] = 0.0f;
	}
	// clock_t timeStart = clock();
//      clock_t timeNow = clock();
//      clock_t timeDiff = timeNow - timeStart;
	//std::cout <<"layer:"<<m_layerNumber<<" "<<timeDiff<<std::endl;
	std::thread t1(&convolutionLayer::calcErrLowerLayer1,this,_OVolume);
	std::thread t2(&convolutionLayer::calcErrLowerLayer2,this,_OVolume);
	if(m_layerNumber != 0) {
		for(uint32_t i = 0; i < m_iDepth*m_iPixelSize*m_iPixelSize; i++) {
			m_errorLowerLayer[i]*=(m_actFunc->d(_iVolumeNet[i]));
		}
	}

	calcfWeightsDelta(_OVolume);


	t1.join();
	t2.join();

	// timeNow = clock();
	//       timeDiff = timeNow - timeStart;
	//      std::cout <<"layer:"<<m_layerNumber<<" "<<timeDiff<<std::endl;

	return EXIT_SUCCESS;

}

int32_t convolutionLayer::update() {
	const float *_filterWeightsDelta = m_fWeightsDelta;
	//std::cout<<"Update: " << m_fSize*m_fSize*m_oDepth*m_iDepth<<std::endl;
	if(m_layerNumber!=0) {
		for (uint32_t i = 0; i < m_fSize*m_fSize*m_oDepth*m_iDepth; ++i) {
			m_fWeights[i] += _filterWeightsDelta[i];
			//std::cout<< _filterWeightsDelta[i] << std::endl;
		}
		for(uint32_t z = 0; z < m_oDepth; z++) {
			m_bias[z] += m_biasDelta[z];
		}
	}
	else{
		for (uint32_t i = 0; i < m_fSize*m_fSize*m_oDepth*m_iDepth; ++i) {
			m_fWeights[i] += _filterWeightsDelta[i]*0.1;
			//std::cout<< _filterWeightsDelta[i] << std::endl;
		}
		for(uint32_t z = 0; z < m_oDepth; z++) {
			m_bias[z] += m_biasDelta[z]*0.1;
		}
	}

	return EXIT_SUCCESS;
}


convolutionLayer::convolutionLayer(uint32_t _oDepth, uint32_t _stride, uint32_t _fSize, uint32_t _iDepth, uint32_t _iPixelSize, uint32_t _layerNumber) :
	m_layerNumber(_layerNumber), m_iPixelSize(_iPixelSize), m_iDepth(_iDepth),m_fSize(_fSize), m_stride(_stride), m_oDepth(_oDepth), m_learningRate(0.01f),  m_actFunc(new convolutionActFunc_tanh(0.6666667)) {
	convInit();
}

std::ostream& operator<<(std::ostream& os, const convolutionLayer& l) {
	float *filterWeights = l.m_fWeights;
	os << "Weights:" << std::setprecision(6) << std::endl;
	for (uint32_t z = 0; z < l.m_oDepth; ++z) {
		for (uint32_t y = 0; y < l.m_fSize; ++y) {
			for(uint32_t x = 0; x < l.m_fSize; x++) {
				os << filterWeights[z*l.m_fSize*l.m_fSize+y*l.m_fSize+x] << "\t";
			}
			os << " | " << (l.m_bias)[z] << std::endl;

		}
		std::cout << std::endl;

	}
	os << "Output:" << std::endl;
	for (uint32_t iOUnit = 0; iOUnit < l.m_nOUnit; ++iOUnit) {
		os << "\t" << l.m_oVolume[iOUnit];
	}
	os << std::endl;
	os << std::endl;

	return os;
}

void convolutionLayer::printOutput(){
	for(uint32_t z = 0; z < m_oDepth; z++) {
		for(uint32_t y = 0; y < m_oSize; y++) {
			for(uint32_t x = 0; x < m_oSize; x++) {
				//std::cout<<m_oVolume[z*m_oSize*m_oSize+y*m_oSize+x]<<" ";
				// volume or volumeNet
				printf("%.6f ",m_oVolume[z*m_oSize*m_oSize+y*m_oSize+x] );
				//std::cout<<m_oVolumeNet[z*m_oSize*m_oSize+y*m_oSize+x]<<" ";
				// std::cout<<z*oSize*oSize+y*oSize+x<<" ";
			}
			std::cout<<std::endl;
		}
		std::cout<<std::endl;
	}
}

void convolutionLayer::printError(){

	for (uint32_t iOUnit = 0; iOUnit < m_nOUnit; ++iOUnit) {
		if(iOUnit%m_oSize == 0) std::cout<<std::endl;
		if(iOUnit%(m_oSize*m_oSize) == 0) std::cout<<std::endl;
		std::cout<<m_error[iOUnit]<<" ";
	}
	std::cout<<std::endl;
}

void convolutionLayer::printErrorLowerLayer(){

	for (uint32_t iUnit = 0; iUnit < m_nIUnit; ++iUnit) {
		if(iUnit%m_iPixelSize == 0) std::cout<<std::endl;
		if(iUnit%(m_iPixelSize*m_iPixelSize) == 0) std::cout<<std::endl;
		std::cout<<m_errorLowerLayer[iUnit]<<" ";
	}
	std::cout<<std::endl;
}

void convolutionLayer::printErrorTotal(){
	float errTotal = 0.f;

	for (uint32_t iOUnit = 0; iOUnit < m_nOUnit; ++iOUnit) {

		//std::cout<< " test "<< std::endl;
		errTotal += 0.5f * m_error[iOUnit] * m_error[iOUnit];
		//std::cout<<"test2"<<std::endl;
	}
	std::cout << "ErrTotal"<< m_layerNumber << ": "<< errTotal << std::endl;

}

void convolutionLayer::printfWeightsDelta(){

	for(uint32_t i = 0; i < m_fSize*m_fSize*m_oDepth*m_iDepth; i++) {
		if(i%m_fSize == 0) std::cout<<std::endl;
		if(i%(m_fSize*m_fSize) == 0) std::cout<<std::endl;
		std::cout<<m_fWeightsDelta[i]<<" ";
	}

}

void convolutionLayer::printfWeights(){

	for(uint32_t i = 0; i < m_fSize*m_fSize*m_oDepth*m_iDepth; i++) {
		if(i%m_fSize == 0) std::cout<<std::endl;
		if(i%(m_fSize*m_fSize) == 0) std::cout<<std::endl;
		std::cout<<m_fWeights[i]<<" ";
	}

}

int32_t convolutionLayer::printWeightsToCsv(std::fstream& f){
	f << "Weights;" << std::endl;
	for(uint32_t i = 0; i < m_fSize*m_fSize*m_oDepth*m_iDepth; ++i) {
		// if(i%m_fSize==0){
//                      f<<std::endl;
//              }
//              if(i%(m_fSize*m_fSize)==0){
//                      f<<std::endl;
//                      f<<(i/(m_fSize*m_fSize))<<std::endl;
//              }
		f << m_fWeights[i] << ";";
	}
	f << std::endl;

	f << "Bias;" << std::endl;
	for(uint32_t i = 0; i < m_oDepth; i++) {
		f << m_bias[i] << ";";
	}
	f << std::endl;
	return EXIT_SUCCESS;
}
