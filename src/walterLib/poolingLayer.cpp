#include "poolingLayer.hpp"

#include <random>
#include <iomanip>
#include <fstream>

int32_t poolingLayer::maxPoolingInit() {
	if ((m_fSize > 0) && (m_stride > 0) && (m_ioDepth > 0)) {

		//calculates the hight(equals width) of the output Volume (Pixel-FilterGröße+2*ZeroPadding)/Schrittweite+1
		m_oSize =(m_iSize-m_fSize)/m_stride+1;
		//std::cout<< "Output Size: " << m_oSize << std::endl;

		m_oVolume = new float[m_oSize*m_oSize*m_ioDepth];
		m_oVolumeNet = new float[m_oSize*m_oSize*m_ioDepth];
		m_errorLowerLayer = new float[m_iSize*m_iSize*m_ioDepth];
		//std::cout<< "errors: "<< (m_iSize*m_iSize*m_ioDepth)<<std::endl;

	} else {
		std::cout << "poolingLayer::maxPoolingInit: m_fSize, m_stride, m_ioDepth must be positive and real." << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}


int32_t poolingLayer::maxPooling(float* _inputVolume, float* _inputVolumeNet){
	float* iVolume = _inputVolume;
	float* iVolumeNet = _inputVolumeNet;
	float max = -INFINITY;
	float maxNet = -INFINITY;
	for(uint32_t z = 0; z < m_ioDepth; z++) {
		for(uint32_t y = 0; y <= m_iSize-m_fSize; y = y + m_stride) {
			for(uint32_t x = 0; x <= m_iSize-m_fSize; x = x + m_stride) {
				//std::cout<<"filter: "<< m_fSize<<std::endl;
				for(uint32_t yfilter = 0; yfilter < m_fSize; yfilter++) {
					for(uint32_t xfilter = 0; xfilter < m_fSize; xfilter++) {
						if (max < iVolume[z * m_iSize * m_iSize + (y + yfilter) * m_iSize + x + xfilter]) {
							//std::cout<<"Pooling is in progress "<< m_fSize<<std::endl;
							max = iVolume[z * m_iSize * m_iSize + (y + yfilter) * m_iSize + x + xfilter];
							maxNet = iVolumeNet[z * m_iSize * m_iSize + (y + yfilter) * m_iSize + x + xfilter];
						}
						/*else{
						    std::cout<<"iVolume: "<< iVolume[z * m_iSize * m_iSize + (y + yfilter) * m_iSize + x + xfilter]<<std::endl;
						   }*/
					}

				}
				// std::cout<< "Max: " << max;
				// std::cout<< " Place: " << z * m_oSize * m_oSize + y/m_stride * m_oSize +x/m_stride<<std::endl;
				// if(std::abs(max) > 2 ){
//                                      std::cout<<"max: "<<max<<std::endl;
//                              }
				m_oVolume[z * m_oSize * m_oSize + y/m_stride * m_oSize +x/m_stride] = max;
				m_oVolumeNet[z * m_oSize * m_oSize + y/m_stride * m_oSize +x/m_stride] = maxNet;
				max = -INFINITY;
			}
		}
	}


	// std::cout<<"Pool"<<std::endl;
//      for(int i = 0; i < 5; i++){
//              std::cout<<m_oVolume[i]<<" ";
//      }
//      std::cout<<std::endl;

	return EXIT_SUCCESS;

}


int32_t poolingLayer::upsampling( float* _error) {

	m_error = _error;


	for(uint32_t z = 0; z < m_ioDepth; z++) {
		for(uint32_t y = 0; y < m_oSize; y++) {
			for(uint32_t x = 0; x < m_oSize; x++) {
				for(uint32_t yFilter = 0; yFilter < m_stride; yFilter++) {
					for(uint32_t xFilter = 0; xFilter < m_stride; xFilter++) {
						// std::cout<< (y*m_iSize*2 + x*m_fSize + yFilter*m_iSize + xFilter)<<" "<< _error[y*m_oSize+x] <<std::endl;
						m_errorLowerLayer[z*m_iSize*m_iSize + y*m_iSize*2 + x*m_fSize + yFilter*m_iSize + xFilter] = m_error[z*m_oSize*m_oSize + y*m_oSize+x];


					}
				}
			}
		}
	}

	return EXIT_SUCCESS;
}


poolingLayer::poolingLayer(uint32_t _depth, uint32_t _stride, uint32_t _filterSize, uint32_t _inputSize, uint32_t _layerNumber) :
	m_iSize(_inputSize), m_fSize(_filterSize), m_stride(_stride), m_ioDepth(_depth) {
	maxPoolingInit();
}


void poolingLayer::printOutput(){
	for(uint32_t z = 0; z < m_ioDepth; z++) {
		for(uint32_t y = 0; y < m_oSize; y++) {
			for(uint32_t x = 0; x < m_oSize; x++) {
				//std::cout<<m_oVolume[z*m_oSize*m_oSize+y*m_oSize+x]<<" ";
				// volume or volumeNet
				printf("%.2f ",m_oVolume[z*m_oSize*m_oSize+y*m_oSize+x] );
				// std::cout<<m_oVolume[z*m_oSize*m_oSize+y*m_oSize+x]<<" ";
				// std::cout<<z*oSize*oSize+y*oSize+x<<" ";
			}
			std::cout<<std::endl;
		}
		std::cout<<std::endl;
	}
}

void poolingLayer::printErrorLowerLayer(){
	for(uint32_t z = 0; z < m_ioDepth; z++) {
		for(uint32_t y = 0; y < m_oSize; y++) {
			for(uint32_t x = 0; x < m_oSize; x++) {
				//std::cout<<m_oVolume[z*m_oSize*m_oSize+y*m_oSize+x]<<" ";
				// volume or volumeNet
				std::cout<<m_errorLowerLayer[z*m_oSize*m_oSize+y*m_oSize+x]<<" ";
				// std::cout<<z*oSize*oSize+y*oSize+x<<" ";
			}
			std::cout<<std::endl;
		}
		std::cout<<std::endl;
	}
}
