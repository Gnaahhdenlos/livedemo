#include "fcLayer.hpp"

// TODO implement exceptions (?)

#include <random>
#include <iomanip>
#include <float.h>

#define BNN_NORMAL_DISTRIBUTION_MEAN    0.05f
#define BNN_NORMAL_DISTRIBUTION_STDDEV  -0.05f


int32_t fcLayer::fInit() {
	if ((m_nOUnit > 0) && (m_nIUnit > 0)) {

		m_errorTotal = 0.0f;
		m_pWeightDelta = new float[m_nOUnit * (m_nIUnit +1)];
		m_pWeight = new float[m_nOUnit * (m_nIUnit + 1)];
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_real_distribution<> d(BNN_NORMAL_DISTRIBUTION_MEAN, BNN_NORMAL_DISTRIBUTION_STDDEV);
		for(uint32_t n = 0; n < m_nOUnit*(m_nIUnit+1); ++n) {
			m_pWeight[n] = d(gen);
		}
		m_pOutputNet = new float[m_nOUnit];
		for (uint32_t n = 0; n < m_nOUnit; ++n)
			m_pOutputNet[n] = 0.f;
		//std::cout<< "errors: "<< m_nIUnit<<std::endl;
		m_errorLowerLayer = new float[m_nIUnit];
		for (uint32_t n = 0; n < m_nIUnit; ++n)
			m_errorLowerLayer[n] = 0.0f;

		m_error = new float[m_nOUnit];
		for (uint32_t n = 0; n < m_nOUnit; ++n)
			m_error[n] = 0.f;

		m_pOutput = new float[m_nOUnit];
		for (uint32_t n = 0; n < m_nOUnit; ++n)
			m_pOutput[n] = 0.f;

	} else {
		std::cout << "fcLayer::fInit: nOUnit and nIUnit must be positive and real." << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}


fcLayer::fcLayer() : m_nOUnit(1), m_nIUnit(1), m_learningRate(0.5f), m_actFunc(new fcActFunc_sigmoid(1.0f)) {
	fInit();
}

fcLayer::fcLayer(uint32_t _nIUnit, uint32_t _nOUnit, int32_t _isOLayer,uint32_t _layerNumber) :
	m_layerNumber(_layerNumber),m_isOLayer(_isOLayer), m_nOUnit(_nOUnit), m_nIUnit(_nIUnit), m_learningRate(0.01f), m_actFunc(new fcActFunc_tanh(0.6666667f)){
	fInit();
}

fcLayer::~fcLayer() {
	if (m_pWeight != nullptr) delete[] m_pWeight;
	if (m_pOutput != nullptr) delete[] m_pOutput;
}

int32_t fcLayer::forward(const float *inp) {
	float tmp;
	const float *tpWeight = m_pWeight;
	switch(m_isOLayer) {
	//Tanh
	case 1:
		tpWeight = m_pWeight;
		//std::cout<<m_nOUnit<<std::endl;
		for (uint32_t iOUnit = 0; iOUnit < m_nOUnit; ++iOUnit) {
			tmp = tpWeight[m_nIUnit];         //add bias
			/*if(tmp != tmp){
			    std::cout<<"Error tpWeightBias";
			   }*/
			for (uint32_t iIUnit = 0; iIUnit < m_nIUnit; ++iIUnit) {

				tmp += tpWeight[iIUnit] * inp[iIUnit];

			}
			m_pOutputNet[iOUnit] = tmp;

			m_pOutput[iOUnit] = m_actFunc->f(tmp);

			tpWeight += m_nIUnit + 1;
		}
		std::cout<<std::endl;
		break;
	//softmax
	case -1:

		float denominator = 0;
		tpWeight = m_pWeight;
		for (uint32_t iOUnit = 0; iOUnit < m_nOUnit; ++iOUnit) {
			tmp = tpWeight[m_nIUnit];         //add bias
			for (uint32_t iIUnit = 0; iIUnit < m_nIUnit; ++iIUnit) {
				tmp += tpWeight[iIUnit] * inp[iIUnit];
			}
			m_pOutputNet[iOUnit] = tmp;
			tpWeight += m_nIUnit + 1;

		}

		const int floatMax = 100;
		for(uint32_t iOUnit = 0; iOUnit < m_nOUnit; iOUnit++) {
			if(m_pOutputNet[iOUnit] > floatMax)
				m_pOutputNet[iOUnit] = floatMax;
			else
			if(m_pOutputNet[iOUnit] < -floatMax)
				m_pOutputNet[iOUnit] = -floatMax;
			denominator += exp(m_pOutputNet[iOUnit]);
		}
		for(uint32_t iOUnit=0; iOUnit < m_nOUnit; iOUnit++) {
			m_pOutput[iOUnit] = exp(m_pOutputNet[iOUnit])/denominator;
		}
		break;
	}
	return EXIT_SUCCESS;
}

int32_t fcLayer::backward( float *prev_error, const float *_OVolume, float *inpNet) {
	float *tpWeightDelta = m_pWeightDelta;
	m_error = prev_error;
	//clock_t timeStart = clock();
	if(m_layerNumber != 0) {
		for (uint32_t iIUnit = 0; iIUnit < m_nIUnit; ++iIUnit) {
			m_errorLowerLayer[iIUnit] = 0;
			for (uint32_t iO = 0; iO <  m_nOUnit; ++iO) {

				m_errorLowerLayer[iIUnit] += m_pWeight[iO * (m_nIUnit + 1) + iIUnit] * m_error[iO];
				//std::cout<<m_errorLowerLayer[iIUnit]<<std::endl;
			}
			m_errorLowerLayer[iIUnit] *= m_actFunc->d(inpNet[iIUnit]);
		}
	}

	for (uint32_t iOUnit = 0; iOUnit < m_nOUnit; ++iOUnit) {
		float tmp = 1;
		tmp *= m_error[iOUnit];
		for (uint32_t iIUnit = 0; iIUnit < m_nIUnit; ++iIUnit) {
			float deltaTmp = tmp * _OVolume[iIUnit];
			tpWeightDelta[iIUnit] = -m_learningRate * deltaTmp;
		}
		tpWeightDelta[m_nIUnit] = -m_learningRate * m_error[iOUnit];         //bias learning
		tpWeightDelta += m_nIUnit+1;
	}
	return EXIT_SUCCESS;
}

//the highest Layer (Output Layer) needs to calculate the own error with the lossFuntion!
int32_t fcLayer::lossFunction(std::vector<float> _target){


	for (uint32_t iOUnit = 0; iOUnit < m_nOUnit; ++iOUnit) {
		//float tmp = m_actFunc->d(m_pOutputNet[iOUnit]);
		m_error[iOUnit] = (m_pOutput[iOUnit] - _target[iOUnit]);
		//m_error[iOUnit] = (m_pOutput[iOUnit] - _target[iOUnit])*m_pOutput[iOUnit]*(1-m_pOutput[iOUnit]);	//not wrong but much better learning results without this substration
	}

	return EXIT_SUCCESS;
}

int32_t fcLayer::update() {
	const float *tpWeightDelta = m_pWeightDelta;
	float *tpWeight = m_pWeight;
	for (uint32_t iOUnit = 0; iOUnit < m_nOUnit; ++iOUnit) {
		for (uint32_t iIUnit = 0; iIUnit < m_nIUnit +1; ++iIUnit) {
			tpWeight[iIUnit] += tpWeightDelta[iIUnit];
		}
		tpWeight += m_nIUnit + 1;
		tpWeightDelta += m_nIUnit + 1;
	}

	return EXIT_SUCCESS;
}

int32_t fcLayer::calcErrorTotal(){
	m_errorTotal = 0.0f;

	for (uint32_t iOUnit = 0; iOUnit < m_nOUnit; ++iOUnit) {
		m_errorTotal += 0.5f * m_error[iOUnit] * m_error[iOUnit];
	}
	return EXIT_SUCCESS;
}

bool fcLayer::checkDetection(uint32_t target){
	float max = 0;
	uint32_t imax = 0;
	for (uint32_t iOUnit = 0; iOUnit < m_nOUnit; ++iOUnit) {
		if(max < m_pOutput[iOUnit]) {
			max = m_pOutput[iOUnit];
			imax = iOUnit;
		}
	}
	return (imax == target);
}

int32_t fcLayer::getDetection(){
	int32_t strokeN = 0;
	float max = 0;
	for (uint32_t iOUnit = 0; iOUnit < m_nOUnit; ++iOUnit) {
		if(max < m_pOutput[iOUnit]) {
			max = m_pOutput[iOUnit];
			strokeN = iOUnit;
		}
	}
	return strokeN;
}

std::ostream& operator<<(std::ostream& os, const fcLayer& l) {
	float *tpWeight = l.m_pWeight;
	os << "Weights Layer: " <<l.m_layerNumber << std::setprecision(6) << std::endl;
	for (uint32_t iOUnit = 0; iOUnit < l.m_nOUnit; ++iOUnit) {
		for (uint32_t iIUnit = 0; iIUnit < l.m_nIUnit; ++iIUnit) {
			os << tpWeight[iIUnit] << "\t";
		}
		os << " | " << tpWeight[l.m_nIUnit] << std::endl;
		tpWeight += l.m_nIUnit + 1;
	}
	os << "Output:" << std::endl;
	for (uint32_t iOUnit = 0; iOUnit < l.m_nOUnit; ++iOUnit) {
		os << "\t" << l.m_pOutput[iOUnit];
	}
	os << std::endl;

	return os;
}

void fcLayer::printErr(){
	for (uint32_t iOUnit = 0; iOUnit < m_nOUnit; ++iOUnit) {
		std::cout << "ErrL"<< m_layerNumber <<" >> " << m_error[iOUnit]<<" ";
	}
	std::cout << std::endl;
	std::cout << std::endl;
}

void fcLayer::printErrorLowerLayer(){

	for (uint32_t iUnit = 0; iUnit < m_nIUnit; ++iUnit) {
		// if(iUnit%m_iPixelSize == 0)std::cout<<std::endl;
		// if(iUnit%(m_iPixelSize*m_iPixelSize) == 0)std::cout<<std::endl;
		std::cout<<m_errorLowerLayer[iUnit]<<" ";
	}
	std::cout<<std::endl;
}

void fcLayer::printErrorTotal(){
	float errTotal = 0.0f;

	for (uint32_t iOUnit = 0; iOUnit < m_nOUnit; ++iOUnit) {
		errTotal += 0.5f * m_error[iOUnit] * m_error[iOUnit];
	}
	std::cout <<"ErrTotal"<< m_layerNumber<< " :" <<errTotal << std::endl;
}

void fcLayer::printOutput(){
	//std::cout << "OutputL"<< m_layerNumber;
	for (uint32_t iOUnit = 0; iOUnit < m_nOUnit; ++iOUnit) {
		printf("%.3f ", m_pOutput[iOUnit]);
		// std::cout << ">" << m_pOutput[iOUnit];
	}
	std::cout << std::endl;
}

void fcLayer::printOutput(uint32_t target){
	//std::cout << "OutputL"<< m_layerNumber;
	float max = 0;
	uint32_t imax = 0;
	for (uint32_t iOUnit = 0; iOUnit < m_nOUnit; ++iOUnit) {
		if(max < m_pOutput[iOUnit]) {
			max = m_pOutput[iOUnit];
			imax = iOUnit;
		}
	}
	std::cout<<imax<<std::endl;
	for (uint32_t iOUnit = 0; iOUnit < m_nOUnit; ++iOUnit) {


		if( iOUnit == target && imax == iOUnit) {
			printf("\x1b[32m%.3f\x1b[0m ", m_pOutput[iOUnit]);
		}
		else if(iOUnit == target) {
			printf("\x1b[31m%.3f\x1b[0m ", m_pOutput[iOUnit]);
		}
		else if(imax == iOUnit) {
			printf("\x1b[34m%.3f\x1b[0m ", m_pOutput[iOUnit]);
		}
		else{
			printf("%.3f ", m_pOutput[iOUnit]);
		}
		// std::cout << ">" << m_pOutput[iOUnit];
	}
	std::cout << std::endl;
}

void fcLayer::printOutputNet(){
	std::cout << "OutputL"<< m_layerNumber;
	for (uint32_t iOUnit = 0; iOUnit < m_nOUnit; ++iOUnit) {
		std::cout << ">" << m_pOutputNet[iOUnit];
	}
	std::cout << std::endl;
}

void fcLayer::printWeights(){

	for(uint32_t n = 0; n < m_nOUnit*(m_nIUnit+1); ++n) {
		std::cout << m_pWeight[n] << ";";
	}
}

int32_t fcLayer::printWeightsToCsv(std::fstream& f){
	f << "Weights" << std::endl;
	for(uint32_t n = 0; n < m_nOUnit*(m_nIUnit+1); ++n) {
		f << m_pWeight[n] << ";";
	}
	f << std::endl;
	return EXIT_SUCCESS;
}
