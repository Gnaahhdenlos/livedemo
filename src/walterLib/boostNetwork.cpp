// C++ std libraries
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <iomanip>
#include <utility>

// C std libraries
#include <cstdlib>
#include <cstdint>

#include "boostNetwork.hpp"

// openCV includes
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <algorithm>    // std::random_shuffle
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand
//#include <Directory>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

//Define
#define NUMBER_CLASSES  43 //Number of different classes

int32_t boostNetwork::netInit(){

	for(int i = 0; i < 10; i++) {
		m_ADetectedImages[i]=0;
		m_AFalseDetectedImages[i]=0;
	}


	for(int j = 0; j < 101; j++) {
		m_rightProbHistogram[j]=0;
		m_falseProbHistogram[j]=0;
		m_relativeRightProbHistogram[j]=0.0f;
		m_relativeFalseProbHistogram[j]=0.0f;
	}


	originalNet = new network();
	imadjustNet = new network();
	histeqNet = new network();
	aheqNet = new network();
	conormNet = new network();

	return EXIT_SUCCESS;
}

int32_t boostNetwork::initWeights(std::string originalWeights, std::string imadjustWeights, std::string histeqWeights, std::string aheqWeights, std::string conormWeights){

	originalNet->initWeights(originalWeights);
	imadjustNet->initWeights(imadjustWeights);
	histeqNet->initWeights(histeqWeights);
	aheqNet->initWeights(aheqWeights);
	conormNet->initWeights(conormWeights);

	return EXIT_SUCCESS;
}

int32_t boostNetwork::forward(cv::Mat img){
	cv::Mat image;

	originalNet->forward(img);

	image = imadjustNet->convertToImadjust(img,1);
	imadjustNet->forward(image);

	image = histeqNet->convertToHisteq(img);
	histeqNet->forward(image);

	image = aheqNet->convertToImadjust(img,4);
	aheqNet->forward(image);

	image = conormNet->convertToContrast(img);
	conormNet->forward(image);

	calcDetection();
	return EXIT_SUCCESS;
}

int32_t boostNetwork::calcDetection(){
	//int32_t strokeNAv = 0;
	//int32_t strokeNMed = 0;
	//int32_t strokeN = 0;
	// int32_t originalStroke = originalNet->calcDetection();
	// int32_t imadjustStroke = imadjustNet->calcDetection();
	// int32_t histeqSstroke = histeqNet->calcDetection();
	// int32_t aheqStroke = aheqNet->calcDetection();
	// int32_t conormStroke = conormNet->calcDetection();

	/*int32_t passes = 0;
	   int32_t maxPasses = 0;
	   for(int i = 0; i < 43; i++){
	        passes = 0;
	        if( originalStroke == i ){passes++;}
	        if( imadjustStroke == i ){passes++;}
	        if( histeqStroke == i ){passes++;}
	        if( aheqStroke == i ){passes++;}
	        if( conormStroke == i ){passes++;}
	        if(passes > maxPasses){maxPasses = passes; m_strokeN = i;}
	   }
	   std::cout<<"Passes: "<<maxPasses<<" Stroke nach Passes: "<< getStrokeName(m_strokeN)<<std::endl;

	   if(maxPasses >= 3){
	        outputOriginal = originalNet->getFCLayer7()->getOutputs();
	        outputImadjust = imadjustNet->getFCLayer7()->getOutputs();
	        outputHisteq = histeqNet->getFCLayer7()->getOutputs();
	        outputAheq = aheqNet->getFCLayer7()->getOutputs();
	        outputConorm = conormNet->getFCLayer7()->getOutputs();
	        for(int i = 0; i < 43; i++){
	                outputAvarage[i] = (outputOriginal[i] + outputImadjust[i] + outputHisteq[i] + outputAheq[i] + outputConorm[i])/5.0f;
	        }
	   }*/

	// std::cout<<"origialStroke: "<<getStrokeName(originalStroke)<<std::endl;
//              std::cout<<"imadjustStroke: "<<getStrokeName(imadjustStroke)<<std::endl;
//              std::cout<<"histeqStroke: "<<getStrokeName(histeqStroke)<<std::endl;
//              std::cout<<"aheqStroke: "<<getStrokeName(aheqStroke)<<std::endl;
//              std::cout<<"conormStroke: "<<getStrokeName(conormStroke)<<std::endl;

	m_maxProbability = 0;
	outputOriginal = originalNet->getFCLayer7()->getOutputs();
	outputImadjust = imadjustNet->getFCLayer7()->getOutputs();
	outputHisteq = histeqNet->getFCLayer7()->getOutputs();
	outputAheq = aheqNet->getFCLayer7()->getOutputs();
	outputConorm = conormNet->getFCLayer7()->getOutputs();


	for(int i = 0; i < 43; i++) {
		outputAvarage[i] = (outputOriginal[i] + outputImadjust[i] + outputHisteq[i] + outputAheq[i] + outputConorm[i])/5.0f;
		if(m_maxProbability < outputAvarage[i]) {
			m_maxProbability = outputAvarage[i];
			m_strokeN = i;
		}
	}
	/*
	   //Average with square outputs
	   for(int i = 0; i < 43; i++){
	        outputAvarage[i] = (outputOriginal[i]*outputOriginal[i] + outputImadjust[i]*outputImadjust[i] + outputHisteq[i]*outputHisteq[i] + outputAheq[i]*outputAheq[i] + outputConorm[i]*outputConorm[i])/5.0f;
	        if(m_maxProbability < outputAvarage[i]){
	                m_maxProbability = outputAvarage[i];
	                m_strokeN = i;
	        }
	   }*/

	/*
	   for(int i = 0; i < 43; i++){
	        outputAvarage[i] = (outputOriginal[i] + outputImadjust[i] + outputHisteq[i] + outputAheq[i] + outputConorm[i])/5.0f;
	        std::vector<float> median {outputOriginal[i],outputImadjust[i], outputHisteq[i], outputAheq[i], outputConorm[i]};
	        std::sort(median.begin(),median.end());
	        if(m_maxProbability < median[2]){
	                outputAvarage[i] = median[2];
	                m_maxProbability = median[2];
	                m_strokeN = i;
	        }
	   }*/
	/*for(int i = 0; i < 43; i++){
	        outputAvarage[i] = (outputOriginal[i] + outputImadjust[i] + outputHisteq[i] + outputAheq[i] + outputConorm[i])/5.0f;
	        std::vector<float> median {outputOriginal[i],outputImadjust[i], outputHisteq[i], outputAheq[i], outputConorm[i]};
	        std::sort(median.begin(),median.end());
	        outputAvarage[i] = (outputAvarage[i] + median[2])/2.0f;
	        if(m_maxProbability < outputAvarage[i]){
	                m_maxProbability = outputAvarage[i];
	                m_strokeN = i;
	        }
	   }*/

	return EXIT_SUCCESS;
}

int32_t boostNetwork::calcPasses(int strokeN){
	int32_t passes = 0;
	if(originalNet->calcDetection() == strokeN) {passes++;}
	if(imadjustNet->calcDetection() == strokeN) {passes++;}
	if(histeqNet->calcDetection() == strokeN) {passes++;}
	if(aheqNet->calcDetection() == strokeN) {passes++;}
	if(conormNet->calcDetection() == strokeN) {passes++;}

	return passes;
}

int32_t boostNetwork::countDetectedImages(std::string strokeDataName, int target){

	if(target == m_strokeN) {
		m_nDetectedImages++;
		m_maxRightPropabilityCumultativ += m_maxProbability;
		m_avRightProbability = m_maxRightPropabilityCumultativ/m_nDetectedImages;

		int prob = (int)(m_maxProbability*100+0.5f);
		std::cout<<"prob:" <<prob<<std::endl;
		m_rightProbHistogram[prob]++;
		for(int i = 0; i < 101; i++) {
			m_relativeRightProbHistogram[i] = ((float)m_rightProbHistogram[i])/((float)m_nDetectedImages);
		}
		m_ADetectedImages[0]++;
		if(m_maxProbability>0.1) {m_ADetectedImages[1]++;}
		if(m_maxProbability>0.2) {m_ADetectedImages[2]++;}
		if(m_maxProbability>0.3) {m_ADetectedImages[3]++;}
		if(m_maxProbability>0.4) {m_ADetectedImages[4]++;}
		if(m_maxProbability>0.5) {m_ADetectedImages[5]++;}
		if(m_maxProbability>0.6) {m_ADetectedImages[6]++;}
		if(m_maxProbability>0.7) {m_ADetectedImages[7]++;}
		if(m_maxProbability>0.8) {m_ADetectedImages[8]++;}
		if(m_maxProbability>0.9) {m_ADetectedImages[9]++;}
	}


	else{
		m_nFalseDetectedImages++;
		m_falseDetectedHist[target] +=1;
		m_falseDetectedImages.push_back(strokeDataName);
		m_maxFalsePropabilityCumultativ += m_maxProbability;
		m_avFalseProbability = m_maxFalsePropabilityCumultativ/m_nFalseDetectedImages;

		int prob = (int)(m_maxProbability*100+0.5f);
		std::cout<<"prob:" <<prob<<std::endl;
		m_falseProbHistogram[prob]++;
		for(int i = 0; i < 101; i++) {
			m_relativeFalseProbHistogram[i] = ((float)m_falseProbHistogram[i])/((float)m_nFalseDetectedImages);
		}

		m_AFalseDetectedImages[0]++;
		if(m_maxProbability>0.1) {m_AFalseDetectedImages[1]++;}
		if(m_maxProbability>0.2) {m_AFalseDetectedImages[2]++;}
		if(m_maxProbability>0.3) {m_AFalseDetectedImages[3]++;}
		if(m_maxProbability>0.4) {m_AFalseDetectedImages[4]++;}
		if(m_maxProbability>0.5) {m_AFalseDetectedImages[5]++;}
		if(m_maxProbability>0.6) {m_AFalseDetectedImages[6]++;}
		if(m_maxProbability>0.7) {m_AFalseDetectedImages[7]++;}
		if(m_maxProbability>0.8) {m_AFalseDetectedImages[8]++;}
		if(m_maxProbability>0.9) {m_AFalseDetectedImages[9]++;}

	}
	m_detectionRate = static_cast<float>(m_nDetectedImages)/static_cast<float>(m_nDetectedImages+m_nFalseDetectedImages);

	for(int i = 0; i < 10; i++) {
		m_ADetectionRate[i] = static_cast<float>(m_ADetectedImages[i])/static_cast<float>(m_AFalseDetectedImages[i]+m_ADetectedImages[i]);
	}

	m_2DHistogram[m_strokeN][target] += 1;

	return EXIT_SUCCESS;
}

int32_t boostNetwork::printDetectionRate(){

	std::cout<<"detectionRate: "<<m_detectionRate<<std::endl;

	return EXIT_SUCCESS;
}

int32_t boostNetwork::printOutput(uint32_t target){
	//std::cout << "OutputL"<< m_layerNumber;
	float max = 0;
	uint32_t imax = 0;
	for (uint32_t iOUnit = 0; iOUnit < 43; ++iOUnit) {
		if(max < outputAvarage[iOUnit]) {
			max = outputAvarage[iOUnit];
			imax = iOUnit;
		}
	}
	std::cout<<imax<<std::endl;
	for (uint32_t iOUnit = 0; iOUnit < 43; ++iOUnit) {


		if( iOUnit == target && imax == iOUnit) {
			printf("\x1b[32m%.3f\x1b[0m ", outputAvarage[iOUnit]);
		}
		else if(iOUnit == target) {
			printf("\x1b[31m%.3f\x1b[0m ", outputAvarage[iOUnit]);
		}
		else if(imax == iOUnit) {
			printf("\x1b[34m%.3f\x1b[0m ", outputAvarage[iOUnit]);
		}
		else{
			printf("%.3f ", outputAvarage[iOUnit]);
		}
		// std::cout << ">" << m_pOutput[iOUnit];
	}
	std::cout << std::endl;
	return EXIT_SUCCESS;
}

int32_t boostNetwork::printDetectionToCsv(std::string CsvDetection){

	std::string fnCsvDetection = CsvDetection;
	std::fstream f;
	mkdir(fnCsvDetection.c_str(), S_IRWXU);
	fnCsvDetection += "Detection";
	//fnCsvDetection += std::to_string(epoche);

	f.open(fnCsvDetection, std::ios::out);

	f<< "Detection Rate: "<<m_detectionRate << std::endl;
	for(int i = 0; i < NUMBER_CLASSES; i++) {
		f<< i<<": "<<m_falseDetectedHist[i]<<std::endl;

	}
	f<<std::endl;
	for(uint32_t y = 0; y < 43; y++) {
		for(uint32_t x = 0; x < 43; x++) {
			f << std::setw(3) << std::setfill('0')<<m_2DHistogram[x][y]<<";";
		}
		f<<std::endl;
	}

	f << "List of false Detected Images: " << std::endl;
	for(uint32_t i = 0; i < m_falseDetectedImages.size(); i++) {
		f << i<<". " << m_falseDetectedImages[i]<<std::endl;
	}

	f.close();

	return EXIT_SUCCESS;
}


boostNetwork::boostNetwork(){
	netInit();
}
