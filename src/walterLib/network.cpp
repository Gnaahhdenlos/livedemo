// C++ std libraries
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <iomanip>

// C std libraries
#include <cstdlib>
#include <cstdint>

#include "network.hpp"

// openCV includes
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <algorithm>    // std::random_shuffle
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand
//#include <Directory>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

//Define
#define NUMBER_CLASSES  6 //Number of different classes

int32_t network::netInit(){
	m_overallError = 0;
	m_avarageError = 0;
	m_nDetectedImages = 0;
	m_nFalseDetectedImages = 0;
	m_maxFalsePropabilityCumultativ = 0;
	m_avFalseProbability = 0;
	m_maxRightPropabilityCumultativ = 0;
	m_avRightProbability = 0;

	for(int i = 0; i < NUMBER_CLASSES; i++) m_falseDetectedHist[i] = 0;
	for(int i = 0; i < NUMBER_CLASSES; i++) {
		for( int j = 0; j < NUMBER_CLASSES; j++) {
			m_2DHistogram[i][j] = 0;
		}
	}

	//Tiefe 100, Schrittweite 1, Filtergröße 7, inputTiefe 3, inputPixelGröße 48, layerNumber 0
	convL0 = new convolutionLayer(10,1,7,3,48,0);
	// _depth 100, _stride 2, _filterSize 2, _inputSize 42
	poolL1 = new poolingLayer(convL0->getOutputDepth(),2,2,convL0->getOutputSize(),1);

	//Tiefe 150, Schrittweite 1, Filtergröße 4, inputTiefe 100, inputPixelGröße 21, layerNumber 2
	convL2 = new convolutionLayer(20,1,4,poolL1->getIODepth(),poolL1->getOutputSize(),2);
	// _depth 150, _stride 2, _filterSize 2, _inputSize 18
	poolL3 = new poolingLayer(convL2->getOutputDepth(),2,2,convL2->getOutputSize(),3);

	//Tiefe 250, Schrittweite 1, Filtergröße 4, inputTiefe 100, inputPixelGröße 9, layerNumber 4
	convL4 = new convolutionLayer(100,1,4,poolL3->getIODepth(),poolL3->getOutputSize(),4);
	// _depth 250, _stride 2, _filterSize 2, _inputSize 6
	poolL5 = new poolingLayer(convL4->getOutputDepth(),2,2,convL4->getOutputSize(),5);

	//fully connected layers
	fcL6 = new fcLayer((poolL5->getIODepth())*(poolL5->getOutputSize())*(poolL5->getOutputSize()),300,1,6);

	fcL7 = new fcLayer(fcL6->getNOUnit(),NUMBER_CLASSES,-1,7);

	return EXIT_SUCCESS;
}

float network::getLearnRateFromCSV(std::string CsvPrevWeights){
	float learnRate;
	std::string line;
	std::string trash;
	std::ifstream weights(CsvPrevWeights.c_str());

	if (!weights.is_open()) {
		printf("Unable to open Weights file %s.\n", CsvPrevWeights.c_str());
		//return EXIT_FAILURE;
	}
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::replace(line.begin(), line.end(), ':', ' ');
	std::istringstream iss(line);
	iss>>trash;
	iss>>learnRate;

	return learnRate;
}

int network::getEpocheFromCSV(std::string CsvPrevWeights){
	int epoche;
	std::string line;
	std::string trash;
	std::ifstream weights(CsvPrevWeights.c_str());

	if (!weights.is_open()) {
		printf("Unable to open Weights file %s.\n", CsvPrevWeights.c_str());
		//return EXIT_FAILURE;
	}
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::replace(line.begin(), line.end(), ':', ' ');
	std::istringstream iss(line);

	for(int i = 0; i < 7; i++) {
		iss>>trash;
	}
	iss>>epoche;

	return epoche;
}



int32_t network::initWeights(std::string CsvWeights){

	float max = 0;
	float mid = 0;
	std::string line;
	std::ifstream weights(CsvWeights.c_str());
	if (!weights.is_open()) {
		printf("Unable to open Weights file %s.\n", CsvWeights.c_str());
		return EXIT_FAILURE;
	}
	std::getline(weights,line);
	//layer0
	std::getline(weights,line);
	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss0(line);
	float *convL0Weights = convL0->getFilterWeights();
	for(uint32_t j = 0; j < convL0->getNumberOfWeights(); j++) {
		iss0 >> convL0Weights[j];
		mid += std::abs(convL0Weights[j]);
		if(std::abs(convL0Weights[j])>max) {
			max = std::abs(convL0Weights[j]);
		}
	}
	mid /= convL0->getNumberOfWeights();
	//convL0->setWeights(convL0Weights);
	//std::cout<<"Mid L0: "<<mid<<std::endl;
	//std::cout<<"Max L0: "<<max<<std::endl;
	mid = 0;
	max = 0;
	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss1(line);
	float *convL0Bias = convL0->getBias();
	for(uint32_t j = 0; j < convL0->getOutputDepth(); j++) {
		iss1 >> convL0Bias[j];
		mid += std::abs(convL0Bias[j]);
		if(std::abs(convL0Bias[j])>max) {
			max = std::abs(convL0Bias[j]);
		}
	}
	//convL0->setBiases(convL0Bias);
	mid /= convL0->getOutputDepth();
	//std::cout<<"Mid L0bias: "<<mid<<std::endl;
	mid = 0;
	//std::cout<<"Max L0bias: "<<max<<std::endl;
	max = 0;
	//layer2
	std::getline(weights,line);
	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss2(line);
	float *convL2Weights = convL2->getFilterWeights();
	for(uint32_t j = 0; j < convL2->getNumberOfWeights(); j++) {
		iss2 >> convL2Weights[j];
		mid += std::abs(convL2Weights[j]);
		if(std::abs(convL2Weights[j])>max) {
			max = std::abs(convL2Weights[j]);
		}
	}
	mid /= convL2->getNumberOfWeights();
	//std::cout<<"Mid L2: "<<mid<<std::endl;
	mid = 0;
	//std::cout<<"Max L2: "<<max<<std::endl;
	//convL2->setWeights(convL2Weights);
	max = 0;
	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss3(line);
	float *convL2Bias = convL2->getBias();
	for(uint32_t j = 0; j < convL2->getOutputDepth(); j++) {
		iss3 >> convL2Bias[j];
		mid += std::abs(convL2Bias[j]);
		if(std::abs(convL2Bias[j])>max) {
			max = std::abs(convL2Bias[j]);
		}
	}
	mid /= convL2->getOutputDepth();
	//std::cout<<"Mid L2bias: "<<mid<<std::endl;
	mid = 0;
	//std::cout<<"Max L2bias: "<<max<<std::endl;

	//convL2->setBiases(convL2Bias);
	//layer4
	max = 0;
	std::getline(weights,line);
	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss4(line);
	float *convL4Weights = convL4->getFilterWeights();

	for(uint32_t j = 0; j < convL4->getNumberOfWeights(); j++) {
		iss4 >> convL4Weights[j];
		mid += std::abs(convL4Weights[j]);
		if(std::abs(convL4Weights[j])>max) {
			max = std::abs(convL4Weights[j]);
		}
	}
	mid /= convL4->getNumberOfWeights();
	//std::cout<<"Mid L4bias: "<<mid<<std::endl;
	mid = 0;
	//std::cout<<"Max L4: "<<max<<std::endl;

	//convL4->setWeights(convL4Weights);
	max = 0;
	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss5(line);
	float *convL4Bias = convL4->getBias();
	for(uint32_t j = 0; j < convL4->getOutputDepth(); j++) {
		iss5 >> convL4Bias[j];
		mid += std::abs(convL4Bias[j]);
		if(std::abs(convL4Bias[j])>max) {
			max = std::abs(convL4Bias[j]);
		}
	}
	mid /= convL4->getOutputDepth();
	//std::cout<<"Mid L4bias: "<<mid<<std::endl;
	mid = 0;
	//std::cout<<"Max L4bias: "<<max<<std::endl;

	//convL4->setBiases(convL4Bias);
	max = 0;
	//layer6
	std::getline(weights,line);
	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss6(line);
	float *fcL6Weights = fcL6->getWeights();
	for(uint32_t j = 0; j < (fcL6->getNOUnit()) * (fcL6->getNIUnit() + 1); j++) {
		iss6 >> fcL6Weights[j];
		mid += std::abs(fcL6Weights[j]);
		if(std::abs(fcL6Weights[j])>max) {
			max = std::abs(fcL6Weights[j]);
		}
	}
	mid /= (fcL6->getNOUnit()) * (fcL6->getNIUnit() + 1);
	//std::cout<<"Mid L6: "<<mid<<std::endl;
	mid = 0;
	//std::cout<<"Max L6: "<<max<<std::endl;
	//fcL6->setWeightsAndBias(fcL6Weights);

	max = 0;
	//layer7
	std::getline(weights,line);
	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss7(line);
	float *fcL7Weights = fcL7->getWeights();
	for(uint32_t j = 0; j < (fcL7->getNOUnit()) * (fcL7->getNIUnit() + 1); j++) {
		iss7 >> fcL7Weights[j];
		mid += std::abs(fcL6Weights[j]);
		if(std::abs(fcL7Weights[j])>max) {
			max = std::abs(fcL7Weights[j]);
		}
	}
	mid /= (fcL7->getNOUnit()) * (fcL7->getNIUnit() + 1);
	//std::cout<<"Mid L7: "<<mid<<std::endl;
	//std::cout<<"Max L7: "<<max<<std::endl;
	//fcL7->setWeightsAndBias(fcL7Weights);
	return EXIT_SUCCESS;
}

int32_t network::shiftWeights(std::string CsvWeights, std::string CsvWeightsSafe){

	//float mid = 0;
	float shift = 65536;
	std::string line;
	std::ifstream weights(CsvWeights.c_str());
	if (!weights.is_open()) {
		printf("Unable to open Weights file %s.\n", CsvWeights.c_str());
		return EXIT_FAILURE;
	}
	std::getline(weights,line);
	//layer0
	std::getline(weights,line);
	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss0(line);
	float convL0Weights[convL0->getNumberOfWeights()];
	for(uint32_t j = 0; j < convL0->getNumberOfWeights(); j++) {
		iss0 >> convL0Weights[j];
		convL0Weights[j] *=shift;
		int16_t tmp = (int16_t)convL0Weights[j];
		convL0Weights[j] = (float)tmp;

	}
	//for(int i = 0; i < 10 ; i++){
	//	std::cout<< convL0Weights[i]<<" ";
	//}
	std::cout<<std::endl;
	convL0->setWeights(convL0Weights);

	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss1(line);
	float convL0Bias[convL0->getOutputDepth()];
	for(uint32_t j = 0; j < convL0->getOutputDepth(); j++) {
		iss1 >> convL0Bias[j];
		convL0Bias[j] *=shift;
		int16_t tmp = (int16_t)convL0Bias[j];
		convL0Bias[j] = (float)tmp;

	}
	convL0->setBiases(convL0Bias);

	//layer2
	std::getline(weights,line);
	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss2(line);
	float convL2Weights[convL2->getNumberOfWeights()];
	for(uint32_t j = 0; j < convL2->getNumberOfWeights(); j++) {
		iss2 >> convL2Weights[j];
		convL2Weights[j] *=shift;
		int16_t tmp = (int16_t)convL2Weights[j];
		convL2Weights[j] = (float)tmp;

	}
	convL2->setWeights(convL2Weights);

	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss3(line);
	float convL2Bias[convL2->getOutputDepth()];
	for(uint32_t j = 0; j < convL2->getOutputDepth(); j++) {
		iss3 >> convL2Bias[j];
		convL2Bias[j] *=shift;
		int16_t tmp = (int16_t)convL2Bias[j];
		convL2Bias[j] = (float)tmp;

	}
	convL2->setBiases(convL2Bias);

	//layer4
	std::getline(weights,line);
	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss4(line);
	float convL4Weights[convL4->getNumberOfWeights()];

	for(uint32_t j = 0; j < convL4->getNumberOfWeights(); j++) {
		iss4 >> convL4Weights[j];
		convL4Weights[j] *=shift;
		int16_t tmp = (int16_t)convL4Weights[j];
		convL4Weights[j] = (float)tmp;

	}
	convL4->setWeights(convL4Weights);

	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss5(line);
	float convL4Bias[convL4->getOutputDepth()];
	for(uint32_t j = 0; j < convL4->getOutputDepth(); j++) {
		iss5 >> convL4Bias[j];
		convL4Bias[j] *=shift;
		int16_t tmp = (int16_t)convL4Bias[j];
		convL4Bias[j] = (float)tmp;

	}
	convL4->setBiases(convL4Bias);

	shift = 16384;
	//layer6
	std::getline(weights,line);
	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss6(line);
	float fcL6Weights[(fcL6->getNOUnit()) * (fcL6->getNIUnit() + 1)];
	for(uint32_t j = 0; j < (fcL6->getNOUnit()) * (fcL6->getNIUnit() + 1); j++) {
		iss6 >> fcL6Weights[j];
		fcL6Weights[j] *=shift;
		int16_t tmp = (int16_t)fcL6Weights[j];
		fcL6Weights[j] = (float)tmp;

	}
	fcL6->setWeightsAndBias(fcL6Weights);

	//layer7
	std::getline(weights,line);
	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss7(line);
	float fcL7Weights[(fcL7->getNOUnit()) * (fcL7->getNIUnit() + 1)];
	for(uint32_t j = 0; j < (fcL7->getNOUnit()) * (fcL7->getNIUnit() + 1); j++) {
		iss7 >> fcL7Weights[j];
		fcL7Weights[j] *=shift;
		int16_t tmp = (int16_t)fcL7Weights[j];
		fcL7Weights[j] = (float)tmp;
	}
	fcL7->setWeightsAndBias(fcL7Weights);

	setEpoche(getEpocheFromCSV(CsvWeights));
	printWeightsToCsv(CsvWeightsSafe);

	return EXIT_SUCCESS;
}

int32_t network::hardcodeWeights(std::string CsvWeights, std::string CsvWeightsSafe){
	float shift = 32768;
	std::string line;
	std::ifstream weights(CsvWeights.c_str());
	if (!weights.is_open()) {
		printf("Unable to open Weights file %s.\n", CsvWeights.c_str());
		return EXIT_FAILURE;
	}
	std::getline(weights,line);
	//layer0
	std::getline(weights,line);
	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss0(line);
	float convL0Weights[convL0->getNumberOfWeights()];
	for(uint32_t j = 0; j < convL0->getNumberOfWeights(); j++) {
		iss0 >> convL0Weights[j];
		convL0Weights[j] *=shift;
		int tmp = (int)convL0Weights[j];
		convL0Weights[j] = (float)tmp;

	}

	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss1(line);
	float convL0Bias[convL0->getOutputDepth()];
	for(uint32_t j = 0; j < convL0->getOutputDepth(); j++) {
		iss1 >> convL0Bias[j];
		convL0Bias[j] *=shift;
		int tmp = (int)convL0Bias[j];
		convL0Bias[j] = (float)tmp;
	}

	//layer2
	std::getline(weights,line);
	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss2(line);
	float convL2Weights[convL2->getNumberOfWeights()];
	for(uint32_t j = 0; j < convL2->getNumberOfWeights(); j++) {
		iss2 >> convL2Weights[j];
		convL2Weights[j] *=shift;
		int tmp = (int)convL2Weights[j];
		convL2Weights[j] = (float)tmp;

	}

	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss3(line);
	float convL2Bias[convL2->getOutputDepth()];
	for(uint32_t j = 0; j < convL2->getOutputDepth(); j++) {
		iss3 >> convL2Bias[j];
		convL2Bias[j] *=shift;
		int tmp = (int)convL2Bias[j];
		convL2Bias[j] = (float)tmp;

	}

	//layer4
	std::getline(weights,line);
	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss4(line);
	float convL4Weights[convL4->getNumberOfWeights()];

	for(uint32_t j = 0; j < convL4->getNumberOfWeights(); j++) {
		iss4 >> convL4Weights[j];
		convL4Weights[j] *=shift;
		int tmp = (int)convL4Weights[j];
		convL4Weights[j] = (float)tmp;

	}

	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss5(line);
	float convL4Bias[convL4->getOutputDepth()];
	for(uint32_t j = 0; j < convL4->getOutputDepth(); j++) {
		iss5 >> convL4Bias[j];
		convL4Bias[j] *=shift;
		int tmp = (int)convL4Bias[j];
		convL4Bias[j] = (float)tmp;

	}
	shift = 8192;
	//layer6
	std::getline(weights,line);
	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss6(line);
	float fcL6Weights[(fcL6->getNOUnit()) * (fcL6->getNIUnit() + 1)];
	for(uint32_t j = 0; j < (fcL6->getNOUnit()) * (fcL6->getNIUnit() + 1); j++) {
		iss6 >> fcL6Weights[j];
		fcL6Weights[j] *=shift;
		int tmp = (int)fcL6Weights[j];
		fcL6Weights[j] = (float)tmp;

	}

	//layer7
	std::getline(weights,line);
	std::getline(weights,line);
	std::getline(weights,line);
	std::replace(line.begin(), line.end(), ';', ' ');
	std::istringstream iss7(line);
	float fcL7Weights[(fcL7->getNOUnit()) * (fcL7->getNIUnit() + 1)];
	for(uint32_t j = 0; j < (fcL7->getNOUnit()) * (fcL7->getNIUnit() + 1); j++) {
		iss7 >> fcL7Weights[j];
		fcL7Weights[j] *=shift;
		int tmp = (int)fcL7Weights[j];
		fcL7Weights[j] = (float)tmp;
	}

	setEpoche(getEpocheFromCSV(CsvWeights));
	std::fstream f;
	mkdir(CsvWeightsSafe.c_str(), S_IRWXU);
	CsvWeightsSafe +="Weights";
	CsvWeightsSafe += std::to_string(m_epoche);
	std::cout<< CsvWeightsSafe<< std::endl;

	f.open(CsvWeightsSafe, std::ios::out);
	//header
	f<<"#include \"weights.hpp\""<<std::endl;
	f<<std::endl;
	f<< "int16_t convL0Weights["<<convL0->getNumberOfWeights()<<"] = {"<<std::endl;
	f<< convL0Weights[0];
	for(uint32_t i = 1; i < convL0->getNumberOfWeights(); i++) {
		f<< ","<<convL0Weights[i];
	}
	f<< "};"<<std::endl;
	f<< std::endl;

	f<< "int16_t convL0Bias["<<convL0->getOutputDepth()<<"] = {"<<std::endl;
	f<< convL0Bias[0];
	for(uint32_t i = 1; i < convL0->getOutputDepth(); i++) {
		f<< ","<<convL0Bias[i];
	}
	f<< "};"<<std::endl;
	f<< std::endl;

	f<< "int16_t convL2Weights["<<convL2->getNumberOfWeights()<<"] = {"<<std::endl;
	f<< convL2Weights[0];
	for(uint32_t i = 1; i < convL2->getNumberOfWeights(); i++) {
		f<< ","<<convL2Weights[i];
	}
	f<< "};"<<std::endl;
	f<< std::endl;

	f<< "int16_t convL2Bias["<<convL2->getOutputDepth()<<"] = {"<<std::endl;
	f<< convL2Bias[0];
	for(uint32_t i = 1; i < convL2->getOutputDepth(); i++) {
		f<< ","<<convL2Bias[i];
	}
	f<< "};"<<std::endl;
	f<< std::endl;

	f<< "int16_t convL4Weights["<<convL4->getNumberOfWeights()<<"] = {"<<std::endl;
	f<< convL0Weights[0];
	for(uint32_t i = 1; i < convL4->getNumberOfWeights(); i++) {
		f<< ","<<convL4Weights[i];
	}
	f<< "};"<<std::endl;
	f<< std::endl;

	f<< "int16_t convL4Bias["<<convL4->getOutputDepth()<<"] = {"<<std::endl;
	f<< convL4Bias[0];
	for(uint32_t i = 1; i < convL4->getOutputDepth(); i++) {
		f<< ","<<convL4Bias[i];
	}
	f<< "};"<<std::endl;
	f<< std::endl;

	f<< "int16_t fcL6Weights["<<(fcL6->getNOUnit()) * (fcL6->getNIUnit() + 1)<<"] = {"<<std::endl;
	f<< fcL6Weights[0];
	for(uint32_t i = 1; i < (fcL6->getNOUnit()) * (fcL6->getNIUnit() + 1); i++) {
		f<< ","<<fcL6Weights[i];
	}
	f<< "};"<<std::endl;
	f<< std::endl;

	f<< "int16_t fcL7Weights["<<(fcL7->getNOUnit()) * (fcL7->getNIUnit() + 1)<<"] = {"<<std::endl;
	f<< fcL7Weights[0];
	for(uint32_t i = 1; i < (fcL7->getNOUnit()) * (fcL7->getNIUnit() + 1); i++) {
		f<< ","<<fcL7Weights[i];
	}
	f<< "};"<<std::endl;
	f<< std::endl;
	f<< std::endl;
	f<< "int16_t *getConvL0Weights(){return convL0Weights;}"<<std::endl;
	f<< "int16_t *getConvL2Weights(){return convL2Weights;}"<<std::endl;
	f<< "int16_t *getConvL4Weights(){return convL4Weights;}"<<std::endl;
	f<< "int16_t *getConvL0Bias(){return convL0Bias;}"<<std::endl;
	f<< "int16_t *getConvL2Bias(){return convL2Bias;}"<<std::endl;
	f<< "int16_t *getConvL4Bias(){return convL4Bias;}"<<std::endl;
	f<< "int16_t *getfcL6Weights(){return fcL6Weights;}"<<std::endl;
	f<< "int16_t *getfcL7Weights(){return fcL7Weights;}"<<std::endl;
	f<<std::endl;

	f.close();

	return EXIT_SUCCESS;
}

int32_t network::printWeightsToCsv(std::string CsvWeights){

	std::string fnCsvWeights = CsvWeights;
	std::fstream f;
	mkdir(fnCsvWeights.c_str(), S_IRWXU);
	fnCsvWeights +="Weights";
	fnCsvWeights += std::to_string(m_epoche);
	std::cout<< fnCsvWeights<< std::endl;

	f.open(fnCsvWeights, std::ios::out);
	//header
	f<< "Learnrate:" << convL0->getLearningRate() <<";"<< "Tanh;" << convL0->getActivationFunction()->getPower() << ";"<< "Inputfactor:input/127.5-1; " <<"Epoche: "<< m_epoche <<";"<<"DetecRate: "<<m_detectionRate<<std::endl;
	//weights
	f<< "Layer0" <<std::endl;
	convL0->printWeightsToCsv(f);
	f<< "Layer2" <<std::endl;
	convL2->printWeightsToCsv(f);
	f<< "Layer4" <<std::endl;
	convL4->printWeightsToCsv(f);

	f<< "Layer6" <<std::endl;
	fcL6->printWeightsToCsv(f);
	f<< "Layer7" <<std::endl;
	fcL7->printWeightsToCsv(f);

	f.close();

	return EXIT_SUCCESS;
}

int32_t network::printDetectionToCsv(std::string CsvDetection, int epoche){

	std::string fnCsvDetection = CsvDetection;
	std::fstream f;
	mkdir(fnCsvDetection.c_str(), S_IRWXU);
	fnCsvDetection += "Detection";
	fnCsvDetection += std::to_string(epoche);

	f.open(fnCsvDetection, std::ios::out);

	f<< "Detection Rate: "<<m_detectionRate << std::endl;
	for(uint32_t i = 0; i < NUMBER_CLASSES; i++) {
		f<< i<<": "<<m_falseDetectedHist[i]<<std::endl;

	}
	f<<std::endl;
	for(uint32_t y = 0; y < NUMBER_CLASSES; y++) {
		for(uint32_t x = 0; x < NUMBER_CLASSES; x++) {
			f << std::setw(3) << std::setfill('0')<<m_2DHistogram[x][y]<<";";
		}
		f<<std::endl;
	}

	f << "List of false Detected Images: " << std::endl;
	for(uint32_t i = 0; i < m_falseDetectedImages.size(); i++) {
		f << i<<". " << m_falseDetectedImages[i]<<" as "<< m_falseDetectedDetection[i] <<std::endl;
	}

	f.close();

	return EXIT_SUCCESS;
}

int32_t network::printOutput(uint32_t n){

	getFCLayer7()->printOutput(n);

	return EXIT_SUCCESS;
}
const float* network::getOutputs(){

	return fcL7->getOutputs();
}

int32_t network::calcAvarageError(int _actualImageCount){
	fcL7->calcErrorTotal();
	m_overallError += fcL7->getErrorTotal();

	m_avarageError = m_overallError/static_cast<float>(_actualImageCount+1);

	return EXIT_SUCCESS;
}

int32_t network::countDetectedImages(std::string strokeDataName, int target){
	bool isDetected;
	isDetected = fcL7->checkDetection(target);
	if(isDetected == 1) {
		m_nDetectedImages++;
		m_maxRightPropabilityCumultativ += m_maxProbability;
		m_avRightProbability = m_maxRightPropabilityCumultativ/m_nDetectedImages;
	}
	else{
		m_nFalseDetectedImages++;
		m_falseDetectedHist[target] +=1;
		m_falseDetectedImages.push_back(strokeDataName);
		int detection = fcL7->getDetection();
		m_falseDetectedDetection.push_back(detection);
		m_maxFalsePropabilityCumultativ += m_maxProbability;
		m_avFalseProbability = m_maxFalsePropabilityCumultativ/m_nFalseDetectedImages;
	}
	m_detectionRate = static_cast<float>(m_nDetectedImages)/static_cast<float>(m_nDetectedImages+m_nFalseDetectedImages);

	m_2DHistogram[fcL7->getDetection()][target] += 1;

	return EXIT_SUCCESS;
}

int32_t network::printFalseDetectedHist(){

	for(uint32_t i = 0; i < NUMBER_CLASSES; i++) {
		printf("%03i ", i);
	}
	std::cout<<std::endl;

	for(uint32_t i = 0; i < NUMBER_CLASSES; i++) {
		printf("%03i ", m_falseDetectedHist[i]);
	}
	std::cout<<std::endl;
	return EXIT_SUCCESS;
}

int32_t network::print2DHistogram(){

	for(uint32_t y = 0; y < NUMBER_CLASSES; y++) {
		for(uint32_t x = 0; x < NUMBER_CLASSES; x++) {
			if(x == y) {
				printf("\x1b[32m%02i\x1b[0m;", m_2DHistogram[x][y]);
			}
			else if(m_2DHistogram[x][y] > 0) {
				printf("\x1b[31m%02i\x1b[0m;", m_2DHistogram[x][y]);
			}
			else{
				printf("%02i;", m_2DHistogram[x][y]);
			}
		}
		std::cout<<std::endl;
	}

	return EXIT_SUCCESS;
}

int32_t network::calcDetection(){

	m_strokeN = 0;
	m_maxProbability = 0;
	const float* output = fcL7->getOutputs();
	for(uint32_t i = 0; i < NUMBER_CLASSES; i++) {
		if(m_maxProbability < output[i]) {
			m_maxProbability = output[i];
			m_strokeN = i;
		}
	}
	return m_strokeN;
}

int32_t network::calcPasses(int strokeN){
	return 5;
}

int32_t network::forward(cv::Mat img){

	//cv::imshow("imgTest", img);
	//cv::waitKey(500);
	float inp[48*48*3];
	float *input = convertInput(img, inp);

	/* for(int i = 0; i < 48*48*3; i++){
	     if(input[i] != input[i]){
	         std::cout<<"Error inputVol";
	     }
	   }*/

	/*clock_t timeStart;
	   clock_t timeNow;
	   double timeDiff;
	   clock_t timeStart2;
	   clock_t timeNow2;
	   double timeDiff2;
	   timeStart2 = clock();

	   timeStart = clock();
	 */
	convL0->forward(input);
	/*timeNow = clock();
	   timeDiff = (double)(timeNow - timeStart)/CLOCKS_PER_SEC;
	   std::cout <<"ConvL0: "<<timeDiff<<std::endl;

	   timeStart = clock();
	 */
	poolL1->maxPooling(convL0->getOutputVolume(), convL0->getOutputVolumeNet());
	/*timeNow = clock();
	   timeDiff = (double)(timeNow - timeStart)/CLOCKS_PER_SEC;
	   std::cout <<"MaxPoolL1: "<<timeDiff<<std::endl;

	   timeStart = clock();
	 */
	convL2->forward(poolL1->getOutputVolume());
	/*timeNow = clock();
	   timeDiff = (double)(timeNow - timeStart)/CLOCKS_PER_SEC;
	   std::cout <<"ConvL2: "<<timeDiff<<std::endl;

	   timeStart = clock();
	 */
	poolL3->maxPooling(convL2->getOutputVolume(), convL2->getOutputVolumeNet());

	/*timeNow = clock();
	   timeDiff = (double)(timeNow - timeStart)/CLOCKS_PER_SEC;
	   std::cout <<"MaxPoolL3: "<<timeDiff<<std::endl;

	   timeStart = clock();
	 */
	convL4->forward(poolL3->getOutputVolume());
	/*timeNow = clock();
	   timeDiff = (double)(timeNow - timeStart)/CLOCKS_PER_SEC;
	   std::cout <<"ConvL4: "<<timeDiff<<std::endl;

	   timeStart = clock();
	 */
	poolL5->maxPooling(convL4->getOutputVolume(), convL4->getOutputVolumeNet());
	/*timeNow = clock();
	   timeDiff = (double)(timeNow - timeStart)/CLOCKS_PER_SEC;
	   std::cout <<"MaxPoolL5: "<<timeDiff<<std::endl;

	   timeStart = clock();
	 */
	fcL6->forward(poolL5->getOutputVolume());
	fcL7->forward(fcL6->getOutputs());
	/*timeNow = clock();
	   timeDiff = (double)(timeNow - timeStart)/CLOCKS_PER_SEC;
	   std::cout <<"fcLayer: "<<timeDiff<<std::endl;

	   timeNow2 = clock();
	   timeDiff2 = (double)(timeNow2 - timeStart2)/CLOCKS_PER_SEC;
	   std::cout <<"TimeAll: "<<timeDiff2<<std::endl;
	 */

	calcDetection();
	return EXIT_SUCCESS;
}

int32_t network::backward(float *input, std::vector<float> binTarget){
	fcL7->lossFunction(binTarget);
	fcL7->backward(fcL7->getError(), fcL6->getOutputs(), fcL6->getOutputsNet());    // first backward pass
	fcL6->backward(fcL7->getErrorLowerLayer(), poolL5->getOutputVolume(), poolL5->getOutputNetVolume());    // second pass
	poolL5->upsampling(fcL6->getErrorLowerLayer());
	convL4->backward(poolL5->getErrorLowerLayer(), poolL3->getOutputVolume(), poolL3->getOutputNetVolume());
	poolL3->upsampling(convL4->getErrorLowerLayer());
	convL2->backward(poolL3->getErrorLowerLayer(), poolL1->getOutputVolume(), poolL1->getOutputNetVolume());
	poolL1->upsampling(convL2->getErrorLowerLayer());
	convL0->backward(poolL1->getErrorLowerLayer(), input, input);
	return EXIT_SUCCESS;
}

int32_t network::update(){
	fcL7->update();
	fcL6->update();
	convL4->update();
	convL2->update();
	convL0->update();
	return EXIT_SUCCESS;
}

void network::printAllErrors(){
	convL0->printErrorTotal();
	convL2->printErrorTotal();
	convL4->printErrorTotal();
	fcL6->printErrorTotal();
	fcL7->printErrorTotal();
	std::cout<<std::endl;
}

void network::setLearnRate(float rate){
	convL0->setLearningRate(rate);
	convL2->setLearningRate(rate);
	convL4->setLearningRate(rate);
	fcL6->setLearningRate(rate);
	fcL7->setLearningRate(rate);
}

network::network(){
	netInit();
}
