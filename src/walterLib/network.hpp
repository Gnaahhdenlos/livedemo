#ifndef __NETWORK_HPP__
#define __NETWORK_HPP__
// C++ std libraries
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>

// C std libraries
#include <cstdlib>
#include <cstdint>

// application specific includes
#include "convolutionLayer.hpp"
#include "poolingLayer.hpp"
#include "fcLayer.hpp"
#include "abstractNetwork.hpp"

// openCV includes
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <algorithm>    // std::random_shuffle
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand
//#include <Directory>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

class network : public abstractNetwork {
private:
convolutionLayer *convL0;                                  //Convolutional layer 0
poolingLayer *poolL1;                            //Pooling layer 1
convolutionLayer *convL2;                                  //Convolutional layer 2
poolingLayer *poolL3;                            //Pooling layer 3
convolutionLayer *convL4;                                  //Convolutional layer 4
poolingLayer *poolL5;                            //Pooling layer 5
fcLayer *fcL6;                                   //Fully connected layer 6
fcLayer *fcL7;                                   //Fully connected layer 7

float m_overallError;                               //Error of the last layer accumulated over all predictions
float m_avarageError;                               //overallError divided by the imageCount
int m_nDetectedImages;                              //number of detected images
int m_nFalseDetectedImages;                         //number of false detected images
float m_detectionRate;                              //detection rate
int m_epoche;                                       //number of the actual learning epoche

//just for statistics
float m_maxProbability;
float m_maxFalsePropabilityCumultativ;
float m_avFalseProbability;
float m_maxRightPropabilityCumultativ;
float m_avRightProbability;

int m_2DHistogram[7][7];                          //number of detections and false detectiosn listed per class
int m_falseDetectedHist[7];                        //number of false detections listed per class

int32_t m_strokeN;                                    //stroke number of the detected image
std::vector<std::string> m_falseDetectedImages;                 //names of the false detected images (just for statistic)
std::vector<int> m_falseDetectedDetection;         //detections of false detected images

/* Initialising the CNN by initialising the different layer with their specific sizes
 * @return  error code
 */
int32_t netInit();


public:
network();
~network();

/* Converts the input image into an inputarray and passes the data forward through the different layers.
 * At the end it calculates also the detection (with calcDetection())
 * @param   img cv::mat which contains the input image
 * @return  error code
 */
int32_t forward(cv::Mat);

/* Calculates the error of the last layer with the lossFunction(binTarget) and propagates the error
 * backwards through the layers
 * @param   input   input image stored in an array
 * @param   binTarget   vector which contains the target values
 * @return  error code
 */
int32_t backward(float*, std::vector<float>);

/* Updates the weights of every cnn layer based on the error calculated with backward()
 * @return  error code
 */
int32_t update();

/* Calculates the avarage error of the netoutput during training of all images
 * @param   _actualImageCount   number of images which are already forward propagated during training
 * @return  error code
 */
int32_t calcAvarageError(int);

/* Keeps an account of the statistics of m_nDetectedImages, m_avRightProbability, m_nFalseDetectedImages,
 * m_falseDetectedHist, m_falseDetectedImages, m_avFalseProbability, m_detetionRate and m_2DHistogram
 * @param   strokeDataName    string which contains the name of the stroke image
 * @param   target          target of the detection
 * @return  error code
 */
int32_t countDetectedImages(std::string, int);

/* Identifies the output with the highest value, which is the predicated stroke.
 * This value is stored in m_strokeN.
 * @return  error code
 */
int32_t calcDetection();

//not necessary for using (just implemented for the abstract class)
int32_t calcPasses(int);

/* Writes the detection rate, the not detected image names and the 2DHistogram into a CSV file
 * @param   CsvDetection  path to save the file
 * @return  error code
 */
int32_t printWeightsToCsv(std::string CsvWeights);

/* Reads the weight data from a Csv file and loads them into the specific weightarrays of each layer
 * @param   CsvWeights  path to the csv-file
 * @return  error code
 */
int32_t initWeights(std::string);

/* Reads the weigth data file(float), converts the weights into int16 with a specific shift and
 * saves the weights in a new csv-file
 * @param   CsvWeights      path to the csv-file
 * @param   CsvWeightsSafe  path to safe the new csv-file
 * @return  error code
 */
int32_t shiftWeights(std::string, std::string);

/* Reads the weigth data file(float), converts the weights into int16 with a specific shift and
 * saves the weights in a .h file in a format that includes already array structs and getmethodes.
 * @param   CsvWeights      path to the csv-file
 * @param   CsvWeightsSafe  path to safe the new file
 * @return  error code
 */
int32_t hardcodeWeights(std::string, std::string);

void clearDetectionRate() {
	m_detectionRate = 0.0f;
}
void clearNDetectedImages() {
	m_nDetectedImages = 0;
}
void clearNFalseDetectedImages() {
	m_nFalseDetectedImages = 0;
}

//-------print---
void printAllErrors();
int32_t printFalseDetectedHist();
int32_t printDetectionToCsv(std::string, int);
int32_t print2DHistogram();
int32_t printOutput(uint32_t);

//------set------
void setLearnRate(float rate);
void setEpoche(int _epoche){
	m_epoche = _epoche;
}

//------get------
/* Reads the learnrate from the weight csv-file which was used to training the net
 * @param   CsvFile path to the csv-file
 * @return  learnrate
 */
float getLearnRateFromCSV(std::string);

/* Reads the epoche from the weight csv-file which the net was trained
 * @param   CsvFile path to the csv-file
 * @return  epoche
 */
int getEpocheFromCSV(std::string);

convolutionLayer* getConvLayer0() {
	return convL0;
}
poolingLayer* getPoolLayer1() {
	return poolL1;
}
convolutionLayer* getConvLayer2() {
	return convL2;
}
poolingLayer* getPoolLayer3() {
	return poolL3;
}
convolutionLayer* getConvLayer4() {
	return convL4;
}
poolingLayer* getPoolLayer5() {
	return poolL5;
}
fcLayer* getFCLayer6() {
	return fcL6;
}
fcLayer* getFCLayer7() {
	return fcL7;
}
const float* getOutputs();
float getAvarageError() {
	return m_avarageError;
}
float getOverallError() {
	return m_overallError;
}
int getNDetectedImages() {
	return m_nDetectedImages;
}
int getNFalseDetectedImages() {
	return m_nFalseDetectedImages;
}
int getEpoche() {
	return m_epoche;
}

int* getFalseDetectedHist() {
	return m_falseDetectedHist;
}
float getDetectionRate() {
	return m_detectionRate;
}
int32_t getStrokeN() {
	return m_strokeN;
}
float getMaxProbability() {
	return m_maxProbability;
}
float getAvFalseProbability() {
	return m_avFalseProbability;
}
float getAvRightProbability() {
	return m_avRightProbability;
}
};

#endif
