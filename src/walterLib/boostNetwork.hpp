#ifndef __BOOSTNETWORK_HPP__
#define __BOOSTNETWORK_HPP__
// C++ std libraries
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <utility>

// C std libraries
#include <cstdlib>
#include <cstdint>

// application specific includes

#include "network.hpp"

// openCV includes
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <algorithm>    // std::random_shuffle
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand
//#include <Directory>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

class boostNetwork : public abstractNetwork {
private:
network *originalNet;                           //Net trained with origianl data
network *imadjustNet;                           //Net trained with imageadjusted data
network *histeqNet;                                     //Net trained with histequalised data
network *aheqNet;                                       //Net trained with adaptive histequalised data
network *conormNet;                                     //Net trained with contrast normalised data

const float *outputOriginal;                    //outputdata last layer of originalNet
const float *outputImadjust;                    //outputdata last layer of imadjustNet
const float *outputHisteq;                              //outputdata last layer of histeqNet
const float *outputAheq;                                //outputdata last layer of aheqNet
const float *outputConorm;                              //outputdata last layer of conormNet
float outputAvarage[43];                                //output avarage of all 5 Net outputdata

//just for statistics
float m_maxProbability;
float m_maxFalsePropabilityCumultativ;
float m_avFalseProbability;
float m_maxRightPropabilityCumultativ;
float m_avRightProbability;

int m_nDetectedImages;                                  //number of detected images
int m_nFalseDetectedImages;                             //number of false detected images
float m_detectionRate;                                  //percentage of detected images
int m_epoche;                                                   //number of the current learn epoche

int m_2DHistogram[43][43];                              //number of detections and false detections listed per class
int m_falseDetectedHist[43];                    //number of false detections listet per class
float m_ADetectionRate[10];                             //detectionrates calculated with different propability thresholds
int m_ADetectedImages[10];                              //numbr of detected images calculated with different propability thresholds
int m_AFalseDetectedImages[10];                 //number of false detected images calculated with different propability thresholds
int m_rightProbHistogram[101];                  //histogram of correct detected images listed per probality
int m_falseProbHistogram[101];                  //histogram of false detected images listed per probality
float m_relativeRightProbHistogram[101];                        //percentage histogram of correct detected images listed per probality
float m_relativeFalseProbHistogram[101];                        //percentage histogram of false detected images listed per probality

int m_strokeN;                                                  // stroke number of the detected image

std::vector<std::string> m_falseDetectedImages;                 //names of the false detected images(for statistic)

/* Initialising 5 different Nets
 * @return		error code
 */
int32_t netInit();


public:
boostNetwork();
~boostNetwork();

/* initialising the weight data of every net
 * @param   originalWeights string containing the path of the original Weight file
 * @param   imadjustWeights string containing the path of the imadjust Weight file
 * @param   histeqWeights string containing the path of the histeq Weight file
 * @param   aheqWeights string containing the path of the aheq Weight file
 * @param   conormWeights string containing the path of the conorm Weight file
 * @return  error code
 */
int32_t initWeights(std::string, std::string, std::string, std::string, std::string);

/* The input image is passed forward through the original net
 * It is also passed forward to the imadjust, histeq, aheq and conorm net after the input is
 * preprocessed by the specific contrast ajustment method.
 * @param   img the input image
 * @return  error code
 */
int32_t forward(cv::Mat);

//resetting variables
void clearDetectionRate() {
	m_detectionRate = 0.0f;
}
void clearNDetectedImages() {
	m_nDetectedImages = 0;
}
void clearNFalseDetectedImages() {
	m_nFalseDetectedImages = 0;
}

/* Calculates the avarage output of the 5 different nets and identifies the output with the
 * highest value, which is the predicated stroke. This value is stored in m_strokeN.
 * @return  error code
 */
int32_t calcDetection();

/* Checks how many of the single nets have the same prediction as the multi coloumn net
 * (Just for statistics)
 * @param   strokeN   number of predicted stroke
 * @return  passes  number of nets which has the same prediction
 */
int32_t calcPasses(int strokeN);

/* This function keep account of the statistics like m_relativeRightProbHistogram,
 * m_ADetectedImages, m_relativeFalseProbHistogram, m_AFalseDetectedImages, m_detectionRate,
 * m_ADetectionRate, m_2DHistogram and the list of not detected images
 * @param   strokeDataName    string which contains the name of the stroke image
 * @param   target          target of the detection
 * @return  error code
 */
int32_t countDetectedImages(std::string, int);
//-------print---
int32_t printDetectionRate();
int32_t printOutput(uint32_t);

/* Writes the detection rate, the not detected image names and the 2DHistogram into a CSV file
 * @param   CsvDetection  path to save the file
 * @return  error code
 */
int32_t printDetectionToCsv(std::string);


//------get------
const float* getOutputs(){
	return outputAvarage;
}
int getStrokeN(){
	return m_strokeN;
}
network* getOriginalNet(){
	return originalNet;
}
network* getImadjustNet(){
	return imadjustNet;
}
network* getHisteqNet(){
	return histeqNet;
}
network* getAheqNet(){
	return aheqNet;
}
float getMaxProbability() {
	return m_maxProbability;
}
float getAvFalseProbability() {
	return m_avFalseProbability;
}
float getAvRightProbability() {
	return m_avRightProbability;
}
float* getDetectionRates() {
	return m_ADetectionRate;
}
int* getnDetectedImages() {
	return m_ADetectedImages;
}
int* getnFalseDetectedImages() {
	return m_AFalseDetectedImages;
}
int* getRightProbHistogram() {
	return m_rightProbHistogram;
}
int* getFalseProbHistogram() {
	return m_falseProbHistogram;
}
float* getRelativeRightProbHistogram() {
	return m_relativeRightProbHistogram;
}
float* getRelativeFalseProbHistogram() {
	return m_relativeFalseProbHistogram;
}
};

#endif
