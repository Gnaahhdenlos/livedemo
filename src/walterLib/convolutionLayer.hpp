#ifndef __CONVOLUTIONLAYER_HPP__
#define __CONVOLUTIONLAYER_HPP__
#include <cstdlib>
#include <cstdint>
#include <cmath>

#include <iostream>
#include <vector>

#include <fstream>
#include <thread>

class convolutionActFunc {
public:
virtual float f(float) const =0;                                // activation function
virtual float d(float) const =0;                                // first order derivative of activation function

convolutionActFunc() {
}
virtual float getPower() = 0;
};

class convolutionActFunc_tanh : public convolutionActFunc {
public:
float a;

float f(float i) const {
	return 1.7159047*tanh(a*i);
}
float d(float i) const {
	float absolut = std::abs(i);
	if(absolut >= 0.755415f && absolut < 2.75f) { return 1.1864-0.4314 * absolut;}
	else if(absolut < 0.755415f) { return -0.388522*(absolut-1.7159047)*(absolut+1.7159047);}
	else {return 0; }
}
// float f(float i) const { return 1.f/(1.f + std::exp(-a*i)); }
//              float d(float i) const { float tmp = f(i); return tmp*(1-tmp); }

convolutionActFunc_tanh(float _a) : convolutionActFunc(), a(_a) {
}
float getPower() {
	return a;
}

};






class convolutionLayer {
private:
//-----------------------------------
//	i -> input
//	o -> output
//	f -> filter
//	n -> number
//-----------------------------------

uint32_t m_layerNumber;                         //number of the layer
uint32_t m_iPixelSize;                          //Size of the input image
uint32_t m_iDepth;                              //number of image Channels
uint32_t m_fSize;                                       //filterSize: the hight as well as the width of the filters
uint32_t m_stride;                                      //step size the filter takes horizontal and vertikal
uint32_t m_oDepth;                                      //number of dirfferent filters equals the depth of the output volume
uint32_t m_zeroPadding;                         //number of zeros on the image margine

uint32_t m_oSize;                                       //the hight as well as the width of the output
uint32_t m_nOUnit;                                      //number of output units
uint32_t m_nIUnit;                                      //number of input units
uint32_t m_nKUnit;                                      //number of Weights needed for one Kernel
uint32_t m_nFUnit;                                      //number of Weights needed for the whole Filter, including all Kernels
uint32_t m_nIPixel;                                     //number of input Pixel of one channel
uint32_t m_nOPixel;                                     //number of output Pixel of one channel
uint32_t m_nWeights;                            //number of weights of the whole layer

float m_learningRate;                           // learning rate for backward pass (backprop)

convolutionActFunc *m_actFunc;                          // activation function (and derivative)

float *m_fWeights;                                              // weights Filter,Volume, counts m_fSize^2*m_oDepth*m_iDepth elements
float *m_flippedFilterWeights;
float *m_fWeightsDelta;                                 // delta to weights, from backprop, before updating them
float *m_bias;
float *m_biasDelta;                                     //delta to bias, from backprop, before updating them
float *m_error;                                         //calculated error to compute the gradients
float *m_errorLowerLayer;                       //calculated error of the layer l-1

//std::vector <float> m_convError;
float *m_oVolume;                               // Volume of the output
float *m_oVolumeNet;                    // Volume of the output with activation function

std::fstream f;

/* Initialises the parameter of the convolutional layer
 * @return  error code
 */
int32_t convInit();

/* Calculates the error of the lower layer with the backpropagation algorithm
 * @param   _OVolume    outputVolume of the lowerlayer
 */
void calcErrLowerLayer(const float*);
/* Calculates the error of the lower layer with the backpropagation algorithm
 * but just for half the neurons, function is used for multithreading
 * @param   _OVolume    outputVolume of the lowerlayer
 */
void calcErrLowerLayer1(const float*);
/* Calculates the error of the lower layer with the backpropagation algorithm
 * but just for half the neurons, function is used for multithreading
 * @param   _OVolume    outputVolume of the lowerlayer
 */
void calcErrLowerLayer2(const float*);

/* Calculates the filter weights deltas by multiplying the specific values of the _OVolumen and the
 * specific error of the filter the weights. The result is multiplied by the learning rate
 * @param   _OVolume    outputVolume of the lowerlayer
 */
void calcfWeightsDelta(const float*);

/* Forward propagation of the input image by convolving with the filter weights.
 * This function only calculates the half of the convolutions, it is used for multithreading
 * @param   _inputVolume    input Volumen (image or previous outputvolumen)
 * @return  error code
 */
int32_t forward1(float*);
/* Forward propagation of the input image by convolving with the filter weights.
 * This function only calculates the half of the convolutions, it is used for multithreading
 * @param   _inputVolume    input Volumen (image or previous outputvolumen)
 * @return  error code
 */
int32_t forward2(float*);

protected:

public:

//	 _oDepth, _stride, _fSize, _iDepth, _iPixelSize, _layerNumber
convolutionLayer(uint32_t, uint32_t, uint32_t, uint32_t, uint32_t, uint32_t);
//if the architecture uses convolution with stride > 1 the function sets the zero padding on the
int32_t setZeroPadding();
/* Forward propagation of the input volume by convolving with the filter weights.
 * @param   _inputVolume    input Volumen (image or previous outputvolumen)
 * @return  error code
 */
int32_t forward(float*);
/* The backward propagation calculates the ErrorLowerLayer and the filter weights delta of the
 * current layer by using multithreading
 * @param   prev_error  the calculated error of this layer
 * @param   _OVolume    outputVolume of the lower layer
 * @param   _iVolumeNet net inputVolume(before the act func)
 * @return  error code
 */
int32_t backward(float*, const float*, float *_iVolumeNet);

/* Updating the filter weights and the bias by adding the calculated delta values
 * @return  error code
 */
int32_t update();

//print methods
friend std ::ostream& operator<<(std::ostream& os, const convolutionLayer& l);
void printOutput();
void printErrorTotal();
void printError();
void printErrorLowerLayer();
void printfWeightsDelta();
void printfWeights();
int32_t printWeightsToCsv(std::fstream&);


// set methods
void setWeights(const float* _Weights) {
	for (uint32_t iOUnit = 0; iOUnit < m_fSize*m_fSize*m_oDepth*m_iDepth; ++iOUnit) {
		m_fWeights[iOUnit] = _Weights[iOUnit];
	}
}
void setBiases(const float* _Biases) {
	for (uint32_t iOUnit = 0; iOUnit < m_oDepth; ++iOUnit)
		m_bias[iOUnit] = _Biases[iOUnit];
}
void setLearningRate(const float _learningRate){
	m_learningRate = _learningRate;
}


// get methods
uint32_t getInputPixelSize() {
	return m_iPixelSize;
}
uint32_t getInputDepth() {
	return m_iDepth;
}
uint32_t getFilterSize() {
	return m_fSize;
}
uint32_t getStride() {
	return m_stride;
}
uint32_t getOutputDepth() {
	return m_oDepth;
}
uint32_t getZeroPadding() {
	return m_zeroPadding;
}
uint32_t getOutputSize() {
	return m_oSize;
}
uint32_t getNumberOfOutputUnits() {
	return m_nOUnit;
}
uint32_t getNumberOfInputUnits() {
	return m_nIUnit;
}
uint32_t getLayerNumber() {
	return m_layerNumber;
}
uint32_t getNumberOfWeights() {
	return m_nWeights;
}

float getLearningRate() {
	return m_learningRate;
}

float* getFilterWeights() {
	return m_fWeights;
}
float* getFilterWeightsDelta() {
	return m_fWeightsDelta;
}
float* getBias() {
	return m_bias;
}
float* getBiasDelta() {
	return m_biasDelta;
}
float* getError() {
	return m_error;
}
float* getErrorLowerLayer() {
	return m_errorLowerLayer;
}
float* getOutputVolume() {
	return m_oVolume;
}
float* getOutputVolumeNet() {
	return m_oVolumeNet;
}

convolutionActFunc* getActivationFunction() {
	return m_actFunc;
}

};

#endif
