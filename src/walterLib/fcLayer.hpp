#ifndef __FC_LAYER_HPP__
#define __FC_LAYER_HPP__

#include <cstdlib>
#include <cstdint>
#include <cmath>

#include <iostream>
#include <vector>
#include <fstream>
#include <float.h>

class fcActFunc {
public:
virtual float f(float) const =0;                                // activation function
virtual float d(float) const =0;                                // first order derivative of activation function

fcActFunc() {
}
virtual float getPower() = 0;
};

class fcActFunc_tanh : public fcActFunc {
public:
float a;

float f(float i) const {
	return 1.7159047*tanh(a*i);
}
float d(float i) const {
	float absolut = std::abs(i);
	if(absolut >= 0.755415f && absolut < 2.75f) { return 1.1864-0.4314 * absolut;}
	else if(absolut < 0.755415f) { return -0.388522*(absolut-1.7159047)*(absolut+1.7159047);}
	else {return 0; }
}

fcActFunc_tanh(float _a) : fcActFunc(), a(_a) {
}
float getPower() {
	return a;
}
};

class fcActFunc_sigmoid : public fcActFunc {
public:
float a;

float f(float i) const {
	return 1.f/(1.f + std::exp(-a*i));
}
float d(float i) const {
	float tmp = f(i); return tmp*(1-tmp);
}

fcActFunc_sigmoid(float _a) : fcActFunc(), a(_a) {
}
float getPower() {
	return a;
}
};


class fcLayer {
private:
uint32_t m_layerNumber;                         // Number of the Layer
int32_t m_isOLayer;                                     // is Output Layer
uint32_t m_nOUnit;                                      // number of output units (neurons)
uint32_t m_nIUnit;                                      // number of input units (neurons of previous layer)


float m_learningRate;                           // learning rate for backward pass (backprop)

float *m_pWeight;                                               // weights of neural network, counts nOUnit*(nIUnit+1) elements MATRIX
float *m_pWeightDelta;                                  // delta to weights, from backprop, before updating them (e.g. adjust hiddenlayer before)
float *m_pOutput;                                               // VECTOR
float *m_pOutputNet;
fcActFunc *m_actFunc;                           // activation function (and derivative)


float *m_error;                 //error of layer l+1
float *m_errorLowerLayer;                 //the error of the layer l
float m_errorTotal;

/* Initialises the fcLayer Object. All weight arrays are allocated and initialised with random numbers between -0.05 and 0.05.
 * All error and output arrays are allocated and initialiesed with 0.
 * @return      error code
 */
int32_t fInit();
protected:


public:
//fcLayer construktor
fcLayer();
//specific fcLayer construktor(_nIUnit, _nOUnit, _isOLayer, _layerNumber)
fcLayer(uint32_t, uint32_t, int32_t, uint32_t);
//fcLayer destructor
~fcLayer();

/* Passes the input (output of prev layer) forward through the layer.
 * The activation function depends on whether its the output layer or a hidden layer.
 * The hidden layer uses tanh and the output layer softmax as act func
 * @param       inp	input volume(output of prev layer)
 * @return              error code
 */
int32_t forward(const float*);

/* Calculates the delta weights and if its not the lowest layer it calcs the error of the lower layer.
 * @param _OVolume	output Volume of the lower layer (layer n-1)
 * @param       prev_error	error of the higher layer or calc error of lossFunction
 * @return                              error code
 */
int32_t backward( float*, const float*, float*);

/* The highest layer needs to calc the own error with the lossFunction
 * This function calcs the output error by subtracting the target from the real output.
 * @param       _target target vector which is 1 if its the target signs number else 0
 * @return                      error code
 */
int32_t lossFunction(std::vector<float>);

/* Updates all weights of the layer by adding the Delta Weights
 * @return              error code
 */
int32_t update();

/* Calculates the total meansquared error of the layer (not necessary for training, just for statistics)
 * @return              error code
 */
int32_t calcErrorTotal();

/* Checks if the number of the highest output equals the target sign number
 * @return (imax == target)
 */
bool checkDetection(uint32_t);

/* Detects the output with the highest value and returns his corresponding sign number
 * @return	signN	number of the sign class which the net detected
 */
int32_t getDetection();
//set methods
void setActFunc(fcActFunc *_actFunc) {
	m_actFunc = _actFunc;
}
void setNOUnit(uint32_t _nOUnit) {
	m_nOUnit = _nOUnit; fInit();
}
void setNIUnit(uint32_t _nIUnit) {
	m_nIUnit = _nIUnit; fInit();
}
void setWeightsAndBias(const float* _pWeights) {
	for (uint32_t i = 0; i < m_nOUnit * (m_nIUnit + 1); i++) {
		// std::cout<<_pWeights[i];
		m_pWeight[i] = _pWeights[i];
	}
}
void setWeights(const float* _pWeights) {
	for (uint32_t iOUnit = 0; iOUnit < m_nOUnit; ++iOUnit)
		for (uint32_t iIUnit = 0; iIUnit < m_nIUnit; ++iIUnit)
			m_pWeight[iOUnit * (m_nIUnit + 1) + iIUnit] = _pWeights[iOUnit * m_nIUnit + iIUnit];
}
void setBiases(const float* _pBiases) {
	for (uint32_t iOUnit = 0; iOUnit < m_nOUnit; ++iOUnit)
		m_pWeight[iOUnit * (m_nIUnit + 1) + m_nIUnit] = _pBiases[iOUnit];
}
void setLearningRate(const float _learningRate) {
	m_learningRate = _learningRate;
}

//print
friend std ::ostream& operator<<(std::ostream& os, const fcLayer& l);

/* Prints the error of every output neuron of a specific layer
 */
void printErr();

/* Prints the error of every output neuron of the lower layer
 */
void printErrorLowerLayer();

/* Calculates and prints the total meansquared error of the layer (not necessary for training, just for statistics)
 */
void printErrorTotal();

/* Prints the output of every neuron of the layer
 */
void printOutput();

/* Prints the output of every neuron of the layer. If the detection is correct, the specific output is marked green.
 * Else the prediction is marked red and the target is marked blue.
 * @param	target	 number of the specific neuron
 */
void printOutput(uint32_t);

/* Prints the net output of every neuron of the layer
 */
void printOutputNet();

/* Prints every weight of every neuron of the layer
 */
void printWeights();

/* Prints every weight of every neuron of the layer to a csv file
 * @return              error code
 */
int32_t printWeightsToCsv(std::fstream& );

//get methods
const uint32_t getNIUnit() const {
	return m_nIUnit;
}
const uint32_t getNOUnit() const {
	return m_nOUnit;
}
const float getLearningRate() const {
	return m_learningRate;
}
const fcActFunc* getActFunc() const {
	return m_actFunc;
}
const float* getOutputs() const {
	return m_pOutput;
}
float* getOutputsNet() {
	return m_pOutputNet;
}
float* getWeights() {
	return m_pWeight;
}
const float* getWeightsDelta() const {
	return m_pWeightDelta;
}
float* getError() {
	return m_error;
}
float* getErrorLowerLayer() {
	return m_errorLowerLayer;
}
float getErrorTotal() {
	return m_errorTotal;
}
fcActFunc* getActivationFunction() {
	return m_actFunc;
}
};

#endif // ifndef __FC_LAYER_HPP__
