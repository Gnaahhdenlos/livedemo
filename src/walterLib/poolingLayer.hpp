#ifndef __POOLINGLAYER_HPP__
#define __POOLINGLAYER_HPP__
#include <cstdlib>
#include <cstdint>
#include <cmath>

#include <iostream>
#include <vector>
#include <fstream>


class poolingLayer {
private:
//uint32_t m_layerNumber;
uint32_t m_iSize;                               //Input Size of the Layer
uint32_t m_fSize;                               //the hight as well as the width of the filters
uint32_t m_stride;                                      //step size the filter takes horizontal and vertikal
uint32_t m_ioDepth;                             //Depth of the convLayer equals the Depth of the Pooling layer

uint32_t m_oSize;

float *m_oVolume;                               //output Volume
float *m_oVolumeNet;                    //net output Volume
float *m_error;                         //error of this layer
float *m_errorLowerLayer;                       //the error of the lower layer

std::fstream f;

/* Initiliasing of the max pooling layer by declaring the variables
 * @return  error code
 */
int32_t maxPoolingInit();


protected:

public:

poolingLayer(uint32_t, uint32_t, uint32_t, uint32_t, uint32_t);
/* Determining the max value of 4 adjacent values of one subsquare
 * This is done for the output Volume and the net output Volume of the lower layer
 * @param   _inputVolume    the output Volumen of the lower layer
 * @param   _inputVolumeNet the net output Volume of the lower layer
 * @return  error code
 */
int32_t maxPooling(float*, float*);

/* Calculating the error of the lower layer by upsampling the error.
 * @param   _error  error of neurons of this layer
 * @return  error code
 */
int32_t upsampling(float*);

//get methods
uint32_t getInputSize() {
	return m_iSize;
}
uint32_t getFilterSize() {
	return m_fSize;
}
uint32_t getStride() {
	return m_stride;
}
uint32_t getIODepth() {
	return m_ioDepth;
}
uint32_t getOutputSize() {
	return m_oSize;
}

float* getOutputVolume() {
	return m_oVolume;
}
float* getOutputNetVolume() {
	return m_oVolumeNet;
}
float* getError() {
	return m_error;
}
float* getErrorLowerLayer() {
	return m_errorLowerLayer;
}


//print functions
void printOutput();
void printErrorLowerLayer();

};

#endif
