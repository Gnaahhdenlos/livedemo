#ifndef __ABSTRACTNETWORK_HPP__
#define __ABSTRACTNETWORK_HPP__
// C++ std libraries
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <utility>

// C std libraries
#include <cstdlib>
#include <cstdint>

// application specific includes

// openCV includes
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <algorithm>    // std::random_shuffle
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand
//#include <Directory>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

//Base Class
class abstractNetwork {
private:
virtual int32_t netInit() = 0;


public:
//boost cnn

virtual int32_t forward(cv::Mat) = 0;
virtual int32_t countDetectedImages(std::string, int) = 0;

virtual int32_t printOutput(uint32_t) = 0;
virtual const float* getOutputs() = 0;
virtual int32_t calcDetection() = 0;
virtual int32_t calcPasses(int) = 0;
virtual int32_t getStrokeN() = 0;
virtual float getMaxProbability() = 0;

/* translating, rotating and rescaling the input training image randomly
 * @param img	the input Image
 * @return imgDst input Image after transforming
 */
cv::Mat preprocess(cv::Mat);

/* adjusts the image.
 * @param img	the input Image
 * @param tol   the toleranz from 0-100
 */
cv::Mat convertToImadjust(cv::Mat, int);

/* Does histogram equalise on the image
 * @param img		input image
 * @return hsteq        histogram equalised image
 */
cv::Mat convertToHisteq(cv::Mat);

/* Does adaptive histogram equalises on the image
 * @param img		input image
 * @return aheq         adapt histogram equalised image
 */
cv::Mat convertToAheq(cv::Mat, int);

/* Does a contrast equalisation
 * It substracts the mean value of the gaussian blurr Y-Channel from the original Y-Channel image value.
 * Divide the result of the substraction by the squared and another time gaussian blurred result of the substraction.
 * @param img	the input Image
 * @return cont	the contrast equalised image
 */
cv::Mat convertToContrast(cv::Mat);

/* converts the input image.
 * the input mat has values from 0-255 and the output array has values from -1 to 1
 * @param img	the input Image
 * @return inp  the converted image allocated in an array
 */
float* convertInput(cv::Mat img, float *input);

/* Returns a string with the name of the stroke which belongs to a specific stroke number
 * @param strokeN the number of the stroke
 * @return strokeName the name of the stroke
 */
std::string getStrokeName(int);

/* Returns a string with the name of the stroke which belongs to a specific stroke number
 * @param strokeN the number of the stroke
 * @return strokeName the name of the stroke
 */
std::string getStrokeNameThreeClasses(int);


/* creates a vector of pairs(string, int). The string contains the image name and the int is the corresponding stroke number.
 * @param fnPrefix	string that contains the Prefix of the datafolder
 * @return images a vector of image names and the corresponding stroke number
 */
std::vector<std::pair<std::string, int> > createImageVector(std::string);
//-------print---
int32_t printDetectionToCsv(std::string, int);
int32_t printDetectionToCsv(std::string);


};

#endif
