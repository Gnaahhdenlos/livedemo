
// C++ std libraries
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <utility>

// C std libraries
#include <cstdlib>
#include <cstdint>

// application specific includes

#include "abstractNetwork.hpp"

// openCV includes
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <random>
#include <algorithm>    // std::random_shuffle
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand
//#include <Directory>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

#define UNIFORM_DISTRIBUTION_START      -2.0f
#define UNIFORM_DISTRIBUTION_END    2.0f
#define PI 3.14159265


cv::Mat abstractNetwork::preprocess(cv::Mat img){
	cv::Mat imgTmp;
	cv::Mat imgDst;
	double randomN;         //random Number between -2;2
	double scl;                     //scaling factor
	double translate;       //translation factor
	int translateSize;      //Pixelnumber of the translation in each direction

	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> d(UNIFORM_DISTRIBUTION_START, UNIFORM_DISTRIBUTION_END);
	randomN= d(gen);
	//translating the Image 0-2 Pixel in each direction
	translate = (randomN+2)/2;

	scl = static_cast<double>(img.rows + translate) / static_cast<double>(img.rows);

	cv::resize(img, imgTmp, cv::Size(0,0), scl, scl, cv::INTER_LINEAR);
	translateSize = imgTmp.rows -img.rows;

	if(translateSize!=0) {
		randomN = d(gen);
		int Y1 = (randomN+2+1)/(2*(3-translateSize));
		randomN = d(gen);
		int X1 = (randomN+2+1)/(2*(3-translateSize));
		int Y2 = imgTmp.rows - translateSize + Y1;
		int X2 = imgTmp.rows - translateSize + X1;
		imgTmp.rowRange(Y1, Y2 ).colRange(X1, X2).copyTo(imgTmp);
	}

	//rotating the Image -5;5 degree

	double angle;
	angle = d(gen)*2.5f;
	// get rotation matrix for rotating the image around its center
	cv::Point2f center(imgTmp.cols/2.0, imgTmp.rows/2.0);

	int cutSize = std::abs(tan ( angle * PI / 180.0 ) * imgTmp.rows)+1;

	//std::cout<<"Angle: "<<angle<<"cutSize: "<<cutSize<<std::endl;
	cv::Mat rot = cv::getRotationMatrix2D(center, angle, 1.0);
	// determine bounding rectangle
	cv::Rect bbox = cv::RotatedRect(center,imgTmp.size(), angle).boundingRect();
	// adjust transformation matrix
	rot.at<double>(0,2) += bbox.width/2.0 - center.x;
	rot.at<double>(1,2) += bbox.height/2.0 - center.y;

	cv::warpAffine(imgTmp, imgDst, rot, bbox.size());

	imgDst.rowRange(cutSize, imgDst.rows - cutSize).colRange(cutSize, imgDst.rows - cutSize).copyTo(imgDst);

	scl = static_cast<double>(img.rows) / static_cast<double>(imgDst.rows);
	cv::resize(imgDst, imgDst, cv::Size(0,0), scl, scl, cv::INTER_LINEAR);

	//random resizing between factor 1-1.1
	cutSize = (d(gen)+2+1)/2;
	//std::cout<<"cutSize: "<<cutSize<<std::endl;
	imgDst.rowRange(cutSize, imgDst.rows - cutSize).colRange(cutSize, imgDst.rows - cutSize).copyTo(imgDst);
	scl = static_cast<double>(img.rows) / static_cast<double>(imgDst.rows);
	cv::resize(imgDst, imgDst, cv::Size(0,0), scl, scl, cv::INTER_LINEAR);

	return imgDst;
}


cv::Mat abstractNetwork::convertToImadjust(cv::Mat img, int tol){
	cv::Mat imadjust;
	cvtColor( img, imadjust, CV_RGB2HSV );
	cv::Vec2i in = cv::Vec2i(0, 255);
	cv::Vec2i out = cv::Vec2i(0, 255);

	tol = std::max(0, std::min(100, tol));

	if (tol > 0) {
		// Compute in and out limits
		// Histogram
		std::vector<int> hist(256, 0);
		for (int32_t r = 0; r < img.rows; ++r) {
			for (int32_t c = 0; c < img.cols; ++c) {
				cv::Vec3b pixel = img.at<cv::Vec3b>(r,c);
				hist[pixel.val[2]]++;
			}
		}

		// Cumulative histogram
		std::vector<int> cum = hist;
		for (uint32_t i = 1; i < hist.size(); ++i) {
			cum[i] = cum[i - 1] + hist[i];
		}

		// Compute bounds
		int total = img.rows * img.cols;
		int low_bound = total * tol / 100;
		int upp_bound = total * (100-tol) / 100;
		in[0] = distance(cum.begin(), lower_bound(cum.begin(), cum.end(), low_bound));
		in[1] = distance(cum.begin(), lower_bound(cum.begin(), cum.end(), upp_bound));
	}

	// Stretching
	float scale = float(out[1] - out[0]) / float(in[1] - in[0]);
	for (int r = 0; r < imadjust.rows; ++r) {
		for (int c = 0; c < imadjust.cols; ++c) {
			cv::Vec3b pixel = img.at<cv::Vec3b>(r,c);
			int vs = std::max(static_cast<int>(pixel.val[2] - in[0]), 0);
			int vd = std::min(static_cast<int>(vs * scale + 0.5f) + out[0], out[1]);
			pixel = imadjust.at<cv::Vec3b>(r,c);
			pixel.val[2] = vd;
			imadjust.at<cv::Vec3b>(r,c) = pixel;
		}
	}
	cvtColor( imadjust, imadjust, CV_HSV2RGB );
	return imadjust;
}



cv::Mat abstractNetwork::convertToHisteq(cv::Mat img){
	cv::Mat histeq;
	std::vector<cv::Mat> channels;
	cvtColor(img, histeq, CV_BGR2HSV);
	cv::split(histeq,channels);
	cv::equalizeHist(channels[2], channels[2]); //equalize histogram on the 3rd channel (V)
	cv::merge(channels,histeq); //merge 3 channels including the modified 3rd channel into one image
	cvtColor(histeq, histeq, CV_HSV2BGR); //converting the color to BGR

	return histeq;
}


cv::Mat abstractNetwork::convertToAheq(cv::Mat img, int clipLim){
	cv::Mat aheq;
	std::vector<cv::Mat> channels;
	cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
	clahe->setClipLimit(clipLim);
	cvtColor(img, aheq, CV_BGR2HSV); //change the color image from BGR to YCrCb format
	cv::split(aheq,channels); //split the image into channels
	clahe->apply(channels[2],channels[2]);
	cv::merge(channels,aheq); //merge 3 channels including the modified 1st channel into one image

	cvtColor(aheq, aheq, CV_HSV2BGR); //change the color image from YCrCb to BGR format (to display image properly)

	return aheq;
}

cv::Mat abstractNetwork::convertToContrast(cv::Mat img){

	cv::Mat cont;

	//New Contrast
	cv::Mat yuv, floatc0, blur, num, den;
	// convert to grayscale
	cv::cvtColor(img, yuv, CV_BGR2YUV);

	std::vector<cv::Mat> channels;
	cv::split(yuv,channels);

	// convert to floating-point image
	channels[0].convertTo(floatc0, CV_32F, 1.0/255.0);

	// numerator = img - gauss_blur(img)
	cv::GaussianBlur(floatc0, blur, cv::Size(15,15), 0);
	num = floatc0 - blur;

	// denominator = sqrt(gauss_blur(img^2))
	cv::GaussianBlur(num.mul(num), blur, cv::Size(15,15), 0);
	cv::pow(blur, 0.5, den);

	// output = numerator / denominator
	floatc0 = num / den;
	floatc0.convertTo(channels[0], CV_8U, 255.0);

	cv::merge(channels,cont);

	cvtColor(cont, cont, CV_YUV2BGR);

	return cont;
}


std::string abstractNetwork::getStrokeName(int strokeN){
	std::string strokeName;

	switch(strokeN) {
	case 0: strokeName = "default"; break;
	case 1: strokeName = "forehand-spin"; break;
	case 2: strokeName = "forehand-slice"; break;
	case 3: strokeName = "backhand-spin"; break;
	case 4: strokeName = "backhand-slice"; break;
	case 5: strokeName = "serve"; break;
	case 8: strokeName = ""; break;
	case 9: strokeName = ""; break;
	case 10: strokeName = ""; break;
	case 11: strokeName = ""; break;
	case 12: strokeName = ""; break;
	case 13: strokeName = ""; break;
	case 14: strokeName = ""; break;
	case 15: strokeName = ""; break;
	default: strokeName = "default"; break;
	}
	return strokeName;
}

std::string abstractNetwork::getStrokeNameThreeClasses(int strokeN){
	std::string strokeName;

	switch(strokeN) {
	case 0: strokeName = "forehand"; break;
	case 1: strokeName = "backhand"; break;
	case 2: strokeName = "serve"; break;
	default: strokeName ="default"; break;
	}
	return strokeName;
}


float* abstractNetwork::convertInput(cv::Mat img, float *input){
	float *inp = input;

	for (int i = 0; i < img.cols; i++) {
		for (int j = 0; j < img.rows; j++) {
			cv::Vec3b intensity = img.at<cv::Vec3b>(j, i);
			for(int k = 0; k < img.channels(); k++) {
				//std::cout<<(static_cast<float>(intensity.val[k]))<<std::endl;
				inp[img.rows*img.cols*k + i*img.rows + j] = static_cast<float>(intensity.val[k])/127.5f-1.0f;
			}
		}
	}
	return inp;
}



std::vector<std::pair<std::string, int> > abstractNetwork::createImageVector(std::string fnPrefix){
	std::vector<std::pair<std::string, int> > images;
	std::string fnFolder;
	std::string fnCsv;
	DIR *dir;
	std::vector<std::string> directories;
	const char *fnPrefixChar = fnPrefix.c_str();
	struct dirent *dirpointer;
	/* Verzeichnis öffnen */
	if((dir=opendir(fnPrefixChar)) != NULL) {
		/* komplettes Verzeichnis Eintrag für Eintrag auslesen */
		while((dirpointer=readdir(dir)) != NULL) {
			//printf("%s\n",(*dirpointer).d_name);
			if((*dirpointer).d_name[0]!='.') {
				directories.push_back(std::string((*dirpointer).d_name));
			}
		}
	}

	for (uint32_t i = 0; i < directories.size(); ++i) {

		fnFolder = fnPrefix;
		//std::cout<<directories[i]<<std::endl;
		fnFolder += directories[i];
		fnFolder += "/";
		fnCsv = fnFolder;
		fnCsv += "imgList";
		fnCsv += directories[i];
		fnCsv += ".csv";


		std::ifstream csv(fnCsv.c_str());
		if (!csv.is_open()) {
			printf("Unable to open csv file %s.\n", fnCsv.c_str());
			// return EXIT_FAILURE;
		}
		std::string line;
		std::getline(csv, line);
		std::string fnImg;
		int32_t classid;

		while (std::getline(csv, line)) {

			fnImg = line.substr(0, line.find(";"));
			line = line.substr(line.find(";")+1);
			line = line.substr(0, line.find(";"));
			std::istringstream iss(line);
			iss >> classid;

			images.push_back(std::make_pair(fnImg,classid));

		}
	}
	return images;
}
