//
//  qrCodeScanner.cpp
//
//
//  Created by Henri Kuper on 20.11.17.
//
//

#include "qrCodeScanner.hpp"

using namespace std;
using namespace zbar;
using namespace cv;


QRCodeScanner::QRCodeScanner(){
	initQRCodeScanner();
}

QRCodeScanner::~QRCodeScanner(){

}

void QRCodeScanner::initQRCodeScanner(){
	m_cameras = new Camera();
	m_cameras->setFrameRate(10.0f);
	m_cameras->setAutoExposureTimeUpperLimit(15000.0f);

	m_scanner.set_config(ZBAR_NONE, ZBAR_CFG_ENABLE, 1);
	m_foundQRCodeRight = false;
	m_foundQRCodeLeft = false;
	m_frameCount = 0;
	m_maxFrameCount = 100;
	m_qrCodeLeft = "test1";
	m_qrCodeRight = "test2";
}

int32_t QRCodeScanner::findQRCode(){
	cout<<"FindQRCode"<<endl;
	int numberOfDetectedQRCodes = 0;
	thread frameGrabbingThread(&Camera::frameGrabbing, m_cameras);

	while(m_frameCount < m_maxFrameCount && (!m_foundQRCodeRight || !m_foundQRCodeLeft)){
		cout<<"FrameCount: "<<m_frameCount<<endl;
		m_frameCount++;
		Mat frameLeft, frameRight;
		while(m_cameras->getLowerBufferSize() < 2){
			this_thread::sleep_for(chrono::milliseconds(10));
			//cout<<"Waiting"<<endl;
		}

		cvtColor(m_cameras->getFirstFrameLeft(),frameLeft,CV_BGR2GRAY);
		cvtColor(m_cameras->getFirstFrameRight(),frameRight,CV_BGR2GRAY);

		if(!m_foundQRCodeLeft){
			m_foundQRCodeLeft = scanFrame(frameLeft, m_qrCodeLeft);
		}
		if(!m_foundQRCodeRight){
			m_foundQRCodeRight = scanFrame(frameRight, m_qrCodeRight);
		}

		cout<<"FoundRight: "<<m_foundQRCodeRight<<" FoundLeft: "<<m_foundQRCodeLeft<<endl;
		m_cameras->deleteFirstFrameLeft();
		m_cameras->deleteFirstFrameRight();

	}
	m_cameras->setStopRunning(true);
	frameGrabbingThread.join();

	if(m_foundQRCodeLeft == true){
		numberOfDetectedQRCodes++;
	}
	if(m_foundQRCodeRight == true){
		numberOfDetectedQRCodes++;
	}
	m_cameras->reset();
	return numberOfDetectedQRCodes;
}

bool QRCodeScanner::scanFrame(Mat &frame, string &qrCode){
	bool foundQRCode = false;
	int width = frame.cols;
	int height = frame.rows;
	Mat imgout = frame.clone();//Mat::zeros(height, width, CV_8UC3);
	uchar *raw = (uchar *)frame.data;
	// wrap image data
	Image image(width, height, "Y800", raw, width * height);
	// scan the image for barcodes
	int n = m_scanner.scan(image);
	// extract results
	for(Image::SymbolIterator symbol = image.symbol_begin(); symbol != image.symbol_end(); ++symbol) {
		foundQRCode = true;
		qrCode = symbol->get_data();
		vector<Point> vp;
		// do something useful with results
		cout << "decoded " << symbol->get_type_name() << " symbol "  << symbol->get_data() <<" "<< endl;
		int n = symbol->get_location_size();
		for(int i=0;i<n;i++){
			vp.push_back(Point(symbol->get_location_x(i),symbol->get_location_y(i)));
		}
		RotatedRect r = minAreaRect(vp);
		Point2f pts[4];
		r.points(pts);
		for(int i=0;i<4;i++){
			line(imgout,pts[i],pts[(i+1)%4],Scalar(255,0,0),3);
		}
		cout<<"Angle: "<<r.angle<<endl;
	}
	// imshow("imgout.jpg",imgout);
	// waitKey(1);
	//clean up
	image.set_data(NULL, 0);
	return foundQRCode;
}

void QRCodeScanner::reset(){
	m_foundQRCodeRight = false;
	m_foundQRCodeLeft = false;
	m_frameCount = 0;
}
