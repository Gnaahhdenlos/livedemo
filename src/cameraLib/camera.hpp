//
//  camera.hpp
//
//
//  Created by Henri Kuper on 05.11.17.
//
//

#ifndef camera_hpp
#define camera_hpp

#include <stdio.h>
#include <stdlib.h>
#include <thread>
#include <numeric>
#include <vector>
//#include <queue>
#include <mutex>

// Include files to use the PYLON API.
#include <pylon/PylonIncludes.h>
#include <pylon/usb/BaslerUsbInstantCamera.h>

//opencv header
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/videoio.hpp"

class Camera{
private:
size_t m_numberOfCamerasToUse; //m_numberOfCamerasToUse
std::vector<cv::Mat> m_bufferOriginalLeft;
std::vector<cv::Mat> m_bufferOriginalRight;

cv::Mat m_cameraMatrix, m_distortionCoefficients;
cv::Mat m_undistortionRemap1, m_undistortionRemap2;

bool m_withUndistortion;

float m_inputVideoResizingFactor;

float m_frameRate;
float m_exposureTime;
float m_autoExposureTimeLowerLimit;
float m_autoExposureTimeUpperLimit;

int m_frameWidth;
int m_frameHeight;
int m_offSetY;

int m_maxFrameBuffer;

int m_frameCountCameraLeft;
int m_frameCountCameraRight;

int m_frameCountVideoLeft;
int m_frameCountVideoRight;

int m_bufferSizeRight;
int m_bufferSizeLeft;

int m_fourcc;

bool m_stopRunning;

std::mutex m_mutexBufferLeft;
std::mutex m_mutexBufferRight;

std::mutex m_mutexFrameCountVideoLeft;
std::mutex m_mutexFrameCountVideoRight;

std::mutex m_mutexFrameCountCameraLeft;
std::mutex m_mutexFrameCountCameraRight;

cv::TickMeter tm;
std::vector<double> video_times;

void cameraInit();
void camerasInit();
void updateBufferSizeRight();
void updateBufferSizeLeft();
public:
	Camera();
	Camera(cv::Mat cameraMatrix, cv::Mat distortionCoefficients);
	~Camera();
	void frameGrabbing();
	void frameGrabbingSingeCamera();
	void writeVideoLeft(std::string safePath);
	void writeVideoRight(std::string safePath);

	void deleteFirstFrameRight();
	void deleteFirstFrameLeft();

	int getFrameCountVideoLeft();
	int getFrameCountVideoRight();
	int getLowerFrameCountVideo();

	int getFrameCountCameraLeft();
	int getFrameCountCameraRight();
	int getLowerFrameCountCamera();

	int getBufferSizeLeft();
	int getBufferSizeRight();
	int getLowerBufferSize();

	cv::Mat getFirstFrameRight(){return m_bufferOriginalRight.front();}
	cv::Mat getFirstFrameLeft(){return m_bufferOriginalLeft.front();}

	void reset();
	void setStopRunning(bool stopRunning){m_stopRunning = stopRunning;}
	void setFrameRate(float frameRate){m_frameRate = frameRate;}
	void setAutoExposureTimeUpperLimit(float autoExposureTimeUpperLimit){m_autoExposureTimeUpperLimit = autoExposureTimeUpperLimit;}

};

#endif
