//
//  qrCodeScanner.hpp
//
//
//  Created by Henri Kuper on 20.11.17.
//
//

#ifndef qrCodeScanner_hpp
#define qrCodeScanner_hpp

#include "zbar.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/videoio.hpp"

#include "camera.hpp"

#include <iostream>
#include <thread>

class QRCodeScanner{
private:

	zbar::ImageScanner m_scanner;
	Camera *m_cameras;

	bool m_foundQRCodeRight;
	bool m_foundQRCodeLeft;

	int m_frameCount;
	int m_maxFrameCount;

	std::string m_qrCodeLeft;
	std::string m_qrCodeRight;

	void initQRCodeScanner();

public:
	QRCodeScanner();
	~QRCodeScanner();

	int32_t findQRCode();
	bool scanFrame(cv::Mat & frame, std::string &qrCode);

	std::string getQRCodeLeft(){return m_qrCodeLeft;}
	std::string getQRCodeRight(){return m_qrCodeRight;}

	void reset();

};

#endif
