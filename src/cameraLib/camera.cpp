//
//  camera.cpp
//
//
//  Created by Henri Kuper on 05.11.17.
//
//

#include "camera.hpp"

// Namespace for using pylon objects.
using namespace Pylon;
using namespace GenApi;
using namespace std;
using namespace cv;

Camera::Camera(){
	m_withUndistortion = false;
	cameraInit();
}

Camera::Camera(cv::Mat cameraMatrix, cv::Mat distortionCoefficients) : m_cameraMatrix(cameraMatrix), m_distortionCoefficients(distortionCoefficients){
	m_withUndistortion = true;
	cameraInit();
}

Camera::~Camera(){

}

void Camera::cameraInit(){

	m_inputVideoResizingFactor = 1;

	m_numberOfCamerasToUse = 2;

	m_frameRate = 35.000525;
	m_exposureTime = 8000.0f;
	m_autoExposureTimeLowerLimit = 10.0;
	m_autoExposureTimeUpperLimit = 8000.0;

	m_frameWidth = 1280;
	m_frameHeight = 720;
	m_offSetY = 120;

	if(m_withUndistortion == true){
		cout<<"Undistort"<<endl;
		initUndistortRectifyMap(m_cameraMatrix, m_distortionCoefficients, Mat(),
														m_cameraMatrix,
														Size(m_frameWidth , m_frameHeight), CV_16SC2, m_undistortionRemap1, m_undistortionRemap2);
		cout<<"m_undistortionRemap1: "<<m_undistortionRemap1.size()<<endl;
		cout<<"m_undistortionRemap2: "<<m_undistortionRemap2.size()<<endl;

	}

	m_maxFrameBuffer = 10;

	m_frameCountCameraLeft = 0;
  m_frameCountCameraRight = 0;

	m_frameCountVideoLeft = 0;
	m_frameCountVideoRight = 0;
	m_bufferSizeRight = 0;
	m_bufferSizeLeft = 0;

	m_fourcc = CV_FOURCC('m', 'p', '4', 'v');

	m_stopRunning = false;
}

void Camera::frameGrabbing(){
	// The exit code of the sample application.
  int whileCounter = 0;
	PylonInitialize();
  try{
		// Get the transport layer factory.
    CTlFactory& tlFactory = CTlFactory::GetInstance();

    // Get all attached devices and exit application if no device is found.
    DeviceInfoList_t devices;
    if ( tlFactory.EnumerateDevices(devices) == 0 )
    {
        throw RUNTIME_EXCEPTION( "No camera present.");
    }

    // Create an array of instant cameras for the found devices and avoid exceeding a maximum number of devices.
    CInstantCameraArray cameras( min( devices.size(), m_numberOfCamerasToUse));

    // Create and attach all Pylon Devices.
    for ( size_t i = 0; i < cameras.GetSize(); ++i){
        cameras[ i ].Attach( tlFactory.CreateDevice( devices[ i ]));
        // Print the model name of the camera.
        cout << "Using device " << cameras[ i ].GetDeviceInfo().GetModelName() << endl;
    }

    cameras[0].Open();
    cameras[1].Open();

    INodeMap& nodemapCamera0 = cameras[0].GetNodeMap();
    INodeMap& nodemapCamera1 = cameras[1].GetNodeMap();
    //frameRate
    CFloatPtr frameRate0(nodemapCamera0.GetNode("AcquisitionFrameRate"));
    frameRate0->SetValue(m_frameRate);
    CFloatPtr frameRate1(nodemapCamera1.GetNode("AcquisitionFrameRate"));
    frameRate1->SetValue(m_frameRate);

    //OverlapMode
    CEnumerationPtr overlapMode0( nodemapCamera0.GetNode("OverlapMode"));
    overlapMode0->FromString( "Off");
    CEnumerationPtr overlapMode1( nodemapCamera1.GetNode("OverlapMode"));
    overlapMode1->FromString( "Off");

    CEnumerationPtr exposureAuto0( nodemapCamera0.GetNode("ExposureAuto"));
    exposureAuto0->FromString( "Continuous");
    CEnumerationPtr exposureAuto1( nodemapCamera1.GetNode("ExposureAuto"));
    exposureAuto1->FromString( "Continuous");

    CFloatPtr exposureTime0(nodemapCamera0.GetNode("ExposureTime"));
    exposureTime0->SetValue(m_exposureTime);
    CFloatPtr exposureTime1(nodemapCamera1.GetNode("ExposureTime"));
    exposureTime1->SetValue(m_exposureTime);

    CFloatPtr autoExposureTimeLowerLimit0(nodemapCamera0.GetNode("AutoExposureTimeLowerLimit"));
    autoExposureTimeLowerLimit0->SetValue(m_autoExposureTimeLowerLimit);
    CFloatPtr autoExposureTimeLowerLimit1(nodemapCamera1.GetNode("AutoExposureTimeLowerLimit"));
    autoExposureTimeLowerLimit1->SetValue(m_autoExposureTimeLowerLimit);

    CFloatPtr autoExposureTimeUpperLimit0(nodemapCamera0.GetNode("AutoExposureTimeUpperLimit"));
    autoExposureTimeUpperLimit0->SetValue(m_autoExposureTimeUpperLimit);
    CFloatPtr autoExposureTimeUpperLimit1(nodemapCamera1.GetNode("AutoExposureTimeUpperLimit"));
    autoExposureTimeUpperLimit1->SetValue(m_autoExposureTimeUpperLimit);

    CIntegerPtr Width0(nodemapCamera0.GetNode("Width"));
    CIntegerPtr Height0(nodemapCamera0.GetNode("Height"));
    Width0->SetValue(m_frameWidth);
    Height0->SetValue(m_frameHeight);

    CIntegerPtr Width1(nodemapCamera1.GetNode("Width"));
    CIntegerPtr Height1(nodemapCamera1.GetNode("Height"));
    Width1->SetValue(m_frameWidth);
    Height1->SetValue(m_frameHeight);

    CIntegerPtr OffsetY0(nodemapCamera0.GetNode("OffsetY"));
    CIntegerPtr OffsetY1(nodemapCamera1.GetNode("OffsetY"));
    OffsetY0->SetValue(m_offSetY);
    OffsetY1->SetValue(m_offSetY);

    CEnumerationPtr pixelFormat0( nodemapCamera0.GetNode("PixelFormat"));
    pixelFormat0->FromString( "RGB8");
    CEnumerationPtr pixelFormat1( nodemapCamera1.GetNode("PixelFormat"));
    pixelFormat1->FromString( "RGB8");

    CEnumerationPtr gainAuto0( nodemapCamera0.GetNode("GainAuto"));
    gainAuto0->FromString( "Once");
    CEnumerationPtr gainAuto1( nodemapCamera1.GetNode("GainAuto"));
    gainAuto1->FromString( "Once");

    CEnumerationPtr bslColorSpaceMode0( nodemapCamera0.GetNode("BslColorSpaceMode"));
    bslColorSpaceMode0->FromString( "sRGB");
    CEnumerationPtr bslColorSpaceMode1( nodemapCamera1.GetNode("BslColorSpaceMode"));
    bslColorSpaceMode1->FromString( "sRGB");

    CEnumerationPtr lightSourcePreset0( nodemapCamera0.GetNode("LightSourcePreset"));
    lightSourcePreset0->FromString( "Off");
    CEnumerationPtr lightSourcePreset1( nodemapCamera1.GetNode("LightSourcePreset"));
    lightSourcePreset0->FromString( "Off");

    //The parameter MaxNumBuffer can be used to control the count of buffers
    // allocated for grabbing. The default value of this parameter is 10.
    cameras[0].MaxNumBuffer = m_maxFrameBuffer;
    cameras[1].MaxNumBuffer = m_maxFrameBuffer;

    // Starts grabbing for all cameras starting with index 0. The grabbing
    // is started for one camera after the other. That's why the images of all
    // cameras are not taken at the same time.
    // However, a hardware trigger setup can be used to cause all cameras to grab images synchronously.
    // According to their default configuration, the cameras are
    // set up for free-running continuous acquisition.
		cameras.StartGrabbing();
		cout<<"Start Grabbing"<<endl;

    // This smart pointer will receive the grab result data.
    CGrabResultPtr ptrGrabResult;
    CImageFormatConverter fc;
    fc.OutputPixelFormat = PixelType_BGR8packed;
    CPylonImage image;
    // Grab c_countOfImagesToGrab from the cameras.
    while( cameras.IsGrabbing() && m_stopRunning == false){

	    whileCounter++;
	    cameras.RetrieveResult( 10000, ptrGrabResult, TimeoutHandling_ThrowException);

	    // When the cameras in the array are created the camera context value
	    // is set to the index of the camera in the array.
	    // The camera context is a user settable value.
	    // This value is attached to each grab result and can be used
	    // to determine the camera that produced the grab result.
	    intptr_t cameraContextValue = ptrGrabResult->GetCameraContext();

			// Image grabbed successfully?
      if (ptrGrabResult->GrabSucceeded()){
       fc.Convert(image, ptrGrabResult);
       if(cameraContextValue == 1){
		      //tm.reset(); tm.start();
		      Mat frameLeft = cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_8UC3,(uint8_t*)image.GetBuffer());
		      m_mutexBufferLeft.lock();
		      m_bufferOriginalLeft.push_back(frameLeft.clone());
		      m_mutexBufferLeft.unlock();
					updateBufferSizeLeft();
					//tm.stop();
					//video_times.push_back(tm.getTimeMilli());
					m_mutexFrameCountCameraLeft.lock();
				  m_frameCountCameraLeft++;
					m_mutexFrameCountCameraLeft.unlock();
				}
				else{
		      //tm.reset(); tm.start();
		      Mat frameRight = cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_8UC3,(uint8_t*)image.GetBuffer());
		      m_mutexBufferRight.lock();
		      m_bufferOriginalRight.push_back(frameRight.clone());
		      m_mutexBufferRight.unlock();
					updateBufferSizeRight();
		    	//tm.stop();
					//video_times.push_back(tm.getTimeMilli());
					m_mutexFrameCountCameraRight.lock();
					m_frameCountCameraRight++;
					m_mutexFrameCountCameraRight.unlock();
				}
			}

	    else{
	    	cout << "Error: " << ptrGrabResult->GetErrorCode() << " " << ptrGrabResult->GetErrorDescription() << endl;
	    }
	    //tm.stop();
		  //video_times.push_back(tm.getTimeMilli());
			//std::sort(video_times.begin(), video_times.end());
			//double video_avg = std::accumulate(video_times.begin(), video_times.end(), 0.0) / video_times.size();
			//double video_avg = tm.getTimeMilli();
			//std::cout << "Video : Avg : " << video_avg << " ms FPS : " << 1000.0 / video_avg/2 << std::endl;
			if(whileCounter%200 == 0){
				cout<<"While Counter: "<<whileCounter/2<<endl;
				cout<<"frameCount1: "<<m_frameCountCameraLeft<<endl;
				cout<<"FrameCount2: "<<m_frameCountCameraRight<<endl;
				cout<<endl;
			}
		}
		cameras.StopGrabbing();
  }
  catch (const GenericException &e){
      // Error handling
      cerr << "An exception occurred." << endl
      << e.GetDescription() << endl;
	}
	PylonTerminate();
}

void Camera::frameGrabbingSingeCamera(){

		int whileCounter = 0;
    // Before using any pylon methods, the pylon runtime must be initialized.
    PylonInitialize();

    try
    {
        // Create an instant camera object with the camera device found first.
        CInstantCamera camera( CTlFactory::GetInstance().CreateFirstDevice());

        // Print the model name of the camera.
        cout << "Using device " << camera.GetDeviceInfo().GetModelName() << endl;

				camera.Open();

				INodeMap& nodemapCamera = camera.GetNodeMap();
				//frameRate
		    CFloatPtr frameRate(nodemapCamera.GetNode("AcquisitionFrameRate"));
		    frameRate->SetValue(m_frameRate);
				//OverlapMode
		    CEnumerationPtr overlapMode( nodemapCamera.GetNode("OverlapMode"));
		    overlapMode->FromString( "Off");
				CEnumerationPtr exposureAuto( nodemapCamera.GetNode("ExposureAuto"));
				exposureAuto->FromString( "Continuous");

				CFloatPtr exposureTime(nodemapCamera.GetNode("ExposureTime"));
				exposureTime->SetValue(m_exposureTime);

				CFloatPtr autoExposureTimeLowerLimit(nodemapCamera.GetNode("AutoExposureTimeLowerLimit"));
				autoExposureTimeLowerLimit->SetValue(m_autoExposureTimeLowerLimit);

				CFloatPtr autoExposureTimeUpperLimit(nodemapCamera.GetNode("AutoExposureTimeUpperLimit"));
		    autoExposureTimeUpperLimit->SetValue(m_autoExposureTimeUpperLimit);

				CIntegerPtr Width(nodemapCamera.GetNode("Width"));
		    CIntegerPtr Height(nodemapCamera.GetNode("Height"));
		    Width->SetValue(m_frameWidth);
		    Height->SetValue(m_frameHeight);

				CIntegerPtr OffsetY(nodemapCamera.GetNode("OffsetY"));
				OffsetY->SetValue(m_offSetY);

				CEnumerationPtr pixelFormat( nodemapCamera.GetNode("PixelFormat"));
				pixelFormat->FromString( "RGB8");

				CEnumerationPtr gainAuto( nodemapCamera.GetNode("GainAuto"));
				gainAuto->FromString( "Once");

				CEnumerationPtr bslColorSpaceMode( nodemapCamera.GetNode("BslColorSpaceMode"));
				bslColorSpaceMode->FromString( "sRGB");

				CEnumerationPtr lightSourcePreset( nodemapCamera.GetNode("LightSourcePreset"));
				lightSourcePreset->FromString( "Off");

        // The parameter MaxNumBuffer can be used to control the count of buffers
        // allocated for grabbing. The default value of this parameter is 10.
        camera.MaxNumBuffer = m_maxFrameBuffer;

        // Start the grabbing of c_countOfImagesToGrab images.
        // The camera device is parameterized with a default configuration which
        // sets up free-running continuous acquisition.
        camera.StartGrabbing();
				cout<<"Start Grabbing"<<endl;

        // This smart pointer will receive the grab result data.
        CGrabResultPtr ptrGrabResult;
				CImageFormatConverter fc;
		    fc.OutputPixelFormat = PixelType_BGR8packed;
				CPylonImage image;
        // Camera.StopGrabbing() is called automatically by the RetrieveResult() method
        // when c_countOfImagesToGrab images have been retrieved.
        while ( camera.IsGrabbing() && m_stopRunning == false){
					whileCounter++;
					camera.RetrieveResult( 10000, ptrGrabResult, TimeoutHandling_ThrowException);

          // Image grabbed successfully?
          if (ptrGrabResult->GrabSucceeded()){
						fc.Convert(image, ptrGrabResult);
						Mat frameLeft = cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_8UC3,(uint8_t*)image.GetBuffer());
			      m_mutexBufferLeft.lock();
			      m_bufferOriginalLeft.push_back(frameLeft.clone());
			      m_mutexBufferLeft.unlock();
						updateBufferSizeLeft();
						//tm.stop();
						//video_times.push_back(tm.getTimeMilli());
						m_mutexFrameCountCameraLeft.lock();
					  m_frameCountCameraLeft++;
						m_mutexFrameCountCameraLeft.unlock();
          }
          else{
            cout << "Error: " << ptrGrabResult->GetErrorCode() << " " << ptrGrabResult->GetErrorDescription() << endl;
          }
					if(whileCounter%100 == 0){
						cout<<"While Counter: "<<whileCounter<<endl;
						cout<<"frameCount1: "<<m_frameCountCameraLeft<<endl;
						cout<<endl;
					}
        }
    }
    catch (const GenericException &e)
    {
        // Error handling.
        cerr << "An exception occurred." << endl
        << e.GetDescription() << endl;
    }

    // // Comment the following two lines to disable waiting on exit.
    // cerr << endl << "Press Enter to exit." << endl;
    // while( cin.get() != '\n');

    // Releases all pylon resources.
    PylonTerminate();
}

void Camera::writeVideoLeft(std::string safePath){
	VideoWriter videoOutputWriter1;
	Mat frameUndistortedLeft, resizedFrameLeft;
	bool success1 = videoOutputWriter1.open(safePath,m_fourcc, m_frameRate, Size(m_frameWidth * m_inputVideoResizingFactor, m_frameHeight * m_inputVideoResizingFactor),true);
	if (!success1){
			std::cout << "!!! Output video could not be opened" << std::endl;
	}
	cout<<"writeVideo1"<<endl;
	m_mutexBufferLeft.lock();
	m_bufferSizeLeft = m_bufferOriginalLeft.size();
	m_mutexBufferLeft.unlock();
	while(m_stopRunning == false || m_bufferSizeLeft > 1){
		if(m_bufferSizeLeft > 1){
			//TODO Checken ob hier mutex eingesetzt werden muss.
			remap(m_bufferOriginalLeft.front(), frameUndistortedLeft, m_undistortionRemap1, m_undistortionRemap2, INTER_LINEAR);
			resize(frameUndistortedLeft, resizedFrameLeft, Size(m_frameWidth * m_inputVideoResizingFactor, m_frameHeight * m_inputVideoResizingFactor), 0, 0, INTER_LINEAR);
			videoOutputWriter1.write(resizedFrameLeft);

			deleteFirstFrameLeft();

			m_mutexFrameCountVideoLeft.lock();
			m_frameCountVideoLeft++;
			m_mutexFrameCountVideoLeft.unlock();
		}
		else{
			this_thread::sleep_for(chrono::milliseconds(5));
		}
	}
	cout<<"videoOutputWriter1 release"<<endl;
	videoOutputWriter1.release();
}

void Camera::writeVideoRight(std::string safePath){
	VideoWriter videoOutputWriter2;
	Mat frameUndistortedRight, resizedFrameRight;
	bool success2 = videoOutputWriter2.open(safePath,m_fourcc, m_frameRate, Size(m_frameWidth * m_inputVideoResizingFactor, m_frameHeight * m_inputVideoResizingFactor),true);
	if (!success2){
			std::cout << "!!! Output video could not be opened" << std::endl;
	}
	cout<<"writeVideo2"<<endl;
	m_mutexBufferRight.lock();
	m_bufferSizeRight = m_bufferOriginalRight.size();
	m_mutexBufferRight.unlock();
	while(m_stopRunning == false || m_bufferSizeRight > 1){

		if( m_bufferSizeRight > 1){
			remap(m_bufferOriginalRight.front(), frameUndistortedRight, m_undistortionRemap1, m_undistortionRemap2, INTER_LINEAR);
			resize(frameUndistortedRight, resizedFrameRight, Size(m_frameWidth * m_inputVideoResizingFactor, m_frameHeight * m_inputVideoResizingFactor), 0, 0, INTER_LINEAR);
			videoOutputWriter2.write(resizedFrameRight);

			deleteFirstFrameRight();

			m_mutexFrameCountVideoRight.lock();
			m_frameCountVideoRight++;
			m_mutexFrameCountVideoRight.unlock();
		}
		else{
			this_thread::sleep_for(chrono::milliseconds(5));
		}
		// cout<<"BufferSizeRight: "<<m_bufferSizeRight<<endl;
		// cout<<"Stop Running: "<<m_stopRunning<<endl;
	}
	cout<<"videoOutputWriter2 release"<<endl;
	videoOutputWriter2.release();
}

void Camera::updateBufferSizeRight(){
	m_mutexBufferRight.lock();
	m_bufferSizeRight = m_bufferOriginalRight.size();
	m_mutexBufferRight.unlock();
}

void Camera::updateBufferSizeLeft(){
	m_mutexBufferLeft.lock();
	m_bufferSizeLeft = m_bufferOriginalLeft.size();
	m_mutexBufferLeft.unlock();
}

void Camera::deleteFirstFrameRight(){
	m_mutexBufferRight.lock();
	//cout<<"deleteFirstFrameRight Size: "<< m_bufferOriginalRight.size()<<endl;
	m_bufferOriginalRight.erase(m_bufferOriginalRight.begin());
	m_mutexBufferRight.unlock();
	updateBufferSizeRight();
}

void Camera::deleteFirstFrameLeft(){
	m_mutexBufferLeft.lock();
	//cout<<"deleteFirstFrameLeft Size: "<< m_bufferOriginalLeft.size()<<endl;
	m_bufferOriginalLeft.erase(m_bufferOriginalLeft.begin());
	m_mutexBufferLeft.unlock();
	updateBufferSizeLeft();
}

int Camera::getLowerFrameCountVideo(){
	int frameCountLeftTmp = getFrameCountVideoLeft();
	int frameCountRightTmp = getFrameCountVideoRight();

	return frameCountLeftTmp < frameCountRightTmp ? frameCountLeftTmp : frameCountRightTmp;
}

int Camera::getFrameCountVideoLeft(){
	m_mutexFrameCountVideoLeft.lock();
	int frameCountLeftTmp = m_frameCountVideoLeft;
	m_mutexFrameCountVideoLeft.unlock();
	return frameCountLeftTmp;
}

int Camera::getFrameCountVideoRight(){
	m_mutexFrameCountVideoRight.lock();
	int frameCountRightTmp = m_frameCountVideoRight;
	m_mutexFrameCountVideoRight.unlock();
	return frameCountRightTmp;
}

int Camera::getFrameCountCameraLeft(){
	m_mutexFrameCountCameraLeft.lock();
	int frameCountCameraLeftTmp = m_frameCountCameraLeft;
	m_mutexFrameCountCameraLeft.unlock();
	return frameCountCameraLeftTmp;
}
int Camera::getFrameCountCameraRight(){
	m_mutexFrameCountCameraRight.lock();
	int frameCountCameraRightTmp = m_frameCountCameraRight;
	m_mutexFrameCountCameraRight.unlock();
	return frameCountCameraRightTmp;
}
int Camera::getLowerFrameCountCamera(){
	int frameCountCameraLeftTmp = getFrameCountCameraLeft();
	int frameCountCameraRightTmp = getFrameCountCameraRight();

	return frameCountCameraLeftTmp < frameCountCameraRightTmp ? frameCountCameraLeftTmp : frameCountCameraRightTmp;
}

int Camera::getLowerBufferSize(){
	int bufferSizeLeftTmp = getBufferSizeLeft();
	int bufferSizeRightTmp = getBufferSizeRight();

	return bufferSizeLeftTmp < bufferSizeRightTmp ? bufferSizeLeftTmp : bufferSizeRightTmp;
}

int Camera::getBufferSizeLeft(){
	m_mutexBufferLeft.lock();
	int bufferSizeLeftTmp = m_bufferSizeLeft;
	m_mutexBufferLeft.unlock();
	return bufferSizeLeftTmp;
}

int Camera::getBufferSizeRight(){
	m_mutexBufferRight.lock();
	int bufferSizeRightTmp = m_bufferSizeRight;
	m_mutexBufferRight.unlock();
	return bufferSizeRightTmp;
}

void Camera::reset(){
	m_mutexBufferRight.lock();
	m_bufferOriginalRight.clear();
	m_mutexBufferRight.unlock();

	m_mutexBufferLeft.lock();
	m_bufferOriginalLeft.clear();
	m_mutexBufferLeft.unlock();

	updateBufferSizeLeft();
	updateBufferSizeRight();
	m_stopRunning = false;
}
