//
//  imageMath.cpp
//
//
//  Created by Henri Kuper on 29.08.17.
//
//

#include "imageMath.hpp"

using namespace cv;
using namespace std;

namespace imgProc{
	void imageMath::angelBetween(const cv::Point &v1, const cv::Point &v2, float &angel){

	    float len1 = sqrt((float)v1.x * v1.x + (float)v1.y * v1.y);
	    float len2 = sqrt((float)v2.x * v2.x + (float)v2.y * v2.y);
	    float dot = (float)v1.x * v2.x + (float)v1.y * v2.y;
	    float a = dot / (len1 * len2);
	    angel = acos(a)/3.141f*180.0; // 0..PI
	}

	void imageMath::calculateDistanceBetween(cv::Point2f p0, cv::Point2f p1, float &distance){
	    Point2f difference = p0-p1;
	    distance = sqrt(difference.x*difference.x + difference.y*difference.y);
	}

	void imageMath::compareHistogram(const cv::Mat &hist1, const cv::Mat &hist2, float &distance){
		double squareSum = 0;
		double hist1Sum = 0;
		double hist2Sum = 0;
		for(uint i = 0; i < hist1.rows; i++){
			squareSum += sqrt(hist1.at<float>(i)*hist2.at<float>(i));
			hist1Sum += hist1.at<float>(i);
			hist2Sum += hist2.at<float>(i);
		}
		distance =sqrt(1.0 - squareSum/sqrt(hist1Sum*hist2Sum));
	}

	void imageMath::calculateMomentMassCenter(const Moments &moment, Point2f &massCenter){
		massCenter.x = moment.m10 / moment.m00;
		massCenter.y = moment.m01 / moment.m00;
	}

	void imageMath::calculateLinearRegression(const Point2f &point0, const Point2f &point1, Point2f &slopeInterceptValues){

    float dx, dy;

    dx = point1.x - point0.x;
    dy = point1.y - point0.y;
		//calc the slope value
    slopeInterceptValues.x = dy / dx;
		//calc the intercept value
    slopeInterceptValues.y = point0.y - slopeInterceptValues.x * point0.x;
	}

	void imageMath::calculateQuadRegression(vector<Point2f> &points, vector<float> &parameters, bool regressionType){
		int pointnumberforregression;
		parameters.clear();

		if(points.size() < 3){
        cout<<"calculateQuadRegression: Curve has to have at least 3 points for quad. Regression!"<<endl;
        return;
    }
		else if(points.size() == 3){
			pointnumberforregression = 3;
		}
		else{
			pointnumberforregression = 4;
		}

		float a, b, c;
		float x, y, xy, xx, xxx, xxxx, xxy;

		x = 0;
		y = 0;
		xy = 0;
		xx = 0;
		xxx = 0;
		xxxx = 0;
		xxy = 0;

		//Do Regression with at least 3 points
		//If the curve contains more than 3 points, use last/first 4 points for regression
		if(regressionType){
			for(int i=points.size()-pointnumberforregression; i<points.size(); i++){
				x += points[i].x;
				y += points[i].y;
				xy += points[i].x*points[i].y;
				xx += points[i].x*points[i].x;
				xxx += points[i].x*points[i].x*points[i].x;
				xxxx += points[i].x*points[i].x*points[i].x*points[i].x;
				xxy += points[i].x*points[i].x*points[i].y;
			}
		}
		else{
			for(int i=0; i<pointnumberforregression; i++){
				x += points[i].x;
				y += points[i].y;
				xy += points[i].x*points[i].y;
				xx += points[i].x*points[i].x;
				xxx += points[i].x*points[i].x*points[i].x;
				xxxx += points[i].x*points[i].x*points[i].x*points[i].x;
				xxy += points[i].x*points[i].x*points[i].y;
			}
		}

		x = x/pointnumberforregression;
		y = y/pointnumberforregression;
		xy = xy/pointnumberforregression;
		xx = xx/pointnumberforregression;
		xxx = xxx/pointnumberforregression;
		xxxx = xxxx/pointnumberforregression;
		xxy = xxy/pointnumberforregression;

		a = ((xxy-y*xx)*(xx-x*x) - (xy-y*x)*(xxx-x*xx)) / ((xxxx-xx*xx)*(xx-x*x) - (xxx-x*xx)*(xxx-x*xx));
		b = (xy - x*y - a*(xxx-x*xx)) / (xx-x*x);
		c = y - a*xx - b*x;

		parameters.push_back(a);
		parameters.push_back(b);
		parameters.push_back(c);

		//cout<<"a: "<<parameters[0]<<endl;
		//cout<<"b: "<<parameters[1]<<endl;
		//cout<<"c: "<<parameters[2]<<endl;
	}

	float imageMath::calculateTanh(float value, float modulation){
		return (float)tanh(modulation * value);
	}
}
