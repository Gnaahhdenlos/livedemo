//
//  imageProcessing.cpp
//
//
//  Created by Henri Kuper on 29.08.17.
//
//


//#define GPU_ON
#include "imageProcessing.hpp"


using namespace cv;
using namespace std;

//--------CPU-GPU shared private functions--------

#ifndef GPU_ON

namespace imgProc {
void imageProcessing::imageAdjustment(Mat& src, Mat& dst, int tolerance){
	cvtColor( src, src, CV_BGR2HSV );
	Vec2i in = Vec2i(0, 255);
	Vec2i out = Vec2i(0, 255);

	tolerance = max(0, min(100, tolerance));

	if (tolerance > 0) {
		// Compute in and out limits
		// Histogram
		std::vector<int> hist(256, 0);
		for (int32_t r = 0; r < src.rows; ++r) {
			for (int32_t c = 0; c < src.cols; ++c) {
				Vec3b pixel = src.at<Vec3b>(r,c);
				hist[pixel.val[2]]++;
			}
		}

		// Cumulative histogram
		std::vector<int> cum = hist;
		for (uint32_t i = 1; i < hist.size(); ++i) {
			cum[i] = cum[i - 1] + hist[i];
		}

		// Compute bounds
		int total = src.rows * src.cols;
		int low_bound = total * tolerance / 100;
		int upp_bound = total * (100-tolerance) / 100;
		in[0] = distance(cum.begin(), lower_bound(cum.begin(), cum.end(), low_bound));
		in[1] = distance(cum.begin(), lower_bound(cum.begin(), cum.end(), upp_bound));
	}

	// Stretching
	float scale = float(out[1] - out[0]) / float(in[1] - in[0]);
	for (int r = 0; r < src.rows; ++r) {
		for (int c = 0; c < src.cols; ++c) {
			Vec3b pixel = src.at<Vec3b>(r,c);
			int vs = std::max(static_cast<int>(pixel.val[2] - in[0]), 0);
			int vd = std::min(static_cast<int>(vs * scale + 0.5f) + out[0], out[1]);
			pixel = src.at<Vec3b>(r,c);
			pixel.val[2] = vd;
			src.at<Vec3b>(r,c) = pixel;
		}
	}
	cvtColor( src, src, CV_HSV2BGR );
}

void imageProcessing::histogramEqualisation(cv::Mat& src, cv::Mat& dst){
	Mat histeq;
	std::vector<Mat> channels;
	cvtColor(src, histeq, CV_BGR2HSV);
	split(histeq,channels);
	equalizeHist(channels[2], channels[2]);         //equalize histogram on the 3rd channel (V)
	merge(channels,histeq);         //merge 3 channels including the modified 3rd channel into one image
	cvtColor(histeq, dst, CV_HSV2BGR);         //converting the color to BGR
}

void imageProcessing::adaptiveHistogramEqualisation(cv::Mat& src, cv::Mat& dst, int clipLimit){
	Mat aheq;
	std::vector<Mat> channels;
	Ptr<CLAHE> clahe = createCLAHE();
	clahe->setClipLimit(clipLimit);
	cvtColor(src, aheq, CV_BGR2HSV);     //change the color image from RGB to HSV format
	split(aheq,channels);           //split the image into channels
	clahe->apply(channels[2],channels[2]);
	//clahe->apply(channels[1],channels[1]);

	merge(channels,aheq);     //merge 3 channels including the modified 1st channel into one image

	cvtColor(aheq, dst, CV_HSV2BGR);     //change the color image from HSV to RGB format (to display image properly)
}

void imageProcessing::contrastNormalisation(Mat& src, Mat& dst){
	Mat cont;
	int filterSize = 99;
	int channelNumber = 0;    //0 bei YUV
	//New Contrast
	Mat yuv, floatc0, blur, num, den;
	// convert to grayscale
	cvtColor(src, yuv, CV_BGR2YUV);    //2YUV

	std::vector<Mat> channels;
	split(yuv,channels);

	// convert to floating-point image
	channels[channelNumber].convertTo(floatc0, CV_32F, 1.0/255.0);

	// numerator = src - gauss_blur(src)
	GaussianBlur(floatc0, blur, Size(filterSize,filterSize), 0);
	num = floatc0 - blur;

	// denominator = sqrt(gauss_blur(src^2))
	GaussianBlur(num.mul(num), blur, Size(filterSize,filterSize), 0);
	pow(blur, 0.5, den);

	// output = numerator / denominator
	floatc0 = num / den;
	floatc0.convertTo(channels[channelNumber], CV_8U, 255.0);
	merge(channels,cont);
	cvtColor(cont, dst, CV_YUV2BGR);
}

void imageProcessing::cvtColorBGR2Lab(cv::Mat& src, cv::Mat& dst){
	dst = Mat(src.rows, src.cols, CV_32FC3);

	float L, M, S, _L, Alph, Beta;
	int R, G, B;
	for (int i = 0; i < src.rows; i++) {
		for (int j = 0; j < src.cols; j++) {
			B = src.at<Vec3b>(i, j)[0];
			G = src.at<Vec3b>(i, j)[1];
			R = src.at<Vec3b>(i, j)[2];

			L = (0.3811 * R) + (0.5783 * G) + (0.0402 * B);
			M = (0.1967 * R) + (0.7244 * G) + (0.0782 * B);
			S = (0.0241 * R) + (0.1288 * G) + (0.8444 * B);

			//for handling log
			if (L == 0.0000) L = 1.0000;
			if (M == 0.0000) M = 1.0000;
			if (S == 0.0000) S = 1.0000;

			//LMS to Lab
			_L = (1.0 / sqrt(3.0)) *((1.0000 * log10(L)) + (1.0000 * log10(M)) + (1.0000 * log10(S)));
			Alph = (1.0 / sqrt(6.0)) * ((1.0000 * log10(L)) + (1.0000 * log10(M)) + (-2.0000 * log10(S)));
			Beta = (1.0 / sqrt(2.0)) * ((1.0000 * log10(L)) + (-1.0000 * log10(M)) + (-0.0000 * log10(S)));

			dst.at<Vec3f>(i, j)[0] = _L;
			dst.at<Vec3f>(i, j)[1] = Alph;
			dst.at<Vec3f>(i, j)[2] = Beta;
		}
	}
}

void imageProcessing::cvtColorLab2BGR(cv::Mat& src, cv::Mat& dst){
	dst = Mat(src.rows, src.cols, CV_8UC3);

	float L, M, S, _L, Alph, Beta;
	for (int i = 0; i < src.rows; i++) {
		for (int j = 0; j < src.cols; j++) {
			_L = src.at<Vec3f>(i, j)[0];
			Alph = src.at<Vec3f>(i, j)[1];
			Beta = src.at<Vec3f>(i, j)[2];

			L = (0.57735 *_L) + (0.408248 * Alph) + (0.707107 * Beta);
			M = (0.57735 * _L) + (0.408248 * Alph) + (-0.707107 * Beta);
			S = (0.57735 * _L) + (-0.816497 * Alph) + (0.00000* Beta);

			L = pow(10, L);
			if (L == 1) L = 0;
			M = pow(10, M);
			if (M == 1) M = 0;
			S = pow(10, S);
			if (S == 1) S = 0;

			dst.at<Vec3b>(i, j)[2] = saturate_cast<uchar>((4.4679 * L) + (-3.5873 * M) + (0.1193 * S));
			dst.at<Vec3b>(i, j)[1] = saturate_cast<uchar>((-1.2186 * L) + (2.3809 * M) + (-0.1624 * S));
			dst.at<Vec3b>(i, j)[0] = saturate_cast<uchar>((0.0497 * L) + (-0.2439 * M) + (1.2045 * S));
		}
	}
}

void imageProcessing::colorTransfer(Mat& src0, Mat& src1, Mat& dst){
	Mat labSrc0, labSrc1, labSrc1Substracted, labSrc1Added, test;
	Scalar mean0, mean1, stdDev0, stdDev1, mean0Test, stdDev0Test;
	Scalar mean0test, mean1test, stdDev0test, stdDev1test;
	Scalar stdDev01(0,0,0,0);

	imgProc::imageProcessing::cvtColorBGR2Lab(src0, labSrc0);
	imgProc::imageProcessing::cvtColorBGR2Lab(src1, labSrc1);

	//calculating the mean value
	meanStdDev(labSrc0, mean0, stdDev0, noArray());
	meanStdDev(labSrc1, mean1, stdDev1, noArray());

	for(int i = 0; i < src0.channels(); i++) {
		stdDev01[i] = stdDev0[i] / stdDev1[i];
	}

	subtract(labSrc1, mean1, labSrc1Substracted, noArray(), CV_32F);

	labSrc1Substracted = labSrc1Substracted.mul(stdDev01);

	add(labSrc1Substracted, mean0, labSrc1Added, noArray(), CV_32FC3);
	imgProc::imageProcessing::cvtColorLab2BGR(labSrc1Added, dst);
}

//--------------------imageObjects

void imageObjects::reshapeContourWithMassCenter(const vector<Point> &contour, vector<Point> &dstContour, int numberOfIterations){
	int xLimitLeft, xLimitRight;

	vector<Point> contourTmp = contour;
	vector<Point> dstTmp;

	for(int i = 0; i < numberOfIterations; i++) {
		Rect boundRect = boundingRect(contourTmp);
		Moments moment = moments(contourTmp);
		Point2f momentCenter(moment.m10 / moment.m00, moment.m01 / moment.m00 );
		int diffLeft = momentCenter.x - boundRect.x;
		int diffRight = boundRect.x + boundRect.width - momentCenter.x;

		if(diffLeft < diffRight) {
			xLimitLeft = boundRect.x;
			xLimitRight = momentCenter.x + (momentCenter.x - boundRect.x);
		}
		else{
			xLimitLeft = momentCenter.x - (boundRect.x + boundRect.width - momentCenter.x);
			xLimitRight = boundRect.x +boundRect.width;
		}

		for(int i = 0; i < contour.size(); i++) {
			Point p = contour[i];
			if(p.x >= xLimitLeft && p.x <= xLimitRight) {
				dstTmp.push_back(p);
			}
		}
		contourTmp = dstTmp;
		dstTmp = vector<Point>();
	}

	dstContour = contourTmp;
}

void imageObjects::mergeContoursWithConvexHull(const vector<Point> &contour0, const vector<Point> &contour1, vector<Point> &dstContour){

	vector<Point> mergedContour;
	mergedContour.insert(mergedContour.end(), contour0.begin(), contour0.end());
	mergedContour.insert(mergedContour.end(), contour1.begin(), contour1.end());
	convexHull(mergedContour, dstContour);

}

//cornerPoints Indizes: 1 - North ### 1 - East ### 2 - South ### 3 - West
void imageObjects::findContourCornerPoints(const std::vector<cv::Point> &contour, std::vector<cv::Point2f> &cornerPoints, bool isRightFieldSide){

	cornerPoints.clear();

	cornerPoints.push_back(contour[0]);
	cornerPoints.push_back(contour[0]);
	cornerPoints.push_back(contour[0]);
	cornerPoints.push_back(contour[0]);
	//Right Image Settings
	if (isRightFieldSide) {
        for(uint32_t i = 1; i < contour.size(); i++) {
            if(contour[i].y < cornerPoints[0].y || (contour[i].y == cornerPoints[0].y && contour[i].x < cornerPoints[0].x)) {
                cornerPoints[0] = contour[i];           //north
            }
            if(contour[i].x > cornerPoints[1].x || (contour[i].x == cornerPoints[1].x && contour[i].y < cornerPoints[1].y)) {
                cornerPoints[1] = contour[i];           //east
            }
            if(contour[i].y > cornerPoints[2].y || (contour[i].y == cornerPoints[2].y && contour[i].x > cornerPoints[2].x)) {
                cornerPoints[2] = contour[i];           //south
            }
            if(contour[i].x < cornerPoints[3].x || (contour[i].x == cornerPoints[3].x && contour[i].y > cornerPoints[3].y)) {
                cornerPoints[3] = contour[i];           //west
            }

        }
	}
	//Left Image Settings
	else{
        for(uint32_t i = 1; i < contour.size(); i++) {
            if(contour[i].y < cornerPoints[0].y || (contour[i].y == cornerPoints[0].y && contour[i].x > cornerPoints[0].x)) {
                cornerPoints[0] = contour[i];           //north
            }
            if(contour[i].x > cornerPoints[1].x || (contour[i].x == cornerPoints[1].x && contour[i].y > cornerPoints[1].y)) {
                cornerPoints[1] = contour[i];           //east
            }
            if(contour[i].y > cornerPoints[2].y || (contour[i].y == cornerPoints[2].y && contour[i].x < cornerPoints[2].x)) {
                cornerPoints[2] = contour[i];           //south
            }
            if(contour[i].x < cornerPoints[3].x || (contour[i].x == cornerPoints[3].x && contour[i].y < cornerPoints[3].y)) {
                cornerPoints[3] = contour[i];           //west
            }
        }
	}
}

void imageObjects::findContourIndexBelongingToPoint(const vector<vector<Point> > &contours, Point2f &point, int &index){
	double contourfound = -1;
	for( int i = 0; i< contours.size(); i++ ) {
		contourfound = pointPolygonTest(contours[i], point, false);
		if(contourfound == 1) {
			index = i;
			break;
		}
	}

	if(contourfound != 1) {
		cout<<"Couldn#t match Point to any Contour"<<endl;
	}
}

bool imageObjects::isInsideQuadrangle(const vector<Point2f> &quadrangle, const Point2f &position, float qudrangleOffset){
	bool isInsideQuadrangle = false;

	if(quadrangle[0].x - qudrangleOffset < position.x && quadrangle[2].x + qudrangleOffset > position.x && quadrangle[0].y - qudrangleOffset < position.y && quadrangle[2].y + qudrangleOffset > position.y) {
		isInsideQuadrangle = true;
	}
	return isInsideQuadrangle;
}

}

#endif
