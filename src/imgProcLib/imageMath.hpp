//
//  imageMath.hpp
//
//
//  Created by Henri Kuper on 29.08.17.
//
//

#ifndef imageMath_hpp
#define imageMath_hpp

//#define GPU_ON

#include <stdio.h>

#include <iostream>
#include <vector>
#include <numeric>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/opencv.hpp"

namespace imgProc{
    class imageMath{
    public:
			/*
				Calculates the angle between two directional vectors.
				The angel is stored in the variable angel and contains the angel in degree.
			*/
			static void angelBetween(const cv::Point &v1, const cv::Point &v2, float &angel);
			/*
				Calculates the distance between two 2D Points and stores the length as a float in distance.
			*/
			static void calculateDistanceBetween(cv::Point2f p0, cv::Point2f p1, float &distance);
			/*
				Calculates the Bhattacharyya distance between the two histograms.
			*/
			static void compareHistogram(const cv::Mat &hist1, const cv::Mat &hist2, float &distance);
			/*
				Calculates the Moments mass center.
			*/
			static void calculateMomentMassCenter(const cv::Moments &moment, cv::Point2f &massCenter);
      /*
        Calculates the linear regression between the two points p0 and p1.
        The x value of slopeInterceptValues contains the slope and the y value contains the intercept.
      */
			static void calculateLinearRegression(const cv::Point2f &point0, const cv::Point2f &point1, cv::Point2f &slopeInterceptValues);
      /*
        Calculates the quadr. regression (a*x^2 + b*x + c) between at least three points.
        The parameters a, b and c are stored into parameters
        regressionType == 1 -> regression with last points; regressionType == 0 -> regression with first points;
      */
      static void calculateQuadRegression(std::vector<cv::Point2f> &points, std::vector<float> &parameters, bool regressionType);
      /*
        Calculates the tangens hyperbolicus with a modulation: tanh(modulation * value). The result will be returned.
      */
      static float calculateTanh(float value, float modulation);
		};
}
#endif
