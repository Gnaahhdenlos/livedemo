//
//  imageProcessing.hpp
//
//
//  Created by Henri Kuper on 29.08.17.
//
//

#ifndef imageProcessing_hpp
#define imageProcessing_hpp

//#define GPU_ON

#include <stdio.h>

#include <iostream>
#include <vector>
#include <numeric>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/opencv.hpp"

namespace imgProc{
    class imageProcessing{
    public:
      /*
        Adjusts the image.
        With a tolerance fromt 0-100.
       */
      static void imageAdjustment(cv::Mat& src, cv::Mat& dst, int tolerance);
      /*
       Does a histogram equalisation on a 3 Channel BGR Mat by transforming it into HSV color space
       and histogram equalises the V channel.
    	 */
      static void histogramEqualisation(cv::Mat& src, cv::Mat& dst);
      /*
        Does an adaptive histogram equalisation. ClipLimit is the patchsize which is used for the histEqualisation.
      */
      static void adaptiveHistogramEqualisation(cv::Mat& src, cv::Mat& dst, int clipLimit);
      /*
        Does a contrast equalisation
        It substracts the mean value of the gaussian blurr Y-Channel from the original Y-Channel image value.
        Divide the result of the substraction by the squared and another time gaussian blurred result of the substraction.
       */
      static void contrastNormalisation(cv::Mat& src, cv::Mat& dst);
      /*
        Converts a BGR colorspace Mat to a L alpha beta color space Mat.
       */
      static void cvtColorBGR2Lab(cv::Mat& src, cv::Mat& dst);
      /*
        Converts a L alpha beta colorspace Mat to a BGR color space Mat.
       */
      static void cvtColorLab2BGR(cv::Mat& src, cv::Mat& dst);
      /*
        Apply the color palette, mood and style from src0 to src1 and stores it in dst.
        http://www.cs.tau.ac.il/~turkel/imagepapers/ColorTransfer.pdf
       */
      static void colorTransfer(cv::Mat& src0, cv::Mat& src1, cv::Mat& dst);
    };

    class imageObjects{
    public:
      /*
        Calculates the massCenter of the contours moment and cuts of every part of the contour
        that is not around the distcance from massCenter to the boundrects nearest sidewise limit.
      */
      static void reshapeContourWithMassCenter(const std::vector<cv::Point> &contour, std::vector<cv::Point> &dstContour, int numberOfIterations);
      /*
        Merges two contours and stores the convex hull of the new contour into dstContour.
      */
      static void mergeContoursWithConvexHull(const std::vector<cv::Point> &contour0, const std::vector<cv::Point> &contour1, std::vector<cv::Point> &dstContour);
      /*
         Detects the contours cornerPoints in each direction:  West, East, North, South
      */
      static void findContourCornerPoints(const std::vector<cv::Point> &contour, std::vector<cv::Point2f> &cornerPoints, bool isRightFieldSide);
      /*
         Determines whether the point is inside a contour and determines the index of that contour
      */
      static void findContourIndexBelongingToPoint(const std::vector<std::vector<cv::Point>> &contours, cv::Point2f &point, int &index);
      /*
        Proofes if a point is inside a given quadrangle. The quadrangle can expanded by a given quadrangleOffset.
      */
      static bool isInsideQuadrangle(const std::vector<cv::Point2f> &quadrangle, const cv::Point2f &position, float qudrangleOffset);
    };
}


#endif /* imageProcessing_hpp */
