#include <opencv2/core/utility.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/videoio.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/opencv.hpp"
#include <iostream>
#include <cstring>
#include <algorithm>
#include <vector>
#include <numeric>
#include <stdlib.h>
#include <stdio.h>
#include "../../imgProcLib/imageProcessing.hpp"
#include "../../imgProcLib/imageMath.hpp"


using namespace std;
using namespace cv;

/*
  Ideen:
  -Wasserscheidentransformation
  -Sobelfilter ohne/mit Vorverarbeitung
  -Laplacian Edge Detection
  -Canny edge detection ohne/mit (mit sobel vorweg eher scheiße)
  -Fluchtpunktbestimmung

*/

int vmin = 0, vmax = 256, smin = 0, smax = 256, hmin = 50, hmax = 100;
//string window_name = "color Filter Demo";

Mat inputFrameLeft, inputFrameRight, inputFrameLeftPilone, inputFrameRightPilone, inputFrameLeftSRGB, inputFrameRightSRGB, diffLeft, diffRight;
Mat grayImageLeft, grayImageRight, grayImageLeftSRGB, grayImageRightSRGB, sobelOutputLeft, sobelOutputRight;
Mat sobelSumLeft, sobelSumRight, sobelSumLeftSRGB, sobelSumRightSRGB, sobelLeft, sobelRight, sobelLeftSRGB, sobelRightSRGB;

Mat hsvFrame;
Mat grayTresholdImage, grayTresholdImage2;
Mat src_gray;
Mat dst, dst_BGR, dst2, dst_BGR2, detected_edges;

int tresh=20, minLength=10, maxGap=2;
string window_name = "Edge Map";

vector<Vec4i> linesP, linesP2;
   vector<Vec2f> lines;

int edgeThresh = 1;
int lowThreshold;
int const max_lowThreshold = 100;
int ratio = 3;
int kernel_size = 3;

Mat m_imgLines, m_calibratedImgLines;

void detecFieldLinesSobel(Mat&, bool);
void cut_Image(Mat&, Mat&, bool);

void calculateLinePointsBorder(Point&, Point&, Point&, Point&, int, int);

void sobel_Tresh(Mat);
void diff_balls(string, string);
void build_difference(string, string, int, int);
void sum_sobel(string, string, /*string, string, */ int, int);
void sum_laplacian(string, string, int, int);
void build_sobel1(Mat, Mat);
void build_sobel2(Mat);
void build_laplacian(Mat);
void CannyThreshold(int, void*);
void build_canny(Mat);
void hsvThreshold(int, void*);
void grayThreshold(int, void*);
void build_Houghlines(int, void*);
void build_HoughlinesP(int, void*);

int main (int argc, char* argv[]){
  string filenameLeft, filenameRight, filenameLeftSRGB, filenameRightSRGB;

  string m_filePathToCameraCalibrationMatrix;

  if(argc >= 2){
     //filenameLeft = argv[1];
     //filenameRight = argv[2];
     filenameLeftSRGB = argv[1];
     filenameRightSRGB = argv[2];

     m_filePathToCameraCalibrationMatrix = argv[3];

     cout<<"wrong arguments"<<endl;
  }
  else{
      return EXIT_FAILURE;
  }
  //RGB // SRGB switch
  VideoCapture captureLeft( filenameLeftSRGB );
  VideoCapture captureRight( filenameRightSRGB );

  captureLeft>>inputFrameLeft;
  captureRight>>inputFrameRight;

  m_calibratedImgLines = Mat::zeros( inputFrameLeft.size(), CV_8UC3 );


  Mat cameraMatrix, distortionCoefficients;

  FileStorage fileStorage(m_filePathToCameraCalibrationMatrix, FileStorage::READ);
  if( !fileStorage.isOpened() ){
      return EXIT_FAILURE;
  }
  fileStorage["Camera_Matrix"]>> cameraMatrix;
  fileStorage["Distortion_Coefficients"]>> distortionCoefficients;
  cout<<"Camera_Matrix: "<<cameraMatrix<<endl;
  cout<<"Distortion_Coefficients: "<<distortionCoefficients<<endl;

  /*Testing Quad Regression
  vector<Point> points;
  points.push_back(Point(1, 1));
  points.push_back(Point(2, 2));
  points.push_back(Point(3, 4));
  points.push_back(Point(4, 6));
  points.push_back(Point(5, 8));
  points.push_back(Point(6, 11));
  points.push_back(Point(7, 9));
  points.push_back(Point(8, 7));
  points.push_back(Point(9, 4));
  points.push_back(Point(10, 2));
  vector<float> parameters;
  cout<<"Didd"<<endl;
  imgProc::imageMath::calculateQuadRegression(points, parameters);
  waitKey(0);
  */

  Mat inputFrameRight_undistorted;
  Mat inputFrameLeft_undistorted;


  undistort(inputFrameRight, inputFrameRight_undistorted, cameraMatrix, distortionCoefficients);
  undistort(inputFrameLeft, inputFrameLeft_undistorted, cameraMatrix, distortionCoefficients);

  detecFieldLinesSobel(inputFrameRight_undistorted, true);

  detecFieldLinesSobel(inputFrameLeft_undistorted, false);

  //sobel_Tresh(inputFrameLeft);

  // diff_balls(filenameLeft, filenameRight);

  /*
  namedWindow( window_name, CV_WINDOW_AUTOSIZE );

  createTrackbar( "Min Threshold:", window_name, &lowThreshold, max_lowThreshold, CannyThreshold );

  cvtColor(inputFrameLeft, src_gray, CV_BGR2GRAY);

  CannyThreshold(0, 0);

  waitKey(0);
  */

  //build_canny(inputFrameRight);

  /*
  cornerHarris( inputFrameLeft, dst, 1, 1, 1, BORDER_DEFAULT );

  imshow( window_name, dst);

  waitKey(0);
  */

  //cvtColor(dest, hsvFrame, CV_BGR2HSV);

  //GaussianBlur( inputFrameLeft, inputFrameLeft, Size(3,3), 0, 0, BORDER_DEFAULT );
  //build_sobel1(inputFrameLeft, inputFrameRight);
  //build_sobel2(inputFrameRight);
  //build_laplacian(inputFrameRight);
  //build_laplacian(inputFrameLeft);



  //Mat dest = sum_sobel(/*filenameLeft, filenameRight,*/ filenameLeftSRGB, filenameRightSRGB, 1, 30);
  //dest = build_difference(filenameLeft, filenameRight, 1, 100);

  //build_sobel2(inputFrameLeft);

  /*
  namedWindow( window_name, CV_WINDOW_NORMAL );

  createTrackbar( "Vmin", window_name, &vmin, 256, hsvThreshold );
  createTrackbar( "Vmax", window_name, &vmax, 256, hsvThreshold );
  createTrackbar( "Smin", window_name, &smin, 256, hsvThreshold );
  createTrackbar( "Smax", window_name, &smax, 256, hsvThreshold );
  createTrackbar( "Hmin", window_name, &hmin, 256, hsvThreshold );
  createTrackbar( "Hmax", window_name, &hmax, 256, hsvThreshold );
  */

  //Mat destination(inputFrameLeft.size(), inputFrameLeft.type());
  /*
  cvtColor(inputFrameRight, hsvFrame, CV_BGR2HSV);
  cvtColor(inputFrameRightPilone, hsvFramePilone, CV_BGR2HSV);
  */

  //cvtColor(inputFrameLeft, grayImageLeft, CV_BGR2GRAY);
  //cvtColor(inputFrameRight, grayImageRight, CV_BGR2GRAY);

  //imgProc::imageProcessing::colorTransfer(inputFrameLeft, inputFrameRight, destination);
  //cvtColor(dest, dest_BGR, CV_GRAY2BGR);
  //cvtColor(dest_BGR, hsvFrame, CV_BGR2HSV);


  //imgProc::imageProcessing::histogramEqualisation(inputFrameLeft, inputFrameLeft);
  //Sobel( grayImageLeft, sobelOutputLeft, -1, 0, 1, 3, 1, 0, BORDER_DEFAULT );
  //Sobel( grayImageRight, sobelOutputRight, -1, 0, 1, 3, 1, 0, BORDER_DEFAULT );

  //int lowThreshold = 100;
  //Canny( sobelOutputLeft, sobelOutputLeft, lowThreshold, lowThreshold*3, 3 );

  //imshow("OriginalFrame",inputFrameLeft);
  //imshow("SobelLeft", sobelOutputLeft);
  //imshow("SobelRight", sobelOutputRight);
  //imshow("OriginalFrameRight",inputFrameRight);
  //imshow("Converted1", destination);

  /*
  hsvThreshold(0,0);
  while(true){
      waitKey(30);
  }

  return 0;
  */
}

//Summiert sobel bei einzelnen tresh auf
void sobel_Tresh(Mat inputFrame){
   Mat inputFrame_gray;
   Mat sobelOutput;
   Mat treshOutput;
   Mat lineImage;
   Mat lineImage2;
   int tresh_hough, tresh_gray, length_hough;
   //int color;

   //Mat lineImage = Mat::zeros( inputFrame.size(), CV_8UC3 );

   cvtColor(inputFrame, inputFrame_gray, CV_BGR2GRAY);
   Sobel( inputFrame_gray, sobelOutput, -1, 0, 1, 1, 1, 0, BORDER_DEFAULT );
   cvtColor(sobelOutput, lineImage, CV_GRAY2BGR);
   cvtColor(sobelOutput, lineImage2, CV_GRAY2BGR);

   for(int i=0; i<25; i++){
      vector<Vec4i> linesP;
      tresh_gray=25-i;
      //color=255-8*i;
      threshold(sobelOutput, treshOutput, tresh_gray, 255, THRESH_BINARY); //Threshold the image
      //imshow("Detected_Lines", treshOutput);
      //waitKey(0);
      for(int j=0; j<10; j++){
         tresh_hough = 200-10*j;
         //length_hough = 160-j;
         HoughLinesP( treshOutput, linesP, 1, CV_PI/180, tresh_hough, 50, 10 );

         for( size_t i = 0; i < linesP.size(); i++ )
         {
            line( lineImage, Point(linesP[i][0], linesP[i][1]), Point(linesP[i][2], linesP[i][3]), Scalar(0,0, 255), 2, 8 );
         }
         /*
         HoughLinesP( treshOutput, linesP, 1, CV_PI/180, tresh_hough, length_hough, 10 );

         for( size_t i = 0; i < linesP.size(); i++ )
         {
            line( lineImage2, Point(linesP[i][0], linesP[i][1]), Point(linesP[i][2], linesP[i][3]), Scalar(0, 0, 255), 2, 8 );
         }
         */
         //imshow("Detected_Lines w. Length", lineImage2);

         imshow("Detected_Lines", lineImage);
         waitKey(0);
      }
      cout<<"Tresh: "<<tresh_gray<<endl;
      imshow("Detected_Lines", lineImage);
      //imshow("Detected_Lines w. Length", lineImage2);
      waitKey(0);
   }
   waitKey(0);
}

void cut_Image(Mat& originalImg, Mat& dstImg, bool left){

   dstImg = originalImg.clone();

   Point points[2][5];

   //Points Left Image
   points[0][0] = Point(0, 0);
   points[0][1] = Point(0, originalImg.rows*0.6);
   points[0][2] = Point(originalImg.cols*0.55, originalImg.rows*0.545);
   points[0][3] = Point(originalImg.cols, originalImg.rows*0.58);
   points[0][4] = Point(originalImg.cols, 0);

   //Points Right Image
   points[1][0] = Point(0, 0);
   points[1][1] = Point(0, originalImg.rows*0.58);
   points[1][2] = Point(originalImg.cols*0.45, originalImg.rows*0.545);
   points[1][3] = Point(originalImg.cols, originalImg.rows*0.6);
   points[1][4] = Point(originalImg.cols, 0);

   if(left){
      const Point* ppt[1] = {points[0]};
      int npt[] = {5};

      //line(cut_Img, point1_L, point2_L, Scalar(0, 0, 255), 2, 8, 0);
      //line(cut_Img, point2_L, point3_L, Scalar(0, 255, 0), 2, 8, 0);
      fillPoly( dstImg, ppt, npt, 1, Scalar( 0, 0, 0 ), 8 );
   }
   else{
      const Point* ppt[1] = {points[1]};
      int npt[] = {5};

      //line(cut_Img, point1_R, point2_R, Scalar(0, 0, 255), 2, 8, 0);
      //line(cut_Img, point2_R, point3_R, Scalar(0, 255, 0), 2, 8, 0);
      fillPoly( dstImg, ppt, npt, 1, Scalar( 0, 0, 0 ), 8 );
   }
   //imshow("Cut Image", cut_Img);
   //waitKey(0);
}

void detecFieldLinesSobel(Mat& originalImg, bool left){
   Mat Img_gray, Img_sobelx, Img_sobely, Img_sobel, Img_sobel_cut, Img_sobelTrashed;
   Mat erosion_dst;
   vector<Vec4i> lines;
   Point point1, point2, borderpoint1, borderpoint2;
   int tresh_hough, length_hough, gap_hough;

   Mat drawingImg = originalImg.clone();

   //m_originalImg = originalImg;

   imshow("Window", originalImg);
   waitKey(0);

   m_imgLines = Mat::zeros( originalImg.size(), CV_8UC3 );

   cvtColor(originalImg, Img_gray, CV_BGR2GRAY);

   //Sobel( Img_gray, Img_sobelx, -1, 1, 0, 1, 1, 0, BORDER_DEFAULT );
   Sobel( Img_gray, Img_sobel, -1, 0, 1, 1, 1, 0, BORDER_DEFAULT );

   //addWeighted(Img_sobelx, 1, Img_sobely, 1, 0, Img_sobel);
   cut_Image(Img_sobel, Img_sobel_cut, left);


   imshow("Window", Img_sobel_cut);
   waitKey(0);

   threshold(Img_sobel_cut, Img_sobelTrashed, 7, 255, THRESH_BINARY); //Threshold the image

   imshow("Window", Img_sobelTrashed);
   waitKey(0);
/*
   //Variate the tresh
   for(int j=0; j<10; j++){
      tresh_hough = 200-5*j;

      HoughLinesP( Img_sobelTrashed, lines, 1, CV_PI/180, tresh_hough, 100, 10 );

      for( uint32_t i = 0; i < lines.size(); i++ ){
         line( m_imgLines, Point(lines[i][0], lines[i][1]), Point(lines[i][2], lines[i][3]), Scalar(255,0,0), 2, 8 ); //eig thickness = 2
      }

      cout<<"Tresh Hough variated:"<<tresh_hough<<endl;
   }
   imshow("Window", m_imgLines);
   waitKey(0);

   //Variate the length
   for(int j=0; j<10; j++){
      length_hough = 100-6*j;

      HoughLinesP( Img_sobelTrashed, lines, 1, CV_PI/180, 200, length_hough, 10 );

      for( uint32_t i = 0; i < lines.size(); i++ ){
         line( m_imgLines, Point(lines[i][0], lines[i][1]), Point(lines[i][2], lines[i][3]), Scalar(0,255,0), 2, 8 ); //eig thickness = 2
      }
      cout<<"Length Hough variated:"<<length_hough<<endl;
   }
   imshow("Window", m_imgLines);
   waitKey(0);
*/
   //Variate the Gab
   //for(int j=0; j<10; j++){
      gap_hough = 5; //17-j;

      HoughLinesP( Img_sobelTrashed, lines, 1, CV_PI/180, 130, 100, gap_hough );

      //cvtColor(Img_sobelTrashed, Img_sobelTrashed, CV_GRAY2BGR);

      //Draw lines up to border
      for( uint32_t i = 0; i < lines.size(); i++ ){
         //line( m_imgLines, Point(lines[i][0], lines[i][1]), Point(lines[i][2], lines[i][3]), Scalar(0,0,255), 2, 8 ); //eig thickness = 2

         point1 = Point(lines[i][0], lines[i][1]);
         point2 = Point(lines[i][2], lines[i][3]);

         calculateLinePointsBorder(point1, point2, borderpoint1, borderpoint2, originalImg.cols, originalImg.rows);

         line( m_imgLines, borderpoint1, borderpoint2, Scalar(0,255,0), 2, 8 ); //eig thickness = 2
         //line( Img_sobelTrashed, borderpoint1, borderpoint2, Scalar(0,255,0), 2, 8 ); //eig thickness = 2
      }

      cout<<"Gap Hough variated:"<<gap_hough<<endl;

      imshow("Window", m_imgLines);
      waitKey(0);
   //}

   //HOUGH auf HOUGH
/*
   cvtColor(m_imgLines, erosion_dst, CV_BGR2GRAY);
   erode(erosion_dst, erosion_dst, Mat());

   //imshow("Window", erosion_dst);
   //waitKey(0);

   HoughLinesP( erosion_dst, lines, 1, CV_PI/360, 67, 78, 78 );

   //Draw lines up to border
   for( uint32_t i = 0; i < lines.size(); i++ ){
      point1 = Point(lines[i][0], lines[i][1]);
      point2 = Point(lines[i][2], lines[i][3]);

      calculateLinePointsBorder(point1, point2, borderpoint1, borderpoint2, originalImg.cols, originalImg.rows);

      //cout<<"Line "<<i<<"Point 1: "<<lines[i][0]<<" | "<<lines[i][1]<<endl;
      //cout<<"Point 2: "<<lines[i][2]<<" | "<<lines[i][3]<<endl;

      cout<<"BorderPoint 1: "<<borderpoint1.x<<" | "<<borderpoint1.y<<endl;
      cout<<"BorderPoint 2: "<<borderpoint2.x<<" | "<<borderpoint2.y<<endl;
      cout<<endl;
      line( m_imgLines, borderpoint1, borderpoint2, Scalar(0,255,0), 2, 8 ); //eig thickness = 2

      imshow("Window", m_imgLines);
      waitKey(1);
   }
*/
/*
   namedWindow( window_name, CV_WINDOW_NORMAL );

   createTrackbar( "Tresh", window_name, &tresh, 500, build_HoughlinesP );
   createTrackbar( "minLength", window_name, &minLength, 1000, build_HoughlinesP );
   createTrackbar( "maxGap", window_name, &maxGap, 1000, build_HoughlinesP );

   build_HoughlinesP(0, 0);
   while(true){
       waitKey(30);
   }
*/
   cvtColor(m_imgLines, m_imgLines, CV_BGR2GRAY);
   threshold(m_imgLines, m_imgLines, 70, 255, THRESH_BINARY_INV);

   vector<vector<Point> > contours;
   vector<Vec4i> hierarchy;
   RNG rng(12345);

   findContours( m_imgLines, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
   Mat drawing = Mat::zeros( m_imgLines.size(), CV_8UC3 );

   cout<<"Numbers of Contours: "<<contours.size()<<endl;
   vector<Point2f> cornerPoints;

   /*
   for( int i = 0; i< contours.size(); i++ ){
      Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
      drawContours( drawing, contours, i, color, -1, 8, hierarchy, 0, Point() );
   }
   */

   Point2f midpoint;
   if(left){
      midpoint = Point(0.164*originalImg.cols, 0.68*originalImg.rows); //Point for left Image
   }
   else{
      midpoint = Point(0.836*originalImg.cols, 0.68*originalImg.rows); //Point for right Image
   }
   int index = 0;

   imgProc::imageObjects::findContourIndexBelongingToPoint(contours, midpoint, index);

   //TODO: Fläche der Kontur ca 9700 Pixel
   double area = contourArea(contours[index], false);
   cout<<"Size of contour: "<<area<<endl;

   if(area > 8000 && area < 12000){
      drawContours( drawingImg, contours, index, Scalar(0, 255, 0), -1, 8, hierarchy, 0, Point() );

      imgProc::imageObjects::findContourCornerPoints(contours[index], cornerPoints, left);

      for(int i = 0; i < cornerPoints.size(); i++){
         cout<<cornerPoints[i].x<<" | "<<cornerPoints[i].y;
         circle(drawingImg, cornerPoints[i], 3, Scalar(0, 0, 255), -1,8,0);
         cout<<" Point added"<<endl;
      }
   }



   // Show in a window
   imshow( "Detected Contour", drawingImg );
   waitKey(0);

   //if(m_calibrationIsRunning == true){
      //m_calibratedImgLines += m_imgLines;
      //imshow("CalibrationImg",m_calibratedImgLines);
   //}

   //imshow("Window", m_calibratedImgLines);
   //waitKey(0);

   //return EXIT_SUCCESS;
}

void calculateLinePointsBorder(Point& point1, Point& point2, Point& borderpoint1, Point& borderpoint2, int imageWidth, int imageHeight){
   double m;
   if(point1.x > point2.x){
      Point switchpoint = point2;
      point2 = point1;
      point1 = switchpoint;
   }
   cout<<"Point 1: "<<point1.x<<" "<<point1.y<<endl;
   cout<<"Point 2: "<<point2.x<<" "<<point2.y<<endl;

   m = (double(point2.y - point1.y)) / double((point2.x - point1.x));

   cout<<"Steigung: "<<m<<endl;
   //3 Cases for each point
   //Left Point contacts upper border
   if((point1.y -m * point1.x) < 0){
      borderpoint1.x = point1.x - abs(1/m)* point1.y;
      borderpoint1.y = 0;
   }
   //Left point contacts lower border
   else if((point1.y -m * point1.x) > imageHeight){
      borderpoint1.x = point1.x - abs(1/m)* (imageHeight - point1.y);
      borderpoint1.y = imageHeight;
   }
   //Left point contacts left border
   else{
      borderpoint1.x = 0;
      borderpoint1.y = point1.y -m * point1.x;
   }

   //Right point contacts upper border
   if((point2.y +m * (imageWidth - point2.x)) < 0){
      borderpoint2.x = point2.x + abs(1/m)* point2.y;
      borderpoint2.y = 0;
   }
   //Right point contacts lower border
   else if((point2.y +m * (imageWidth - point2.x)) > imageHeight){
      borderpoint2.x = point2.x + abs(1/m)* (imageHeight - point2.y);
      borderpoint2.y = imageHeight;
   }
   //Right point contacts right border
   else{
      borderpoint2.x = imageWidth;
      borderpoint2.y = point2.y + m* (imageWidth-point2.x);
   }

   cout<<"BorderPoint 1: "<<borderpoint1.x<<" | "<<borderpoint1.y<<endl;
   cout<<"BorderPoint 2: "<<borderpoint2.x<<" | "<<borderpoint2.y<<endl;

   cout<<endl;
}

void diff_balls(string filenameLeft, string filenameRight){
   VideoCapture captureL( filenameLeft );
   VideoCapture captureR( filenameRight );
   int frameNumberLeft = captureL.get(CV_CAP_PROP_FRAME_COUNT);
   int frameNumberRight = captureR.get(CV_CAP_PROP_FRAME_COUNT);

   //Seek video to last frame
   captureL.set(CV_CAP_PROP_POS_FRAMES,frameNumberLeft-1);
   captureR.set(CV_CAP_PROP_POS_FRAMES,frameNumberRight-1);

   //Capture the last frame
   captureL>>inputFrameLeftPilone;
   captureR>>inputFrameRightPilone;

   //Rewind video
   captureL.set(CV_CAP_PROP_POS_FRAMES,0);
   captureR.set(CV_CAP_PROP_POS_FRAMES,0);

   captureL>>inputFrameLeft;
   captureR>>inputFrameRight;

   absdiff(inputFrameLeft,inputFrameLeftPilone, diffLeft);
   absdiff(inputFrameRight,inputFrameRightPilone, diffRight);

   namedWindow( "window_Left", CV_WINDOW_AUTOSIZE );
   namedWindow( "window_Right", CV_WINDOW_AUTOSIZE );
   imshow("window_Left", diffLeft);
   imshow("window_Right", diffRight);
   waitKey(0);

   namedWindow( window_name, CV_WINDOW_NORMAL );

   createTrackbar( "Vmin", window_name, &vmin, 256, hsvThreshold );
   createTrackbar( "Vmax", window_name, &vmax, 256, hsvThreshold );
   createTrackbar( "Smin", window_name, &smin, 256, hsvThreshold );
   createTrackbar( "Smax", window_name, &smax, 256, hsvThreshold );
   createTrackbar( "Hmin", window_name, &hmin, 256, hsvThreshold );
   createTrackbar( "Hmax", window_name, &hmax, 256, hsvThreshold );

   cvtColor(diffRight, hsvFrame, CV_BGR2HSV);

   hsvThreshold(0,0);
   while(true){
       waitKey(30);
   }
}

void build_difference(string filenameLeft, string filenameRight, int frameSkip, int frameNumber){

   /*
   if(frameNumber<=1){
      return EXIT_FAILURE;
   }
   */
   namedWindow( "Difference_Left", CV_WINDOW_AUTOSIZE );
   namedWindow( "Difference_Right", CV_WINDOW_AUTOSIZE );

   VideoCapture captureL( filenameLeft );
   VideoCapture captureR( filenameRight );
   Mat diffMatLeft1, diffMatLeft2, diffMatRight1, diffMatRight2;
   Mat buffFrameLeft, buffFrameRight;

   captureL>>inputFrameLeft;
   captureR>>inputFrameRight;

   for(int i=0; i<frameSkip; i++){
      captureL>>buffFrameLeft;
      captureR>>buffFrameRight;
   }

   absdiff(inputFrameLeft, buffFrameLeft, diffMatLeft1);
   absdiff(inputFrameRight, buffFrameRight, diffMatRight1);

   inputFrameLeft = buffFrameLeft.clone();
   inputFrameRight = buffFrameRight.clone();

   // bool bSuccess;
   for(int i=0; i<frameNumber-1; i++){
         for(int j=0; j<frameSkip; j++){
            captureL>>buffFrameLeft;
            captureR>>buffFrameRight;
         }

      absdiff(inputFrameLeft, buffFrameLeft, diffMatLeft2);
      absdiff(inputFrameRight, buffFrameRight, diffMatRight2);

      //imshow("window_Left", diffMatRight2);
      //waitKey(0);
      bitwise_or(diffMatLeft1, diffMatLeft2, diffMatLeft1);
      bitwise_or(diffMatRight1, diffMatRight2, diffMatRight1);

      //imshow("window_Left", diffMatRight2);
      //waitKey(0);

      inputFrameLeft = buffFrameLeft.clone();
      inputFrameRight = buffFrameRight.clone();

   }

   imshow( "Difference_Left", diffMatLeft1);
   imshow( "Difference_Right", diffMatRight1);
   waitKey(0);

   destroyWindow("Difference_Left");
   destroyWindow("Difference_Right");
   //return diffMatLeft1;
}

void sum_sobel(string filenameLeft, string filenameRight, /*string filenameLeftSRGB, string filenameRightSRGB,*/ int frameSkip, int frameNumber){

   Mat destL, destR;
   /*
   if(frameNumber<=1){
      return EXIT_FAILURE;
   }
   */

   VideoCapture captureL( filenameLeft );
   VideoCapture captureR( filenameRight );
   //VideoCapture captureLSRGB( filenameLeftSRGB );
   //VideoCapture captureRSRGB( filenameRightSRGB );

   captureL>>inputFrameLeft;
   captureR>>inputFrameRight;
   //captureLSRGB>>inputFrameLeftSRGB;
   //captureRSRGB>>inputFrameRightSRGB;

   cvtColor(inputFrameLeft, grayImageLeft, CV_BGR2GRAY);
   cvtColor(inputFrameRight, grayImageRight, CV_BGR2GRAY);
   //cvtColor(inputFrameLeftSRGB, grayImageLeftSRGB, CV_BGR2GRAY);
   //cvtColor(inputFrameRightSRGB, grayImageRightSRGB, CV_BGR2GRAY);

   Sobel( grayImageLeft, sobelSumLeft, -1, 0, 1, 1, 1, 0, BORDER_DEFAULT );
   Sobel( grayImageRight, sobelSumRight, -1, 0, 1, 1, 1, 0, BORDER_DEFAULT );
   //Sobel( grayImageLeftSRGB, sobelSumLeftSRGB, -1, 0, 1, 1, 1, 0, BORDER_DEFAULT );
   //Sobel( grayImageRightSRGB, sobelSumRightSRGB, -1, 0, 1, 1, 1, 0, BORDER_DEFAULT );

   // bool bSuccess;
   for(int i=0; i<frameNumber-1; i++){
         for(int j=0; j<frameSkip; j++){
            captureL>>inputFrameLeft;
            captureR>>inputFrameRight;
            //captureLSRGB>>inputFrameLeftSRGB;
            //captureRSRGB>>inputFrameRightSRGB;
         }

      cvtColor(inputFrameLeft, grayImageLeft, CV_BGR2GRAY);
      cvtColor(inputFrameRight, grayImageRight, CV_BGR2GRAY);
      //cvtColor(inputFrameLeftSRGB, grayImageLeftSRGB, CV_BGR2GRAY);
      //cvtColor(inputFrameRightSRGB, grayImageRightSRGB, CV_BGR2GRAY);


      Sobel( grayImageLeft, sobelLeft, -1, 0, 1, 1, 1, 0, BORDER_DEFAULT );
      Sobel( grayImageRight, sobelRight, -1, 0, 1, 1, 1, 0, BORDER_DEFAULT );
      //Sobel( grayImageLeftSRGB, sobelLeftSRGB, -1, 0, 1, 1, 1, 0, BORDER_DEFAULT );
      //Sobel( grayImageRightSRGB, sobelRightSRGB, -1, 0, 1, 1, 1, 0, BORDER_DEFAULT );

      //bitwise_or(sobelLeft, sobelSumLeft, sobelSumLeft);
      //bitwise_or(sobelRight, sobelSumRight, sobelSumRight);

      addWeighted( sobelLeft, 0.5, sobelSumLeft, 0.5, 0, sobelSumLeft );
      addWeighted( sobelRight, 0.5, sobelSumRight, 0.5, 0, sobelSumRight );

      //addWeighted( sobelLeftSRGB, 1, sobelSumLeftSRGB, 1, 0, sobelSumLeftSRGB );
      //addWeighted( sobelRightSRGB, 1, sobelSumRightSRGB, 1, 0, sobelSumRightSRGB );

      /*if(i==2 || i==10 || i==29){
         imshow("window_Left", sobelSumLeft);
         imshow("window_Right", sobelSumRight);
         waitKey(0);
      }*/

      }

   //addWeighted( sobelSumLeft, 0.5, sobelSumLeftSRGB, 0.5, 0, destL );
   //addWeighted( sobelSumRight, 0.5, sobelSumRightSRGB, 0.5, 0, destR );

   dst=sobelLeft.clone();
   //threshold(dst, dst, 5, 255, THRESH_BINARY); //Threshold the image

   grayTresholdImage=sobelRight.clone();
   grayTresholdImage2=sobelLeft.clone();

   threshold(grayTresholdImage, dst, 5, 255, THRESH_BINARY); //Threshold the Right image
   threshold(grayTresholdImage2, dst2, 5, 255, THRESH_BINARY); //Threshold the Left image

   /*
   grayTresholdImage=destL.clone();
   namedWindow( window_name, CV_WINDOW_AUTOSIZE );
   createTrackbar( "tresh", window_name, &tresh, 256, grayThreshold );

   grayThreshold(0,0);
   while(true){
       waitKey(30);
   }
   */

   imshow("dst", dst);
   waitKey(0);
   namedWindow( window_name, CV_WINDOW_NORMAL );

   createTrackbar( "Tresh", window_name, &tresh, 500, build_HoughlinesP );
   createTrackbar( "minLength", window_name, &minLength, 1000, build_HoughlinesP );
   createTrackbar( "maxGap", window_name, &maxGap, 1000, build_HoughlinesP );

   build_HoughlinesP(0, 0);
   while(true){
       waitKey(30);
   }

   // return destL;
}

void sum_laplacian(string filenameLeft, string filenameRight, int frameSkip, int frameNumber){

   /*
   if(frameNumber<=1){
      return EXIT_FAILURE;
   }
   */
   namedWindow( "window_Left", CV_WINDOW_AUTOSIZE );
   namedWindow( "input_Frame", CV_WINDOW_AUTOSIZE );

   VideoCapture captureL( filenameLeft );
   VideoCapture captureR( filenameRight );

   Mat laplacianSumLeft, laplacianSumRight, laplacianLeft, laplacianRight;

   captureL>>inputFrameLeft;
   captureR>>inputFrameRight;

   cvtColor(inputFrameLeft, grayImageLeft, CV_BGR2GRAY);
   cvtColor(inputFrameRight, grayImageRight, CV_BGR2GRAY);

   Laplacian( grayImageLeft, laplacianSumLeft, -1, 1, 1, 0, BORDER_DEFAULT );
   Laplacian( grayImageRight, laplacianSumRight, -1, 1, 1, 0, BORDER_DEFAULT );

   imshow("window_Left", laplacianSumLeft);
   imshow("window_Right", laplacianSumRight);
   waitKey(0);
   // bool bSuccess;
   for(int i=0; i<frameNumber-1; i++){
         for(int j=0; j<frameSkip; j++){
            captureL>>inputFrameLeft;
            captureR>>inputFrameRight;
         }

      cvtColor(inputFrameLeft, grayImageLeft, CV_BGR2GRAY);
      cvtColor(inputFrameRight, grayImageRight, CV_BGR2GRAY);

      Laplacian( grayImageLeft, laplacianLeft, -1, 1, 1, 0, BORDER_DEFAULT );
      Laplacian( grayImageRight, laplacianRight, -1, 1, 1, 0, BORDER_DEFAULT );

      //bitwise_or(sobelLeft, sobelSumLeft, sobelSumLeft);
      //bitwise_or(sobelRight, sobelSumRight, sobelSumRight);

      addWeighted( laplacianLeft, 1, laplacianSumLeft, 1, 0, laplacianSumLeft );
      addWeighted( laplacianRight, 1, laplacianSumRight, 1, 0, laplacianSumRight );

      imshow("window_Left", laplacianSumLeft);
      imshow("window_Right", laplacianSumRight);
      waitKey(0);
   }

   destroyWindow("window_Left");
   destroyWindow("window_Right");
}


//Vergleich mit/Ohne HistogramEqualisation
void build_sobel1(Mat inputFrameLeft, Mat inputFrameRight){

   imshow( "input_Left", inputFrameLeft);
   imshow( "input_Right", inputFrameRight);
   //waitKey(0);
   cvtColor(inputFrameLeft, grayImageLeft, CV_BGR2GRAY);
   cvtColor(inputFrameRight, grayImageRight, CV_BGR2GRAY);

   Sobel( grayImageLeft, sobelOutputLeft, -1, 0, 1, 1, 1, 0, BORDER_DEFAULT );
   Sobel( grayImageRight, sobelOutputRight, -1, 0, 1, 1, 1, 0, BORDER_DEFAULT );

   imshow( "Sobel_Left", sobelOutputLeft);
   imshow( "Sobel_Right", sobelOutputRight);
   waitKey(0);

   Mat inputFrameLeftEqualized;
   Mat inputFrameRightEqualized;
   imgProc::imageProcessing::histogramEqualisation(inputFrameLeft, inputFrameLeftEqualized);
   imgProc::imageProcessing::histogramEqualisation(inputFrameRight, inputFrameRightEqualized);

   imshow( "input_Left", inputFrameLeftEqualized);
   imshow( "input_Right", inputFrameRightEqualized);


   cvtColor(inputFrameLeftEqualized, grayImageLeft, CV_BGR2GRAY);
   cvtColor(inputFrameRightEqualized, grayImageRight, CV_BGR2GRAY);


   Sobel( grayImageLeft, sobelOutputLeft, -1, 0, 1, 1, 1, 0, BORDER_DEFAULT );
   Sobel( grayImageRight, sobelOutputRight, -1, 0, 1, 1, 1, 0, BORDER_DEFAULT );

   imshow( "Sobel_Left", sobelOutputLeft);
   imshow( "Sobel_Right", sobelOutputRight);
   waitKey(0);

   //GaussianBlur( grayImageLeft, grayImageLeft, Size(3,3), 0, 0, BORDER_DEFAULT );
   //GaussianBlur( grayImageRight, grayImageRight, Size(3,3), 0, 0, BORDER_DEFAULT );

   //Sobel( grayImageLeft, sobelOutputLeft, -1, 1, 0, 3, 1, 0, BORDER_DEFAULT );
   //Sobel( grayImageRight, sobelOutputRight, -1, 1, 0, 3, 1, 0, BORDER_DEFAULT );

   //destroyWindow("Sobel_Left");
   //destroyWindow("Sobel_Right");
}

void build_sobel2(Mat inputFrame){
   Mat inputFrame_gray;
   Mat grad_x, grad_y;
   Mat abs_grad_x, abs_grad_y;
   Mat grad;
   //int ddepth = CV_16S; //Value 3??

   //GaussianBlur( inputFrame, inputFrame, Size(3,3), 0, 0, BORDER_DEFAULT );

   cvtColor( inputFrame, inputFrame_gray, CV_BGR2GRAY );

   namedWindow( "Sobel_2", CV_WINDOW_AUTOSIZE );

   //Scharr( src_gray, grad_x, ddepth, 1, 0, scale, delta, BORDER_DEFAULT );
   Sobel( inputFrame_gray, grad_x, -1, 1, 0, 3, 1, 0, BORDER_DEFAULT );
   //convertScaleAbs( grad_x, abs_grad_x );

   //Scharr( src_gray, grad_y, ddepth, 0, 1, scale, delta, BORDER_DEFAULT );
   Sobel( inputFrame_gray, grad_y, -1, 0, 1, 3, 1, 0, BORDER_DEFAULT );
   //convertScaleAbs( grad_y, abs_grad_y );

   addWeighted( grad_x, 0.5, grad_y, 0.5, 0, grad );
   imshow( "Sobel_2", grad );
   waitKey(0);

   addWeighted( grad_x, 0.8, grad_y, 0.8, 0, grad );
   imshow( "Sobel_2", grad );
   waitKey(0);

   addWeighted( grad_x, 1, grad_y, 1, 0, grad );
   imshow( "Sobel_2", grad );
   waitKey(0);
   destroyWindow( "Sobel_2" );
}

void build_laplacian(Mat inputFrame){
   Mat inputFrame_gray;
   Mat laplace;
   cvtColor( inputFrame, inputFrame_gray, CV_BGR2GRAY );

   Laplacian(inputFrame_gray, laplace, -1, 1, 1, 0, BORDER_DEFAULT );

   imshow("Laplacian", laplace);
   waitKey(0);
}

void build_canny(Mat inputFrame){

   Mat cropedFrame = inputFrame(Rect(0, 300, inputFrame.cols, inputFrame.rows-300));
   imshow( window_name, cropedFrame );
   waitKey(0);

   cvtColor(cropedFrame, src_gray, CV_BGR2GRAY);
   blur( src_gray, detected_edges, Size(3,3) );

   Canny( detected_edges, detected_edges, 10, 30, kernel_size );

   dst = Scalar::all(0);
   cropedFrame.copyTo( dst, detected_edges);

   imshow( window_name, dst );
   waitKey(0);

   //resize(dst, dst, Size(), 0.82, 0.82, INTER_LINEAR);
   //imshow( window_name, dst );
   //waitKey(0);

   cvtColor(dst, hsvFrame, CV_BGR2HSV);
   inRange(hsvFrame, Scalar(1, 0, 0), Scalar(40, 255, 255), dst); //Threshold the image

   namedWindow( window_name, CV_WINDOW_AUTOSIZE );

   createTrackbar( "Tresh", window_name, &tresh, 500, build_HoughlinesP );
   createTrackbar( "minLength", window_name, &minLength, 1000, build_HoughlinesP );
   createTrackbar( "maxGap", window_name, &maxGap, 100, build_HoughlinesP );



   build_HoughlinesP(0, 0);
   while(true){
       waitKey(30);
   }

}

//////////////////TRACKBAR FUNCTIONS

void CannyThreshold(int, void*){
  /// Reduce noise with a kernel 3x3
  blur( src_gray, detected_edges, Size(3,3) );
  //blur( src_gray, detected_edgesblur, Size(5,5) );
  //detected_edges=src_gray.clone();

  /// Canny detector
  Canny( detected_edges, detected_edges, lowThreshold, lowThreshold*3, kernel_size );
  //Canny( detected_edgesblur, detected_edgesblur, lowThreshold, lowThreshold*3, kernel_size );
  /// Using Canny's output as a mask, we display our result
  dst = Scalar::all(0);
  //dstblur = Scalar::all(0);

  inputFrameRight.copyTo( dst, detected_edges);
  //inputFrameLeft.copyTo( dstblur, detected_edgesblur);

  imshow( window_name, dst );
  //imshow( "Blur", dstblur );
  cout<<"tresh: "<<lowThreshold<<endl;
 }


void hsvThreshold(int, void*){
  Mat colorFilterdBinaryImg;
  inRange(hsvFrame, Scalar(hmin, smin, vmin), Scalar(hmax, smax, vmax), colorFilterdBinaryImg); //Threshold the image

  imshow(window_name, colorFilterdBinaryImg);
  cout<<"hmin: "<<hmin<<" hmax: "<<hmax<<endl;
  cout<<"smin: "<<smin<<" smax: "<<smax<<endl;
  cout<<"vmin: "<<vmin<<" vmax: "<<vmax<<endl;

}

void build_Houghlines(int, void*){
   HoughLines(dst, lines, 1, CV_PI/180, tresh, 0, 0 );
   //imshow("dst", dst);
   //waitKey(0);

   for( size_t i = 0; i < lines.size(); i++ )
   {
      float rho = lines[i][0], theta = lines[i][1];
      Point pt1, pt2;
      double a = cos(theta), b = sin(theta);
      double x0 = a*rho, y0 = b*rho;
      pt1.x = cvRound(x0 + 1000*(-b));
      pt1.y = cvRound(y0 + 1000*(a));
      pt2.x = cvRound(x0 - 1000*(-b));
      pt2.y = cvRound(y0 - 1000*(a));
      //line( dst, pt1, pt2, Scalar(0,0,255), 3, CV_AA);
      line( dst, pt1, pt2, 200, 1, 8, 0);
   }
   imshow(window_name, dst);
}

void build_HoughlinesP(int, void*){
   //vector<Vec4i> lines;
   HoughLinesP( dst, linesP, 1, CV_PI/360, tresh, minLength, maxGap );
   //HoughLinesP( dst2, linesP2, 1, CV_PI/360, tresh, minLength, maxGap );
   cvtColor(dst, dst_BGR, CV_GRAY2BGR);
   //cvtColor(dst2, dst_BGR2, CV_GRAY2BGR);
   for( size_t i = 0; i < linesP.size(); i++ )
   {
      line( dst_BGR, Point(linesP[i][0], linesP[i][1]), Point(linesP[i][2], linesP[i][3]), 200, 2, 8 );
   }
   /*
   for( size_t i = 0; i < linesP2.size(); i++ )
   {
      line( dst_BGR2, Point(linesP2[i][0], linesP2[i][1]), Point(linesP2[i][2], linesP2[i][3]), 200, 2, 8 );
   }
   */
   imshow(window_name, dst_BGR);
   //imshow("anderes window", dst_BGR2);
   cout<<"Tresh: "<<tresh<<endl;
   cout<<"minLength: "<<minLength<<endl;
   cout<<"maxGap: "<<maxGap<<endl;
}

//Tresh Binary
void grayThreshold(int, void*){
  Mat dst;
  threshold(grayTresholdImage, dst, tresh, 255, THRESH_BINARY); //Threshold the right image
  threshold(grayTresholdImage2, dst2, tresh, 255, THRESH_BINARY); //Threshold the left image
  imshow(window_name, dst);
  imshow("Nicht summiert", dst2);
  cout<<"tresh: "<<tresh<<endl;
}
