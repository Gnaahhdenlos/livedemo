#include "opencv2/core/utility.hpp"
#include "opencv2/core.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/video.hpp"
//#define GPU_ON

#ifdef GPU_MODE
#include "opencv2/gpu/gpu.hpp"
#include "opencv2/cudabgsegm.hpp"
#include "opencv2/cudalegacy.hpp"
#endif

#include <iostream>
#include <ctype.h>
#include <algorithm>
#include <vector>
#include <numeric>



using namespace cv;

using namespace std;
#ifdef GPU_ON
	using namespace cv::cuda;
    struct bufferBG                                     // Optimized CUDA versions
    {   // Data allocations are very expensive on CUDA. Use a buffer to solve: allocate once reuse later.
    cuda::GpuMat frame, frameGray, bgImage, resized, reresized, bgImageMOG2, output;
    };
#endif


int main (int argc, char* argv[]){

  char* filename = argc >= 2 ? argv[1] : (char*)"768x576.avi";
  VideoCapture capture( filename );

  float resizeFactor = 1.0f;
  float reresizeFactor = 1.0f/resizeFactor;

  TickMeter tm;
  vector<double> times;
#ifndef GPU_ON
  Mat frame, frameGray, HSV, bgImage, bgImageHSV, diffH, diffS, output, outputErode, resized, bgImageMOG2, sobelOutput, sobelOutputBG;
  vector<Mat> HSVChannels(3);
  vector<Mat> bgHSVChannels(3);
  capture.read(bgImage);

  cvtColor(bgImage, bgImageHSV, CV_BGR2HSV);
  split(bgImageHSV, bgHSVChannels);
  Ptr<BackgroundSubtractor> bgS = createBackgroundSubtractorMOG2(50, 10, false);
  int frameCount = 0;
  while(true){
    cout<<"frameCount: "<<frameCount<<endl;
    capture.read(frame);
    cvtColor(frame,frameGray, CV_BGR2GRAY);
    frameCount++;
		tm.reset(); tm.start();
    resize(frameGray, resized, Size(), resizeFactor, resizeFactor, INTER_LINEAR);
    if(frameCount< 30){
        bgS->apply(resized, output, 0.07);
        bgS->getBackgroundImage(bgImageMOG2);
    }
    else if(frameCount%30 == 0){
        bgS->apply(resized, output, 0.07);
        bgS->getBackgroundImage(bgImageMOG2);
    }
    else{
        //bgS->apply(resized, output, 0);
        absdiff(bgImageMOG2, resized, output);
        threshold(output, output, 7, 255.0f, THRESH_BINARY);
    }

    int erosion_size = 1;
    erode(output, outputErode, getStructuringElement( MORPH_ELLIPSE,
                                       Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                                       Point( erosion_size, erosion_size ) ));
    erosion_size = 1;
    dilate(outputErode, outputErode, getStructuringElement( MORPH_ELLIPSE, Size( 2*erosion_size + 1, 2*erosion_size+1 ), Point( erosion_size, erosion_size ) ),Point(-1,-1), 2);
    tm.stop();
    resize(outputErode, outputErode, Size(), reresizeFactor, reresizeFactor, INTER_NEAREST);

    if(frameCount > 60){
    times.push_back(tm.getTimeMilli());
    std::sort(times.begin(), times.end());
    double avg = std::accumulate(times.begin(), times.end(), 0.0) / times.size();
    std::cout << "Avg : " << avg << " ms FPS : " << 1000.0 / avg << std::endl;
    }

		if(frameCount%10 == 0){
			imshow("outputErode",outputErode);
	    imshow("output",output);
	    waitKey(30);
		}

    // cvtColor(frame, HSV, CV_BGR2HSV);
    // split(HSV, HSVChannels);
    // absdiff(bgHSVChannels[0], HSVChannels[0], diffH);
    // absdiff(bgHSVChannels[1], HSVChannels[1], diffS);
    // imshow("H", HSVChannels[0]);
    // imshow("S", HSVChannels[1]);
    // imshow("V", HSVChannels[2]);
    // imshow("DiffH", diffH);
    // imshow("DiffS", diffS);
    // waitKey(30);

  }
#else
    bufferBG* buffer = new bufferBG();
    Mat frame, output;
    float scaleFactor = 0.3f;
    float rescaleFactor = 1.0f/scaleFactor;
    Ptr<BackgroundSubtractor> bgS = cuda::createBackgroundSubtractorMOG2(50, 10, false);
    int frameCount = 0;

    while(true){
        frameCount++;
        capture.read(frame);
    		//cuda::GpuMat frameGray, bgImage;
    		tm.reset(); tm.start();
    		//3.3ms
        buffer->frame.upload(frame);
    		tm.stop();
            //frameGray.upload(frame);
            //5.6ms
    		cuda::cvtColor(buffer->frame, buffer->frameGray, CV_BGR2GRAY);
    		//0.95ms
    		cuda::resize(buffer->frameGray, buffer->resized, Size(), scaleFactor, scaleFactor, INTER_LINEAR);

        if(frameCount< 30){
            bgS->apply( buffer->resized,  buffer->bgImage, 0.05);
  		  //bgS->apply( frameGray, bgImage, 0.05);
            bgS->getBackgroundImage(buffer->bgImageMOG2);
        }
        else if(frameCount%30 == 0){
            bgS->apply(buffer->resized,  buffer->bgImage, 0.02);
  		  //bgS->apply( frameGray, bgImage, 0.05);
            bgS->getBackgroundImage(buffer->bgImageMOG2);
        }
        else{
            //bgS->apply( buffer->resized,  buffer->bgImage, 0);
            //bgS->apply( frameGray, bgImage, 0);
            cuda::absdiff(buffer->bgImageMOG2, buffer->resized, buffer->output);
            cuda::threshold(buffer->output, buffer->bgImage, 10, 255.0f, THRESH_BINARY);
        }

        //2.8ms
        cuda::resize(buffer->bgImage, buffer->reresized, Size(), rescaleFactor, rescaleFactor, INTER_NEAREST);
  	    //1.34ms
        buffer->reresized.download(output);

        //bgImage.download(output);

    		if(frameCount > 30){
      			times.push_back(tm.getTimeMilli());
      			std::sort(times.begin(), times.end());
      			double avg = std::accumulate(times.begin(), times.end(), 0.0) / times.size();
      			std::cout << "Avg : " << avg << " ms FPS : " << 1000.0 / avg << std::endl;
    		}
        imshow("output",output);
        waitKey(30);
    }


#endif
  return 0;
}
