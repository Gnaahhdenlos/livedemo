//#define GPU_ON
#include "opencv2/core/utility.hpp"
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/video.hpp"

#ifdef GPU_MODE
#include "opencv2/gpu/gpu.hpp"
#include "opencv2/cudabgsegm.hpp"
#include "opencv2/cudalegacy.hpp"
#include "opencv2/cudafilters.hpp"
#include "opencv2/cudaimgproc.hpp"
#endif

#include <iostream>
#include <ctype.h>
#include <algorithm>
#include <vector>
#include <numeric>

using namespace cv;
using namespace std;

#ifdef GPU_ON

	//using namespace cv::cuda;
    struct bufferBG                                     // Optimized CUDA versions
    {   // Data allocations are very expensive on CUDA. Use a buffer to solve: allocate once reuse later.
    cuda::GpuMat frame, diffImage0GPU, diffImage1GPU, bufferMat, bitwiseAnd, openMat, closeMat, resized, reresized;
    };
#endif

int main (int argc, char* argv[]){

	  char* filename = argc >= 2 ? argv[1] : (char*)"768x576.avi";
	  VideoCapture capture( filename );

	  float resizeFactor = 0.3f;
	  float reresizeFactor = 1.0f/resizeFactor;

	  TickMeter tm;
	  vector<double> times;

#ifdef GPU_ON
		bufferBG* buffer = new bufferBG();
		vector<cuda::GpuMat> grayFrameBufferGPU;
		Mat frame, output, output2;
		int frameCount = 0;

		cuda::GpuMat originalFrameGPU, originalResized;

		grayFrameBufferGPU.push_back(cuda::GpuMat());
		capture.read(frame);
		originalFrameGPU.upload(frame);
		cuda::resize(originalFrameGPU, originalResized, Size(), resizeFactor, resizeFactor, INTER_LINEAR);
		cuda::cvtColor(originalResized, grayFrameBufferGPU.back(), COLOR_BGR2GRAY);

		grayFrameBufferGPU.push_back(cuda::GpuMat());
		capture.read(frame);
		originalFrameGPU.upload(frame);
		cuda::resize(originalFrameGPU, originalResized, Size(), resizeFactor, resizeFactor, INTER_LINEAR);
		cuda::cvtColor(originalResized, grayFrameBufferGPU.back(), COLOR_BGR2GRAY);


		grayFrameBufferGPU.push_back(cuda::GpuMat());
		capture.read(frame);
		originalFrameGPU.upload(frame);
		cuda::resize(originalFrameGPU, originalResized, Size(), resizeFactor, resizeFactor, INTER_LINEAR);
		cuda::cvtColor(originalResized, grayFrameBufferGPU.back(), COLOR_BGR2GRAY);
		int an = 1;

		Mat element = getStructuringElement(MORPH_ELLIPSE,Size(an*2+1, an*2+1), Point(-1, -1));
		Ptr<cuda::Filter> erodeFilter = cuda::createMorphologyFilter(MORPH_ERODE, CV_8UC1, element);
		Ptr<cuda::Filter> dilateFilter = cuda::createMorphologyFilter(MORPH_DILATE, CV_8UC1, element);

		cuda::GpuMat erodeMat, dilateMat;
		while(true){
				frameCount++;
				capture.read(frame);
				imshow("Frame",frame);


				//grayFrameBufferGPU.erase(grayFrameBufferGPU.begin());

				//grayFrameBufferGPU.push_back(cuda::GpuMat());

				//2.4ms
				originalFrameGPU.upload(frame);

				//3.4ms
				cuda::resize(originalFrameGPU, originalResized, Size(), resizeFactor, resizeFactor, INTER_LINEAR);

				buffer->bufferMat = grayFrameBufferGPU[0];
				grayFrameBufferGPU[0] = grayFrameBufferGPU[1];
				grayFrameBufferGPU[1] = grayFrameBufferGPU[2];
				grayFrameBufferGPU.back() = buffer->bufferMat;

				//0.75ms
				cuda::cvtColor(originalResized, grayFrameBufferGPU.back(), COLOR_BGR2GRAY);

				//2.9ms
				cuda::Stream stream;
				//cuda::GpuMat diffImage0GPU, diffImage1GPU;
				cuda::absdiff(grayFrameBufferGPU[0], grayFrameBufferGPU[1], buffer->diffImage0GPU);
				cuda::absdiff(grayFrameBufferGPU[1], grayFrameBufferGPU[2], buffer->diffImage1GPU);
				cuda::threshold(buffer->diffImage0GPU,buffer->diffImage0GPU, 10, 255, THRESH_BINARY);
				cuda::threshold(buffer->diffImage1GPU,buffer->diffImage1GPU, 10, 255, THRESH_BINARY);
				cuda::bitwise_and(buffer->diffImage0GPU, buffer->diffImage1GPU, buffer->bitwiseAnd,noArray());

				//cuda::resize(buffer->bitwiseAnd, buffer->resized, Size(), resizeFactor, resizeFactor, INTER_NEAREST);

				//0.5ms
				buffer->bitwiseAnd.download(output);
				tm.reset(); tm.start();
				//2.5ms
				erode(output, output, element);
				dilate(output, output, element);
				tm.stop();

				//1.35ms
				resize(output, output, Size(), reresizeFactor, reresizeFactor, INTER_NEAREST);

				//erodeFilter->apply(buffer->bitwiseAnd, erodeMat);
				//dilateFilter->apply(erodeMat, dilateMat);


				//diffImage1GPU.download(output);
				//dilateMat.download(output);

				//buffer->diffImage1GPU.download(output2);
				times.push_back(tm.getTimeMilli());
				std::sort(times.begin(), times.end());
				double avg = std::accumulate(times.begin(), times.end(), 0.0) / times.size();
				std::cout << "Avg : " << avg << " ms FPS : " << 1000.0 / avg << std::endl;

				imshow("Output",output);
				//imshow("Output2",output2);
				waitKey(30);
		}

#endif

		return 0;
}
