#include "opencv2/opencv.hpp"
#include <iostream>
#include <cstring>
#include <algorithm>
#include <vector>
#include <numeric>
#include <stdlib.h>
#include <stdio.h>
#include "../../imgProcLib/imageProcessing.hpp"

using namespace std;
using namespace cv;

Mat inputFrameLeft, inputFrameRight, hsvFrame;

void hsvThreshold(int, void*);

int main (int argc, char* argv[]){
  string filenameLeft, filenameRight;
  if(argc >= 3){
     filenameLeft = argv[1];
     filenameRight = argv[2];
  }
  else{
      return EXIT_FAILURE;
  }
	VideoCapture captureLeft( filenameLeft );
	VideoCapture captureRight( filenameRight );

	if( !captureLeft.isOpened() || !captureRight.isOpened() )
	{
			cout<<"Could not open video file"<<endl;
			return -1;
	}

	while(true){
			captureLeft>>inputFrameLeft;
			captureRight>>inputFrameRight;
			if( inputFrameLeft.empty() || inputFrameRight.empty()){
						break;
			}


	}

  return 0;
}
