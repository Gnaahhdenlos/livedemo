#include <opencv2/core/utility.hpp>
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"

#include <iostream>
#include <ctype.h>

using namespace cv;
using namespace std;

Mat image;

bool backprojMode = false;
bool selectObject = false;
int trackObject = 0;
bool showHist = true;
Point origin;
Rect selection;
int vmin = 10, vmax = 256, smin = 30;

// User draws box around object to track. This triggers CAMShift to start tracking
static void onMouse( int event, int x, int y, int, void* )
{
    if( selectObject )
    {
        selection.x = MIN(x, origin.x);
        selection.y = MIN(y, origin.y);
        selection.width = std::abs(x - origin.x);
        selection.height = std::abs(y - origin.y);

        selection &= Rect(0, 0, image.cols, image.rows);
    }

    switch( event )
    {
    case EVENT_LBUTTONDOWN:
        origin = Point(x,y);
        selection = Rect(x,y,0,0);
        selectObject = true;
        break;
    case EVENT_LBUTTONUP:
        selectObject = false;
        if( selection.width > 0 && selection.height > 0 )
            trackObject = -1;   // Set up CAMShift properties in main() loop
        break;
    }
}

string hot_keys =
    "\n\nHot keys: \n"
    "\tESC - quit the program\n"
    "\tc - stop the tracking\n"
    "\tb - switch to/from backprojection view\n"
    "\th - show/hide object histogram\n"
    "\tp - pause video\n"
    "To initialize tracking, select the object with mouse\n";

static void help()
{
    cout << "\nThis is a demo that shows mean-shift based tracking\n"
            "You select a color objects such as your face and it tracks it.\n"
            "This reads from video camera (0 by default, or the camera number the user enters\n"
            "Usage: \n"
            "   ./camshiftdemo [camera number]\n";
    cout << hot_keys;
}

const char* keys =
{
    "{help h | | show help message}{@camera_number| 0 | camera number}"
};

int main( int argc, const char** argv )
{
    VideoCapture cap;
    Rect trackWindow;
    int hsize = 16;
    float hranges[] = {0,180};
    const float* phranges = hranges;
    CommandLineParser parser(argc, argv, keys);
    if (parser.has("help"))
    {
        help();
        return 0;
    }
    int camNum = parser.get<int>(0);
    cap.open(camNum);

    if( !cap.isOpened() )
    {
        help();
        cout << "***Could not initialize capturing...***\n";
        cout << "Current parameter's value: \n";
        parser.printMessage();
        return -1;
    }
    cout << hot_keys;
    namedWindow( "Histogram", 0 );
    namedWindow( "CamShift Demo", 0 );
    setMouseCallback( "CamShift Demo", onMouse, 0 );
    createTrackbar( "Vmin", "CamShift Demo", &vmin, 256, 0 );
    createTrackbar( "Vmax", "CamShift Demo", &vmax, 256, 0 );
    createTrackbar( "Smin", "CamShift Demo", &smin, 256, 0 );

    Mat frame, hsv, hue, mask, hist, histimg = Mat::zeros(200, 320, CV_8UC3), backproj;
    bool paused = false;

    for(;;)
    {
        if( !paused )
        {
            cap >> frame;
            if( frame.empty() )
                break;
        }

        frame.copyTo(image);

        if( !paused )
        {
            cvtColor(image, hsv, COLOR_BGR2HSV);

            if( trackObject )
            {
                int _vmin = vmin, _vmax = vmax;

                inRange(hsv, Scalar(0, smin, MIN(_vmin,_vmax)),
                        Scalar(180, 256, MAX(_vmin, _vmax)), mask);
                int ch[] = {0, 0};
                hue.create(hsv.size(), hsv.depth());
                mixChannels(&hsv, 1, &hue, 1, ch, 1);

                if( trackObject < 0 )
                {
                    // Object has been selected by user, set up CAMShift search properties once
                    Mat roi(hue, selection), maskroi(mask, selection);
                    calcHist(&roi, 1, 0, maskroi, hist, 1, &hsize, &phranges);
                    normalize(hist, hist, 0, 255, NORM_MINMAX);

                    trackWindow = selection;
                    trackObject = 1; // Don't set up again, unless user selects new ROI

                    histimg = Scalar::all(0);
                    int binW = histimg.cols / hsize;
                    Mat buf(1, hsize, CV_8UC3);
                    for( int i = 0; i < hsize; i++ )
                        buf.at<Vec3b>(i) = Vec3b(saturate_cast<uchar>(i*180./hsize), 255, 255);
                    cvtColor(buf, buf, COLOR_HSV2BGR);

                    for( int i = 0; i < hsize; i++ )
                    {
                        int val = saturate_cast<int>(hist.at<float>(i)*histimg.rows/255);
                        rectangle( histimg, Point(i*binW,histimg.rows),
                                   Point((i+1)*binW,histimg.rows - val),
                                   Scalar(buf.at<Vec3b>(i)), -1, 8 );
                    }
                }

                // Perform CAMShift
                calcBackProject(&hue, 1, 0, hist, backproj, &phranges);
                backproj &= mask;
                RotatedRect trackBox = CamShift(backproj, trackWindow,
                                    TermCriteria( TermCriteria::EPS | TermCriteria::COUNT, 10, 1 ));
                if( trackWindow.area() <= 1 )
                {
                    int cols = backproj.cols, rows = backproj.rows, r = (MIN(cols, rows) + 5)/6;
                    trackWindow = Rect(trackWindow.x - r, trackWindow.y - r,
                                       trackWindow.x + r, trackWindow.y + r) &
                                  Rect(0, 0, cols, rows);
                }

                if( backprojMode )
                    cvtColor( backproj, image, COLOR_GRAY2BGR );
                ellipse( image, trackBox, Scalar(0,0,255), 3, LINE_AA );
            }
        }
        else if( trackObject < 0 )
            paused = false;

        if( selectObject && selection.width > 0 && selection.height > 0 )
        {
            Mat roi(image, selection);
            bitwise_not(roi, roi);
        }

        imshow( "CamShift Demo", image );
        imshow( "Histogram", histimg );

        char c = (char)waitKey(10);
        if( c == 27 )
            break;
        switch(c)
        {
        case 'b':
            backprojMode = !backprojMode;
            break;
        case 'c':
            trackObject = 0;
            histimg = Scalar::all(0);
            break;
        case 'h':
            showHist = !showHist;
            if( !showHist )
                destroyWindow( "Histogram" );
            else
                namedWindow( "Histogram", 1 );
            break;
        case 'p':
            paused = !paused;
            break;
        default:
            ;
        }
    }

    return 0;
}


/*static Mat image;
static Rect2d boundingBox;
static bool paused;
static bool selectObject = false;
static bool startSelection = false;

cv::TickMeter tm;
std::vector<double> times;

static void onMouse( int event, int x, int y, int, void* )
{
  if( !selectObject )
  {
    switch ( event )
    {
      case EVENT_LBUTTONDOWN:
        //set origin of the bounding box
        startSelection = true;
        boundingBox.x = x;
        boundingBox.y = y;
        boundingBox.width = boundingBox.height = 0;
        break;
      case EVENT_LBUTTONUP:
        //sei with and height of the bounding box
        boundingBox.width = std::abs( x - boundingBox.x );
        boundingBox.height = std::abs( y - boundingBox.y );
        paused = false;
        selectObject = true;
        break;
      case EVENT_MOUSEMOVE:

        if( startSelection && !selectObject )
        {
          //draw the bounding box
          Mat currentFrame;
          image.copyTo( currentFrame );
          rectangle( currentFrame, Point((int) boundingBox.x, (int)boundingBox.y ), Point( x, y ), Scalar( 255, 0, 0 ), 2, 1 );
          imshow( "Tracking API", currentFrame );
        }
        break;
    }
  }
}

//
//  Hot keys:
//   q - quit the program
//   p - pause video
//

int main( int argc, char** argv )
{
    //open the capture
    //VideoCapture cap(argv[1]);
    VideoCapture cap(0);
    if( !cap.isOpened() )
    {
      return -1;
    }
    bool isFirstFrame = true;
    Mat bgImage;
    //
    //  "MIL", "BOOSTING", "MEDIANFLOW", "TLD"
    //
    string tracker_algorithm = "MIL";
    if ( argc>1 ) tracker_algorithm = argv[1];

    Mat frame;
    paused = false;
    namedWindow( "Tracking API", 0 );
    setMouseCallback( "Tracking API", onMouse, 0 );


    Ptr<Tracker> tracker = TrackerKCF::create();
    if( tracker == NULL )
    {
        cout << "***Error in the instantiation of the tracker...***\n";
        return -1;
    }

    //get the first frame
    cap >> frame;
    frame.copyTo( image );
    if(isFirstFrame == true){
        isFirstFrame = false;
        frame.copyTo(bgImage);
    }
    imshow( "Tracking API", image );

    bool initialized = false;
    int frameCounter = 0;

    for ( ;; )
    {
        char c = (char) waitKey( 2 );
        if( c == 'q' || c == 27 )
            break;
        if( c == 'p' )
            paused = !paused;

        if ( !paused )
        {
            tm.reset();tm.start();
            cap >> frame;
            if(frame.empty())
            {
                break;
            }
            frame.copyTo( image );
            // if(!initialized){
            //   imshow( "Tracking API", image );
            //   waitKey( 2 );
            // }
            if( selectObject )
            {
                if( !initialized )
                {
                    //initializes the tracker
                    if( !tracker->init( frame, boundingBox ) )
                    {
                    cout << "***Could not initialize tracker...***\n";
                    return -1;
                    }
                    initialized = true;
                }
                else
                {
                    //updates the tracker
                    if( tracker->update( frame, boundingBox ) )
                    {
                        rectangle( image, boundingBox, Scalar( 255, 0, 0 ), 2, 1 );
                    }
                }
                //Mat resizeImg;
                //resize(image, resizeImg, Size(), 0.5, 0.5, INTER_LINEAR);
                imshow( "Tracking API", image );
                //
                frameCounter++;
            }
            else{

              imshow( "Tracking API", image );
              frameCounter++;
            }
            tm.stop();
            times.push_back(tm.getTimeMilli());
            std::sort(times.begin(), times.end());
            double frame_avg = std::accumulate(times.begin(), times.end(), 0.0) / times.size();
            std::cout << "KCF: Avg : " << frame_avg << " ms FPS : " << 1000.0 / frame_avg << std::endl;
        }
    }

    return 0;
}*/

/*

*/




/*//#define GPU_MODE
#include <iostream>
#include <numeric>
#include <vector>
#include "opencv2/opencv.hpp"

#ifdef GPU_MODE
#include "opencv2/gpu/gpu.hpp"
#endif
using namespace cv;
using namespace std;

int main (int argc, char* argv[]){

    char* filename = argc >= 2 ? argv[1] : (char*)"768x576.avi";
    VideoCapture capture( filename );

    //HOGDescriptor hog;
    //hog.setSVMDetector(hog.getDefaultPeopleDetector());

    HOGDescriptor hog( Size( 48, 96 ), Size( 16, 16 ), Size( 8, 8 ), Size( 8, 8 ), 9, 1, -1,
                      HOGDescriptor::L2Hys, 0.2, false, cv::HOGDescriptor::DEFAULT_NLEVELS);
    hog.setSVMDetector( HOGDescriptor::getDaimlerPeopleDetector() );

    Ptr<BackgroundSubtractor> bgS = createBackgroundSubtractorMOG2(1000, 10, false);
    Mat frame,output;
    int frameCount = 0;
    while(true)
    {
        capture.read(frame);
        if (!frame.data)
        return 0;
        if( frame.cols > 800 )
        resize( frame, frame, Size(), 0.8, 0.8 );

        frameCount++;
        if(frameCount< 60){
            bgS->apply(frame, output, 0.1);
        }
        else{
            bgS->apply(frame, output, 0.001);
        }

        erode(output,output,Mat());
        dilate(output,output,Mat());
        imshow("bgs", output);
        // Find contours
        vector<vector<Point> > contours;
        findContours( output, contours, RETR_LIST, CHAIN_APPROX_SIMPLE );

        for ( size_t i = 0; i < contours.size(); i++)
        {
            Rect r = boundingRect( contours[i] );
            if( r.height > 50 & r.width < r.height )
            {
                r.x -= r.width / 2;
                r.y -= r.height / 2;
                r.width += r.width;
                r.height += r.height;
                r = r & Rect( 0, 0, frame.cols, frame.rows );

                Mat roi;
                cvtColor( frame( r ), roi, COLOR_BGR2GRAY);

                std::vector<Rect> rects;

                if( roi.cols > hog.winSize.width & roi.rows > hog.winSize.height )
                hog.detectMultiScale( roi, rects);

                for (size_t i=0; i<rects.size(); i++)
                {
                    rects[i].x += r.x;
                    rects[i].y += r.y;

                    rectangle( frame, rects[i], Scalar( 0, 0, 255 ), 2 );
                }
            }
        }

        imshow("display", frame);
        if(waitKey(30)==27)
        {
            break;
        }
    }
    return 0;

}*/


/*int main (int argc, char* argv[])
{


    cv::TickMeter tm;
    std::vector<double> cpu_times;
    std::vector<double> gpu_times;

    Mat element = getStructuringElement(MORPH_ELLIPSE, Size(5,5));
    int iterations = 1;
    try
    {
	std::cout<<"Read Img"<<std::endl;
	cv::Mat dst_cpu;
	Mat grayImg, threshImg, erodeImg;
	Mat filterKernel = (Mat_<double>(1,3) << 1,1,1);
    //cv::Mat src_cpu1 = cv::imread("/Users/henrikuper/Downloads/josnow.png", CV_LOAD_IMAGE_COLOR);
    //cv::Mat src_cpu2 = cv::imread("/Users/henrikuper/Downloads/josnow.png", CV_LOAD_IMAGE_COLOR);

    cv::Mat src_cpu1 = cv::imread("/home/nvidia/Pictures/josnow.png", CV_LOAD_IMAGE_COLOR);
    cv::Mat src_cpu2 = cv::imread("/home/nvidia/Pictures/josnow.png", CV_LOAD_IMAGE_COLOR);

    cvtColor(src_cpu1, grayImg, COLOR_BGR2GRAY);
    threshold(grayImg, threshImg, 150.0, 255.0, THRESH_BINARY);

    cout<<"Height "<<grayImg.rows<<" Width "<<grayImg.cols<<endl;
    //Ptr<cv::FilterEngine> erodeFilter0 = createMorphologyFilter(MORPH_ERODE, threshImg.type(), element);
	//Ptr<BaseRowFilter> rowFilter0 = getLinearRowFilter(CV_8UC1, CV_32F, filterKernel, -1, BORDER_DEFAULT);
	//cv::imshow("test", threshImg);
	//waitKey(30000);
	std::cout<<"CPU-Calculation"<<std::endl;
	for(int i = 0; i < 100; i++){
		tm.reset(); tm.start();
		//cv::threshold(grayImg, dst_cpu, 128.0, 255.0, CV_THRESH_BINARY);
		//dst_cpu = src_cpu1 + src_cpu2;
		//dst_cpu = src_cpu1 - src_cpu2;

		//erode(threshImg, erodeImg, element, Point(-1,-1), iterations);
		//dilate(threshImg, erodeImg, element, Point(-1,-1), iterations);
		//blur(threshImg, erodeImg, Size(5,5), Point(-1,-1));
		//bitwise_and(threshImg, threshImg, erodeImg);
		//erodeFilter0->apply(threshImg, grayImg);
		//rowFilter0->apply(threshImg, grayImg);
		resize(threshImg,grayImg,Size(),0.25,0.25,INTER_LINEAR);
		resize(grayImg,grayImg,Size(),4,4,INTER_LINEAR);
		tm.stop();
		cpu_times.push_back(tm.getTimeMilli());
	}

#ifdef GPU_MODE
    cv::gpu::GpuMat dst_gpu, src_gpu1, src_gpu2, erodeImg_gpu, threshImg_gpu, resizeImg_gpu;
	//imshow("test", threshImg);
	//waitKey(1000);
    threshImg_gpu.upload(threshImg);
    Ptr<gpu::FilterEngine_GPU> erodeFilter = gpu::createMorphologyFilter_GPU(MORPH_ERODE, threshImg_gpu.type(), element);
    //Ptr<gpu::BaseRowFilter_GPU> rowFilter = gpu::getLinearRowFilter_GPU(CV_8UC1, CV_32F, filterKernel, -1, BORDER_DEFAULT);
    src_gpu2.upload(src_cpu2);
    src_gpu1.upload(grayImg);
	std::cout<<"GPU-Calculation"<<std::endl;
	for(int i = 0; i < 100; i++){
		//cout<<i<<endl;
		tm.reset(); tm.start();
		//cv::gpu::threshold(src_gpu1, dst_gpu, 128.0, 255.0, CV_THRESH_BINARY);
		//cv::gpu::add(src_gpu1, src_gpu2, dst_gpu);
		//cv::gpu::subtract(src_gpu1, src_gpu2, dst_gpu);
		//gpu::erode(threshImg_gpu, erodeImg_gpu, element, Point(-1,-1), iterations);
		//gpu::dilate(threshImg_gpu, erodeImg_gpu, element, Point(-1,-1), iterations);
		//src_gpu1.upload(grayImg);
		//src_gpu1.download(grayImg);
		//gpu::blur(threshImg_gpu, erodeImg_gpu, Size(5,5), Point(-1,-1));
		//gpu::bitwise_and(threshImg_gpu, threshImg_gpu, erodeImg_gpu);
		//erodeFilter->apply(threshImg_gpu, src_gpu1);
		//rowFilter->apply(threshImg_gpu, src_gpu1);
		gpu::resize(threshImg_gpu,resizeImg_gpu,Size(),0.05,0.05,INTER_LINEAR);
		//cout<<"Height "<<resizeImg_gpu.rows<<" Width "<<resizeImg_gpu.cols<<endl;
		//gpu::erode(resizeImg_gpu, erodeImg_gpu, element, Point(-1,-1), iterations);
		//gpu::resize(erodeImg_gpu, resizeImg_gpu,Size(),20,20,INTER_LINEAR);

		tm.stop();
		gpu_times.push_back(tm.getTimeMilli());
	}

    resizeImg_gpu.download(dst_cpu);
	imshow("test", dst_cpu);
	waitKey(10000);
#endif
    std::sort(cpu_times.begin(), cpu_times.end());
    std::sort(gpu_times.begin(), gpu_times.end());

    double cpu_avg = std::accumulate(cpu_times.begin(), cpu_times.end(), 0.0) / cpu_times.size();
    double gpu_avg = std::accumulate(gpu_times.begin(), gpu_times.end(), 0.0) / gpu_times.size();

    std::cout << "CPU : Avg : " << cpu_avg << " ms FPS : " << 1000.0 / cpu_avg << std::endl;
    std::cout << "GPU : Avg : " << gpu_avg << " ms FPS : " << 1000.0 / gpu_avg << std::endl;


    }
    catch(const cv::Exception& ex)
    {
        std::cout << "Error: " << ex.what() << std::endl;
    }
    return 0;
}*/
