#include <iostream>
#include <sstream>
#include <time.h>
#include <stdio.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

//tracklib include
#include "../../trackLib/fieldDetection.hpp"
#include "../../trackLib/configData.hpp"

/*
  TODO:
  Das gesamt Ziel ist eine calibrierung des Platzes in der Klasse fieldCalibration der trackLib zu implementieren, die anschließend in einer xml Datei gespeichert wird.
  1. mainFieldCalibration main function durchlesen und nachvollziehen. Die meisten Aufrufe sollten bekannt sein.
  Neu ist nur das Auslesen der cameraCalibration Matrizen aus einer xml Date. (Das Kannst du vorerst überspringen)
  2. Das Program ans laufen kriegen mit allen nötigen Paramtern, die du übergeben musst.
  3. calibrateFieldLinesWithCalibrationVideo() der Klasse fieldDetection durchlesen und verstehen, der wichtigste Punkt beginnt bei m_useCalibrationType == 2 in Zeile 295 (auch die aufgerufenden Funktion ganz grob überfliegen)
  4. detecFieldLines so umschreiben, dass deinen entwickelten Funktion integriert werden.
  5. Das Ziel ist hier, dass man 4 Punkte eindeutig zuordnen kann. (es müssen nicht die vier CenterFieldPoints sein, wie bisher)

  6. Die vier gefundenen Punkte in die xml datei schreiben. Die punkte befinden sich wahrscheinlich in einem vektor, was das schreiben ein wenig kniffelig macht.
  Einfach noch mal nachfragen, dann gehen wir das zusammen durch, oder sonst die folgende Dokumentation von opencv und andere im web druchgehen.
  http://docs.opencv.org/2.4/doc/tutorials/core/file_input_output_with_xml_yml/file_input_output_with_xml_yml.html
*/


using namespace cv;
using namespace std;

void help(){
    cout<<"Wrong Args"<<endl;
    cout<<"Try: \n ./calibrateField \n filePathToRightVideoCalibration \n filePathToLeftVideoCalibration\n filePathToUndistortionParameters\n filePathTofieldLinesCalibrationData"<<endl;
}

int main(int argc, char* argv[]){
  //Initialising of the ConfigData sturct, whicht contains every important information, given by the command line.
  struct ConfigData config;

  if(argc != 5){
    help();
    return 0;
  }
  else{
    config.m_filePathToRightVideoCalibration = argv[1];
    config.m_filePathToLeftVideoCalibration = argv[2];
    config.m_filePathToCameraCalibrationMatrix = argv[3];
    config.m_filePathToFieldCalibrationPoints = argv[4];
    config.m_useCalibrationType = 3;
  }

  //Opening and checking the videoStreams for the calibration
  bool success;
  int frameHeight, frameWidth;
  VideoCapture rightCalibrationVideoStream;
  VideoCapture leftCalibrationVideoStream;

  success = rightCalibrationVideoStream.open(config.m_filePathToRightVideoCalibration);
  if ( !rightCalibrationVideoStream.isOpened() && success){
    cout << "Cannot open " << config.m_filePathToRightVideoCalibration << endl;
    return EXIT_FAILURE;
  }
  success = leftCalibrationVideoStream.open(config.m_filePathToLeftVideoCalibration);
  if ( !leftCalibrationVideoStream.isOpened() && success){
    cout << "Cannot open " << config.m_filePathToLeftVideoCalibration << endl;
    return EXIT_FAILURE;
  }

  frameWidth = rightCalibrationVideoStream.get(CV_CAP_PROP_FRAME_WIDTH);
  frameHeight = rightCalibrationVideoStream.get(CV_CAP_PROP_FRAME_HEIGHT);

  //Reading of the cameraCalibrationMatrix from the cameraCalibrationFile
  Mat cameraMatrix, distortionCoefficients;

  FileStorage fileStorage(config.m_filePathToCameraCalibrationMatrix, FileStorage::READ);
  if( !fileStorage.isOpened() ){
      return EXIT_FAILURE;
  }
  fileStorage["Camera_Matrix"]>> cameraMatrix;
  fileStorage["Distortion_Coefficients"]>> distortionCoefficients;
  cout<<"Camera_Matrix: "<<cameraMatrix<<endl;
  cout<<"Distortion_Coefficients: "<<distortionCoefficients<<endl;

  FieldDetection *rightVideoFieldDetection = new FieldDetection(frameHeight, frameWidth, cameraMatrix, distortionCoefficients, true, config);
  FieldDetection *leftVideoFieldDetection = new FieldDetection(frameHeight, frameWidth, cameraMatrix, distortionCoefficients, false, config);

  rightVideoFieldDetection->calibrateFieldLinesWithCalibrationVideo(rightCalibrationVideoStream);
  leftVideoFieldDetection->calibrateFieldLinesWithCalibrationVideo(leftCalibrationVideoStream);

  return 0;
}
