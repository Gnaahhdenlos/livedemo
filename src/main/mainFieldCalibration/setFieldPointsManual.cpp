#include <opencv2/highgui/highgui.hpp>
#include "opencv2/opencv.hpp"
#include <iostream>
#include "../../cameraLib/camera.hpp"
#include <thread>

using namespace std;
using namespace cv;

Mat m_inputFrameLeft, m_inputFrameLeft_undistorted;
Mat m_inputFrameRight, m_inputFrameRight_undistorted;

Mat m_cameraMatrix, m_distortionCoefficients;

vector<vector<Point> > m_cornerPoints;
int m_vectorIndex;
int m_pointIndex;
bool m_validClicks;

bool m_liveCalibration;

Camera *m_cameras;

//TODO: create hpp file

void help(){
    cout<<"Wrong Args"<<endl;
    cout<<"Try: \n ./setFieldPointsManual \n filePathToRightVideoCalibration \n filePathToLeftVideoCalibration\n filePathToCameraCalibrationMatrix \n          for Calibration with Videos"<<endl;
    cout<<"Or Try: \n ./setFieldPointsManual \n filePathToCameraCalibrationMatrix \n          for live Calibration with Camera"<<endl;
}

void CallBackFunc(int event, int x, int y, int flags, void* userdata)
{
   if  ( event == EVENT_LBUTTONDOWN )
   {
      m_cornerPoints[m_vectorIndex][m_pointIndex].x = x;
      m_cornerPoints[m_vectorIndex][m_pointIndex].y = y;
      cout << "Point " << m_pointIndex << " position set to (" << x << ", " << y << ")" << endl;
      m_pointIndex++;
      if(m_pointIndex>=4){
         m_pointIndex = 0;
         m_validClicks = 1;
      }
   }
   else if  ( event == EVENT_RBUTTONDOWN )
   {
      m_cornerPoints[m_vectorIndex][m_pointIndex-1].x = x;
      m_cornerPoints[m_vectorIndex][m_pointIndex-1].y = y;
      cout << "Point " << m_pointIndex-1 << " position changed to (" << x << ", " << y << ")" << endl;
   }
}

void writeIntoXml(){
   //string filenameXml = "/Users/henrikuper/Wingfield/alphaConfigFiles/fieldLinesCalibrationData.xml";
   string filenameXml = "/home/wingfield/alphaConfigFiles/fieldLinesCalibrationData.xml";
   FileStorage fs(filenameXml, FileStorage::WRITE);
   time_t rawtime; time(&rawtime);

   fs << "calibrationDate" << asctime(localtime(&rawtime));
   fs << "image_Width" << m_inputFrameLeft.cols;
   fs << "image_Height" << m_inputFrameLeft.rows;

   fs << "right_side_points_preset";
   // Write right points
   fs << "[:";
   for (int j = 0; j < m_cornerPoints[0].size(); ++j)
   {

       fs << "[:" << m_cornerPoints[0][j].x << m_cornerPoints[0][j].y << "]";
   }
   fs << "]";

   fs << "left_side_points_preset";
   // Write left points
   fs << "[:";
   for (int j = 0; j < m_cornerPoints[1].size(); ++j)
   {

       fs << "[:" << m_cornerPoints[1][j].x << m_cornerPoints[1][j].y << "]";
   }
   fs << "]";

   cout<<"m_cornerPoints successfully writed into 'fieldLinesCalibrationData.xml'"<<endl;
   cout<<endl;
}

void sortPoints(int index){
   Point west=m_cornerPoints[index][0];
   Point north=m_cornerPoints[index][1];
   Point east=m_cornerPoints[index][2];
   Point south=m_cornerPoints[index][3];

   for(int i=0; i<m_cornerPoints[index].size(); i++){
      if(m_cornerPoints[index][i].x < west.x){
         west = m_cornerPoints[index][i];
         //switchPoints(m_cornerPoints[index][i], m_cornerPoints[index][0]);
      }
      if(m_cornerPoints[index][i].y < north.y){
         north = m_cornerPoints[index][i];
         //switchPoints(m_cornerPoints[index][i], m_cornerPoints[index][2]);
      }
      if(m_cornerPoints[index][i].x > east.x){
         east = m_cornerPoints[index][i];
         //switchPoints(m_cornerPoints[index][i], m_cornerPoints[index][1]);
      }
      if(m_cornerPoints[index][i].y > south.y){
         south = m_cornerPoints[index][i];
         //switchPoints(m_cornerPoints[index][i], m_cornerPoints[index][3]);
      }
   }

   m_cornerPoints[index][0] = west;
   m_cornerPoints[index][1] = north;
   m_cornerPoints[index][2] = east;
   m_cornerPoints[index][3] = south;

   if(west==north || west==east || west==south || north==east || north==south || east==south ){
      m_validClicks=0;
   }
}

int main(int argc, char* argv[])
{
   string filenameLeft, filenameRight, filePathToCameraCalibrationMatrix;

   if(argc == 4){
     filenameRight = argv[1];
     filenameLeft = argv[2];
     filePathToCameraCalibrationMatrix = argv[3];
     m_liveCalibration = false;
   }
   else if(argc == 2){
     filePathToCameraCalibrationMatrix = argv[1];
     m_liveCalibration = true;
   }
   else{
     help();
     return 0;
   }

  //Undistort the inputFrames
  FileStorage fileStorage(filePathToCameraCalibrationMatrix, FileStorage::READ);
  if( !fileStorage.isOpened() ){
     return EXIT_FAILURE;
  }
  fileStorage["Camera_Matrix"]>> m_cameraMatrix;
  fileStorage["Distortion_Coefficients"]>> m_distortionCoefficients;
  cout<<"Camera_Matrix: "<<m_cameraMatrix<<endl;
  cout<<"Distortion_Coefficients: "<<m_distortionCoefficients<<endl;


  //string matchName = "/Users/luiskuper/Documents/07\ WF/alphaConfigFiles/videos/match";
  //string pathToWeights = "/Users/luiskuper/Documents/07\ WF/alphaConfigFiles/Weights83";
  //string pathToFieldCalibrationPoints = "/Users/luiskuper/Documents/07\ WF/alphaConfigFiles/fieldLinesCalibrationData.xml";
  //string pathToCalibrationMatrix = "/Users/luiskuper/Documents/07\ WF/alphaConfigFiles/out_camera_data.xml";

  //Two types of calibration: live Calibration and Calibration with input Video
  if(m_liveCalibration){
    m_cameras = new Camera();
    m_cameras->setFrameRate(10);
    thread frameGrabbingThread(&Camera::frameGrabbing, m_cameras);
    cout<<"Start Camera Captures"<<endl;

    //thread frameGrabbingThread(&Camera::frameGrabbing, m_cameras);
    namedWindow("Left Camera", WINDOW_AUTOSIZE);
    namedWindow("Right Camera", WINDOW_AUTOSIZE);
    int cameraFrameCounter = 0;
    do {
      cout<<"Camera frameCounter: "<<cameraFrameCounter<<endl;
      cameraFrameCounter++;
      while(m_cameras->getBufferSizeLeft() > 1){
        m_cameras->deleteFirstFrameLeft();
      }
      while(m_cameras->getBufferSizeRight() > 1){
        m_cameras->deleteFirstFrameRight();
      }
      if(m_cameras->getBufferSizeLeft() > 0){
        cout<<"m_cameras->getBufferSizeLeft() > 0"<<endl;
        m_inputFrameLeft = m_cameras->getFirstFrameLeft();
        undistort(m_inputFrameLeft, m_inputFrameLeft_undistorted, m_cameraMatrix, m_distortionCoefficients);
        m_cameras->deleteFirstFrameLeft();
        imshow("Left Camera", m_inputFrameLeft);
      }
      if(m_cameras->getBufferSizeRight() > 0){
        m_inputFrameRight = m_cameras->getFirstFrameRight();
        undistort(m_inputFrameRight, m_inputFrameRight_undistorted, m_cameraMatrix, m_distortionCoefficients);
        m_cameras->deleteFirstFrameRight();
        imshow("Right Camera", m_inputFrameRight);
      }
    }  while(waitKey(100) != 's');
    destroyWindow("Left Camera");
    destroyWindow("Right Camera");
    m_cameras->setStopRunning(true);
    frameGrabbingThread.join();

  }
  else{
    VideoCapture captureLeft ( filenameLeft );
    VideoCapture captureRight ( filenameRight );
    captureLeft>>m_inputFrameLeft;
    captureRight>>m_inputFrameRight;
    cout<<"Grabbed Frame from Video Capture"<<endl;

    undistort(m_inputFrameRight, m_inputFrameRight_undistorted, m_cameraMatrix, m_distortionCoefficients);
    undistort(m_inputFrameLeft, m_inputFrameLeft_undistorted, m_cameraMatrix, m_distortionCoefficients);

  }

   vector<Point> points;
   points.push_back(Point(0, 0));
   points.push_back(Point(0, 0));
   points.push_back(Point(0, 0));
   points.push_back(Point(0, 0));
   m_cornerPoints.push_back(points);
   m_cornerPoints.push_back(points);

   namedWindow("Window", CV_WINDOW_AUTOSIZE);

   setMouseCallback("Window", CallBackFunc, NULL);

   m_pointIndex=0;
   m_vectorIndex=0;
   m_validClicks = 0;

   while(m_validClicks == 0){
      cout<<"Set points on right fieldside by clicking in the window:"<<endl;

      //TODO: only show if image is not empty
      imshow("Window", m_inputFrameRight_undistorted);
      waitKey(0);

      if(m_validClicks == 0){
         cout<<"Failed: you have to set at least 4 points!"<<endl;
      }
      cout<<endl;
   }

   m_pointIndex=0;
   m_vectorIndex++;
   m_validClicks = 0;

   while(m_validClicks == 0){
      cout<<"Set points on left fieldside by clicking in the window:"<<endl;

      imshow("Window", m_inputFrameLeft_undistorted);
      waitKey(0);

      if(m_validClicks == 0){
         cout<<"Failed: you have to set at least 4 points!"<<endl;
      }
      cout<<endl;
   }

   sortPoints(0);
   sortPoints(1);

   if(m_validClicks == 0){
      cout<<"Failed: invalid Points set: west==north || west==east || west==south || north==east || north==south || east==south"<<endl;
      return 0;
   }

   writeIntoXml();

   return 0;
}
