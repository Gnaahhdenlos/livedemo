//
//  mainMiner.cpp
//
//
//  Created by Henri Kuper on 12.04.17.
//
//

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>

#include "../../../trackLib/fieldDetection.hpp"
#include "../../../trackLib/ballDetection.hpp"
#include "../../../trackLib/curveDetection.hpp"
#include "../../../trackLib/curve.hpp"

using namespace std;
using namespace cv;

int imgCount = 0;
int framesPrediction = 42;
bool isRight = false;

/*
  TODO:
  Durch die umstellung der Tracklib, muss für die funktionsfähigkeit der des dataminers einiges angepasst werden.
  - einführen von playerSegmentation, BallDetection etc.
*/

void help(){
    cout<<"To start the Programm correctly type:"<<endl;
    cout<<"./miner2 filepath to folder which contains videoList.csv and filepath to the folder which contains imgList.csv filePath to out_camera_data.xml "<<endl;
    cout<<"Example: ./miner ../sequencesNeuronalesNetz/ ../imagesNeuronalesNetz/ ../out_camera_data.xml"<<endl;
}

string getCurveCharacter(int curveCharacterisationNumber){
    string curveCharacter;
    switch(curveCharacterisationNumber){
        case 7: curveCharacter = "Impact";break;
        case 8: curveCharacter = "Impact";break;
        case 9: curveCharacter = "Stroke left player";break;
        case 10: curveCharacter = "Stroke right player";break;
        case 11: curveCharacter = "Stroke left player";break;
        case 12: curveCharacter = "Stroke right player";break;
        case 13: curveCharacter = "Stroke left player";break;
        case 14: curveCharacter = "Stroke right player";break;
        case 15: curveCharacter = "Stroke left player";break;
        case 16: curveCharacter = "Stroke right player";break;
        case 17: curveCharacter = "From right over net";break;
        case 18: curveCharacter = "From left over net";break;
        case 19: curveCharacter = "Rally ending right";break;
        case 20: curveCharacter = "Rally ending left";break;
        case 21: curveCharacter = "Serve left player";break;
        case 22: curveCharacter = "Serve right player";break;
        default: curveCharacter = "default";break;
    }
    return curveCharacter;
}

int actualCurveNumer(vector<Curve>& curves){
    int curveNumber = -1;
    //int curveImgCount = 0;
    //TODO: auch für left und right unterscheiden
    if(curves.size() > 0){
        bool foundRight = false;
        int i = curves.size()-1;
        cout<<"CurveCharacterSize: "<<i<<endl;
        //cout<<"iStartRight: "<<i<<endl;
        if(isRight == true){
            while(foundRight == false && i >= 0){
                //cout<<"i Right: "<<i<<endl;
                int curveImgCountTmp = curves[i].getLastFrameCount();
                if(getCurveCharacter(curves[i].getCharacterisation()) != "default" && getCurveCharacter(curves[i].getCharacterisation()) != "Impact" && getCurveCharacter(curves[i].getCharacterisation()) != "From right over net" && getCurveCharacter(curves[i].getCharacterisation()) != "From left over net" && getCurveCharacter(curves[i].getCharacterisation()) != "Rally ending right" && getCurveCharacter(curves[i].getCharacterisation()) != "Rally ending left" && getCurveCharacter(curves[i].getCharacterisation()) != "Stroke left player" && getCurveCharacter(curves[i].getCharacterisation()) != "Serve left player" && curveImgCountTmp < imgCount -framesPrediction + 5){
                    curveNumber = i;
                    //curveImgCount = curveImgCountTmp;
                    foundRight = true;
                }
                i--;
            }
            //cout<<"i: "<<i<<endl;
        }
        else{
            while(foundRight == false && i >= 0){
                //cout<<"i Right: "<<i<<endl;
                int curveImgCountTmp = curves[i].getLastFrameCount();
                //cout<<"Character: "<<getCurveCharacter(curveCharacterisation[i])<<endl;
                //cout<<"curveImgCount: "<<curveImgCountTmp<<endl;
                //cout<<"ImgCountToChck: "<<(imgCount -framesPrediction + 5)<<endl;
                if(getCurveCharacter(curves[i].getCharacterisation()) != "default" && getCurveCharacter(curves[i].getCharacterisation()) != "Impact" && getCurveCharacter(curves[i].getCharacterisation()) != "From right over net" && getCurveCharacter(curves[i].getCharacterisation()) != "From left over net" && getCurveCharacter(curves[i].getCharacterisation()) != "Rally ending right" && getCurveCharacter(curves[i].getCharacterisation()) != "Rally ending left" && getCurveCharacter(curves[i].getCharacterisation()) != "Stroke right player" && getCurveCharacter(curves[i].getCharacterisation()) != "Serve right player"&& curveImgCountTmp < imgCount -framesPrediction + 5){
                    curveNumber = i;
                    //curveImgCount = curveImgCountTmp;
                    foundRight = true;
                }
                i--;
            }
            //cout<<"i: "<<i<<endl;
        }
    }
    return curveNumber;
}


int main (int argc, const char * argv[]){

    int frame_width = 0;
    int frame_height = 0;

    Mat cameraMatrix;
    Mat distortionCoefficients;

    string filePath;
    string calibrationPath;

    VideoCapture cap;
    VideoCapture calibrationVideo;

    struct ConfigData config;


    vector<Mat> imgBuffer;

    string outputFile = "/Users/henrikuper/Wingfield/data/imagesNeuronalesNetz/";
    string toVideoList;
    string toImgList;
    string fnVideoListCsv;
    string fnImgListCsv;

    int videoCount;
    int checkedVideoCount;
    int outputImgCount;
    int labeledImgCount;
    vector<string> videoNames;

    if(argc >= 3){
        //filePath = argv[1];

        toVideoList = argv[1];
        toImgList = argv[2];
        outputFile = toImgList;
        fnImgListCsv = toImgList;
        fnImgListCsv += "imgList.csv";

        FileStorage fileStorage(argv[3], FileStorage::READ);
        if( !fileStorage.isOpened() ){
            return EXIT_FAILURE;
        }
        fileStorage["Camera_Matrix"]>> cameraMatrix;
        fileStorage["Distortion_Coefficients"]>> distortionCoefficients;

        ifstream imgListCsv(fnImgListCsv.c_str());
        if (!imgListCsv.is_open()) {
            printf("Unable to open imgList %s.\n", fnImgListCsv.c_str());
            return -1;
        }

        config.m_useCalibrationType = 2;

        std::string line;
        std::getline(imgListCsv,line);
        std::getline(imgListCsv,line);

        outputImgCount = stoi(line);
        cout<<outputImgCount<<endl;

        std::getline(imgListCsv,line);
        std::getline(imgListCsv,line);

        labeledImgCount = stoi(line);

        fnVideoListCsv = toVideoList;
        fnVideoListCsv += "videoList.csv";

        ifstream videoListCsv(fnVideoListCsv.c_str());
        if (!videoListCsv.is_open()) {
            printf("Unable to open Weights file %s.\n", fnVideoListCsv.c_str());
            return -1;
        }

        std::getline(videoListCsv,line);
        std::getline(videoListCsv,line);

        videoCount = stoi(line);
        cout<<videoCount<<endl;

        std::getline(videoListCsv,line);
        std::getline(videoListCsv,line);

        checkedVideoCount = stoi(line);

        for(int i = 0; i < videoCount; i++){
            std::getline(videoListCsv,line);
            videoNames.push_back(line);
        }
    }
    else{
        help();
        return -1;
    }


    string rightSide = "R";

    for(int v = checkedVideoCount; v < videoCount; v++){
        string filePath = toVideoList;

        filePath += videoNames[v];
        //filePath += ".mp4";
        cout<<filePath<<endl;
        cout<<"isRight: "<<(int)videoNames[v].find(rightSide)<<endl;
        if((int)videoNames[v].find(rightSide) > -1){
            isRight = true;
        }
        cout<<"isRight: "<<isRight<<endl;
        cap.open(filePath);
        cout<<filePath<<endl;
        frame_width=   cap.get(CV_CAP_PROP_FRAME_WIDTH);
        frame_height=   cap.get(CV_CAP_PROP_FRAME_HEIGHT);
        calibrationPath = argv[2];

        calibrationVideo.open(filePath);
        Mat cameraMatrix;
        Mat distortionCoefficients;
        FieldDetection *lines = new FieldDetection(frame_height, frame_width, cameraMatrix,distortionCoefficients, isRight, config);
        lines->calibrateFieldLinesWithCalibrationVideo(calibrationVideo);


        PlayerSegmentation *player = new PlayerSegmentation(lines, frame_width, frame_height);
        BallDetection *balls = new BallDetection(frame_height, frame_width, lines, player);
        CurveDetection *curveDetec = new CurveDetection(frame_height, frame_width, lines, player, balls);
        //BallDetection *balls = new BallDetection(frame_height, frame_width, lines);
        //balls->setHomographyMat(lines->getHomographyMat());
        //balls->setfieldModel(lines->getFieldModel());
        //balls->setCalibrationImg(lines->getCalibrationImg());

        int actualCurve = -1;
        Mat imgOriginal;
        for(int i = 0; i < 1500; i++){ //change to 0 or less than 1500
            cap.read(imgOriginal); // read a new frame from video
        }

        imgCount = 0;

        while (true){

            Mat imgLines;

            bool bSuccess = cap.read(imgOriginal); // read a new frame from video

            if (!bSuccess){
                cout << "Cannot read a frame from video stream" << endl;
                break;
            }

            imgBuffer.push_back(imgOriginal.clone());
            if((int32_t)imgBuffer.size() > framesPrediction){
                imgBuffer.erase(imgBuffer.begin());
            }
            curveDetec->detecCurves();

            Mat videoImg;
            Rect boundRect;
            boundRect.height = 0;
            if((int32_t)imgBuffer.size() == framesPrediction){

                vector<Curve> curves = curveDetec->getCurves();
                //vector<vector<int> > flightCurvesImgCount = curve->getFlightCurvesImgCount();

                int newCurve = actualCurveNumer(curves);

                if(actualCurve < newCurve){
                    cout<<"New Curve: "<<newCurve<<endl;
                    actualCurve = newCurve;
                    int numberOfSnappedImages;
                    if(getCurveCharacter(curves[actualCurve].getCharacterisation()) == "Serve left player" || getCurveCharacter(curves[actualCurve].getCharacterisation()) == "Serve right player"){
                        numberOfSnappedImages = 30;
                    }
                    else{
                        numberOfSnappedImages = 15;
                    }
                    for(int i = 0; i < numberOfSnappedImages; i++){
                        if(i < (int32_t)imgBuffer.size()-1){
                            vector<vector<Rect> > boundRects = player->getHumanMaskBoundRectsOfFrames();

                            for(uint32_t j = 0; j <boundRects[i + imgCount - framesPrediction].size(); j++){
                                if(boundRect.height < boundRects[i + imgCount - framesPrediction][j].height){
                                    boundRect = boundRects[i + imgCount - framesPrediction][j];
                                }
                            }

                            float width = boundRect.height*1.0;
                            float x = boundRect.x + boundRect.width/2 - width/2;
                            if(x < 0){
                                x = 0;
                            }
                            if(x + width > 1920){
                                width = 1920 - x-1;
                            }
                            boundRect.x = x;
                            boundRect.width = width;

                            Mat originalImgRoi = imgBuffer[i].clone();
                            Mat roi = originalImgRoi(boundRect);
                            cout<<"Found Stroke"<<endl;
                            imshow("stroke", roi);
                            waitKey(30);

                            char buffer[25];

                            sprintf(buffer, "%07d", outputImgCount);
                            string fn = outputFile;
                            fn += buffer;
                            fn +=".ppm";
                            imwrite(fn, roi);

                            ifstream imgListCsv(fnImgListCsv.c_str());
                            if (!imgListCsv.is_open()) {
                                printf("Unable to open imgList %s.\n", fnImgListCsv.c_str());
                                return -1;
                            }

                            std::string line;
                            std::getline(imgListCsv,line);
                            std::getline(imgListCsv,line);
                            std::getline(imgListCsv,line);
                            std::getline(imgListCsv,line);

                            labeledImgCount = stoi(line);

                            outputImgCount++;
                            std::fstream f;
                            f.open(fnImgListCsv, std::ios::out);
                            f<<"outputImgCount"<<endl;
                            f<<outputImgCount<<endl;
                            f<<"LabeledImgCount"<<endl;
                            f<<labeledImgCount<<endl;

                        }
                    }
                }

                videoImg = imgBuffer[0];
            }
            imgCount++;
            cout<<"imgCount: "<<imgCount<<endl;
        }

        checkedVideoCount++;
        std::fstream f;
        f.open(fnVideoListCsv, std::ios::out);
        f<<"videoCount"<<endl;
        f<<videoCount<<endl;
        f<<"checkedVideoCount"<<endl;
        f<<checkedVideoCount<<endl;
        for(uint32_t i = 0; i <videoNames.size(); i++){
            f<<videoNames[i]<<endl;
        }
        f.close();
    }
    return 0;
}
