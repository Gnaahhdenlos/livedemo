//
//  mainMiner.cpp
//  
//
//  Created by Henri Kuper on 12.04.17.
//
//

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

void help(){
    cout<<"To start the Programm correctly type:"<<endl;
    cout<<"./miner filepath to folder which contains videoList.csv and filepath to the folder which contains imgList.csv"<<endl;
    cout<<"Example: ./miner ../sequencesNeuronalesNetz/ ../imagesNeuronalesNetz/"<<endl;
}

int main (int argc, const char * argv[])
{
    VideoCapture cap;
    
    namedWindow("video capture", CV_WINDOW_AUTOSIZE);
    
    int imageCount = 0;
    Mat currentImg, HSVImg, binaryImg0, binaryImg1, binaryImg2, originalImgRoi, roi;
    
    vector<Mat> binaryImages;
    vector<Mat> originalImages;
    
    int m_lowH = 25;
    int m_highH = 46;
    
    int m_lowS = 60;
    int m_highS = 255;
    
    int m_lowV = 60;
    int m_highV = 255;
    int minCannyThresh = 100;
    int maxCannyThresh = 255;
    
    //int outputImgCount = 0;
    
    int frame_width = 0;
    int frame_height = 0;
    
    Mat originalImg;
    HOGDescriptor hog;
    hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());
    
    string outputFile = "/Users/henrikuper/Wingfield/data/imagesNeuronalesNetz/";
    string toVideoList;
    string toImgList;
    string fnVideoListCsv;
    string fnImgListCsv;
    
    int videoCount;
    int checkedVideoCount;
    int imgCount;
    int labeledImgCount;
    
    if(argc == 3){
        
        toVideoList = argv[1];
        toImgList = argv[2];
        outputFile = toImgList;
        
        
        fnImgListCsv = toImgList;
        fnImgListCsv += "imgList.csv";
        
        ifstream imgListCsv(fnImgListCsv.c_str());
        if (!imgListCsv.is_open()) {
            printf("Unable to open imgList %s.\n", fnImgListCsv.c_str());
            return -1;
        }
        
        std::string line;
        std::getline(imgListCsv,line);
        std::getline(imgListCsv,line);
        
        imgCount = stoi(line);
        cout<<imgCount<<endl;
        
        std::getline(imgListCsv,line);
        std::getline(imgListCsv,line);
        
        labeledImgCount = stoi(line);
        
        fnVideoListCsv = toVideoList;
        fnVideoListCsv += "videoList.csv";
        
        ifstream videoListCsv(fnVideoListCsv.c_str());
        if (!videoListCsv.is_open()) {
            printf("Unable to open Weights file %s.\n", fnVideoListCsv.c_str());
            return -1;
        }
        
        std::getline(videoListCsv,line);
        std::getline(videoListCsv,line);
        
        videoCount = stoi(line);
        cout<<videoCount<<endl;
        
        std::getline(videoListCsv,line);
        std::getline(videoListCsv,line);
        
        checkedVideoCount = stoi(line);

        
        /*string filePath = argv[1];
        cap.open(filePath);
        //cout<<filePath<<endl;
        frame_width=   cap.get(CV_CAP_PROP_FRAME_WIDTH);
        frame_height=   cap.get(CV_CAP_PROP_FRAME_HEIGHT);*/
        
    }
    else{
        help();
        return -1;
    }

    for(int v = checkedVideoCount; v < videoCount; v++){
        
        string filePath = toVideoList;
        
        char buffer[25];
        sprintf(buffer, "%06d", v);
        filePath += buffer;
        filePath += ".mp4";
        cout<<filePath<<endl;
        
        cap.open(filePath);
        frame_width=   cap.get(CV_CAP_PROP_FRAME_WIDTH);
        frame_height=   cap.get(CV_CAP_PROP_FRAME_HEIGHT);

        bool bSuccess = true;
        while (bSuccess)
        {
            
            bSuccess = cap.read(originalImg); // read a new frame from video
            //bSuccess = false;
            if (!bSuccess){
                cout << "Cannot read a frame from video stream" << endl;
                //break;
            }
            /*cap >> originalImg;
             if (!originalImg.data)
             continue;
             */
            else{
                cvtColor(originalImg, HSVImg, COLOR_BGR2HSV); //Convert the captured frame from BGR to HSV
                
                Mat colorFilteredBinaryImgNew;
                Mat cannyOutput;
                vector<vector<Point> > possibleBallContours;
                vector<Vec4i> hierarchy;
                
                inRange(HSVImg, Scalar(m_lowH, m_lowS, m_lowV), Scalar(m_highH, m_highS, m_highV), colorFilteredBinaryImgNew); //Threshold the image
                
                imshow("cfg", colorFilteredBinaryImgNew);
                waitKey(30);
                
                binaryImages.push_back(colorFilteredBinaryImgNew);
                originalImages.push_back(originalImg);
                
                if(binaryImages.size() > 3){
                    binaryImages.erase(binaryImages.begin());
                    originalImages.erase(originalImages.begin());
                }
                
                //Calculate the moments of the thresholded image
                //currentImg = colorFilteredBinaryImg.clone();
                cout<<binaryImages.size()<<endl;
                if(binaryImages.size() == 3){
                    //cout<<"test"<<endl;
                    
                    int erodeDilateSize = 4;
                    dilate( binaryImages[0], binaryImg0, getStructuringElement(MORPH_ELLIPSE, Size(erodeDilateSize, erodeDilateSize)) );
                    dilate( binaryImages[2], binaryImg2, getStructuringElement(MORPH_ELLIPSE, Size(erodeDilateSize, erodeDilateSize)) );
                    
                    //Mat colorFilteredBinaryImg = binaryImages[1] - binaryImages[0] - binaryImages[2];
                    Mat colorFilteredBinaryImg = binaryImages[1] - binaryImg0 - binaryImg2;
                    
                    
                    
                    
                    erodeDilateSize = 4;
                    //morphological opening (removes small objects from the foreground)
                    erode(colorFilteredBinaryImg, colorFilteredBinaryImg, getStructuringElement(MORPH_ELLIPSE, Size(erodeDilateSize, erodeDilateSize)) );
                    dilate( colorFilteredBinaryImg, colorFilteredBinaryImg, getStructuringElement(MORPH_ELLIPSE, Size(erodeDilateSize, erodeDilateSize)) );
                    
                    //morphological closing (removes small holes from the foreground)
                    dilate( colorFilteredBinaryImg, colorFilteredBinaryImg, getStructuringElement(MORPH_ELLIPSE, Size(erodeDilateSize, erodeDilateSize)) );
                    erode(colorFilteredBinaryImg, colorFilteredBinaryImg, getStructuringElement(MORPH_ELLIPSE, Size(erodeDilateSize, erodeDilateSize)) );
                    
                    imshow("cf", colorFilteredBinaryImg);
                    waitKey(30);
                    /// Detect edges using canny
                    Canny( colorFilteredBinaryImg, cannyOutput, minCannyThresh, maxCannyThresh*2, 3 );
                    /// Find contours
                    findContours( cannyOutput, possibleBallContours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
                    
                    vector<Moments> possibleBallMoments(possibleBallContours.size());
                    
                    for( uint32_t i = 0; i < possibleBallContours.size(); i++){
                        possibleBallMoments[i] = moments(possibleBallContours[i], false);
                    }
                    
                    vector<Point2f> possibleBallMomentsCenter(possibleBallContours.size());
                    for(uint32_t i = 0; i < possibleBallContours.size(); i++){
                        Point2f ballMomentsCenter = Point2f(possibleBallMoments[i].m10/possibleBallMoments[i].m00, possibleBallMoments[i].m01/possibleBallMoments[i].m00);
                        if(ballMomentsCenter == ballMomentsCenter){
                            //to catch points out of range
                            if(ballMomentsCenter.x >= frame_width || ballMomentsCenter.y >= frame_height || ballMomentsCenter.x < 0 || ballMomentsCenter.y < 0){
                                cout<<ballMomentsCenter<<endl;
                            }
                            else{
                                possibleBallMomentsCenter[i] = ballMomentsCenter;
                            }
                        }
                    }
                    
                    if(possibleBallMomentsCenter.size()>1){
                        float differenceBetweenPoints = 10.0;
                        for(uint32_t i = 0; i < possibleBallMomentsCenter.size()-1; i++){
                            Point2f p1 = possibleBallMomentsCenter[i];
                            Point2f p2 = possibleBallMomentsCenter[i+1];
                            if((p1.x > p2.x && p1.x - differenceBetweenPoints < p2.x)||(p1.x < p2.x && p1.x + differenceBetweenPoints > p2.x)){
                                if((p1.y > p2.y && p1.y - differenceBetweenPoints < p2.y)||(p1.y < p2.y && p1.y + differenceBetweenPoints > p2.y)){
                                    possibleBallMomentsCenter.erase(possibleBallMomentsCenter.begin()+i+1);
                                }
                            }
                        }
                    }
                    
                    int x0,y0, width, height;
                    
                    for(uint32_t i = 0; i < possibleBallMomentsCenter.size(); i++){
                        if(possibleBallMomentsCenter[i].x - 160 > 0){
                            x0 = possibleBallMomentsCenter[i].x - 160;
                        }
                        else{
                            x0 = 0;
                        }
                        if(possibleBallMomentsCenter[i].y - 256 > 0){
                            y0 = possibleBallMomentsCenter[i].y - 256;
                        }
                        else{
                            y0 = 0;
                        }
                        if(possibleBallMomentsCenter[i].x + 160 < originalImg.cols){
                            width = 320;
                        }
                        else{
                            width = originalImg.cols - x0;
                        }
                        if(possibleBallMomentsCenter[i].y + 256 < originalImg.rows){
                            height = 256+128;
                        }
                        else{
                            height = originalImg.rows - y0;
                        }
                        
                        cout<<x0<<" "<<y0<<" "<<width<<" "<<height<<endl;
                        
                        
                        originalImgRoi = originalImages[1].clone();
                        
                        cout<<originalImgRoi.cols<<" "<<originalImgRoi.rows<<endl;
                        roi = originalImgRoi(cv::Rect(x0, y0, width, height));
                        
                        Mat binaryImgRoi = colorFilteredBinaryImg.clone();
                        Mat roiBinary = binaryImgRoi(cv::Rect(x0, y0, width, height));
                        
                        vector<Rect> found, found_filtered;
                        hog.detectMultiScale(roi, found, 0, Size(8,8), Size(32,32), 1.05, 2);
                        //hog.detect(img, found, 0, Size(8,8), Size(32,32));
                        
                        size_t j, k;
                        for (k=0; k<found.size(); k++)
                        {
                            Rect r = found[k];
                            for (j=0; j<found.size(); j++)
                                if (j!=k && (r & found[j])==r)
                                    break;
                            if (j==found.size())
                                found_filtered.push_back(r);
                        }
                        for (k=0; k<found_filtered.size(); k++)
                        {
                            Rect r = found_filtered[k];
                            //r.x += cvRound(r.width*0.1);
                            //r.width = cvRound(r.width*0.9);
                            //r.y += cvRound(r.height*0.06);
                            //r.height = cvRound(r.height*0.9);
                            
                            int widthGlobal = r.width * 3;
                            int heightGlobal = r.height;
                            
                            int xGlobal = x0 + r.x + r.width/2;
                            int yGlobal = y0 + r.y;
                            
                            if(yGlobal > possibleBallMomentsCenter[i].y){
                                yGlobal = possibleBallMomentsCenter[i].y;
                                heightGlobal = r.height + r.y - (possibleBallMomentsCenter[i].y -y0);
                                cout<<"heightGlobal: "<<heightGlobal<<endl;
                            }
                            
                            if(possibleBallMomentsCenter[i].x < x0 + r.x){
                                xGlobal = possibleBallMomentsCenter[i].x - 35;
                                widthGlobal = x0 + r.x + r.width - possibleBallMomentsCenter[i].x + 70;
                            }
                            else if(possibleBallMomentsCenter[i].x > x0 + r.x + r.width){
                                xGlobal = x0 + r.x - 35;
                                widthGlobal = possibleBallMomentsCenter[i].x - (x0 + r.x) + 70;
                            }
                            else{
                                xGlobal = x0 + r.x - 35;
                                widthGlobal = r.width *1.7;
                            }
                            
                            if(xGlobal < 0){
                                xGlobal = 0;
                            }
                            
                            /*if(xGlobal - widthGlobal/2 > 0){
                                xGlobal -= widthGlobal/2;
                            }
                            else{
                                xGlobal = 0;
                            }*/
                            if(xGlobal + widthGlobal > originalImg.cols){
                                widthGlobal = originalImg.cols - xGlobal;
                            }
                            cout<<xGlobal<<" "<<yGlobal<<" "<<widthGlobal<<" "<<heightGlobal<<endl;
                            Mat outputImg = originalImgRoi(cv::Rect(xGlobal, yGlobal, widthGlobal, heightGlobal));
                            //rectangle(roi, r.tl(), r.br(), cv::Scalar(0,255,0), 2);
                            
                            imshow("outputImg", outputImg);
                            waitKey(30);
                            
                            char buffer[25];
                            
                            sprintf(buffer, "%07d", imgCount);
                            string fn = outputFile;
                            fn += buffer;
                            fn +=".ppm";
                            imwrite(fn, outputImg);
                            
                            imgCount++;
                            std::fstream f;
                            f.open(fnImgListCsv, std::ios::out);
                            f<<"ImgCount"<<endl;
                            f<<imgCount<<endl;
                            f<<"LabeledImgCount"<<endl;
                            f<<labeledImgCount<<endl;
                            
                        }
                        
                        if(found_filtered.size() > 0){
                            imshow("roi", roi);
                            //imshow("roiBlack", roiBinary);
                            //imshow("outputImg", outputImg);
                        }
                        
                        /*imshow("video capture", roi);
                        if (waitKey(30) >= 0)
                            break;*/
                        
                    }
                }
                /*int erodeDilateSize = 7;
                 dilate( currentImg, currentImg, getStructuringElement(MORPH_ELLIPSE, Size(erodeDilateSize, erodeDilateSize)) );
                 if(imageCount > 0){
                 secondToPreviousImg = previousImg.clone();
                 }
                 previousImg = currentImg.clone();
                 */
                imageCount++;
            }
        }
        checkedVideoCount++;
        std::fstream f;
        f.open(fnVideoListCsv, std::ios::out);
        f<<"videoCount"<<endl;
        f<<videoCount<<endl;
        f<<"checkedVideoCount"<<endl;
        f<<checkedVideoCount<<endl;
    }
    return 0;
}









