#include <iostream>

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <sstream> 

#include <string>
#include <fstream>

#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <sys/stat.h>
#include <sys/types.h>


//klasse 38 bild 1580
//#define ROWS_IMG    48
//#define COLS_IMG    48
#define SIZE_IMG    48
#define NUMBER_CLASSES 8  //Number of different classes

int startClass = 1;
int numberClasses = 6;

/*Mitten drin zufällig 10 prozent der liste raussuchen und in neue datein schreiben
 rest dann erst in die Trainingsdatei überführen
 */


void help(int32_t argc, char **argv) {
    printf("Usage:\n");
    printf("%s <path to the folder where folders like 00000 are placed> <Path to folder where the converted Images should be saved>\n", argv[0]);
	std::cout<< "Dont forget the / at the end of the path!"<< std::endl;
}

int myrandom (int i) { return std::rand()%i;}

// adapts the image count of the trainingsdata
void adaptImagesCount(std::string fnPrefixTarget){
	std::cout<<fnPrefixTarget<<std::endl;
	int max = 0;
	int counts[numberClasses+1];
    //std::cout<<"debug7.1"<<std::endl;
	for(int i = startClass; i < numberClasses; i++){
		
		char buffer[25];
		std::string fnFolder = fnPrefixTarget;
		sprintf(buffer, "%05d", i);
		fnFolder += buffer;	
		fnFolder += "/";
		std::string fnCsv = fnFolder;	
		fnCsv += "imgList";
		fnCsv += buffer;
		fnCsv += ".csv";
		
		std::ifstream csv(fnCsv.c_str());
	    if (!csv.is_open()) {
	        printf("Unable to open Weights file %s.\n", fnCsv.c_str());
	        //return EXIT_FAILURE;
	    }
		std::string line;
		int count = 0;
		std::getline(csv,line);
		while(std::getline(csv,line)){
			count++;
		}
		counts[i] = count;
    	if(count > max){
    		max = count;
    	}
	}
    //std::cout<<"debug7.2"<<std::endl;
	for(int i = startClass; i < numberClasses; i++){
		//std::cout<<"debug7.2.1"<<std::endl;
		char buffer[25];
		std::string fnFolder = fnPrefixTarget;
		sprintf(buffer, "%05d", i);
		fnFolder += buffer;	
		fnFolder += "/";
		std::string fnCsv = fnFolder;	
		fnCsv += "imgList";
		fnCsv += buffer;
		fnCsv += ".csv";
		//std::cout<<"debug7.2.2"<<std::endl;
		std::ifstream csv(fnCsv.c_str());
	    if (!csv.is_open()) {
	        printf("Unable to open Weights file %s.\n", fnCsv.c_str());
	        //return EXIT_FAILURE;
	    }
        else{
           //printf("File is open. \n");
        }
		std::vector<std::string> imageNames;
		std::string line;
		std::getline(csv,line);
		while(std::getline(csv,line)){
			imageNames.push_back(line);
		}
    	std::fstream f;
		f.open(fnCsv.c_str(),std::ios::app);
		int n = 0;
		int count = counts[i];
        
        if(!imageNames.empty()){
            while(count < max){
                f<<imageNames[n]<<std::endl;
                n++;
                count++;
                if(n==counts[i]){
                    n = 0;
                }
            }
        }
	}
	
}



int32_t main(int32_t argc, char **argv) {
    if (argc < 3) {
        help(argc, argv);
        return EXIT_FAILURE;
    }
    
	const std::string fnPrefix(argv[1]);
	const std::string fnPrefixTarget(argv[2]);
    const std::string fnPrefixTest(argv[3]);
	std::string fnFolder;
	std::string fnCsv;
    //printf("%s\n", fnPrefix.c_str());
	
    std::string fnTest = fnPrefixTest;
    fnTest += "00000/";
    mkdir(fnTest.c_str(), S_IRWXU);
    
    std::string testCsvFile = fnTest;
    testCsvFile += "imgList00000.csv";

    std::fstream f;
    //to clear the file before starting
    f.open(testCsvFile, std::ios::out);
    f.close();

    
	for(int i = startClass; i < numberClasses; i++){
        std::cout<<i<<std::endl;
		char buffer[25];
		fnFolder = fnPrefix;
		sprintf(buffer, "%05d", i);
		fnFolder += buffer;	
		fnFolder += "/";
		fnCsv = fnFolder;	
		fnCsv += "imgList";
		fnCsv += buffer;
		fnCsv += ".csv";
		
		
		std::string path = fnPrefixTarget;
		path += buffer;
        std::cout<<"mkdir: "<<path<<std::endl;
		mkdir(path.c_str(), S_IRWXU);
		
        
        /*path = fnPrefixTest;
        path += buffer;
        mkdir(path.c_str(), S_IRWXU);*/
		// printf("%s\n", fnFolder.c_str());
		//printf("%s\n", fnCsv.c_str());
		
		int progress = static_cast<int>(100.f / (numberClasses -1) * static_cast<double>(i)+0.1f);
		std::cout<< "["<< progress <<"%]"<<std::endl;
	    std::ifstream csv(fnCsv.c_str());

	    if (!csv.is_open()) {
	        printf("Unable to open csv file %s.\n", fnCsv.c_str());
	        return EXIT_FAILURE;
	    }
		
	    std::string line;
	    std::getline(csv, line);
        
        std::vector<std::string> imageNames;
        
        
        while(std::getline(csv,line)){
            imageNames.push_back(line);
        }
        if(imageNames.size() > 0){
            
            //std::fstream f;
            f.open(testCsvFile, std::ios::app);

            std::random_shuffle ( imageNames.begin(), imageNames.end(), myrandom);
            int size = (int)((double)imageNames.size() * 0.9);
            for(int j = imageNames.size()-1; j > size; j--){
                
                
                std::string fnTestImg = fnTest;
                
                //fnTestImg += fnImg;
                
                
                std::string fnImg;
                int32_t classid;
    
                fnImg = imageNames[j].substr(0, imageNames[j].find(";"));
                // std::cout<< fnImg<< std::endl;
                line = imageNames[j].substr(imageNames[j].find(";")+1);
                
                std::replace(line.begin(), line.end(), ';', ' ');
                std::istringstream iss(line);
                
                iss >> classid;
                //std::cout<<"debug2"<<std::endl;
                //std::cout<<fnFolder + fnImg<<std::endl;
                cv::Mat imgSrc = cv::imread(fnFolder + fnImg);
                cv::Mat imgDst;
                
                double sclHeight = static_cast<double>(SIZE_IMG) / static_cast<double>(imgSrc.rows);
                double sclWidth = static_cast<double>(SIZE_IMG) / static_cast<double>(imgSrc.cols);
                
                cv::resize(imgSrc, imgDst, cv::Size(0,0), sclWidth, sclHeight, cv::INTER_LINEAR);
               
                
                f << fnImg << ";"<< classid << ";";
                f << std::endl;
                std::string imgFile = fnPrefixTest;
                
                imgFile +="00000/";
                imgFile += fnImg;
                //std::cout<< imgFile<< std::endl;
                //imgFile += sprintf(buffer, "%05d", i);
                //std::cout<<"debug4"<<std::endl;
                cv::imwrite(imgFile, imgDst);
                
                
                imageNames.erase(imageNames.begin()+j);
            }
            f.close();
        }
        
		std::string csvFile = fnPrefixTarget;
		csvFile += buffer;
		csvFile += "/imgList";
		csvFile += buffer;
		csvFile += ".csv";
		
 		//printf("%s\n", csvFile.c_str());
		std::fstream f;
        f.open(csvFile, std::ios::out);
        f<< "Filename;ClassId;" <<std::endl;
        //std::cout<<"debug1"<<std::endl;
	    //while (std::getline(csv, line)) {
        for(uint32_t j = 0; j < imageNames.size(); j++){
                
            
			std::string fnImg;
	        int32_t classid;


	        fnImg = imageNames[j].substr(0, imageNames[j].find(";"));
	       // std::cout<< fnImg<< std::endl;
	        line = imageNames[j].substr(imageNames[j].find(";")+1);
	        std::replace(line.begin(), line.end(), ';', ' ');
	        std::istringstream iss(line);

	        iss >> classid;
            //std::cout<<"debug2"<<std::endl;
            
            cv::Mat imgSrc = cv::imread(fnFolder + fnImg);
            cv::Mat imgDst;
            
            double sclHeight = static_cast<double>(SIZE_IMG) / static_cast<double>(imgSrc.rows);
            double sclWidth = static_cast<double>(SIZE_IMG) / static_cast<double>(imgSrc.cols);
            
            cv::resize(imgSrc, imgDst, cv::Size(0,0), sclWidth, sclHeight, cv::INTER_LINEAR);
			//std::cout<<"debug3"<<std::endl;
			
	// 		cv::waitKey(60);
			f << fnImg << ";"<< classid << ";";
			f << std::endl;
			std::string imgFile = fnPrefixTarget;
			imgFile += buffer;
			imgFile +="/";
			imgFile += fnImg;
			//imgFile += sprintf(buffer, "%05d", i);
            //std::cout<<"debug4"<<std::endl;
			cv::imwrite(imgFile, imgDst);
            //std::cout<<"debug5"<<std::endl;
	       // cv::imshow("imgDst", imgDst);
	        // cv::waitKey(30);
			
	        
	    }
	     f.close();
		 //std::cout<<"debug6"<<std::endl;
	}	
	//use only for trainingsdata!!
    
    
    adaptImagesCount(fnPrefixTarget);
	
	return EXIT_SUCCESS;
}


//konsolen befehle:
//display 00000_00000.ppm
//head -n4 00000_00000.ppm
//hexdump 00000_00000.ppm
