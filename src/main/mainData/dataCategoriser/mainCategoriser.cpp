//
//  mainCategoriser.cpp
//  
//
//  Created by Henri Kuper on 18.04.17.
//
//

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

void help(){
    cout<<"To start the Programm correctly type:"<<endl;
    cout<<"./categoriser filepath to folder which contains imgList.csv and filepath to the folder saving folder"<<endl;
    cout<<"Example: ./categoriser ../imagesNeuronalesNetz/ ../imagesCategorised/"<<endl;
}

int main (int argc, const char * argv[]){
    
    namedWindow("Current Image", CV_WINDOW_AUTOSIZE);
    
    string toImgList;
    string fnImgListCsv;
    string toCategorisedImages;
    string fnImgCategorisedImagesCsv;
    
    int imgCount;
    int labeledImgCount;

    
    if(argc == 3){
        toImgList = argv[1];
        toCategorisedImages = argv[2];
        fnImgListCsv = toImgList;
        fnImgListCsv += "imgList.csv";
        
        ifstream imgListCsv(fnImgListCsv.c_str());
        if (!imgListCsv.is_open()) {
            printf("Unable to open Weights file %s.\n", fnImgListCsv.c_str());
            return -1;
        }
        
        string line;
        getline(imgListCsv,line);
        getline(imgListCsv,line);
        
        imgCount = stoi(line);
        cout<<imgCount<<endl;
        
        getline(imgListCsv,line);
        getline(imgListCsv,line);
        
        labeledImgCount = stoi(line);
        
    }
    else{
        help();
        return -1;
    }
    
    for(int i = labeledImgCount; i < imgCount; i++){
        char buffer[25];
        
        sprintf(buffer, "%07d", i);
        string fnImages = toImgList;
        fnImages += buffer;
        fnImages +=".ppm";
        Mat imgSrc = imread(fnImages);
        imshow("Current Image", imgSrc);
        waitKey(30);
        cout<<"Enter the Categorie for Image "<<i<<": "<<endl;
        int categorieNum;
        cin>>categorieNum;
        cout<<categorieNum<<endl;
        if(categorieNum > 14){
            cout<<"Are you serious? Get your shit together and type the number again. The right Number. Its your last chance!"<<endl;
            cin>>categorieNum;
        }
        string fnImgCategorisedImages = toCategorisedImages;
        
        sprintf(buffer, "%05d", categorieNum);
        string fnCategory = toCategorisedImages;
        fnCategory += buffer;
        fnCategory += "/";
        string fnCategoryCsv = fnCategory;
        fnCategoryCsv += "imgList";
        fnCategoryCsv += buffer;
        fnCategoryCsv += ".csv";
        
        
        string imageName = buffer;
        imageName += "_";
        sprintf(buffer, "%07d", i);
        imageName += buffer;
        imageName += ".ppm";
        
        std::fstream f;
        cout<<fnCategoryCsv<<endl;
        f.open(fnCategoryCsv, std::ios::app);
        f<<imageName<<";"<<categorieNum<<";"<<endl;
        f.close();
        
        fnCategory += imageName;
        
        imwrite(fnCategory, imgSrc);
        
        //std::fstream f;
        f.open(fnImgListCsv, std::ios::out);
        f<<"ImgCount"<<endl;
        f<<imgCount<<endl;
        f<<"LabeledImgCount"<<endl;
        f<<i+1<<endl;
        
    }
    
    return 0;
}









