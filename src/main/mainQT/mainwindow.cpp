#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setStyleSheet("background-color: #44BF87;");
    initButtons();

    m_qrScanner = new QRCodeScanner();

    m_qrCodesScanned = false;
    m_sessionIsRunning = false;
    m_stopSession = false;
    m_startScanQRCodes = false;
    m_changeOfSide = false;
    m_timeForChangeOfSide = 5000;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initButtons(){
    m_changeOfSideTimer = new QTimer(this);

    m_sessionButton = findChild<QPushButton*>("sessionButton");
    m_changeOfSidesButton = findChild<QPushButton*>("changeOfSidesButton");

    m_welcomeLabel = findChild<QLabel*>("welcomeLabel");
    m_sessionStatusLabel = findChild<QLabel*>("sessionLabel");

    m_welcomeLabel->setStyleSheet("QLabel {color : white; }");
    m_sessionStatusLabel->setStyleSheet("QLabel {color : white; }");

    m_sessionButton->setStyleSheet("background-color:white; color : #44BF87;");
    m_changeOfSidesButton->setStyleSheet("background-color:white; color : #44BF87;");
    m_changeOfSidesButton->setVisible(false);

    connect(m_sessionButton, SIGNAL (released()), this, SLOT (sessionButtonPressed()));
    connect(m_changeOfSidesButton, SIGNAL (released()), this, SLOT (changeOfSidesButtonPressed()));
    connect(m_changeOfSideTimer, SIGNAL (timeout()), this, SLOT (makeChangeOfSideButtonVisible()));
}

void MainWindow::sessionButtonPressed(){
    if(m_qrCodesScanned == false){
        scanQRCode();
    }
    else if(m_qrCodesScanned == true && m_sessionIsRunning == false){
        startSession();
    }
    else if(m_sessionIsRunning == true){
        stopSession();
    }
    else{
        m_sessionStatusLabel->setText("Please scan the QR Codes first");
    }
}

void MainWindow::changeOfSidesButtonPressed(){
    cout<<"changeOfSidesButtonPressed"<<endl;
    m_changeOfSidesButton->setVisible(false);
    m_changeOfSide = true;
    m_changeOfSideTimer->start(m_timeForChangeOfSide);
}

void MainWindow::makeChangeOfSideButtonVisible(){
   cout<<"SetChangeOfSideButtonVisible"<<endl;
   m_changeOfSideTimer->stop();
   m_changeOfSidesButton->setVisible(true);
}

void MainWindow::scanQRCode(){
    std::cout<<"scanQRCodeButtonPressed"<<std::endl;
    m_startScanQRCodes = true;
    m_sessionStatusLabel->setText("Scanner is Ready. \nPlease hold your QR Codes infront of the cameras.");
}

void MainWindow::startSession(){

    std::cout<<"startSession"<<std::endl;
    m_startSession = true;
    m_sessionIsRunning = true;
    m_sessionStatusLabel->setText("Session is running...");
    m_sessionButton->setText("Stop Session");
    //m_changeOfSidesButton->setVisible(true);
    m_changeOfSideTimer->start(m_timeForChangeOfSide);
}

void MainWindow::stopSession(){
    m_changeOfSideTimer->stop();
    m_sessionIsRunning = false;
    m_qrCodesScanned = false;
    m_stopSession = true;
    m_sessionButton->setVisible(false);
    m_sessionButton->setText("Scan QR Code");
    m_changeOfSidesButton->setVisible(false);
    m_sessionStatusLabel->setText("Session is stopped. \n The System needs a few minutes \nto finish calculation and to upload the data.");

}

void MainWindow::setSessionStatus(QString status){
  m_sessionButton->setText(status);
}
void MainWindow::setSessionButtonToStart(){
  m_sessionButton->setText("Start Session");
}

void MainWindow::qrCodesScannedSuccesfully(){
   m_sessionStatusLabel->setText("You signed in successfully");
   m_sessionButton->setText("Start Session");
   m_qrCodesScanned = true;
}
void MainWindow::qrCodesNotScannedSuccesfully(int numberOfCodes){
  m_qrCodesScanned = false;
  if(numberOfCodes == 0){
    m_sessionStatusLabel->setText("Could not detec any QR Codes. \nPlease try again.");
  }
  else{
    m_sessionStatusLabel->setText("Could detec " + QString::number(numberOfCodes) + " QR Code. \nPlease try again.");
  }
}

void MainWindow::resetGui(){
  m_sessionStatusLabel->setText("");
  m_sessionButton->setText("Scan QR Code");
  m_sessionButton->setVisible(true);
  cout<<"m_qrCodesScanned: "<<m_qrCodesScanned<<endl;
  cout<<"m_sessionIsRunning: "<<m_sessionIsRunning<<endl;
  cout<<"m_startSession: "<<m_startSession<<endl;
  cout<<"m_stopSession: "<<m_stopSession<<endl;
}
