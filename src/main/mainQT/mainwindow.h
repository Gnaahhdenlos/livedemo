#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QApplication>
#include <QPushButton>
#include <QLabel>
#include <QPalette>
#include <QTimer>
#include <iostream>

#include "../../trackLib/configData.hpp"
#include "../../trackLib/appController.hpp"

#include "../../cameraLib/camera.hpp"
#include "../../cameraLib/qrCodeScanner.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow{
    Q_OBJECT

private:
//gui variables
  Ui::MainWindow *ui;

  QTimer *m_changeOfSideTimer;
  int m_timeForChangeOfSide;

  QPushButton *m_sessionButton;
  QPushButton *m_changeOfSidesButton;

  QLabel *m_welcomeLabel;
  QLabel *m_sessionStatusLabel;

  // function variables
  QRCodeScanner *m_qrScanner;

  bool m_qrCodesScanned;
  bool m_sessionIsRunning;
  bool m_startSession;
  bool m_stopSession;
  bool m_startScanQRCodes;
  bool m_changeOfSide;

  void initButtons();
  void scanQRCode();
  void startSession();
  void stopSession();

private slots:
  void sessionButtonPressed();
  void changeOfSidesButtonPressed();
  void makeChangeOfSideButtonVisible();

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

  void qrCodesScannedSuccesfully();
  void qrCodesNotScannedSuccesfully(int numberOfCodes);

  void resetGui();

  void setSessionStatus(QString status);

  void setStartScanQRCodes(bool startScanQRCodes){m_startScanQRCodes = startScanQRCodes;}
  void setStopSession(bool stopSession){m_stopSession = stopSession;}
  void setStartSession(bool startSession){m_startSession = startSession;}
  void setQRCodesScanned(bool qrCodesScanned){m_qrCodesScanned = qrCodesScanned;}
  void setChangeOfSide(bool changeOfSide){m_changeOfSide = changeOfSide;}

  void setSessionButtonToStart();


  bool getStartScanQRCodes(){return m_startScanQRCodes;}
  //bool getSessionIsRunning(){return m_sessionIsRunning;}
  bool getStopSession(){return m_stopSession;}
  bool getStartSession(){return m_startSession;}
  bool getChangeOfSide(){return m_changeOfSide;}


};

#endif // MAINWINDOW_H
