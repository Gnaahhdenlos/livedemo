cmake_minimum_required (VERSION 2.6)
project (mainTracking)

find_package (OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})

IF(APPLE)
	include_directories(/Library/Frameworks/pylon.framework)
	include_directories(/Library/Frameworks/pylon.framework/Headers)
	include_directories(/Library/Frameworks/pylon.framework/Headers/GenICam)
	link_directories(/Library/Frameworks/pylon.framework/Versions/A/Libraries)
	link_directories(/Library/Frameworks/pylon.framework/Libraries)
ELSEIF(UNIX)
	include_directories(/opt/pylon5/include)
	link_directories(/opt/pylon5/lib64)
ENDIF()

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)


find_package(Qt5Widgets REQUIRED)

set ( SOURCES main.cpp mainwindow.cpp)
set ( MOC_HEADERS mainwindow.h)
set ( UIS mainwindow.ui)
set ( RESOURCES main.qrc)

set ( BASLER_LIBS pylonbase GCBase_gcc_v3_0_Basler_pylon_v5_0 pylonutility)

add_executable(detecWithQT ${SOURCES})

target_link_libraries (detecWithQT trackLib ${OpenCV_LIBS} -lpthread -lm ${BASLER_LIBS} Qt5::Widgets)

set(CMAKE_CXX_FLAGS "-std=c++11 -Wall -O3")
