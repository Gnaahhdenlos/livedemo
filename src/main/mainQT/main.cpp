#include "mainwindow.h"
#include <QApplication>
#include <iostream>
#include <thread>

#include "../../trackLib/configData.hpp"
#include "../../trackLib/appController.hpp"

#include "../../cameraLib/camera.hpp"
#include "../../cameraLib/qrCodeScanner.hpp"

using namespace std;

// GUI variables
int argc1 = 1;
char *argv1[] = {(char *)"./detecWithQT"};
QApplication a(argc1, argv1);
MainWindow w;

// function variables
QRCodeScanner *m_qrScanner;
AppController *controller;
bool running, startScanner, startSession, stopSession, changeOfSide;

void startEvents();
void updateStatus();

int main(int argc, char *argv[]){

  w.setWindowTitle("WINGFIELD");
  w.setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
  w.show();

  running = true;
  startScanner = false;
  startSession = false;
  m_qrScanner = new QRCodeScanner();

  thread startEventsThread(&startEvents);
  thread updateStatusThread(&updateStatus);

  a.exec();
  running = false;
  startEventsThread.join();
  updateStatusThread.join();
  return 1;
}

void startEvents(){
  while(running == true){
    if(startScanner == true){
      cout<<"StartEvent scanner"<<endl;
      w.setStartScanQRCodes(false);
      startScanner = false;

      int numberOfCodes = m_qrScanner->findQRCode();
      if(numberOfCodes == 2){
        w.qrCodesScannedSuccesfully();
      }
      else{
        w.qrCodesNotScannedSuccesfully(numberOfCodes);
      }
      //Nur für testing Start!!
      //w.qrCodesScannedSuccesfully();
      //Nur für testing Ende!!
      m_qrScanner->reset();
      w.setStartScanQRCodes(false);
      startScanner = false;
    }
    if(startSession == true){
      cout<<"StartEvent Session"<<endl;
      struct ConfigData config;
      string matchName = "/home/wingfield/alphaConfigFiles/videos/match";
      string pathToWeights = "/home/wingfield/alphaConfigFiles/Weights83";
      string pathToCalibrationMatrix = "/home/wingfield/alphaConfigFiles/out_camera_data.xml";
      string pathToFieldCalibrationPoints = "/home/wingfield/alphaConfigFiles/fieldLinesCalibrationData.xml";

      // string matchName = "/Users/henrikuper/Wingfield/alphaConfigFiles/videos/match";
      // string pathToWeights = "/Users/henrikuper/Wingfield/alphaConfigFiles/Weights83";
      // string pathToCalibrationMatrix = "/Users/henrikuper/Wingfield/alphaConfigFiles/out_camera_data.xml";
      // string pathToFieldCalibrationPoints = "/Users/henrikuper/Wingfield/alphaConfigFiles/fieldLinesCalibrationData.xml";

      time_t now = time(NULL);
      tm *gmtm = gmtime(&now);

      matchName.append(to_string(1900 + gmtm->tm_year));
      matchName.append(to_string(1 + gmtm->tm_mon));
      matchName.append(to_string(gmtm->tm_mday));
      matchName.append(to_string(1 + gmtm->tm_hour));
      matchName.append(to_string(1 + gmtm->tm_min));
      matchName.append(to_string(1 + gmtm->tm_sec));

      string matchNameRight = matchName + "R.avi";
      string matchNameLeft = matchName + "L.avi";
      string matchOutput = matchName + "Output.avi";
      cout<<"matchNameRight: "<<matchNameRight<<endl;

      config.setFilePathToVideoRight(matchNameRight);
      config.setFilePathToCalibrationVideoRight(matchNameRight);

      config.setFilePathToVideoLeft(matchNameLeft);
      config.setFilePathToCalibrationVideoLeft(matchNameLeft);

      config.setSafePathOutputVideo(matchOutput);
      config.setFilePathToWeights(pathToWeights);

      config.setFilePathToCameraCalibrationMatrix(pathToCalibrationMatrix);
      config.setFilePathToFieldCalibrationPoints(pathToFieldCalibrationPoints);

      config.setCalibrationType(1);
      config.setWithCamera(true);

      controller = new AppController(config);
      controller->setQRCodes(m_qrScanner->getQRCodeLeft(), m_qrScanner->getQRCodeRight());
      controller->controlWithCameraAndGUI();
      delete controller;
      controller = NULL;

      w.setStartSession(false);
      startSession = false;
      cout<<"StartSessionThread"<<endl;
      //Nur für testing Start!!
      // while(stopSession == false){
      //     this_thread::sleep_for(chrono::milliseconds(1));
      // }
      //Nur für testing Ende!!

      //cout<<"StartSessionThread"<<endl;
      //this_thread::sleep_for(chrono::milliseconds(5000));
      cout<<"ResetGui"<<endl;
      w.resetGui();
    }
    this_thread::sleep_for(chrono::milliseconds(50));
  }
}

void updateStatus(){
  while(running == true){
    startScanner = w.getStartScanQRCodes();
    stopSession = w.getStopSession();
    startSession = w.getStartSession();
    changeOfSide = w.getChangeOfSide();

    if(stopSession == true){
      cout<<"Session Stopped"<<endl;
      controller->stopSession();
      w.setStopSession(false);
      this_thread::sleep_for(chrono::milliseconds(50));
      stopSession = false;
    }
    if(changeOfSide == true){
      cout<<"ChangeOfSide == true"<<endl;
      w.setChangeOfSide(false);
      changeOfSide = false;
      controller->setChangeOfSidesEvent();
    }
    this_thread::sleep_for(chrono::milliseconds(50));
  }
}
