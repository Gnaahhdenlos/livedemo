// C++ std libraries
#include <iostream>
#include <sstream> 
#include <string>
#include <fstream>

// C std libraries
#include <cstdlib>
#include <cstdint>

// application specific includes

#include "../../walterLib/network.hpp"

// openCV includes
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <algorithm>    // std::random_shuffle
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand
//#include <Directory>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

//Define
//#define NUMBER_CLASSES  3 //Number of different classes

int startClass = 0;
int endClass = 3;
int numberOutputClasses = 3;

void help(int32_t argc, char **argv) {
    printf("Usage:\n");
    printf("%s <path to the folder where folders like 00000 are placed> <path for the weight data> <Convertion Number> <optional path to prev Weights for continuous learning\n", argv[0]);
	std::cout<< "Dont forget the / at the end of the path!"<< std::endl;
	std::cout<<"convertionNumber: "<<std::endl;
	std::cout<<"original = 1"<<std::endl;
	std::cout<<"imadjust = 2"<<std::endl;
	std::cout<<"histeq = 3"<<std::endl;
	std::cout<<"aheq = 4"<<std::endl;
	std::cout<<"conorm = 5"<<std::endl;
}

/* Calculates the progress of the training by printout time to finish the training epoche 
 */
void progressCalc(int i, int nImages, time_t timeStart ){
	float progress = (100.f / nImages * static_cast<double>(i)); 
	time_t timeNow = time(&timeNow);
	time_t timeDiff = timeNow - timeStart;
	timeDiff = (timeDiff/progress*100.f - timeDiff);
	if(timeDiff > 60){
		timeDiff /=60;
		std::cout<< "["<< progress <<"%]" << std::endl;
		std::cout<< timeDiff <<" minutes left" << std::endl;
	}
	else{
		std::cout<< "["<< progress <<"%]" << std::endl;
		std::cout<< timeDiff <<" seconds left" << std::endl;
	}
}

/* Function to create a random int
 * @param	i	threshould value
 * @return	std::rand()%i	random integer			
 */
int myrandom (int i) { return std::rand()%i;}

/* Main function for training a net.
 * It initialises a starting learnrate, a target vector, a random image vector and a net
 * While the detectionrate of the trainingsdate is lower then a threshould a new random image vector is generated.
 * The images are preprocesses and forward and backward propagated through the net. 
 * The resultant weights are stored in a csv-file
 * @param 	argc	number of arguments
 * @param	argv	programm name, path to trainings img, path to store weights, convert number, (optional: path of weights for continouse training)
 */
int32_t main(int32_t argc, char **argv) {
	int epoche = 0;
	const float startRate = 0.001f/1.6f;
	//const float endRate = 0.0000001;
	//const float factor = 0.90;
	float rate = startRate;	
	
	std::vector<float> binTarget(numberOutputClasses);
	std::vector<uint32_t> nTarget(numberOutputClasses);
	for(int i = 0; i < numberOutputClasses; i++){
		nTarget[i] = 0;
	}
	
	std::srand ( unsigned ( std::time(0) ) );
	std::vector<std::pair<std::string, int>> randomImages;
	std::time_t timeStart;
	
	if (argc < 3) {
        help(argc, argv);
        return EXIT_FAILURE;
    }
	const std::string fnPrefix(argv[1]);
	const std::string CsvWeights(argv[2]);
	std::string Conv(argv[3]);
	std::string ConvMod;
	
	switch(std::stoi(Conv)){
		case 1: ConvMod = "original"; std::cout<<ConvMod<<std::endl;break;
		case 2: ConvMod = "imadjust"; std::cout<<ConvMod<<std::endl;break;
		case 3: ConvMod = "histeq"; std::cout<<ConvMod<<std::endl;break;
		case 4: ConvMod = "aheq"; std::cout<<ConvMod<<std::endl;break;
		case 5: ConvMod = "conorm"; std::cout<<ConvMod<<std::endl;break;		
		default: help(argc, argv);return EXIT_FAILURE; break;				
	}
	
	
	network *net = new network();
	
	if(argc == 5){
		const std::string CsvPrevWeights(argv[4]);
		net->initWeights(CsvPrevWeights);
		rate = net->getLearnRateFromCSV(CsvPrevWeights);
		epoche = (net->getEpocheFromCSV(CsvPrevWeights)-1);
	}
	//------------training-------------
	
	randomImages = net->createImageVector(fnPrefix);
	
	while(net->getDetectionRate()<0.9999f){
		//rate = 0.01f;
		std::cout<<"Rate: "<<rate<<" Epoche: "<<epoche<<std::endl;
		if(net->getDetectionRate()<0.96){
			rate = 0.01f;
		}
		/*else if(net->getDetectionRate()<0.98){
			rate = 0.009f;
		}
		else if(net->getDetectionRate()<0.99){
			rate = 0.008f;
		}
		else if(net->getDetectionRate()<0.999){
			rate = 0.007f;
		}
		else{
			rate = 0.006f;
		}*/
		net->setLearnRate(rate);
		
		epoche++;
		net->setEpoche(epoche);
		std::random_shuffle ( randomImages.begin(), randomImages.end(), myrandom);
		timeStart = time(&timeStart);
		
		net->clearDetectionRate();
		net->clearNDetectedImages();
		net->clearNFalseDetectedImages(); 
		
		
		for (uint32_t i = 0 ; i < randomImages.size(); i++){
			std::cout<<"Rate: "<<rate<<" Epoche: "<<epoche<<std::endl;
			//--------time calculation------------
			progressCalc(i, randomImages.size(), timeStart);
				
			//----------Target calculation----------
			for(int32_t j = startClass; j < endClass; j++){
				if(j == randomImages[i].second){
						binTarget[j-startClass] = 1.0f;
						nTarget[j-startClass] += 1;
				} 
				else binTarget[j] = 0.0f;
			}
			
			for (std::vector<uint32_t>::iterator it = nTarget.begin(); it!=nTarget.end(); ++it)
			    std::cout << *it << ' ';
	        std::cout<<std::endl;
			
			std::cout<< randomImages[i].first << std::endl;
			for (std::vector<float>::iterator it = binTarget.begin(); it!=binTarget.end(); ++it)
			    std::cout << *it << ' ';
	        std::cout<<std::endl;
		
			//----------open and convert Input image-------
			std::string folder = randomImages[i].first.substr(0, randomImages[i].first.find("_"));
			folder += "/";
			cv::Mat img = cv::imread(fnPrefix + folder + randomImages[i].first);
            
            if (img.empty()){
                std::cout<<"unable to open "<<fnPrefix + folder + randomImages[i].first<<std::endl;
            }
            cv::imshow("Img", img);
            cv::waitKey(30);
			img = net->preprocess(img);
			
			switch(std::stoi(Conv)){
				case 1: break;
				case 2: img = net->convertToImadjust(img,1);break;
				case 3: img = net->convertToHisteq(img);break;
				case 4: img = net->convertToAheq(img, 4);break;
				case 5: img = net->convertToContrast(img);break;						
			}
			//cv::imshow("img", img);
			float inp[48*48*3];
			float *input = net->convertInput(img, inp);
			//----------forward and backprop ----------
				net->forward(img);

				net->getFCLayer7()->printOutput(randomImages[i].second);

				net->backward(input, binTarget);
			
				net->update();
				//net->calcAvarageError(i);
				net->countDetectedImages(randomImages[i].first, randomImages[i].second);
				std::cout<<"AvarageErr: "<<net->getAvarageError()<<std::endl;
				std::cout<<"DetecRate: "<<net->getDetectionRate()<<std::endl;
				std::cout<<std::endl;
				
				//only safe the actual weights every 500 training step to reduce computing time
				if(i%500==0){
					net->printWeightsToCsv(CsvWeights);			
				}			
		}
		net->printWeightsToCsv(CsvWeights);
	}
}
