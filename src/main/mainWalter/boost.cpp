#include <iostream>

#include <cstdlib>
#include <cstdint>
#include <cstdio>
#include <sstream> 

#include <string>
#include <fstream>

#include <algorithm>

#include "../../walterLib/boostNetwork.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <sys/stat.h>
#include <sys/types.h>


#define NUMBER_CLASSES 43

void help(int32_t argc, char **argv) {
    printf("Usage:\n");
    printf("%s <path to the folder Final_test_*> <path Original Weights><path to ImadjustWeights><path to HisteqWeights><path to AheqWeights><path to ConromWeights><path to write the outcome>\n", argv[0]);
	std::cout<< "Dont forget the / at the end of the path!"<< std::endl;
}

/* Main function which initialises a boostNet with the 5 different weightfiles.
 * It creates an image vector of all testimages and puts every image of the vector through the make boostNet
 * and checks if the detection is correct.
 * The results and the statistic of the tests get stored in the csv-file.
*/
int32_t main(int32_t argc, char **argv) {
    if (argc < 7) {
        help(argc, argv);
        return EXIT_FAILURE;
    }

	std::string fnPrefix = argv[1];
	std::string CsvWeightsOriginal = argv[2];
	std::string CsvWeightsImadjust = argv[3];
	std::string CsvWeightsHisteq = argv[4];
	std::string CsvWeightsAheq = argv[5];
	std::string CsvWeightsConorm = argv[6];
	std::string CsvDetection = argv[7];
	
	boostNetwork *boostNet = new boostNetwork();
	boostNet->initWeights(CsvWeightsOriginal, CsvWeightsImadjust, CsvWeightsHisteq, CsvWeightsAheq, CsvWeightsConorm);	
		
    std::vector<std::pair<std::string, int>> images = boostNet->createImageVector(fnPrefix);

    for (uint32_t i = 0 ; i < images.size(); i++){

        //----------Target calculation----------
        std::vector<float> binTarget(43);
        for(int j = 0; j < NUMBER_CLASSES; j++){
            if(j == images[i].second){ 
                binTarget[j] = 1.0f;
            }
            else binTarget[j] = 0.0f;
        }
    
        std::cout<< images[i].first << std::endl;
        for (std::vector<float>::iterator it = binTarget.begin(); it!=binTarget.end(); ++it)
            std::cout << *it << ' ';
        std::cout<<std::endl;

        //----------open and convert Input image-------
        std::string folder = images[i].first.substr(0, images[i].first.find("_"));
        folder += "/";
        cv::Mat img = cv::imread(fnPrefix + folder + images[i].first);
        boostNet->forward(img);
        boostNet->calcDetection();
        //net->getFCLayer7()->lossFunction(binTarget);
        boostNet->countDetectedImages(images[i].first, images[i].second);
        std::string strokeName = boostNet->getStrokeName(boostNet->getStrokeN());
        std::string strokeNameRight = boostNet->getStrokeName(images[i].second);
        std::cout<<"Detect: "<<strokeName<<std::endl;
        std::cout<<"Target: "<<strokeNameRight<<std::endl;
        std::cout<<"AvarageRightProbility: "<<boostNet->getAvRightProbability()<<std::endl;
        std::cout<<"AvarageFalseProbility: "<<boostNet->getAvFalseProbability()<<std::endl;
        boostNet->printDetectionRate();
        float* detectionRates = boostNet->getDetectionRates();
        int* nDetectedImages = boostNet->getnDetectedImages();
        int* nFalseDetectedImages = boostNet->getnFalseDetectedImages();
        for(int j = 0; j < 10; j++){
            std::cout<<"detection rate"<<j<<": "<<detectionRates[j]<<std::endl;
        }
        
        for(int j = 0; j < 10; j++){
            float notDetected = static_cast<float>(i-nFalseDetectedImages[j]-nDetectedImages[j]+1)/static_cast<float>(i+1);
            std::cout<<"Not Detected Images[%]"<<j<<": "<< (notDetected) <<std::endl;
        }
        int* rightProbHistogram = boostNet->getRightProbHistogram();
        int* falseProbHistogram = boostNet->getFalseProbHistogram();
        float* relativeRightProbHistogram = boostNet->getRelativeRightProbHistogram();
        float* relativeFalseProbHistogram = boostNet->getRelativeFalseProbHistogram();			
        std::cout<<"RightHist;FalseHist;RelRight;RelFalse;"<<std::endl;
        for(int i = 0; i < 101; i++){
            std::cout<<rightProbHistogram[i]<<";"<<falseProbHistogram[i]<<";"<<relativeRightProbHistogram[i]<<";"<<relativeFalseProbHistogram[i]<<";"<<std::endl;
        }
        
        boostNet->printOutput(images[i].second);
        cv::imshow("img", img);
        cv::waitKey(1);
        boostNet->printDetectionToCsv(CsvDetection);
    }
}
