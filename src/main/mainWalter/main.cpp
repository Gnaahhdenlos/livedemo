#include <iostream>

#include <cstdlib>
#include <cstdint>
#include <cstdio>
#include <sstream> 

#include <string>
#include <fstream>

#include <algorithm>
#include "../../walterLib/network.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <sys/stat.h>
#include <sys/types.h>


#define NUMBER_CLASSES 7

int numberClasses = 6;

void help(int32_t argc, char **argv) {
    printf("Usage:\n");
    printf("%s <path to the folder where folders like 00000 are placed> <path for the weight data><path to write the outcome><Convertion Number><weights start and end number>\n", argv[0]);
	std::cout<< "Dont forget the / at the end of the path!"<< std::endl;
}

/* Main function which initialises a single Net with weights from a file.
 * It creates an image vector of all testimages and puts every image of the vector through the net 
 * and checks if the detection is correct.
 * The results and the statistic of the tests get stored in the csv-file.
*/
int32_t main(int32_t argc, char **argv) {
    if (argc < 7) {
        help(argc, argv);
        return EXIT_FAILURE;
    }
	
	std::string fnPrefix = argv[1];
	std::string CsvWeightsX = argv[2];
	std::string CsvDetection = argv[3];
	std::string Conv(argv[4]);
	std::string sstart(argv[5]);
	std::string send(argv[6]);
	std::string ConvMod;
	
	switch(std::stoi(Conv)){
		case 1: ConvMod = "original"; std::cout<<ConvMod<<std::endl;break;
		case 2: ConvMod = "imadjust"; std::cout<<ConvMod<<std::endl;break;
		case 3: ConvMod = "histeq"; std::cout<<ConvMod<<std::endl;break;
		case 4: ConvMod = "aheq"; std::cout<<ConvMod<<std::endl;break;
		case 5: ConvMod = "conorm"; std::cout<<ConvMod<<std::endl;break;		
		default: help(argc, argv);return EXIT_FAILURE; break;				
	}
	
	int start = std::stoi(sstart);
	int end = std::stoi(send);
	if(start < 1){
		start = 1;
	}
	std::cout<<"start: "<<start<<" end: "<<end<<std::endl;
	for(int n = start; n < end; n++){	
		std::string CsvWeights = CsvWeightsX;
		CsvWeights += std::to_string(n);
		network *net = new network();
		net->initWeights(CsvWeights);
		net->setEpoche(net->getEpocheFromCSV(CsvWeights));
		std::vector<std::pair<std::string, int>> images = net->createImageVector(fnPrefix);
		
		clock_t timeStart2;
		clock_t timeNow2;
		double timeDiff2;
		timeStart2 = clock();
		for (uint32_t i = 0 ; i < images.size(); i++){

			//----------Target calculation----------
			std::vector<float> binTarget(numberClasses);
			for(int j = 0; j < numberClasses; j++){
					if(j == images[i].second){ 
						binTarget[j] = 1.0f;
					}
					else binTarget[j] = 0.0f;
				}
		
			std::cout<< images[i].first << std::endl;
			for (std::vector<float>::iterator it = binTarget.begin(); it!=binTarget.end(); ++it)
			    std::cout << *it << ' ';
	        std::cout<<std::endl;
	
			//----------open and convert Input image-------
			//std::string folder = images[i].first.substr(0, images[i].first.find("_"));
			std::string folder = "00000/";
            //folder += "/";
            std::cout<<fnPrefix + folder + images[i].first<<std::endl;
			cv::Mat img = cv::imread(fnPrefix + folder + images[i].first);
			
            if (img.empty()){
                std::cout<<"unable to open "<<fnPrefix + folder + images[i].first<<std::endl;
            }
            
			switch(std::stoi(Conv)){
				case 1: break;
				case 2: img = net->convertToImadjust(img,1);break;
				case 3: img = net->convertToHisteq(img);break;
				case 4: img = net->convertToAheq(img, 4);break;
				case 5: img = net->convertToContrast(img); break;
			}
		
			//----------forward----------
			net->forward(img);
			net->getFCLayer7()->lossFunction(binTarget);
			//std::string strokeName = net->getStrokeName(net->getFCLayer7()->getDetection());
			//std::string strokeNameRight = net->getStrokeName(images[i].second);
            std::string strokeName = net->getStrokeNameThreeClasses(net->getFCLayer7()->getDetection());
            std::string strokeNameRight = net->getStrokeNameThreeClasses(images[i].second);
			std::cout<<"Detect: "<<strokeName<<std::endl;
			std::cout<<"Target: "<<strokeNameRight<<std::endl;
		
			net->getFCLayer7()->printOutput(images[i].second);	
			net->getFCLayer7()->printErrorTotal();
			net->calcAvarageError(i);
			net->countDetectedImages(images[i].first, images[i].second);
			std::cout<<"AvarageErr: "<<net->getAvarageError()<<std::endl;
			std::cout<<"AvarageRightProbility: "<<net->getAvRightProbability()<<std::endl;
			std::cout<<"AvarageFalseProbility: "<<net->getAvFalseProbability()<<std::endl;
			std::cout<<"DetecRate: "<<net->getDetectionRate()<<std::endl;
			net->printFalseDetectedHist();
			std::cout<<std::endl;
			net->printDetectionToCsv(CsvDetection, net->getEpoche());
		
			timeNow2 = clock();
			timeDiff2 = (double)(timeNow2 - timeStart2)/CLOCKS_PER_SEC;
			timeDiff2 = timeDiff2/(i+1);
			std::cout <<"TimeAll: "<<timeDiff2<<std::endl;	
		
		}
		//net->print2DHistogram();
		net->printDetectionToCsv(CsvDetection, net->getEpoche());
	}	
}
