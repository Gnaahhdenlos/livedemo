
#include <iostream>
#include <string>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <ctime>

#include <sys/stat.h>
#include <sys/types.h>

#include "../../trackLib/fieldDetection.hpp"
#include "../../trackLib/ballDetection.hpp"
#include "../../trackLib/curveDetection.hpp"

using namespace cv;
using namespace std;

int thresh = 100;
int max_thresh = 255;
bool isCalibrated = false;

clock_t timeStart;
clock_t timeNow;
double timeDiff;

clock_t timeStartOverall;
clock_t timeNowOverall;
double timeDiffOverall;

void printTime(string timeName){
    timeNow = clock();
    timeDiff = (double)(timeNow - timeStart)/CLOCKS_PER_SEC;
    std::cout <<timeName<<timeDiff<<std::endl;
    timeStart = clock();
}


int main( int argc, char** argv )
{
    int frameWidth = 0;
    int frameHeight = 0;
    VideoCapture cap;
    VideoCapture calibrationVideo;
    VideoWriter writer;

    vector<Mat> imgBuffer;
    //bool bufferIsReady = false;
    int startFrame = 3200; //minimum 3
    int endFrame = 1500;

    FieldDetection *lines = new FieldDetection();

    if(argc < 2) {
        cap.open(0); //capture the video from web cam

        if ( !cap.isOpened() )  // if not success, exit program
        {
            cout << "Cannot open the web cam" << endl;
            return -1;
        }

        frameWidth=   cap.get(CV_CAP_PROP_FRAME_WIDTH);
        frameHeight=   cap.get(CV_CAP_PROP_FRAME_HEIGHT);
        writer.open("/Users/henrikuper/Downloads/live.avi",CV_FOURCC('m', 'p', '4', 'v'), 10, cv::Size(frameWidth,frameHeight),true);
        cout<<frameWidth<<" "<<frameHeight<<endl;

    }

    if(argc >= 2){
        string filePath = argv[1];
        string safePath = argv[2];
        cap.open(filePath);
        cout<<filePath<<endl;
        frameWidth=   cap.get(CV_CAP_PROP_FRAME_WIDTH);
        frameHeight=   cap.get(CV_CAP_PROP_FRAME_HEIGHT);
        //
        //fourcc = CV_FOURCC('m', 'p', '4', 'v');

        //success = vout.open('output.mp4',fourcc,fps,size,True)
        bool success = writer.open(safePath,CV_FOURCC('m', 'p', '4', 'v'), 60, cv::Size(frameWidth,frameHeight),true);
        if (!success){
            std::cout << "!!! Output video could not be opened" << std::endl;
            return -1;
        }
        else{
            cout<<"Output is opend"<<endl;
        }
    }
    if(argc >= 4){
        string filePath = argv[3];
        calibrationVideo.open(filePath);
        frameWidth=   cap.get(CV_CAP_PROP_FRAME_WIDTH);
        frameHeight=   cap.get(CV_CAP_PROP_FRAME_HEIGHT);
        lines->calibrateFieldLinesWithCalibrationVideo(calibrationVideo);
        isCalibrated = true;
    }
    if(argc >=5){
        startFrame = stoi(argv[4]);
        cout<<"startFrame: "<<startFrame<<endl;
        endFrame = stoi(argv[5]);
        cout<<"EndFrame: "<<endFrame<<endl;
    }

    cout<<"argc "<< argc<<endl;
    for(int i = 0; i < argc; i++){
        cout<<i<<": "<<argv[i]<<endl;
    }

    /*else if (argc >=5){
        cout<<"Wrong Args"<<endl;
        return -1;
    }*/

    PlayerSegmentation *player= new PlayerSegmentation(lines, frameWidth, frameHeight);
    BallDetection *balls = new BallDetection(frameHeight, frameWidth, lines, player);
    CurveDetection *curve = new CurveDetection(frameHeight, frameWidth, lines, player, balls);
   // balls->setHomographyMat(lines->getHomographyMat());
    //balls->setfieldModel(lines->getFieldModel());
    //balls->setCalibrationImg(lines->getCalibrationImg());



    int imgCount = 0;
    /*for(int i = 0; i < 140; i++){
        Mat imgOriginal;
        bool bSuccess = cap.read(imgOriginal); // read a new frame from video

        if (!bSuccess){
            cout << "Cannot read a frame from video stream" << endl;
            break;
        }
        imgCount++;
        cout<<"imgCount: "<<imgCount<<endl;
    }*/
    while (true)// && imgCount < endFrame)
    {
        timeStart = clock();
        timeStartOverall = clock();
        Mat imgOriginal;
        Mat imgLines;
        Mat imgBalls;
        int framesPrediction = 12;

        bool bSuccess = cap.read(imgOriginal); // read a new frame from video

        if (!bSuccess){
            cout << "Cannot read a frame from video stream" << endl;
            break;
        }

        imgBuffer.push_back(imgOriginal.clone());
        if((int32_t)imgBuffer.size() > framesPrediction){
            imgBuffer.erase(imgBuffer.begin());
        }
        printTime("ReadImg: ");

        if(isCalibrated == false){
            lines->detecFieldLines(imgOriginal);
        }


        if(isCalibrated == true){
            imgLines =lines->getCalibratedImgLines();
        }
        else{
            imgLines = lines->getImgLines();
        }

        //printTime("Calibration: ");

        //balls->detecBallWithColor(imgOriginal);
        if(imgCount > startFrame){
            //main.cpp ist nicht auf graustufenbilder angepasst (daher wird es abstürzen)
            curve->detecCurves();
            //balls->detecBallWithDiff(imgOriginal);

            imgBalls = curve->getDrawnCurves();

            Mat videoImg;
            printTime("DetecBall: ");
            if((int32_t)imgBuffer.size() == framesPrediction){
                //imshow("Balls", imgBalls);
                Mat fieldModel = curve->getFieldModel();
                resize(fieldModel, fieldModel, Size(), 0.2, 0.2, INTER_LINEAR);
                videoImg = imgBuffer[0] + imgBalls;// + imgLines;

                fieldModel.copyTo(videoImg(Rect(videoImg.cols - fieldModel.cols, videoImg.rows - fieldModel.rows, fieldModel.cols, fieldModel.rows)));

                if(imgCount%15 == 0){
                    Mat resizedVideoImg;
                    resize(videoImg,resizedVideoImg, Size(), 0.5, 0.5, INTER_LINEAR);
                    imshow("Original", resizedVideoImg); //show the original image
                    Mat resizedFieldModel;
                    resize(curve->getFieldModel(), resizedFieldModel, Size(), 0.5, 0.5, INTER_LINEAR);
                    //imshow("resizedFieldModel", resizedFieldModel);

                    waitKey(30);
                    printTime("ShowData: ");
                }

                //Viedeo write takes much
                writer.write(videoImg);
            }

        }

        printTime("WriteVideo: ");

        //waitKey(1);
        //printTime("WaitKey: ");

        /*if (waitKey(30) == 27) //wait for 'esc' key press for 30ms. If 'esc' key is pressed, break loop
         {
         cout << "esc key is pressed by user" << endl;
         break;
         }*/
        imgCount++;
        cout<<"imgCount: "<<imgCount<<endl;
        /*if(imgCount == 799){
         cout<<"imgCount = 799"<<endl;
         }*/

        timeNowOverall = clock();
        timeDiffOverall = (double)(timeNowOverall - timeStartOverall)/CLOCKS_PER_SEC;
        std::cout <<"TimeOverall: "<<timeDiffOverall<<std::endl;
        std::cout <<std::endl;

    }
    writer.release();
    return 0;
}

//Ideen:
/*Moments oMoments = moments(imgThresholded);

 double dM01 = oMoments.m01;
 double dM10 = oMoments.m10;
 double dArea = oMoments.m00;

 // if the area <= 10000, I consider that the there are no object in the image and it's because of the noise, the area is not zero
 cout<<dArea<<endl;
 if (dArea > 10000)
 {
 //calculate the position of the ball
 int posX = dM10 / dArea;
 int posY = dM01 / dArea;

 if (iLastX >= 0 && iLastY >= 0 && posX >= 0 && posY >= 0)
 {
 //Draw a red line from the previous point to the current point
 line(imgLines, Point(posX, posY), Point(iLastX, iLastY), Scalar(0,0,255), 2);
 }

 iLastX = posX;
 iLastY = posY;
 }*/
