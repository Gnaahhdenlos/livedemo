
#include <iostream>
#include <string>
#include <ctime>
//
// #include <opencv2/core/core.hpp>
// #include <opencv2/highgui/highgui.hpp>
// #include <opencv2/imgproc/imgproc.hpp>

#include "../../trackLib/configData.hpp"
#include "../../trackLib/appController.hpp"

using namespace cv;
using namespace std;

void help(){
    cout<<"Wrong Args"<<endl;
    cout<<"Try: filePathToWeights\n filePathToCameraCalibrationMatrix\n filePathToFieldPoints"<<endl;
}

int main( int argc, char** argv ){

    struct ConfigData config;
    string matchName = "/home/wingfield/alphaConfigFiles/videos/match";

    time_t now = time(NULL);
    tm *gmtm = gmtime(&now);

    matchName.append(to_string(1900 + gmtm->tm_year));
    matchName.append(to_string(1 + gmtm->tm_mon));
    matchName.append(to_string(gmtm->tm_mday));
    matchName.append(to_string(1 + gmtm->tm_hour));
    matchName.append(to_string(1 + gmtm->tm_min));
    matchName.append(to_string(1 + gmtm->tm_sec));

    string matchNameRight = matchName + "R.avi";
    string matchNameLeft = matchName + "L.avi";
    string matchOutput = matchName + "Output.avi";
    cout<<"matchNameRight: "<<matchNameRight<<endl;
    if(argc == 4){

      config.setFilePathToVideoRight(matchNameRight);
      config.setFilePathToCalibrationVideoRight(matchNameRight);

      config.setFilePathToVideoLeft(matchNameLeft);
      config.setFilePathToCalibrationVideoLeft(matchNameLeft);

      config.setSafePathOutputVideo(matchOutput);
      config.setFilePathToWeights((string)argv[1]);

      config.setFilePathToCameraCalibrationMatrix((string)argv[2]);
      config.setFilePathToFieldCalibrationPoints((string)argv[3]);

      config.setCalibrationType(1);
      config.setWithCamera(true);
      cout<<"Using video data for calibration"<<endl;
    }
    else{
        help();
        return EXIT_FAILURE;
    }
    cout<<"CreateAppControl"<<endl;
    AppController *controller = new AppController(config);
    cout<<"Control"<<endl;
    controller->controlWithCamera();
    cout<<"End of MainControllerCamera"<<endl;
    return 0;
}
