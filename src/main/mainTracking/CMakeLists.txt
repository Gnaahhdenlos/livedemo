cmake_minimum_required (VERSION 2.6)
project (mainTracking)

find_package (OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})

IF(APPLE)
	include_directories(/Library/Frameworks/pylon.framework)
	include_directories(/Library/Frameworks/pylon.framework/Headers)
	include_directories(/Library/Frameworks/pylon.framework/Headers/GenICam)
	link_directories(/Library/Frameworks/pylon.framework/Versions/A/Libraries)
	link_directories(/Library/Frameworks/pylon.framework/Libraries)
ELSEIF(UNIX)
	include_directories(/opt/pylon5/include)
	link_directories(/opt/pylon5/lib64)
ENDIF()

set(BASLER_LIBS pylonbase GCBase_gcc_v3_0_Basler_pylon_v5_0 pylonutility)

add_executable(detecSingle main.cpp )
add_executable(detecDouble mainEvent.cpp )
add_executable(detec mainController.cpp )
add_executable(detecLive mainControllerCamera.cpp )

target_link_libraries (detecSingle trackLib cameraLib ${OpenCV_LIBS} -lpthread -lm ${BASLER_LIBS} zbar)
target_link_libraries (detecDouble trackLib cameraLib ${OpenCV_LIBS} -lpthread -lm ${BASLER_LIBS} zbar)
target_link_libraries (detec trackLib cameraLib ${OpenCV_LIBS} -lpthread -lm ${BASLER_LIBS} zbar)
target_link_libraries (detecLive trackLib cameraLib ${OpenCV_LIBS} -lpthread -lm ${BASLER_LIBS} zbar)

set(CUDA_NVCC_FLAGS ${CUDA_NVCC_FLAGS} -arch compute_52 -code sm_52)
set(CMAKE_CXX_FLAGS "-std=c++11 -Wall -O3")
