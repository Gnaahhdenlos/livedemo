
#include <iostream>
#include <string>
//
// #include <opencv2/core/core.hpp>
// #include <opencv2/highgui/highgui.hpp>
// #include <opencv2/imgproc/imgproc.hpp>

#include "../../trackLib/configData.hpp"
#include "../../trackLib/appController.hpp"

using namespace cv;
using namespace std;

void help(){
    cout<<"Wrong Args"<<endl;
    cout<<"Try:\n filePathToRightVideo\n filePathToRightVideoCalibration \n filePathToLeftVideo\n filePathToLeftVideoCalibration\n safePathOutputVideo\n filePathToWeights\n filePathToCameraCalibrationMatrix\n filePathToFieldPoints\n CalibrationType: 1 for preset Points"<<endl;
}

int main( int argc, char** argv ){

    struct ConfigData config;

    if(argc == 10){
        config.setFilePathToVideoRight(argv[1]);
        config.setFilePathToCalibrationVideoRight(argv[2]);

        config.setFilePathToVideoLeft(argv[3]);
        config.setFilePathToCalibrationVideoLeft(argv[4]);

        config.setSafePathOutputVideo(argv[5]);
        config.setFilePathToWeights(argv[6]);
        config.setFilePathToCameraCalibrationMatrix(argv[7]);
        config.setFilePathToFieldCalibrationPoints(argv[8]);
        config.setCalibrationType(atoi(argv[9]));
    }
    else if(argc == 8){
        config.setFilePathToVideoRight(argv[1]);
        config.setFilePathToCalibrationVideoRight(argv[1]);

        config.setFilePathToVideoLeft(argv[2]);
        config.setFilePathToCalibrationVideoLeft(argv[2]);

        config.setSafePathOutputVideo(argv[3]);
        config.setFilePathToWeights(argv[4]);
        config.setFilePathToCameraCalibrationMatrix(argv[5]);
        config.setFilePathToFieldCalibrationPoints(argv[6]);
        config.setCalibrationType(atoi(argv[7]));
        cout<<"Using video data for calibration"<<endl;
    }
    else{
        help();
        return EXIT_FAILURE;
    }

    AppController *controller = new AppController(config);

    controller->control();

    return 0;
}
