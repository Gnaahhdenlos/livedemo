
#include <iostream>
#include <string>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <ctime>

#include "../../trackLib/fieldDetection.hpp"
#include "../../trackLib/ballDetection.hpp"
#include "../../trackLib/configData.hpp"
//#include "../../trackLib/eventDetectionOld.hpp"

using namespace cv;
using namespace std;

void help(){
    cout<<"Wrong Args"<<endl;
    cout<<"Try: filePathToRightVideo filePathToRightVideoCalibration filePathToLeftVideo filePathToLeftVideoCalibration safePathOutputVideo filePathToWeights filePathToCameraCalibrationMatrix"<<endl;
}

int main( int argc, char** argv ){

    struct ConfigData config;

    if(argc == 10){
        config.m_filePathToRightVideo = argv[1];
        config.m_filePathToRightVideoCalibration = argv[2];

        config.m_filePathToLeftVideo = argv[3];
        config.m_filePathToLeftVideoCalibration = argv[4];

        config.m_safePathOutputVideo = argv[5];
        config.m_filePathToWeights = argv[6];
        config.m_filePathToCameraCalibrationMatrix = argv[7];
        config.m_filePathToFieldCalibrationPoints = argv[8];
        config.m_useCalibrationType = atoi(argv[9]);
    }
    else if(argc == 8){
        config.m_filePathToRightVideo = argv[1];
        config.m_filePathToRightVideoCalibration = argv[1];

        config.m_filePathToLeftVideo = argv[2];
        config.m_filePathToLeftVideoCalibration = argv[2];

        config.m_safePathOutputVideo = argv[3];
        config.m_filePathToWeights = argv[4];
        config.m_filePathToCameraCalibrationMatrix = argv[5];
        config.m_filePathToFieldCalibrationPoints = argv[6];
        config.m_useCalibrationType = atoi(argv[7]);
        cout<<"Using video data for calibration"<<endl;
    }
    else{
        help();
        return EXIT_FAILURE;
    }

    // EventDetectionOld *eventDetec = new EventDetectionOld(config);
    //
    // eventDetec->analysingVideos();

    return 0;
}
