#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include "opencv2/opencv.hpp"
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;

string m_filename;
string m_filename_resized;
string m_filePathToCameraCalibrationMatrix;

Mat m_cameraMatrix;
Mat m_distortionCoefficients;

Mat m_undistortionRemap1;
Mat m_undistortionRemap2;

int m_frameWidth;
int m_frameHeight;

Mat m_frame;

double m_resizingFactor;

void help(){
    cout<<"Resizes and undistorts Video"<<endl;
    cout<<"Wrong Args"<<endl;
    cout<<"Try: \n ./resizeVideo \n filePathToVideo \n filePathToCameraCalibrationMatrix \n resizingFactor"<<endl;
}

int main(int argc, char* argv[]){
   if(argc != 4){
      help();
      return 0;
   }

   m_filename = argv[1];
   m_filePathToCameraCalibrationMatrix = argv[2];
   m_resizingFactor = atof(argv[3]);

   cout<<"m_resizingFactor: "<<m_resizingFactor<<endl;

   FileStorage fileStorage(m_filePathToCameraCalibrationMatrix, FileStorage::READ);
   if( !fileStorage.isOpened() ){
       return EXIT_FAILURE;
   }
   fileStorage["Camera_Matrix"]>> m_cameraMatrix;
   fileStorage["Distortion_Coefficients"]>> m_distortionCoefficients;

   m_filename_resized = m_filename;
   m_filename_resized.insert(m_filename.length()-4, "_07");

   VideoCapture inputVideoCapture(m_filename);
   m_frameWidth = (inputVideoCapture.get(CV_CAP_PROP_FRAME_WIDTH));
   m_frameHeight = (inputVideoCapture.get(CV_CAP_PROP_FRAME_HEIGHT));

   int totalFrameNumber = inputVideoCapture.get(CV_CAP_PROP_FRAME_COUNT);
   int currentFrameNumber = 0;
   float numberOfFramesForOnePercent = (float)totalFrameNumber/100;
   int percentCounter = 0;

   initUndistortRectifyMap(m_cameraMatrix, m_distortionCoefficients, Mat(),
 	                        m_cameraMatrix,
 	                        Size(m_frameWidth, m_frameHeight), CV_16SC2, m_undistortionRemap1, m_undistortionRemap2);

   Size S = Size(m_frameWidth*m_resizingFactor, m_frameHeight*m_resizingFactor);

   VideoWriter writer;
   writer.open(m_filename_resized, inputVideoCapture.get(CV_CAP_PROP_FOURCC), inputVideoCapture.get(CV_CAP_PROP_FPS), S, true);
   cout<<"Started writing resized Video into: "<<m_filename_resized<<endl;

   while(true){
      currentFrameNumber = inputVideoCapture.get(CV_CAP_PROP_POS_FRAMES);
      if(currentFrameNumber >= (float)percentCounter * numberOfFramesForOnePercent){
        cout<<"Current Progress: "<<percentCounter<<"%"<<endl;
        //fflush(stdout);
        percentCounter++;
      }
      inputVideoCapture >> m_frame;
      if(m_frame.empty()){
         break;
      }

      remap(m_frame, m_frame, m_undistortionRemap1, m_undistortionRemap2, INTER_LINEAR);
      //resize(m_frame, m_frame, Size(), m_resizingFactor, m_resizingFactor, INTER_LINEAR);

      resize(m_frame, m_frame, S, 0, 0, INTER_CUBIC);
/*
      cout<<"After rezising: "<<m_frame.size().width<<endl;
      cout<<m_frame.size().height<<endl;
      imshow("Window", m_frame);
      waitKey(0);
*/
      writer << m_frame;
   }
   inputVideoCapture.release();

   cout<<"Stored resized Video into " << m_filename_resized<<endl;
}
